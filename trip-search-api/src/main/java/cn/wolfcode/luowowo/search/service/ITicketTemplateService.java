package cn.wolfcode.luowowo.search.service;

import cn.wolfcode.luowowo.article.query.TicketObject;
import cn.wolfcode.luowowo.search.template.TicketTemplate;

import java.util.List;

/**
 *
 */
public interface ITicketTemplateService {

    /**
     * 主题,地区查询
     * @param qo
     * @return
     */
    List<TicketTemplate> queryByDestandThemeId(TicketObject qo);

    /**
     * 更新或修改
     * @param template
     */
    void saveOrUpdate(TicketTemplate template);

    /**
     * 根据destId查询
     * @param ajaxDestId
     * @return
     */
    List<TicketTemplate> queryByDestId(Long ajaxDestId);
}

