package cn.wolfcode.luowowo.hotel.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Setter
@Getter
public class HotelPrice extends BaseDomain {

    private Long hotelId;  // 酒店

    private BigDecimal price;     // 价格

    private String info;    // 说明

}