package cn.wolfcode.luowowo.airticket.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Administrator on 2019/8/24.
 */
@Setter
@Getter
public class FlightVO implements Serializable {
    private Long id;
    private String flightNo;//航班号
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date depTime;//出发时间
    private String depPort;//出发机场
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private String arrTime;//到达时间
    private String arrPort;//降落机场

    private int price;

}
