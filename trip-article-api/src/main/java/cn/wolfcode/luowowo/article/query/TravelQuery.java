package cn.wolfcode.luowowo.article.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by zhi on 2019/8/11.
 */
@Setter
@Getter
public class TravelQuery extends QueryObject {
    private Long destId = -1L;
    private int orderType = -1;      // 排序
    private int travelTimeType = -1; // 旅游时间
    private int perExpendType = -1;  // 人均消费
    private int dayType = -1;        // 旅游天数
    private int state = -1;          // 状态
    private String lastUpdateTimeOrderBy = "t.lastUpdateTime asc";
    //排序
    public String getOrderBy(){
        if(orderType == 1){
            return " t.createTime desc "; //最新
        }else if(orderType == 2){
            return " t.viewnum desc";  //最热
        }
        return "t.createTime desc";
    }


    // 人均消费
    public TravelCondition getPerExpends(){
        return TravelCondition.TRAVEL_PER_EXPENDS.get(perExpendType);
    }

    // 旅游月份
    public TravelCondition getTravelTime(){
        return TravelCondition.TRAVEL_TIME.get(travelTimeType);
    }

    // 旅游天数
    public TravelCondition getDays(){
        return TravelCondition.TRAVEL_DAYS.get(dayType);
    }





}
