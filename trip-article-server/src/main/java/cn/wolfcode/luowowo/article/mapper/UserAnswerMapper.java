package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.UserAnswer;

import java.util.List;

public interface UserAnswerMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserAnswer record);

    UserAnswer selectByPrimaryKey(Long id);

    List<UserAnswer> selectAll();

    int updateByPrimaryKey(UserAnswer record);
}