package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.TravelCommend;
import cn.wolfcode.luowowo.article.mapper.TravelCommendMapper;
import cn.wolfcode.luowowo.article.query.TravelCommendQuery;
import cn.wolfcode.luowowo.article.service.ITravelCommendService;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by zhi on 2019/8/16.
 */
@Service
public class TravelCommendServiceImpl implements ITravelCommendService {

    @Autowired
    private TravelCommendMapper travelCommendMapper;

    @Override
    public PageInfo queryForList(TravelCommendQuery qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        return new PageInfo(travelCommendMapper.selectForList(qo));
    }

    @Override
    public void saveOrUpdate(TravelCommend travelCommend) {
        if (travelCommend.getId() == null) {
            travelCommendMapper.insert(travelCommend);
        }else {
            travelCommendMapper.updateByPrimaryKey(travelCommend);
        }
    }

    @Override
    public void delete(Long id) {
        travelCommendMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<TravelCommend> getTop10() {
        return travelCommendMapper.selectTop10();
    }
}
