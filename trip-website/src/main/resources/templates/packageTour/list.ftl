﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>跟团游</title>
<#--搜索框-->
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.js"></script>
<#--筛选框-->
    <link href="/styles/base.css" rel="stylesheet" type="text/css">
    <link href="/styles/gonglve.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/jquery/jquery.js"></script>
    <script type="text/javascript" src="/js/plugins/jquery-form/jquery.form.js"></script>
    <link href="/js/plugins/jqPaginator/jqPagination.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/plugins/jqPaginator/jq-paginator.min.js"></script>
    <script type="text/javascript" src="/js/system/gonglve.js"></script>
    <script>
        //搜索框
        $(function () {
            $(".select_box").click(function (event) {
                event.stopPropagation();
                $(this).find(".option").toggle();
                $(this).parent().siblings().find(".option").hide();
            });
            $(document).click(function (event) {
                var eo = $(event.target);
                if ($(".select_box").is(":visible") && eo.attr("class") != "option" && !eo.parent(".option").length)
                    $('.option').hide();
            });
            $(".option li").click(function () {
                var check_value = $(this).text();
                var zlValue = $('.option li:eq(1)').html();
                var bqValue = $('.option li:eq(2)').html();
                $(this).parent().siblings(".select_txt").text(check_value);
                $("#select_value").val(check_value);
                if (check_value == zlValue) {
                    $('#searchPlaceholder').prop('placeholder', '请输入电器名称');
                } else if (check_value == bqValue) {
                    $('#searchPlaceholder').prop('placeholder', '请输入家用名称');
                } else {
                    $('#searchPlaceholder').prop('placeholder', '请输入服装名称');
                }
            });
        })


        //筛选框

    </script>
</head>
    <body>
        <#assign currentNav="strategy">
            <#include "../common/navbar.ftl">
            <#--搜索框-->
            <div class="searchbox">
                <div class="mod_select">
                    <div class="select_box">
                        <span class="select_txt">服装</span>
                        <span class="select-icon"></span>
                        <ul class="option">
                            <li>服装</li>
                            <li>电器</li>
                            <li>家用</li>
                        </ul>
                    </div>
                </div>
                <form action="">
                    <input type="hidden" name="" value="" id="select_value">
                    <input type="text" name="" id="searchPlaceholder" class="import" placeholder="">
                    <input type="submit" value="搜   索" class="btn-search">
                </form>
            </div>
            <#--条件筛选框-->
    <div align="center">
            <div class="gl_wrap" style="border:solid;width:1000px;height:200px;align:center;">
                <div class="filter-hd clearfix" data-type="1" id="saleType" align="left">
                    <a data-v="0" class="cur" data-type-filter="1" href="javascript:void(0);" id="saleType0">全部</a>
                    <a data-v="36" data-type-filter="1" href="javascript:void(0);" id="saleType36">境内半自助游</a>
                    <a data-v="32" data-type-filter="1" href="javascript:void(0);" id="saleType32">私家团</a>
                    <a data-v="35" data-type-filter="1" href="javascript:void(0);" id="saleType35">出境跟团游</a>
                    <a data-v="33" data-type-filter="1" href="javascript:void(0);" id="saleType33">游学团</a>
                    <a data-v="6" data-type-filter="1" href="javascript:void(0);" id="saleType6">出境半自助游</a>
                    <a data-v="30" data-type-filter="1" href="javascript:void(0);" id="saleType30">境内跟团游</a>
                </div>

                <h3>
                    <a href="javascript:;">出发日期:</a>
                </h3>
                <ol>
                        <li><input type="checkbox" name="radio" checked>不限</li>
                        <li><input type="checkbox" name="radio">1月</li>
                        <li><input type="checkbox" name="radio">2月</li>
                        <li><input type="checkbox" name="radio">3月</li>
                        <li><input type="checkbox" name="radio">4月</li>
                        <li><input type="checkbox" name="radio">5月</li>
                        <li><input type="checkbox" name="radio">6月</li>
                        <li><input type="checkbox" name="radio">7月</li>
                        <li><input type="checkbox" name="radio">8月</li>
                        <li><input type="checkbox" name="radio">9月</li>
                        <li><input type="checkbox" name="radio">10月</li>
                        <li><input type="checkbox" name="radio">11月</li>
                        <li><input type="checkbox" name="radio">12月</li>
                </ol>
                <span class="sp_toggle">
                        <a href="javascript:void(0);" class="more_selector">更多</a>
                    </span>
                <div class="clear"></div>

                <h3>
                    <a href="javascript:;">价格区间：</a>
                </h3>
                <ol>
                    <li><input type="checkbox" name="radio" checked>不限</li>
                    <li><input type="checkbox" name="radio">1050-2000</li>
                    <li><input type="checkbox" name="radio">2000-3000</li>
                    <li><input type="checkbox" name="radio">3000-99999</li>
                </ol>
                <span class="sp_toggle">
                    <a href="javascript:void(0);" class="more_selector">更多</a>
                </span>
                <div class="clear"></div>

                <h3>
                    <a href="javascript:;">产品特色：</a>
                </h3>
                <ol>
                    <li><input type="checkbox" name="radio" checked>不限</li>
                    <li><input type="checkbox" name="radio">全程无自费</li>
                    <li><input type="checkbox" name="radio">含自由活动</li>
                    <li><input type="checkbox" name="radio">纯玩无购物</li>
                    <li><input type="checkbox" name="radio">含wifi／电话卡</li>
                </ol>
                <span class="sp_toggle">
                    <a href="javascript:void(0);" class="more_selector">更多</a>
                </span>
                <div class="clear"></div>

                <h3>
                    <a href="javascript:;">参与大促：</a>
                </h3>
                <ol>
                    <li><input type="checkbox" name="radio">大促</li>
                </ol>
                <span class="sp_toggle">
                    <a href="javascript:void(0);" class="more_selector">更多</a>
                </span>
                <div class="clear"></div>
            </div>
            <!--数据-->
            <form action="/strategy/searchPage" method="post" id="searchForm">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <input type="hidden" name="orderBy" id="orderBy" value="viewnum">
                <input type="hidden" name="type" id="typeId" value="-1">
                <input type="hidden" name="typeValue" id="typeValue" value="-1">
                <div id="strategySearchPageData">

                </div>
            </form>
            <script>
                $(".strategySearch").click(function () {
                    var type = $(this).data("type");
                    var value = $(this).data("value")
                    var cl;
                    if(type == 'orderBy'){
                        $("#orderBy").val(value);
                        cl = "upt_on"

                        type = $(".onfs").data("type");
                        value = $(".onfs").data("value");
                    }else{
                        cl = "onfs"

                        $("#currentNav").html($(this).text());
                    }

                    $(".strategySearch").removeClass(cl);
                    $(this).addClass(cl);
                    $("#typeId").val(type);
                    $("#typeValue").val(value);

                    $("#currentPage").val(1);
                    $("#searchForm").submit();
                })
            </script>
            </div>
    </div>
            <#include "../common/footer.ftl">
        <div style="text-align:center;margin:50px 0; font:normal 14px/24px 'MicroSoft YaHei';">
    </div>
    </body>
</html>
