package cn.wolfcode.luowowo.hotel.service;

import cn.wolfcode.luowowo.hotel.domain.Hotel;

import java.util.List;

/**
 * Created by zhi on 2019/8/22.
 */
public interface IHotelService {

    /**
     * 根据id查
     * @param hotelId
     * @return
     */
    List<Hotel> queryById(List<Long> hotelId);

    /**
     * 根据地区id查
     * @param cityId
     * @return
     */
    List<Hotel> queryByCityId(Long cityId);

    /**
     * 查全部
     * @return
     */
    List<Hotel> list();

    /**
     * 获单个
     * @param id
     * @return
     */
    Hotel get(Long id);

    /**
     * 根据地区查询酒店
     * @param id
     * @return
     */
    List<Hotel> getByDestId(Long id);
}
