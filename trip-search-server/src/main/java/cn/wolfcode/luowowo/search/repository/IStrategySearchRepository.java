package cn.wolfcode.luowowo.search.repository;

import cn.wolfcode.luowowo.search.template.StrategyTemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by zhi on 2019/8/17.
 */
@Repository
public interface IStrategySearchRepository extends ElasticsearchRepository<StrategyTemplate, Long>{

    /**
     * 通过地区名称查询
     * @param destName
     * @return
     */
    List<StrategyTemplate> findByDestName(String destName);
}
