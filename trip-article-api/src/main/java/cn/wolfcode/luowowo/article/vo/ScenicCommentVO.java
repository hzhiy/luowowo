package cn.wolfcode.luowowo.article.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ScenicCommentVO implements Serializable{

    public static final int COMMENT_TYPE_COMMENT = 0; //普通评论
    public static final int COMMENT_TYPE = 1; //评论的评论

    private String id;  //id
    private Long scenicId; // 景点Id
    private String scenicName; // 景点名
    private Long userId;    //用户id
    private String username; //用户名
    private String city; // 用户城市 暂时用不上
    private int level; // 等级
    private String headUrl;   // 用户头像
    private int type = COMMENT_TYPE_COMMENT; //评论类别
    private Date createTime; //创建时间
    private String content;  //评论内容

    private String imgUrls; // 图片

    private int starNum; // 评价星级
    private int thumbupnum;     //点赞数
    private List<Long> thumbuplist = new ArrayList<>(); // 点赞列表


    // 星星转文字
    public String getStarCommend() {
        if (starNum == 1) {
            return "千万别去";
        } else if (starNum == 2) {
            return "不推荐";
        } else if (starNum == 3) {
            return "一般般";
        } else if (starNum == 4) {
            return "值得一去";
        } else if (starNum == 5) {
            return "必须推荐";
        }
        return null;
    }
}
