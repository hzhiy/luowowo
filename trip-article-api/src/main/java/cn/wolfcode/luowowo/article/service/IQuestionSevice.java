package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.Question;
import cn.wolfcode.luowowo.article.query.QuestionQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 提问
 */
public interface IQuestionSevice {
    void saveOrUpdate(Question question);

    /**
     * 提问列表
     * @param qo
     * @return
     */
    PageInfo<Question> queryForList(QuestionQuery qo);

    /**
     * 查询单个
     * @param id
     * @return
     */
    Question get(Long id);
}
