package cn.wolfcode.luowowo.comment.service.impl;

import cn.wolfcode.luowowo.article.query.TravelDetailQuery;
import cn.wolfcode.luowowo.article.vo.QuestionAnswerVO;
import cn.wolfcode.luowowo.comment.domain.QuestionAnswer;
import cn.wolfcode.luowowo.comment.domain.StrategyComment;
import cn.wolfcode.luowowo.comment.query.QuestionAnswerQuery;
import cn.wolfcode.luowowo.comment.repository.IQuestionAnswerRepository;
import cn.wolfcode.luowowo.comment.service.IQuestionAnswerService;
import com.alibaba.dubbo.config.annotation.Service;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.StringUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by zhi on 2019/8/13.
 */
@Service
public class QuestionAnswerServiceImpl implements IQuestionAnswerService {

    @Autowired
    private IQuestionAnswerRepository repository;
    @Autowired
    private MongoTemplate template;

    public QuestionAnswer saveOrUpdate(QuestionAnswer questionAnswer) {
        questionAnswer.setCreateTime(new Date());
        return repository.save(questionAnswer);
    }

    @Override
    public Page list(QuestionAnswerQuery qo) {
        // 查询条件
        Query query = new Query();
        query.addCriteria(Criteria.where("questionId").is(qo.getQuestionId()));
        // 查总数
        long count = template.count(query, QuestionAnswer.class);
        if (count == 0) {
            return Page.empty();
        }
        // 分页:PageRequest.of(当前页0开始, 每页显示条数, 排序规则)
        //按照金牌、创建时间进行排序
        Pageable request = PageRequest.of(qo.getCurrentPage() - 1, qo.getPageSize(), Sort.by(Sort.Direction.DESC, "isgold","createTime"));
        query.with(request);
        // 查某一页
        List<QuestionAnswer> list = template.find(query, QuestionAnswer.class);
        return new PageImpl(list, request, count);

    }


    @Override
    public List<QuestionAnswer> listByCreateTimeTop10() {
        Query query = new Query();
        Pageable request = PageRequest.of(0, 10, Sort.by(Sort.Direction.DESC, "createTime"));
        query.with(request);
        List<QuestionAnswer> list = template.find(query, QuestionAnswer.class);
        return list;
    }

    @Override
    public List<QuestionAnswer> queryForAll() {
        Query query = new Query();
        Pageable request = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.DESC, "isgold","createTime"));
        query.with(request);
        List<QuestionAnswer> list = template.find(query, QuestionAnswer.class);
        return list;
    }

    @Override
    public QuestionAnswerVO getByQuestionId(Long id) {
        Query query = new Query();
        Pageable request = PageRequest.of(0,1,Sort.by(Sort.Direction.DESC,"thumbsupnum"));
        query.with(request);
        query.addCriteria(Criteria.where("questionId").is(id));
        List<QuestionAnswer> answers = template.find(query, QuestionAnswer.class);
        if (answers!=null && answers.size()>0){
            QuestionAnswerVO vo = new QuestionAnswerVO();
            try {
                BeanUtils.copyProperties(vo,answers.get(0));
                return vo;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        }
        return null;
    }

    @Override
    public QuestionAnswer getById(String aid) {
        Optional<QuestionAnswer> questionAnswer = repository.findById(aid);
        return questionAnswer.get();
    }
}
