package cn.wolfcode.luowowo.mgrsite.job;

import cn.wolfcode.luowowo.article.service.IStrategyDetailService;
import cn.wolfcode.luowowo.article.vo.StrategyPersistenceStatisVO;
import cn.wolfcode.luowowo.cache.emum.RedisKeys;
import cn.wolfcode.luowowo.cache.service.IStrategyStatisRedisService;
import cn.wolfcode.luowowo.cache.vo.StrategyStatisVO;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Set;

/**
 * 定时执行redis中的数据落地到MySQL中
 */
//@Component
public class RedisDataPersistenceJob {

    @Reference
    private IStrategyStatisRedisService strategyStatisRedisService;
    @Reference
    private IStrategyDetailService strategyDetailService;
    //  秒  分  时  日  月  星期  [年]
    @Scheduled(cron = "0 0 3 * * ?")
    public void redisDataPersistence(){
        System.out.println("--------------begin--------------");
        // 获得所有攻略的key
        Set<String> keys = strategyStatisRedisService.getKeys(RedisKeys.STRATRGY_STATIS_VO.getPrefix());
        // 遍历
        for (String key : keys) {
            // 一个个获取
            String voStr = strategyStatisRedisService.get(key);
            // 转换成VO对象
            StrategyStatisVO vo = JSON.parseObject(voStr, StrategyStatisVO.class);
            // 调用接口同步到数据库中
            StrategyPersistenceStatisVO strategyPersistenceStatisVO = new StrategyPersistenceStatisVO();
            try {
                BeanUtils.copyProperties(strategyPersistenceStatisVO, vo);
                strategyDetailService.updateStatisData(strategyPersistenceStatisVO);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        }
        System.out.println("--------------end--------------");

    }

}
