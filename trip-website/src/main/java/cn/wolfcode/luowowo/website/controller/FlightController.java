package cn.wolfcode.luowowo.website.controller;

import cn.wolfcode.luowowo.airticket.domain.AirPort;
import cn.wolfcode.luowowo.airticket.domain.Airline;
import cn.wolfcode.luowowo.airticket.domain.Flight;
import cn.wolfcode.luowowo.airticket.query.FlightQuery;
import cn.wolfcode.luowowo.airticket.service.IAirlineService;
import cn.wolfcode.luowowo.airticket.service.IAirportService;
import cn.wolfcode.luowowo.airticket.service.ICityService;
import cn.wolfcode.luowowo.airticket.service.IFlightService;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * Created by Administrator on 2019/8/23.
 */
@Controller
@RequestMapping("flight")
public class FlightController {

    @Reference
    private ICityService cityService;

    @Reference
    private IFlightService flightService;

    @Reference
    private IAirportService airportService;

    @Reference
    private IAirlineService airlineService;


    @RequestMapping("")
    public String index(Model model){
        //hotFlights
        model.addAttribute("hotFlights",cityService.getHotCity());
        //initialA
        model.addAttribute("initialA",cityService.getNormalCity(1));
        //initialF
        model.addAttribute("initialF",cityService.getNormalCity(2));
        //initialK
        model.addAttribute("initialK",cityService.getNormalCity(3));
        //initialQ
        model.addAttribute("initialQ",cityService.getNormalCity(4));
        //initialX
        model.addAttribute("initialX",cityService.getNormalCity(5));
        return "flight/index";
    }

    @RequestMapping("/search")
    @ResponseBody
    public Object search(@ModelAttribute("qo") FlightQuery qo){
        AjaxResult result = new AjaxResult();
        result.addObject(flightService.queryForList(qo));
        return result;
    }


    @RequestMapping("/searchPage")
    public String searchPage(Model model,@ModelAttribute("qo") FlightQuery qo){
        //城市
        //hotFlights
        model.addAttribute("hotFlights",cityService.getHotCity());
        //initialA
        model.addAttribute("initialA",cityService.getNormalCity(1));
        //initialF
        model.addAttribute("initialF",cityService.getNormalCity(2));
        //initialK
        model.addAttribute("initialK",cityService.getNormalCity(3));
        //initialQ
        model.addAttribute("initialQ",cityService.getNormalCity(4));
        //initialX
        model.addAttribute("initialX",cityService.getNormalCity(5));
        //起飞机场
        model.addAttribute("depPort",airportService.listByAirCode(qo.getOrgCity()));
        //降落机场
        model.addAttribute("arrPort",airportService.listByAirCode(qo.getDstCity()));
        //航空公司
        //model.addAttribute("airline",airlineService.);
       return "flight/search";
    }


    @RequestMapping("/searchResult")
    @ResponseBody
    public Object searchResult(@ModelAttribute("qo") FlightQuery qo){
        AjaxResult result = new AjaxResult();
        PageInfo<Flight> flights = flightService.queryForList(qo);
        List<Flight> list = flights.getList();
        Set<String> depPortName = new HashSet<>();
        Set<String> arrPortName = new HashSet<>();
        Set<String> airlineName = new HashSet<>();
        //起飞机场
        List<AirPort> depPorts = new ArrayList<>();
        //降落机场
        List<AirPort> arrPorts = new ArrayList<>();
        //航空公司
        List<Airline> airlines = new ArrayList<>();
        for (Flight flight : list) {
            String depPort = flight.getDepPort().getName();
            if (!depPortName.contains(depPort)){
                depPortName.add(depPort);
                depPorts.add(flight.getDepPort());
            }
            String arrPort = flight.getArrPort().getName();
            if (!arrPortName.contains(arrPort)){
                arrPortName.add(arrPort);
                arrPorts.add(flight.getArrPort());
            }
            String airline = flight.getAirline().getName();
            if (!airlineName.contains(airline)){
                airlineName.add(airline);
                airlines.add(flight.getAirline());
            }
        }

        Map<String,Object> map = new HashMap<>();
        map.put("depPorts",depPorts);
        map.put("arrPorts",arrPorts);
        map.put("airlines",airlines);
        map.put("flights",flights);
        //result.addObject(flights);
        result.addObject(map);
        return result;
    }

    @RequestMapping("/searchResult2")
    @ResponseBody
    public Object searchResult2(@ModelAttribute("qo") FlightQuery qo){
        AjaxResult result = new AjaxResult();
        PageInfo<Flight> flights = flightService.queryForList(qo);

        Map<String,Object> map = new HashMap<>();
        map.put("flights",flights);
        //result.addObject(flights);
        result.addObject(map);
        return result;
    }





}
