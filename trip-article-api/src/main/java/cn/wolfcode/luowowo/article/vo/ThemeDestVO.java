package cn.wolfcode.luowowo.article.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by zhi on 2019/8/22.
 */
@Setter
@Getter
@ToString
public class ThemeDestVO implements Serializable {
    private Long id;            // 地区id
    private String name;        // 地区名
    private String img;         // 地区封面
    private String shortName;   // 国家名
}
