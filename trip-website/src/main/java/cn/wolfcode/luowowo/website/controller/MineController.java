package cn.wolfcode.luowowo.website.controller;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.domain.StrategyDetail;
import cn.wolfcode.luowowo.article.domain.Travel;
import cn.wolfcode.luowowo.article.service.IDestinationService;
import cn.wolfcode.luowowo.article.service.IScenicService;
import cn.wolfcode.luowowo.article.service.IStrategyDetailService;
import cn.wolfcode.luowowo.article.service.ITravelService;
import cn.wolfcode.luowowo.article.vo.*;
import cn.wolfcode.luowowo.cache.service.IDestRedisService;
import cn.wolfcode.luowowo.cache.service.IStrategyStatisRedisService;
import cn.wolfcode.luowowo.cache.service.ITravelStatisRedisService;
import cn.wolfcode.luowowo.cache.service.IUserInfoRedisService;
import cn.wolfcode.luowowo.comment.domain.ScenicComment;
import cn.wolfcode.luowowo.comment.service.IScenicCommentService;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import cn.wolfcode.luowowo.hotel.domain.Hotel;
import cn.wolfcode.luowowo.hotel.service.IHotelService;
import cn.wolfcode.luowowo.hotel.vo.MineTravelStatisVO;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.member.service.IUserInfoService;
import cn.wolfcode.luowowo.website.annotation.RequireLogin;
import cn.wolfcode.luowowo.website.annotation.UserParam;
import com.alibaba.dubbo.config.annotation.Reference;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("mine")
public class MineController {

    @Reference
    private ITravelService travelService;
    @Reference
    private IDestinationService destinationService;
    @Reference
    private IStrategyDetailService strategyDetailService;
    @Reference
    private ITravelStatisRedisService travelStatisRedisService;
    @Reference
    private IStrategyStatisRedisService strategyStatisRedisService;
    @Reference
    private IUserInfoRedisService userInfoRedisService;
    @Reference
    private IUserInfoService userInfoService;
    @Reference
    private IScenicCommentService scenicCommentService;
    @Reference
    private IScenicService scenicService;
    @Reference
    private IDestRedisService destRedisService;
    @Reference
    private IHotelService hotelService;


    /**
     * 个人中心首页
     * @param model
     * @param userInfo
     * @return
     */
    @RequestMapping("/home")
    @RequireLogin
    public String home(Model model, @UserParam UserInfo userInfo){
        model.addAttribute("userInfo",userInfo);
        //个人游记
        List<Travel> travels = travelService.queryByUserId(userInfo.getId());
        //区域
        List<MineTravelStatisVO>list=new ArrayList<>();
        travels.forEach(travel -> {
            MineTravelStatisVO vo = new MineTravelStatisVO();
            List<Destination> toasts = destinationService.getToasts(travel.getDest().getId());
            vo.setTravel(travel);
            if (toasts.size()>2){
                vo.setCity(toasts.get(2).getName());
            }else if (toasts.size()==2){
                vo.setCity(toasts.get(1).getName());
            }
            vo.setCountry(toasts.get(0).getName());
            list.add(vo);
        });
        model.addAttribute("vo",list);
        //统计游记总数
        model.addAttribute("travelTotal",travels.size());
        //关注列表
        List<UserInfo>atten=userInfoRedisService.getAttentionUser(userInfo.getId());
        if (atten!=null){
            model.addAttribute("attention",atten);//关注列表
            model.addAttribute("attentionCount",atten.size());//关注人数
        }
        //粉丝
        List<UserInfo> fans = userInfoRedisService.getFans(userInfo.getId());
        System.out.println(fans);
        if (fans!=null) {
            model.addAttribute("fansCount",fans.size());
        }
        //获取景区点评
        List<ScenicAndCommentVO>listVO=new ArrayList<>();
        List<ScenicComment> scenicComments = scenicCommentService.queryByUserId(userInfo.getId());
        scenicComments.forEach(s->{
            ScenicAndCommentVO vo = new ScenicAndCommentVO();
            ScenicCommentVO scenicCommentVO = new ScenicCommentVO();
            try {
                BeanUtils.copyProperties(scenicCommentVO,s);
                vo.setScenicCommentVO(scenicCommentVO);
                vo.setScenic(scenicService.getById(s.getScenicId()));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            listVO.add(vo);
        });
        model.addAttribute("scenicVO",listVO);
        model.addAttribute("commentCount",scenicComments.size());
        return "mine/homepage";
    }


    /**
     * 我的游记
     * @param model
     * @param userInfo
     * @return
     */
    @RequestMapping("/mytravelnotes")
    @RequireLogin
    public String mytravelnotes(Model model,@UserParam UserInfo userInfo){
        //个人游记
        List<Travel> travels = travelService.queryByUserId(userInfo.getId());
        List<MineTravelStatisVO>list=new ArrayList<>();
        travels.forEach(travel -> {
            MineTravelStatisVO vo = new MineTravelStatisVO();
            List<Destination> toasts = destinationService.getToasts(travel.getDest().getId());
            vo.setTravel(travel);
            if (toasts.size()>2){
                vo.setCity(toasts.get(2).getName());
            }else if (toasts.size()==2){
                vo.setCity(toasts.get(1).getName());
            }
            vo.setCountry(toasts.get(0).getName());
            list.add(vo);
        });
        model.addAttribute("travels",list);
        //个人游记数据统计
        UserTravelStatisVo vo = new UserTravelStatisVo();
        vo.setTravelCount(travels.size());//游记篇数
        int replyCount=0;
        for (Travel travel : travels) {
            replyCount+=travel.getReplynum();
            vo.setViewNum(travel.getViewnum());//游记总阅读量
        }
        vo.setReplyNum(replyCount);//游记总回复数
        model.addAttribute("vo",vo);
        //用户
        model.addAttribute("userInfo",userInfo);
        return "mine/mytravelnotes";
    }


    /**
     * 我的点评
     * @param model
     * @param userInfo
     * @return
     */
    @RequestMapping("/review")
    @RequireLogin
    public String review(Model model,@UserParam UserInfo userInfo){
        model.addAttribute("userInfo",userInfo);
        //获取点评
        List<ScenicAndCommentVO>listVO=new ArrayList<>();
        CommentStatisVO commentStatisVO = new CommentStatisVO();
        List<ScenicComment> scenicComments = scenicCommentService.queryByUserId(userInfo.getId());
        int count=0;//评论数
        int thumbupnum=0;//点赞数
        for (ScenicComment s : scenicComments) {
            ScenicAndCommentVO vo = new ScenicAndCommentVO();
            ScenicCommentVO scenicCommentVO = new ScenicCommentVO();
            try {
                thumbupnum+=s.getThumbupnum();
                BeanUtils.copyProperties(scenicCommentVO,s);
                vo.setScenicCommentVO(scenicCommentVO);
                vo.setScenic(scenicService.getById(s.getScenicId()));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            listVO.add(vo);
        }
        commentStatisVO.setThumbupnum(thumbupnum);
        commentStatisVO.setCount(scenicComments.size());
        model.addAttribute("scenicVO",listVO);
        model.addAttribute("commentStatisVO",commentStatisVO);
        return "mine/review";
    }


    /**
     * 我的收藏
     * @param model
     * @param userInfo
     * @return
     */
    @RequestMapping("/travelcollection")
    @RequireLogin
    public String travelcollection(Model model,@UserParam UserInfo userInfo){
        //地点收藏
        List<Destination> dests=destRedisService.queryByUserIdFavor(userInfo.getId());
        List<MineTravelStatisVO>list=new ArrayList<>();
        for (Destination dest : dests) {
            MineTravelStatisVO vo = new MineTravelStatisVO();
            List<Destination> toasts = destinationService.getToasts(dest.getId());
            List<Hotel>hotels=hotelService.getByDestId(dest.getId());
            System.out.println(hotels);
            vo.setHotel(hotels);
            vo.setHotelCount(hotels.size());
            vo.setDest(dest);
            if (toasts.size()>2){
                vo.setCity(toasts.get(2).getName());
            }else if (toasts.size()==2){
                vo.setCity(toasts.get(1).getName());
            }else {
                vo.setCity(toasts.get(0).getName());
            }
            vo.setCountry(toasts.get(0).getName());
            list.add(vo);
        }
        model.addAttribute("vo",list);
        //游记收藏
        List<Travel> travels = travelStatisRedisService.queryByUserIdToFavor(userInfo.getId());
        model.addAttribute("travels",travels);
        //攻略收藏
        List<StrategyDetail> details = strategyStatisRedisService.queryByUserIdToFavor(userInfo.getId());
        model.addAttribute("details",details);


        model.addAttribute("userInfo",userInfo);
        return "mine/travelcollection";
    }


    /**
     * 个人设置
     * @param model
     * @param userInfo
     * @return
     */
    @RequestMapping("/setting")
    @RequireLogin
    public String setting(Model model,@UserParam UserInfo userInfo){

        model.addAttribute("userInfo",userInfo);
        return "mine/setting";
    }



    //用户关注
    @RequestMapping("/attention")
    @ResponseBody
    public AjaxResult attention(Long uid, @UserParam UserInfo userInfo){
        AjaxResult result = new AjaxResult();
        if (userInfo == null){
            result.setSuccess(false);
            result.setCode(102);
            result.setMsg("请先登录");
            return result;
        }
        boolean flag = userInfoRedisService.attentionUser(userInfo.getId(), uid);
        if (flag){
            return result;
        }
        result.setSuccess(flag);
        return result;
    }


    //关注用户个人信息
    @RequestMapping("/attentionUserMsg")
    public String attentionUserMsg(Model model,Long uid){
        UserInfo userInfo = userInfoService.get(uid);
        model.addAttribute("userInfo",userInfo);
        List<Travel> travels = travelService.queryByUserId(uid);
        //关注列表
        List<UserInfo>atten=userInfoRedisService.getAttentionUser(uid);
        if (atten!=null){
            model.addAttribute("attention",atten);//关注列表
            model.addAttribute("attentionCount",atten.size());//关注人数
        }
        //粉丝
        List<UserInfo> fans = userInfoRedisService.getFans(uid);
        if (fans!=null) {
            model.addAttribute("fansCount",fans.size());
        }
        //区域
        List<MineTravelStatisVO>list=new ArrayList<>();
        travels.forEach(travel -> {
            MineTravelStatisVO vo = new MineTravelStatisVO();
            List<Destination> toasts = destinationService.getToasts(travel.getDest().getId());
            vo.setTravel(travel);
            if (toasts.size()>2){
                vo.setCity(toasts.get(2).getName());
            }else if (toasts.size()==2){
                vo.setCity(toasts.get(1).getName());
            }
            vo.setCountry(toasts.get(0).getName());
            list.add(vo);
        });
        model.addAttribute("vo",list);
        //统计游记总数
        model.addAttribute("travelTotal",travels.size());
        return "mine/user";
    }



    /**
     * ta的游记
     * @param model
     * @return
     */
    @RequestMapping("/usertravelnotes")
    public String usertravelnotes(Model model,Long uid){
        //个人游记
        List<Travel> travels = travelService.queryByUserId(uid);
        List<MineTravelStatisVO>list=new ArrayList<>();
        travels.forEach(travel -> {
            MineTravelStatisVO vo = new MineTravelStatisVO();
            List<Destination> toasts = destinationService.getToasts(travel.getDest().getId());
            vo.setTravel(travel);
            if (toasts.size()>2){
                vo.setCity(toasts.get(2).getName());
            }else if (toasts.size()==2){
                vo.setCity(toasts.get(1).getName());
            }
            vo.setCountry(toasts.get(0).getName());
            list.add(vo);
        });
        model.addAttribute("travels",list);
        //个人游记数据统计
        UserTravelStatisVo vo = new UserTravelStatisVo();
        vo.setTravelCount(travels.size());//游记篇数
        int replyCount=0;
        for (Travel travel : travels) {
            replyCount+=travel.getReplynum();
            vo.setViewNum(travel.getViewnum());//游记总阅读量
        }
        vo.setReplyNum(replyCount);//游记总回复数
        model.addAttribute("vo",vo);
        //用户
        model.addAttribute("userInfo",userInfoService.get(uid));
        return "mine/usertravelnotes";
    }
}
