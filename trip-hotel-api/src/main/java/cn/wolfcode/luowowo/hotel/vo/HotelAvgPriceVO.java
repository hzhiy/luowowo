package cn.wolfcode.luowowo.hotel.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by zhi on 2019/8/23.
 */
@Setter
@Getter
public class HotelAvgPriceVO implements Serializable {
    private Integer star;           // 星级
    private BigDecimal avgPrice;    // 均价
}
