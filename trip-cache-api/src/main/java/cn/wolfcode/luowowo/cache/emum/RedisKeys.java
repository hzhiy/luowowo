package cn.wolfcode.luowowo.cache.emum;

/**
 * Created by zhi on 2019/8/8.
 */

import lombok.Getter;

/**
 * 美化key设计
 */
@Getter
public enum RedisKeys {
    // 验证码
    VERIFY_CODE("verify_code", 300L),
    // 登录的token
    LOGIN_TOKEN("user_login_token", 60 * 30L),
    // 攻略对象的前缀
    STRATRGY_STATIS_VO("stratrgy_statis_vo", -1L),
    // 用户收藏攻略集合的前缀
    USER_STRATEGY_STATIS_FAVOR("user_strategy_statis_favor", -1L),
    // 某用户顶某攻略的前缀
    STRATEGY_STATIS_THUMBSUP("strategy_statis_thumbsup", -1L),
    // 游记对象的前缀
    TRAVEL_STATIS_VO("travel_statis_vo", -1L),
    // 用户收藏游记集合的前缀
    USER_TRAVEL_STATIS_FAVOR("user_travel_statis_favor", -1L),
    // 用户收藏酒店集合的前缀
    USER_HOTEL_STATIS_FAVOR("user_hotel_statis_favor", -1L),
    // 某用户顶某游记的前缀
    TRAVEL_STATIS_THUMBSUP("travel_statis_thumbsup", -1L),
    // 攻略推荐排行的前缀
    STRATEGY_STATIS_COMMEND_SORT("strategy_statis_commend_sort", -1L),
    // 攻略热门排行的前缀
    STRATEGY_STATIS_HOT_SORT("strategy_statis_hot_sort", -1L),
    // 游记排行的前缀
    TRAVEL_STATIS_SORT("travel_statis_sort", -1L),

    //通过父的id拿子的主题
    DEST_THEME("dest_theme",-1L),

    //通过月份的id拿热门的地点
    DEST_THEME_MONTH("dest_theme_month",-1L),
    //主题全部月份的集合list
    THEME_MONTH_LIST("theme_month_list",-1L),

    // 酒店首页主题地区的前缀
    THEME_DEST_VO("theme_dest_vo", -1L),
    //关注列表
    USER_STATIS_ATTENTION("user_statis_attention", -1L),
    //粉丝列表
    USER_STATIS_FANS("user_statis_fans", -1L),
    //问题共享数据前缀
    QUESTION_STATIC_VO("question_static_vo",-1L),
    //用户点赞问题攻略
    QUESTION_STATIS_THUMBSUP("question_statis_thumbsup", -1L),
    //回答的点赞
    ANSWER_STATIS_THUMBSUP("answer_statis_thumbsup", -1L),
    //用户回答数的
    QUESTION_ANSWER_STATIS_VO_DAY("question_answer_statis_vo_day",-1L),
    //用户回答数的
    QUESTION_ANSWER_STATIS_VO_WEEK("question_answer_statis_vo_week",-1L),
    //用户回答数的
    QUESTION_ANSWER_STATIS_VO_MONTH("question_answer_statis_vo_month",-1L),

    //用户回答数的paihang
    QUESTION_ANSWER_STATIS_VO_DAY_SORT("question_answer_statis_vo_day_sort",-1L),
    //用户回答数的
    QUESTION_ANSWER_STATIS_VO_WEEK_SORT("question_answer_statis_vo_week_sort",-1L),
    //用户回答数的
    QUESTION_ANSWER_STATIS_VO_MONTH_SORT("question_answer_statis_vo_month_sort",-1L),

    //用户点赞相关
    //用户回答数的
    QUESTION_THUMBSUP_STATIS_VO_DAY("question_thumbsup_statis_vo_day",-1L),
    //用户回答数的
    QUESTION_THUMBSUP_STATIS_VO_WEEK("question_thumbsup_statis_vo_week",-1L),
    //用户回答数的
    QUESTION_THUMBSUP_STATIS_VO_MONTH("question_thumbsup_statis_vo_month",-1L),

    //用户回答数的paihang
    QUESTION_THUMBSUP_STATIS_VO_DAY_SORT("question_thumbsup_statis_vo_day_sort",-1L),
    //用户回答数的
    QUESTION_THUMBSUP_STATIS_VO_WEEK_SORT("question_thumbsup_statis_vo_week_sort",-1L),
    //用户回答数的
    QUESTION_THUMBSUP_STATIS_VO_MONTH_SORT("question_answer_statis_vo_month_sort",-1L),

    //地区收藏
    USER_DEST_FAVOR("user_dest_favor",-1L),

    //地区门票
    TICKET_STATIS_DEST("ticket_statis_dest",-1L);





    private  String prefix;  // 前缀
    private Long time;      // 过期时间
    private RedisKeys(String prefix, Long time){
        this.prefix = prefix;
        this.time = time;
    }
    // 连接真实key
    public String join(String... keys){
        StringBuilder sb = new StringBuilder(10);
        sb.append(prefix);
        for (String key : keys) {
            sb.append(":").append(key);
        }
        return sb.toString();
    }


}
