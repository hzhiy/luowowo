package cn.wolfcode.luowowo.search.template;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;

@Document(indexName="luowowo_hotel",type="hotel")
@Getter
@Setter
public class HotelTemplate implements Serializable {

    public static final String INDEX_NAME = "luowowo_hotel";
    public static final String TYPE_NAME = "hotel";

    @Id
    //@Field 每个文档的字段配置（类型、是否分词、是否存储、分词器 ）
    @Field(store=true, index = true,type = FieldType.Long)
    private Long id;  //酒店id

    @Field(index=true,analyzer="ik_max_word",store=true,searchAnalyzer="ik_max_word",type = FieldType.Text)
    private String name;
    @Field(index=true,analyzer="ik_max_word",store=true,searchAnalyzer="ik_max_word",type = FieldType.Text)
    private String internationalName;
    @Field(index=true,analyzer="ik_max_word",store=true,searchAnalyzer="ik_max_word",type = FieldType.Text)
    private String info;
    @Field(index=true,store=true,type = FieldType.Long)
    private Long positionID;
    @Field(index=true,store=true,type = FieldType.Keyword)
    private String positionName;
    @Field(index=true,store=true,type = FieldType.Keyword)
    private String positionInfo;
    @Field(index=true,store=true,type = FieldType.Long)
    private Long cityID;
    @Field(index=true,store=true,type = FieldType.Keyword)
    private String cityName;
    @Field(index=true,store=true,type = FieldType.Keyword)
    private String cityInfo;
    @Field(store=true, index = true,type = FieldType.Keyword)
    private String star;
    @Field(store=true, index = true,type = FieldType.Keyword)
    private String grade;
    @Field(store=true, index = true,type = FieldType.Keyword)
    private String history;
    @Field(store=true, index = true,type = FieldType.Keyword)
    private String scale;
    @Field(store=true, index = true,type = FieldType.Keyword)
    private String checkin;
    @Field(store=true, index = true,type = FieldType.Keyword)
    private String departure;
    @Field(store=true, index = true,type = FieldType.Keyword)
    private String address;
    @Field(store=true, index = true,type = FieldType.Keyword)
    private String coverUrl;
    @Field(store=true, index = true,type = FieldType.Double)
    private double price;

}
