package cn.wolfcode.luowowo.member.service.impl;

import cn.wolfcode.luowowo.cache.emum.RedisKeys;
import cn.wolfcode.luowowo.cache.service.IUserInfoRedisService;
import cn.wolfcode.luowowo.common.exception.LogicException;
import cn.wolfcode.luowowo.common.util.AssertUtil;
import cn.wolfcode.luowowo.common.util.Consts;
import cn.wolfcode.luowowo.common.util.MD5Utils;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.member.mapper.UserInfoMapper;
import cn.wolfcode.luowowo.member.service.IUserInfoService;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;

import java.util.List;
import java.util.UUID;

/**
 * Created by zhi on 2019/8/7.
 */
@Service
public class UserInfoServiceImpl implements IUserInfoService {

    @Autowired
    private UserInfoMapper userInfoMapper;
    @Reference
    private IUserInfoRedisService userInfoRedisService;


    public UserInfo get(long id) {
        return userInfoMapper.selectByPrimaryKey(id);
    }

    public boolean queryByPhone(String phone) {
        AssertUtil.hasLength(phone, "号码不能为空");
        return userInfoMapper.selectCountByPhone(phone) > 0;
    }

    public void sendVerifyCode(String phone) {
        AssertUtil.hasLength(phone, "发送号码不能为空");
        // 制造验证码
        String code = UUID.randomUUID().toString().substring(0, 4);
        // 模拟发送短信
        StringBuilder sb = new StringBuilder(100);
        sb.append("您的验证码是:").append(code).append(",请在").append(Consts.VERIFY_CODE_VAI_TIME).append("分钟内使用!");
        System.out.println(sb.toString());
        // 存到缓存中
        userInfoRedisService.saveVerifyCode(phone, code);

    }

    public void regist(String phone, String nickname, String password, String rpassword, String verifyCode)  {
        // 判断参数
        AssertUtil.hasLength(phone, "号码不能为空");
        AssertUtil.hasLength(nickname, "昵称不能为空");
        AssertUtil.hasLength(password, "密码不能为空");
        AssertUtil.hasLength(rpassword, "确认密码不能为空");
        AssertUtil.hasLength(verifyCode, "验证码不能为空");
        // 判断密码一致性
        AssertUtil.isEquals(password, rpassword, "密码不一致");


        // 验证验证码
        String code = userInfoRedisService.getVerifyCode(phone);
        if (!verifyCode.equals(code)){  // 这里把verifyCode放前面因为获取验证码可能过期了或者为空
            throw new LogicException("验证码错误或已过期");
        }

        // 注册
        UserInfo userInfo = new UserInfo();
        userInfo.setPhone(phone);

        userInfo.setNickname(nickname);
        userInfo.setPassword(DigestUtils.md5DigestAsHex(password.getBytes()));

        userInfo.setHeadImgUrl("/images/default.png");
        userInfo.setLevel(1);
        userInfo.setState(0);
        userInfoMapper.insert(userInfo);
    }

    public String userLogin(String username, String password) {
        // 判断空
        AssertUtil.hasLength(username, "用户名为空");
        AssertUtil.hasLength(password, "密码 为空");
        // 验证用户
        UserInfo userInfo = userInfoMapper.selectByUsernameAndPassword(username, DigestUtils.md5DigestAsHex(password.getBytes()));
        if (userInfo == null){
            throw new LogicException("用户名或密码错误");
        }
        // 存入redis中
        String token = userInfoRedisService.setUserInfo(username, userInfo);
        return token;
    }

    @Override
    public List<UserInfo> list() {
        return userInfoMapper.selectAll();
    }

    @Override
    public void updateUser(UserInfo userInfo) {
        userInfoMapper.updateByUserInfo(userInfo);
    }

    @Override
    public void updatePhoneSendVerifyCode(String phone,Long uid) {
        // 判断参数
        AssertUtil.hasLength(phone, "号码不能为空");
        String oldPohone = userInfoMapper.selectPhoneById(uid);
        if (!phone.equals(oldPohone)){
            throw new LogicException("与原手机号不一致");
        }
        // 制造验证码
        String code = UUID.randomUUID().toString().substring(0, 4);
        // 模拟发送短信
        StringBuilder sb = new StringBuilder(100);
        sb.append("您的验证码是:").append(code).append(",请在").append(Consts.VERIFY_CODE_VAI_TIME).append("分钟内使用!");
        System.out.println(sb.toString());
        // 存到缓存中
        userInfoRedisService.saveVerifyCode(phone, code);
    }

    @Override
    public void checkoutVerfyCode(String verifyCode, String phone,Long id) {
        // 判断参数
        AssertUtil.hasLength(phone, "号码不能为空");
        String code = userInfoRedisService.getVerifyCode(phone);
        if (code==null){
            throw new LogicException("验证码已过期!");
        }
        if (!verifyCode.equals(code)){
            throw new LogicException("验证码不一致!");
        }
        if (id!=null){
            UserInfo userInfo = new UserInfo();
            userInfo.setId(id);
            userInfo.setPhone(phone);
            userInfoMapper.updateByPhone(userInfo);
        }

    }

    @Override
    public void newPhoneSendVerifyCode(String phone) {
        // 判断参数
        AssertUtil.hasLength(phone, "号码不能为空");
        // 制造验证码
        String code = UUID.randomUUID().toString().substring(0, 4);
        // 模拟发送短信
        StringBuilder sb = new StringBuilder(100);
        sb.append("您的验证码是:").append(code).append(",请在").append(Consts.VERIFY_CODE_VAI_TIME).append("分钟内使用!");
        System.out.println(sb.toString());
        // 存到缓存中
        userInfoRedisService.saveVerifyCode(phone, code);
    }

    @Override
    public void updateHeadImgUrl(UserInfo userInfo) {
        userInfoMapper.updateByHeadImgUrl(userInfo);
    }

    @Override
    public void updatePassword(String password, Long id,String verifyCode,String phone) {
        if (!verifyCode.equals(userInfoRedisService.getVerifyCode(phone))){
            throw new LogicException("验证码不一致");
        }
        UserInfo userInfo = new UserInfo();
        userInfo.setPassword(DigestUtils.md5DigestAsHex(password.getBytes()));
        userInfo.setId(id);
        userInfoMapper.updatePassword(userInfo);
    }
}