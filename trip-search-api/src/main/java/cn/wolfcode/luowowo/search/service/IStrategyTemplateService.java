package cn.wolfcode.luowowo.search.service;

import cn.wolfcode.luowowo.search.query.SearchQueryObject;
import cn.wolfcode.luowowo.search.template.StrategyTemplate;
import cn.wolfcode.luowowo.search.vo.StatisVO;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

/**
 * Created by zhi on 2019/8/17.
 */
public interface IStrategyTemplateService {

    /**
     * 新增或者修改
     * @param strategyTemplate
     */
    void saveOrUpdate(StrategyTemplate strategyTemplate);

    /**
     * 主题推荐
     * @return
     */
    List<Map<String,Object>> getThemeCommend();

    /**
     * 攻略导航
     * @param strategyType 识别是哪种攻略
     * @return
     */
    List<StatisVO> queryConditionGruop(Integer strategyType);

    /**
     * 查询首页攻略列表
     * @param qo
     * @return
     */
    Page queryStrategyByTypeId(SearchQueryObject qo);

    /**
     * 通过地区名查询
     * @param destName
     * @return
     */
    List<StrategyTemplate> findByDestName(String destName);
}

