package cn.wolfcode.luowowo.cache.service.impl;

import cn.wolfcode.luowowo.cache.emum.RedisKeys;
import cn.wolfcode.luowowo.cache.service.IUserInfoRedisService;
import cn.wolfcode.luowowo.common.util.Consts;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.member.service.IUserInfoService;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by zhi on 2019/8/7.
 */
@Service
public class UserInfoRedisServiceImpl implements IUserInfoRedisService {
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Reference
    private IUserInfoService userInfoService;


    public void saveVerifyCode(String key, String value) {
        String k = RedisKeys.VERIFY_CODE.join(key);
        redisTemplate.opsForValue().set(k, value, Consts.VERIFY_CODE_VAI_TIME * 60, TimeUnit.SECONDS);
    }

    public String getVerifyCode(String key) {
        String k = RedisKeys.VERIFY_CODE.join(key);
         return redisTemplate.opsForValue().get(k);

    }

    public String setUserInfo(String username, UserInfo userInfo) {
        String s = RedisKeys.LOGIN_TOKEN.join(UUID.randomUUID().toString().replace("-",""));
        redisTemplate.opsForValue().set(s, JSON.toJSONString(userInfo), Consts.USER_INFO_TOKEN_VAI_TIME * 60, TimeUnit.SECONDS);
        return s;
    }

    public UserInfo getUserInfo(String key) {
        if (StringUtils.hasLength(key)){
            String s1 = redisTemplate.opsForValue().get(key);
            return JSON.parseObject(s1, UserInfo.class);
        }
        return null;
    }

    @Override
    public boolean attentionUser(Long id, Long uid) {
        String key=RedisKeys.USER_STATIS_ATTENTION.join(id.toString());//当前用户
        List<Long> fans = this.getFansKey(uid);//粉丝
        if (redisTemplate.hasKey(key)){
            String atten = redisTemplate.opsForValue().get(key);//当前用户
            List<Long> attention = JSON.parseArray(atten, Long.class);
            if (attention.contains(uid)){
                //取消关注
                attention.remove(uid);
                redisTemplate.opsForValue().set(key,attention.toString());
                fans.remove(id);
                redisTemplate.opsForValue().set(RedisKeys.USER_STATIS_FANS.join(uid.toString()),fans.toString());
                return false;
            }
            //关注
            attention.add(uid);
            redisTemplate.opsForValue().set(key,attention.toString());
            fans.add(id);
            redisTemplate.opsForValue().set(RedisKeys.USER_STATIS_FANS.join(uid.toString()),fans.toString());
            return true;
        }
        //关注
        List<Long> list = new ArrayList<>();
        list.add(uid);
        redisTemplate.opsForValue().set(key,list.toString());
        fans.add(id);
        redisTemplate.opsForValue().set(RedisKeys.USER_STATIS_FANS.join(uid.toString()),fans.toString());
        return true;
    }

    @Override
    public List<UserInfo> getAttentionUser(Long id) {
        String key=RedisKeys.USER_STATIS_ATTENTION.join(id.toString());
        List<UserInfo>userInfos=new ArrayList<>();
        if (redisTemplate.hasKey(key)) {
            String atten = redisTemplate.opsForValue().get(key);
            List<Long> attention = JSON.parseArray(atten, Long.class);
            attention.forEach(a->{
                UserInfo user = userInfoService.get(a);
                userInfos.add(user);
            });
            return userInfos;
        }
        return null;
    }

    @Override
    public List<UserInfo> getFans(Long uid) {
        String key=RedisKeys.USER_STATIS_FANS.join(uid.toString());
        if (redisTemplate.hasKey(key)){
            String fans = redisTemplate.opsForValue().get(key);
            List<Long> list = JSON.parseArray(fans, Long.class);
            List<UserInfo> userInfoList=new ArrayList<>();
            for (Long f : list) {
                UserInfo userInfo = userInfoService.get(f);
                userInfoList.add(userInfo);
            }
            return userInfoList;
        }
        return null;
    }

    //粉丝
    public List<Long> getFansKey(Long uid) {
        String key=RedisKeys.USER_STATIS_FANS.join(uid.toString());
        if (!redisTemplate.hasKey(key)){
            List<Long>fans=new ArrayList<>();
            redisTemplate.opsForValue().set(key,fans.toString());
            return fans;
        }
        return JSON.parseArray(redisTemplate.opsForValue().get(RedisKeys.USER_STATIS_FANS.join(uid.toString())),Long.class);
    }
}
