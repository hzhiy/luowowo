
<ul class="clearfix" style="display: block;" data-id="0">
<#list list as hs>
    <li>
        <a href="${hs.url!}" target="_blank" data-type="hotel" data-name="${hs.name}">
            <div class="pic">
                <img src="${hs.coverUrl}" class="img-show">
            </div>
            <div class="bag-opa"></div>
            <div class="fraction">${hs.grade}分</div>
            <div class="info">
                <div class="prize"></div>
                <p>${hs.name}</p>
                <p class="eng">${(hs.internationalName)!}</p></div>
        </a>
    </li>
</#list>
</ul>