package cn.wolfcode.luowowo.website.controller;

import cn.wolfcode.luowowo.article.domain.Question;
import cn.wolfcode.luowowo.article.query.QuestionQuery;
import cn.wolfcode.luowowo.article.service.IQuestionSevice;
import cn.wolfcode.luowowo.cache.emum.RedisKeys;
import cn.wolfcode.luowowo.cache.service.IQuestionAnswerStatisRedisService;
import cn.wolfcode.luowowo.cache.vo.QuestionAnswerStatisVO;
import cn.wolfcode.luowowo.cache.vo.QuestionStatisVO;
import cn.wolfcode.luowowo.comment.domain.QuestionAnswer;
import cn.wolfcode.luowowo.comment.query.QuestionAnswerQuery;
import cn.wolfcode.luowowo.comment.service.IQuestionAnswerService;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.website.annotation.UserParam;
import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2019/8/21.
 */
@Controller
@RequestMapping("questionAnswer")
public class QuestionAnswerController {

    @Reference
    private IQuestionSevice questionSevice;

    @Reference
    private IQuestionAnswerService questionAnswerService;

    @Reference
    private IQuestionAnswerStatisRedisService questionAnswerStatisRedisService;


    @RequestMapping("/input")
    public String input(){

        return "questionAnswer/public";
    }

    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(Question question, @UserParam UserInfo userInfo){
        question.setQuestioner(userInfo);
        questionSevice.saveOrUpdate(question);

        return AjaxResult.SUCCESS;
    }

    //问题列表
    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo")QuestionQuery qo){
        PageInfo<Question> pageInfo = questionSevice.queryForList(qo);

        //从reids中获取热数据，设置到从数据中查询来的数据
        List<Question> list = pageInfo.getList();
        for (Question question : list) {
            //统计数据
            QuestionStatisVO vo = questionAnswerStatisRedisService.getQuestionStatisVO(question.getId());
            //从mongodb中获取每个问题对应的最高的点赞的数据，设置到回答的vo中
            question.setQuestionAnswer(questionAnswerService.getByQuestionId(question.getId()));
            try {
                BeanUtils.copyProperties(question,vo);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        model.addAttribute("pageInfo",pageInfo);
        return "questionAnswer/wenda";
    }

    //问题的回答列表
    @RequestMapping("/answerList")
    public String answerList(Model model, @ModelAttribute("qo")QuestionAnswerQuery qo){

        //问题数据
         model.addAttribute("question",questionSevice.get(qo.getQuestionId()));
        //问题回答数据

        //按金牌回答和最新时间排序
        Page page = questionAnswerService.list(qo);
        model.addAttribute("page",page );
        return "questionAnswer/wendaDetail";
    }

    //添加回答
    @RequestMapping("/answerAdd")
    public String answerAdd(Model model,QuestionAnswer qa,@UserParam UserInfo userInfo){
        qa.setAnswererId(userInfo.getId());
        qa.setHeadImgUrl(userInfo.getHeadImgUrl());
        qa.setNickname(userInfo.getNickname());
        qa.setLevel(userInfo.getLevel());
        qa.setIsgold(false);
        QuestionAnswer questionAnswer = questionAnswerService.saveOrUpdate(qa);
        List<QuestionAnswer> answers = new ArrayList<>();
        answers.add(questionAnswer);
        model.addAttribute("answers",answers);

        //回答数+1
        questionAnswerStatisRedisService.answernumIncrease(questionAnswer.getQuestionId(),1, QuestionStatisVO.QUESTION_ANSWERNUM);

        //用户回答数+1
        questionAnswerStatisRedisService.userAnswersIncrease(userInfo.getId(),1);
        //用户问答排行榜分值+1
        questionAnswerStatisRedisService.addAnswerScore(userInfo.getId(),1);


        return "questionAnswer/commentTpl";
    }




    //展示问题的所有回答
    @RequestMapping("listAll")
    public String listAll(Model model){
        List<QuestionAnswer> answers = questionAnswerService.queryForAll();
        model.addAttribute("answers",answers);
        return "questionAnswer/commentTpl";
    }


    // 问题顶
  /*  @RequestMapping("questionThumbsup")
    @ResponseBody
    public Object questionThumbsup(Long qid, @UserParam UserInfo userInfo){
        AjaxResult result = new AjaxResult();
        if (userInfo == null){
            result.setSuccess(false);
            result.setCode(102);
            result.setMsg("请先登录");
            return result;
        }
        boolean ret = questionAnswerStatisRedisService.questionThumbsup(qid, userInfo.getId());
        result.addObject(questionAnswerStatisRedisService.getQuestionStatisVO(qid));
        result.setSuccess(ret);


       *//* if (ret) {
            strategyStatisRedisService.addScore(RedisKeys.STRATEGY_STATIS_COMMEND_SORT.getPrefix(), RedisKeys.STRATRGY_STATIS_VO.join(sid.toString()), 1);
        }*//*
        return result;
    }*/
    //回答的顶
    @RequestMapping("answerThumbsup")
    @ResponseBody
    public Object answerThumbsup(String aid,Long answererId, @UserParam UserInfo userInfo){
        AjaxResult result = new AjaxResult();
        if (userInfo == null){
            result.setSuccess(false);
            result.setCode(102);
            result.setMsg("请先登录");
            return result;
        }


        //回答被点赞数加1
        boolean ret = questionAnswerStatisRedisService.answerThumbsup(aid, userInfo.getId());

        //boolean ret = questionAnswerStatisRedisService.questionThumbsup(aid, userInfo.getId());
        result.addObject(questionAnswerService.getById(aid));
        result.setSuccess(ret);

       /* if (ret) {
            //用户社区问答月/周/日  点赞数加+1
            //用户被点赞数+1
            questionAnswerStatisRedisService.userThumbsupIncrease(answererId,1);
            //用户点赞排行榜分值+1
            questionAnswerStatisRedisService.addThumbsupScore(answererId,1);

        }*/
        return result;
    }


    @RequestMapping("/rank")
    public String rank(Model model,int type,int rank){
        List<QuestionAnswerStatisVO> ranks = questionAnswerStatisRedisService.queryForList(type, rank);
        model.addAttribute("ranks",ranks);
        return "questionAnswer/rankTpl";
    }



}
