package cn.wolfcode.luowowo.mgrsite.controller;

import cn.wolfcode.luowowo.article.domain.Region;
import cn.wolfcode.luowowo.article.service.IDestinationService;
import cn.wolfcode.luowowo.article.service.IRegionService;
import cn.wolfcode.luowowo.article.query.RegionQuery;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by zhi on 2019/8/8.
 */
@Controller
@RequestMapping("region")
public class RegionController {

    @Reference
    private IRegionService regionService;

    @Reference
    private IDestinationService destinationService;

    @RequestMapping("list")
    private String list(Model model, @ModelAttribute("qo") RegionQuery qo){
            qo.setOrder("sequence asc");

        model.addAttribute("pageInfo", regionService.query(qo));
        return "region/list";
    }



    @RequestMapping("getDestByDeep")
    @ResponseBody
    private Object getDestByDeep(int deep){
        return destinationService.getDestByDeep(deep);
    }

    @RequestMapping("getDestByRegionId")
    @ResponseBody
    private Object getDestByRegionId(Long rid){
        return destinationService.getDestByRegionId(rid);
    }

    @RequestMapping("saveOrUpdate")
    @ResponseBody
    private Object saveOrUpdate(Region region){
        regionService.saveOrUpdate(region);
        return AjaxResult.SUCCESS;
    }


    @RequestMapping("changeHotValue")
    @ResponseBody
    private Object changeHotValue(Long id, Integer hot){
        regionService.changeHotValue(id, hot);
        return AjaxResult.SUCCESS;
    }


    @RequestMapping("delete")
    @ResponseBody
    private Object delete(Long id){
        regionService.delete(id);
        return AjaxResult.SUCCESS;
    }

}
