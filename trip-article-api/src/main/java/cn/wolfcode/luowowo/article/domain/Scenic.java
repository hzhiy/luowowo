package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * 景点
 */
@Getter
@Setter
public class Scenic extends BaseDomain {

    private String name; // 景点名

    private String englishName;

    private String coverUrl;

    private Long dest_id;

    private Long parent_id;

    private String infoImgs;

    private String info;

    private Integer commentNum; // 点评数

    private Integer favorNum;

    private String localName;

    private Integer status;

    private Date compileTime;

    private Long theme_id; // 关联的主题id

    private List<Scenic> includeScenicArr;

    public String[] getImgArr() {
        if (StringUtils.hasLength(infoImgs)) {
            return infoImgs.split(";");
        }
        return null;
    }

}