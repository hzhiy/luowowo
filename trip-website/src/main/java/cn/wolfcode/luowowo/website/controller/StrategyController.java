package cn.wolfcode.luowowo.website.controller;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.domain.StrategyContent;
import cn.wolfcode.luowowo.article.domain.StrategyDetail;
import cn.wolfcode.luowowo.article.query.StrategyDetailQuery;
import cn.wolfcode.luowowo.article.service.*;
import cn.wolfcode.luowowo.cache.emum.RedisKeys;
import cn.wolfcode.luowowo.cache.service.IStrategyStatisRedisService;
import cn.wolfcode.luowowo.cache.vo.StrategyStatisVO;
import cn.wolfcode.luowowo.comment.domain.StrategyComment;
import cn.wolfcode.luowowo.comment.service.IStrategyCommentService;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.search.query.SearchQueryObject;
import cn.wolfcode.luowowo.search.service.IStrategyTemplateService;
import cn.wolfcode.luowowo.search.vo.StatisVO;
import cn.wolfcode.luowowo.website.annotation.UserParam;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("strategy")
public class StrategyController {

    @Reference
    private IStrategyService strategyService;
    @Reference
    private IDestinationService destinationService;
    @Reference
    private IStrategyDetailService strategyDetailService;
    @Reference
    private IStrategyTagService strategyTagService;
    @Reference
    private IStrategyCommentService strategyCommentService;
    @Reference
    private IStrategyStatisRedisService strategyStatisRedisService;
    @Reference
    private IStrategyCommendService strategyCommendService;
    @Reference
    private IStrategyTemplateService strategyTemplateService;

    @RequestMapping("")
    public String index(Model model){
        // 获取推荐前5
        model.addAttribute("commends", strategyCommendService.getTop5());
        // 攻略推荐前10
        List<StrategyStatisVO> list = strategyStatisRedisService.getAllZSetStrategyVO(RedisKeys.STRATEGY_STATIS_COMMEND_SORT.getPrefix());
        List<StrategyStatisVO> abroadCds = new ArrayList<>();   // 国外
        List<StrategyStatisVO> unabroadCds = new ArrayList<>();   // 国内
        for (StrategyStatisVO vo : list) {
            if (vo.isIsabroad() && abroadCds.size() < 10){
                abroadCds.add(vo);
            }else if (vo.isIsabroad() == false && unabroadCds.size() < 10){
                unabroadCds.add(vo);
            }
        }
        model.addAttribute("abroadCds", abroadCds);         // 国外攻略推荐排行前10
        model.addAttribute("unabroadCds", unabroadCds);     // 国内攻略推荐排行前10
        // 热门攻略推荐
        List<StrategyStatisVO> hotCds = strategyStatisRedisService.getAllZSetStrategyVO(RedisKeys.STRATEGY_STATIS_HOT_SORT.getPrefix());
        model.addAttribute("hotCds", hotCds.size() >= 10? hotCds.subList(0, 10): hotCds);
        // 主题推荐
        List<Map<String, Object>> themeCds = strategyTemplateService.getThemeCommend();
        model.addAttribute("themeCds", themeCds.subList(0, themeCds.size()>10?10:themeCds.size()));
        // 国内攻略
        List<StatisVO> chinas = strategyTemplateService.queryConditionGruop(StatisVO.CHINAS_STRATEGY);
        model.addAttribute("chinas", chinas);
        // 国外攻略
        List<StatisVO> abroads = strategyTemplateService.queryConditionGruop(StatisVO.ABROADS_STRATEGY);
        model.addAttribute("abroads", abroads);
        // 主题攻略
        List<StatisVO> themes = strategyTemplateService.queryConditionGruop(StatisVO.THEMES_STRATEGY);
        model.addAttribute("themes", themes);
        return "strategy/index";
    }

    @RequestMapping("list")
    public String list(Model model, @ModelAttribute("qo") StrategyDetailQuery qo) {
        List<Destination> toasts = destinationService.getToasts(qo.getDestId());
        toasts.remove(toasts.size()-1);
        model.addAttribute("toasts", toasts);
        model.addAttribute("dest", destinationService.getDestById(qo.getDestId()));
        model.addAttribute("tags", strategyTagService.list());

        model.addAttribute("pageInfo", strategyDetailService.query(qo));
        return "strategy/list";
    }


    @RequestMapping("detail")
    public String detail(Model model, Long id, @UserParam UserInfo userInfo) {
        StrategyDetail detail = strategyDetailService.get(id);
        StrategyContent content = strategyDetailService.getContent(detail.getId());
        detail.setStrategyContent(content);
        model.addAttribute("detail", detail);
        strategyStatisRedisService.viewNumIncrease(id, 1);
        model.addAttribute("vo", strategyStatisRedisService.getStrategyStatisVO(id));
        if (userInfo != null){
            boolean ret = strategyStatisRedisService.isUserFavorStrategy(userInfo.getId(), id);
            if (ret){
                model.addAttribute("isFavor", ret);
            }
        }
        // 攻略热门推荐分数+1
        strategyStatisRedisService.addScore(RedisKeys.STRATEGY_STATIS_HOT_SORT.getPrefix(), RedisKeys.STRATRGY_STATIS_VO.join(id.toString()), 1);

        return "strategy/detail";
    }

    // 添加评论
    @RequestMapping("commentAdd")
    @ResponseBody
    public Object commentAdd(StrategyComment comment, @UserParam UserInfo userInfo){

        comment.setUserId(userInfo.getId());
        comment.setUsername(userInfo.getNickname());
        comment.setCity(userInfo.getCity());
        comment.setLevel(userInfo.getLevel());
        comment.setHeadUrl(userInfo.getHeadImgUrl());

        // 点赞没有引入redis统计 暂时简单查询
        //StrategyDetail detail = strategyDetailService.get(comment.getDetailId());
        //strategyDetailService.updateReplynum(detail.getId(), detail.getReplynum() + 1);
        // 点赞
        strategyStatisRedisService.replyNumIncrease(comment.getDetailId(), 1);

        strategyCommentService.saveOrUpdate(comment);
        // 攻略热门推荐分数+1
        strategyStatisRedisService.addScore(RedisKeys.STRATEGY_STATIS_HOT_SORT.getPrefix(), RedisKeys.STRATRGY_STATIS_VO.join(comment.getDetailId().toString()), 1);

        return AjaxResult.SUCCESS.addObject(strategyStatisRedisService.getStrategyStatisVO(comment.getDetailId()).getReplynum());
    }

    // 加载评论
    @RequestMapping("comment")
    public String comment(Model model, @ModelAttribute("qo") StrategyDetailQuery qo, @UserParam UserInfo userInfo){
        model.addAttribute("page", strategyCommentService.list(qo));
        return "strategy/commentTpl";
    }

    // 评论点赞
    @RequestMapping("commentThumbUp")
    @ResponseBody
    public Object commentThumbUp(String toid, Long fromid){
        strategyCommentService.commentThumbUp(toid, fromid);
        return AjaxResult.SUCCESS;
    }

    // 收藏
    @RequestMapping("favor")
    @ResponseBody
    public Object favor(Long sid, @UserParam UserInfo userInfo){
        AjaxResult result = new AjaxResult();
        if (userInfo == null){
            result.setSuccess(false);
            result.setCode(102);
            result.setMsg("请先登录");
            return result;
        }
        boolean ret = strategyStatisRedisService.favor(sid, userInfo.getId());
        result.addObject(strategyStatisRedisService.getStrategyStatisVO(sid));
        result.setSuccess(ret);
        // +1||-1 分数  第一个参数: 识别是攻略推荐还是攻略热门  第二个参数: value  第三个参数: 是加还是减
        strategyStatisRedisService.addScore(RedisKeys.STRATEGY_STATIS_COMMEND_SORT.getPrefix() ,RedisKeys.STRATRGY_STATIS_VO.join(sid.toString()), ret?1:-1);
        return result;
    }

    // 顶, 我顶!
    @RequestMapping("strategyThumbup")
    @ResponseBody
    public Object strategyThumbup(Long sid, @UserParam UserInfo userInfo){
        AjaxResult result = new AjaxResult();
        if (userInfo == null){
            result.setSuccess(false);
            result.setCode(102);
            result.setMsg("请先登录");
            return result;
        }
        boolean ret = strategyStatisRedisService.strategyThumbup(sid, userInfo.getId());
        result.addObject(strategyStatisRedisService.getStrategyStatisVO(sid));
        result.setSuccess(ret);
        if (ret) {
            strategyStatisRedisService.addScore(RedisKeys.STRATEGY_STATIS_COMMEND_SORT.getPrefix(), RedisKeys.STRATRGY_STATIS_VO.join(sid.toString()), 1);
        }
        return result;
    }
    // 假分享
    @RequestMapping("sharenum")
    @ResponseBody
    public Object sharenum(Long sid, @UserParam UserInfo userInfo){
        AjaxResult result = new AjaxResult();
        if (userInfo == null){
            result.setSuccess(false);
            result.setCode(102);
            result.setMsg("请先登录");
            return result;
        }
        boolean ret = strategyStatisRedisService.sharenum(sid, userInfo.getId());
        result.addObject(strategyStatisRedisService.getStrategyStatisVO(sid));
        result.setSuccess(ret);
        return result;
    }


    // 攻略显示
    @RequestMapping("searchPage")
    public String searchPage(Model model, @ModelAttribute("qo")SearchQueryObject qo){
        model.addAttribute("page", strategyTemplateService.queryStrategyByTypeId(qo));
        return "strategy/searchPageTpl";
    }



}
