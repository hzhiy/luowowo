package cn.wolfcode.luowowo.comment.service;

import cn.wolfcode.luowowo.article.query.TravelDetailQuery;
import cn.wolfcode.luowowo.article.vo.QuestionAnswerVO;
import cn.wolfcode.luowowo.comment.domain.QuestionAnswer;
import cn.wolfcode.luowowo.comment.query.QuestionAnswerQuery;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * 问题回答接口
 */
public interface IQuestionAnswerService {

    /**
     * 添加
     * @param questionAnswer
     */
    QuestionAnswer saveOrUpdate(QuestionAnswer questionAnswer);

    /**
     * 查询问题评论
     * @param qo
     */
    Page list(QuestionAnswerQuery qo);


    /**
     * 最新回答动态10条
     * @return
     */
    List<QuestionAnswer> listByCreateTimeTop10();

    /**
     * 显示所有回答
     * @return
     */
    List<QuestionAnswer> queryForAll();

    /**
     * 根据问题id获取单个
     * @param id
     * @return
     */
    QuestionAnswerVO getByQuestionId(Long id);

    /**
     * 获取单个
     * @param aid
     * @return
     */
    QuestionAnswer getById(String aid);
}
