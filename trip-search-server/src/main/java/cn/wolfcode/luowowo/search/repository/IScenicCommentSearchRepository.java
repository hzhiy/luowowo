package cn.wolfcode.luowowo.search.repository;

import cn.wolfcode.luowowo.search.template.ScenicCommentTemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * 景点评论查询接口
 */
@Repository
public interface IScenicCommentSearchRepository extends ElasticsearchRepository<ScenicCommentTemplate, String> {

}
