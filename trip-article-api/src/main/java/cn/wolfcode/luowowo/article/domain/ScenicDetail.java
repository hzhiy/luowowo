package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

/**
 * 景点明细
 */
@Setter
@Getter
public class ScenicDetail extends BaseDomain {

    private String name;

    private Long scenic_id;

    private String info;

    private String arrivalWay;

    private String ticketing;

    private String opentime;

    private String cost;

    private String tel;

    private String url;

    private String takeTime;

    private String imgs;

    private String themeType;

    public String[] getImgArr() {
        if (StringUtils.hasLength(imgs)) {
            return imgs.split(";");
        }
        return new String[0];
    }
}