package cn.wolfcode.luowowo.comment.service;

import cn.wolfcode.luowowo.article.vo.UsersReplyVO;
import cn.wolfcode.luowowo.comment.domain.ScenicComment;
import cn.wolfcode.luowowo.comment.query.ScenicCommentQuery;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface IScenicCommentService {
    ScenicComment addComment(ScenicComment scenicComment);

    /**
     * 根据景点Id查评论对象的list
     *
     * @param qo 景点查询封装对象
     * @return 景点对象的list
     */
    Page<ScenicComment> queryCommentByScenicId(ScenicCommentQuery qo);

    /**
     * 根据用户id查询景点点评
     *
     * @param uid
     * @return
     */
    List<ScenicComment> queryByUserId(Long uid);

    /**
     * 景点评论的点赞
     *
     * @param toid   评论id
     * @param fromid 用户id
     */
    void commentThumbUp(String toid, Long fromid);

    ScenicComment getscenicCommentById(String toid);

    /**
     * 通过评论id找评论对象
     *
     * @param toid
     * @return
     */
    ScenicComment queryByCommentId(String toid);

    /**
     * 准确来说是单条回复
     * <p>
     * toid        评论id
     * fromId      被评论用户id
     * fromName    被评论用户名
     * fromHeadUrl 被评论用户头像
     * userInfo    回复者
     * content     对话内容
     *
     * @return 评论的list对象数组
     */
    List<ScenicComment> usersReply(UsersReplyVO usersReplyVO, UserInfo userInfo);

    /**
     * @return 返回景所有点评论对象
     */
    List<ScenicComment> findAll();

    /**
     * 返回各个标签的总数
     * @return
     */
    Map<String, List> tagsCount();

    /**
     * 返回总数
     *
     * @return
     */
    long totalCommentNum();
}
