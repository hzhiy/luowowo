package cn.wolfcode.luowowo.search.service;

import cn.wolfcode.luowowo.search.query.SearchQueryObject;
import cn.wolfcode.luowowo.search.template.HotelTemplate;
import org.springframework.data.domain.Page;

/**
 * Created by zhi on 2019/8/22.
 */
public interface IHotelTemplateService {
    /**
     * 增加或修改
     * @param hotelTemplate
     */
    void saveOrUpdate(HotelTemplate hotelTemplate);

    /**
     * 搜索酒店列表
     * @param qo
     * @return
     */
    Page queryHotelByCityName(SearchQueryObject qo);
}
