package cn.wolfcode.luowowo.common.util;

import cn.wolfcode.luowowo.common.exception.LogicException;

/**
 * Created by zhi on 2019/8/7.
 */
public class AssertUtil {
    public static void
    hasLength(String str, String msg) throws LogicException {
        if (str == null || "".equals(str.trim())){
            throw new LogicException(msg);
        }
    }

    public static void isEquals(String v1, String v2, String msg) throws LogicException{
        hasLength(v1, "值一为空");
        hasLength(v2, "值二为空");
        if (!v1.equals(v2)){
            throw new LogicException(msg);
        }

    }
}
