package cn.wolfcode.luowowo.hotel.service;

import cn.wolfcode.luowowo.hotel.domain.HotelPrice;
import cn.wolfcode.luowowo.hotel.vo.HotelAvgPriceVO;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by zhi on 2019/8/22.
 */
public interface IHotelPriceService {
    /**
     * 获得酒店的房价信息
     * @param id
     * @return
     */
    List<HotelPrice> getByHotelId(Long id);

    /**
     * 查询地区的星级均价
     * @param cityId
     * @return
     */
    List<HotelAvgPriceVO> queryStarAvg(Long cityId);
}
