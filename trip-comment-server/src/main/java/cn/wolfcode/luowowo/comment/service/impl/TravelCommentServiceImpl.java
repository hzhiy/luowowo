package cn.wolfcode.luowowo.comment.service.impl;

import cn.wolfcode.luowowo.article.query.TravelDetailQuery;
import cn.wolfcode.luowowo.comment.domain.TravelComment;
import cn.wolfcode.luowowo.comment.repository.ITravelCommentRepository;
import cn.wolfcode.luowowo.comment.service.ITravelCommentService;
import cn.wolfcode.luowowo.comment.service.ITravelCommentService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by zhi on 2019/8/13.
 */
@Service
public class TravelCommentServiceImpl implements ITravelCommentService {

    @Autowired
    private ITravelCommentRepository travelCommentRepository;
    @Autowired
    private MongoTemplate template;

    public TravelComment saveOrUpdate(TravelComment comment) {
        if (!StringUtils.hasLength(comment.getId())) {
            Optional<TravelComment> optional = travelCommentRepository.findById(comment.getRefComment().getId());
            if (optional.isPresent()) {
                TravelComment refComment = optional.get();
                comment.setRefComment(refComment);
            }
            comment.setCreateTime(new Date());
            travelCommentRepository.save(comment);
        }
        return comment;

    }

    public List<TravelComment> list(TravelDetailQuery qo) {
        // 查询条件
        Query query = new Query();
        query.addCriteria(Criteria.where("travelId").is(qo.getTravelId()));
        // 查总数
        long count = template.count(query, TravelComment.class);
        if (count == 0) {
            return Collections.EMPTY_LIST;
        }
        // 分页:PageRequest.of(当前页0开始, 每页显示条数, 排序规则)
        //Pageable request = PageRequest.of(qo.getCurrentPage() - 1, qo.getPageSize(), Sort.by(Sort.Direction.DESC, "createTime"));
        //query.with(request);
        // 查所有评论
        List<TravelComment> list = template.find(query, TravelComment.class);
        return list;

    }

    public void commentThumbUp(String mongoId, Long userId) {

    }

    @Override
    public List<TravelComment> listByCreateTimeTop10() {
        Query query = new Query();
        Pageable request = PageRequest.of(0, 10, Sort.by(Sort.Direction.DESC, "createTime"));
        query.with(request);
        List<TravelComment> list = template.find(query, TravelComment.class);
        return list;
    }
}
