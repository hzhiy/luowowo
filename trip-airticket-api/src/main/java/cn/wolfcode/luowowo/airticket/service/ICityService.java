package cn.wolfcode.luowowo.airticket.service;

import cn.wolfcode.luowowo.airticket.domain.City;

import java.util.List;

/**
 * Created by Administrator on 2019/8/24.
 */
public interface ICityService {

    List<City> getHotCity();

    List<City> getNormalCity(int i);
}
