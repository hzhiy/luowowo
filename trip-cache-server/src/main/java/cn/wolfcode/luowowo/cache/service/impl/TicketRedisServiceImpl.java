package cn.wolfcode.luowowo.cache.service.impl;

import cn.wolfcode.luowowo.article.domain.Ticket;
import cn.wolfcode.luowowo.cache.emum.RedisKeys;
import cn.wolfcode.luowowo.cache.service.ITicketRedisService;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Service
public class TicketRedisServiceImpl implements ITicketRedisService {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public void hasKey(String key, List<Ticket> list) {
        List<Ticket> ticket = new ArrayList<>();//对象空集合
        if (redisTemplate.hasKey(key)) {//判断是否有key
            //有key
            String ticketStr = redisTemplate.opsForValue().get(key);//获取value
            if (StringUtils.hasLength(ticketStr)) {//判断是否有值
                //value有值
                ticket = JSON.parseArray(ticketStr, Ticket.class);//有值转换ticket集合
                for (Ticket t : list) {
                    if (!ticket.contains(t)) {//判断缓存原有的集合是否有该门票对象
                        ticket.add(t);//没有添加
                    }
                }
                redisTemplate.opsForValue().set(key, JSON.toJSONString(ticket));//保存
            } else {
                //value没有值
                for (Ticket t : list) {
                    ticket.add(t);//添加对象值集合中
                }
                redisTemplate.opsForValue().set(key, JSON.toJSONString(ticket));
            }
        } else {
            //没key
            for (Ticket t : list) {
                ticket.add(t);//集合中添加对象
            }
            redisTemplate.opsForValue().set(key, JSON.toJSONString(ticket));
        }

    }

    @Override
    public List<Ticket> getByDestId(Long ajaxDestId) {
        String ticketStr = redisTemplate.opsForValue().get(RedisKeys.TICKET_STATIS_DEST.join(ajaxDestId.toString()));
        List<Ticket> list = JSON.parseArray(ticketStr, Ticket.class);
        System.out.println(list);
        return list;
    }
}
