package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.TicketTheme;
import java.util.List;

public interface TicketThemeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(TicketTheme record);

    TicketTheme selectByPrimaryKey(Long id);

    List<TicketTheme> selectAll();

    int updateByPrimaryKey(TicketTheme record);
}