package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.TicketContent;
import cn.wolfcode.luowowo.article.mapper.TicketContentMapper;
import cn.wolfcode.luowowo.article.service.ITicketContentService;
import cn.wolfcode.luowowo.article.service.ITicketThemeService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 */
@Service
public class TicketContentServiceImpl implements ITicketContentService{

    @Autowired
    private TicketContentMapper ticketContentMapper;

    @Override
    public TicketContent getByDetailId(Long id) {
        return ticketContentMapper.selectByPrimaryKey(id);
    }
}
