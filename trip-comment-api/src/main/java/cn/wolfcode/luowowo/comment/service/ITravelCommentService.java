package cn.wolfcode.luowowo.comment.service;

import cn.wolfcode.luowowo.article.query.TravelDetailQuery;
import cn.wolfcode.luowowo.comment.domain.TravelComment;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * 游记评论接口
 */
public interface ITravelCommentService {

    /**
     * 添加
     * @param comment
     */
    TravelComment saveOrUpdate(TravelComment comment);

    /**
     * 查询攻略评论
     * @param qo
     */
    List<TravelComment> list(TravelDetailQuery qo);

    /**
     * 点赞或者取消点赞
     * @param mongoId
     * @param userId
     */
    void commentThumbUp(String mongoId, Long userId);

    /**
     * 最新评论动态10条
     * @return
     */
    List<TravelComment> listByCreateTimeTop10();

}
