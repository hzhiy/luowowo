package cn.wolfcode.luowowo.search.service.impl;

import cn.wolfcode.luowowo.search.repository.IUserInfoSearchRepository;
import cn.wolfcode.luowowo.search.service.IUserInfoTemplateService;
import cn.wolfcode.luowowo.search.template.DestinationTemplate;
import cn.wolfcode.luowowo.search.template.UserInfoTemplate;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import org.elasticsearch.client.ElasticsearchClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

import java.util.List;

/**
 * Created by zhi on 2019/8/17.
 */
@Service
public class UserInfoTemplateServiceImpl implements IUserInfoTemplateService {

    @Autowired
    private IUserInfoSearchRepository userInfoSearchRepository;
    @Autowired
    private ElasticsearchTemplate template;
    @Autowired
    private ElasticsearchClient client;

    public void saveOrUpdate(UserInfoTemplate userInfoTemplate) {
        userInfoSearchRepository.save(userInfoTemplate);
    }

    @Override
    public List<UserInfoTemplate> findByDestName(String destName) {
        return userInfoSearchRepository.findByDestName(destName);
    }

}
