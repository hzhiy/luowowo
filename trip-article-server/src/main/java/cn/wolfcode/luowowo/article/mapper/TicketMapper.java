package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.Ticket;
import java.util.List;

public interface TicketMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Ticket record);

    Ticket selectByPrimaryKey(Long id);

    List<Ticket> selectAll();

    int updateByPrimaryKey(Ticket record);

}