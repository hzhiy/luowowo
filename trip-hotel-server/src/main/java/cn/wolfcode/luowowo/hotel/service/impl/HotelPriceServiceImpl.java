package cn.wolfcode.luowowo.hotel.service.impl;

import cn.wolfcode.luowowo.hotel.domain.HotelPrice;
import cn.wolfcode.luowowo.hotel.mapper.HotelPriceMapper;
import cn.wolfcode.luowowo.hotel.service.IHotelPriceService;
import cn.wolfcode.luowowo.hotel.service.IHotelService;
import cn.wolfcode.luowowo.hotel.vo.HotelAvgPriceVO;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by zhi on 2019/8/22.
 */
@Service
public class HotelPriceServiceImpl implements IHotelPriceService {
    @Autowired
    private HotelPriceMapper hotelPriceMapper;

    @Override
    public List<HotelPrice> getByHotelId(Long id) {
        return hotelPriceMapper.selectByHotelId(id);
    }

    @Override
    public List<HotelAvgPriceVO> queryStarAvg(Long cityId) {
        return hotelPriceMapper.selectStarAvg(cityId);
    }
}
