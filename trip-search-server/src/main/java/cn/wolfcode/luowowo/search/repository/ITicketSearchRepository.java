package cn.wolfcode.luowowo.search.repository;

import cn.wolfcode.luowowo.search.template.TicketTemplate;
import cn.wolfcode.luowowo.search.template.TravelTemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by zhi on 2019/8/17.
 */
@Repository
public interface ITicketSearchRepository extends ElasticsearchRepository<TicketTemplate, Long>{

    List<TicketTemplate> findByDestId(Long ajaxDestId);
}
