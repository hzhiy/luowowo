package cn.wolfcode.luowowo.airticket.mapper;

import cn.wolfcode.luowowo.airticket.domain.AirPort;
import java.util.List;

public interface AirPortMapper {
    int deleteByPrimaryKey(Long id);

    int insert(AirPort record);

    AirPort selectByPrimaryKey(Long id);

    List<AirPort> selectAll();

    int updateByPrimaryKey(AirPort record);

    List<AirPort> selectByAircode(String orgCity);
}