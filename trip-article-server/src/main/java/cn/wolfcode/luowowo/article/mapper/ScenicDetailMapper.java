package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.ScenicDetail;
import java.util.List;

public interface ScenicDetailMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ScenicDetail record);

    ScenicDetail selectByPrimaryKey(Long id);

    List<ScenicDetail> selectAll();

    int updateByPrimaryKey(ScenicDetail record);

    ScenicDetail selectByScenicId(Long scenicId);
}