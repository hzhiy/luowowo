package cn.wolfcode.luowowo.hotel.domain;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Setter
@Getter
@ToString
public class Hotel extends BaseDomain {

    private String name;                    // 酒店名
    private String internationalName;       // 国际名
    private String info;                    // 说明
    private Destination position;           // 地区
    private Destination city;               // 城市
    private String star;                    // 星级
    private String grade;                   // 分数
    private String history;                 // 历史
    private String scale;                   // 规模
    private String checkin;                 // 入住时间
    private String departure;               // 离开时间
    private String address;                 // 地址

    private String coverUrl;                // 封面
    private Integer commentNum;             // 评论数
    private String imgs;
    private List<String> images;              // 附带图片
    private List<HotelPrice> prices;        // 酒店房间与价格

}

