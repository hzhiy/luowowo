package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.StrategyCommend;
import cn.wolfcode.luowowo.article.query.StrategyCommendQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 攻略推荐服务
 */
public interface IStrategyCommendService {
    /**
     * 分页
     * @param qo
     * @return
     */
    PageInfo queryForList(StrategyCommendQuery qo);

    /**
     * 增加或更新
     * @param strategyCommend
     */
    void saveOrUpdate(StrategyCommend strategyCommend);

    /**
     * 删除
     * @param id
     */
    void delete(Long id);


    /**
     * 攻略推荐前5
     * @return
     */
    List<StrategyCommend> getTop5();
}
