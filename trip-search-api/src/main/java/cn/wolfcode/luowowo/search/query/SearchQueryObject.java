package cn.wolfcode.luowowo.search.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;


/**
 * Created by zhi on 2019/8/18.
 */
@Setter
@Getter
public class SearchQueryObject extends QueryObject {
    // 攻略首页
    public static final int ALL_STRATEGY = -1;     // 全部
    public static final int ABROADS_STRATEGY = 0;     // 国外攻略
    public static final int CHINAS_STRATEGY = 1;      // 国内攻略
    public static final int THEMES_STRATEGY = 2;      // 主题攻略

    // 首页站内搜索
    public static final int SEARCH_ALL = -1;        // 全部
    public static final int SEARCH_DEST = 0;        // 地区
    public static final int SEARCH_STRATEGY = 1;    // 攻略
    public static final int SEARCH_TRAVEL = 2;      // 游记
    public static final int SEARCH_USER = 3;        // 用户


    private String keyword = "";        // 搜索关键字
    private String orderBy = "viewnum";
    private Integer type = -1;
    private Long typeValue = -1L;

    private String name;    // 酒店搜索用
    private String cityName;  // es精准搜索酒店城市名用
    private String positionName;  // es精准搜索酒店地区名用

    public Pageable getPage(){
        return PageRequest.of(super.getCurrentPage()-1, super.getPageSize(), Sort.by(Sort.Direction.DESC, orderBy));
    }
    public Pageable getHotelPage(){
        return PageRequest.of(super.getCurrentPage()-1, super.getPageSize(), Sort.by(type == 1? Sort.Direction.DESC : Sort.Direction.ASC, orderBy));
    }



    public Pageable getPageableNoSort(){
        return PageRequest.of(super.getCurrentPage()-1, super.getPageSize());
    }
}
