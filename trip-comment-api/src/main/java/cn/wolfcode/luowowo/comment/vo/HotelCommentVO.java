package cn.wolfcode.luowowo.comment.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by zhi on 2019/8/24.
 */
@Setter
@Getter
public class HotelCommentVO implements Serializable {
    private Double count = 0.0;          // 总评论数
    private Double ensemble = 0.0;       // 总体评价
    private Double location = 0.0;       // 位置
    private Double cleanliness = 0.0;    // 清洁度
    private Double facility = 0.0;       // 设施
    private Double service = 0.0;        // 服务
    private Double comfort = 0.0;        // 舒适度
    private Double food = 0.0;           // 餐饮

}
