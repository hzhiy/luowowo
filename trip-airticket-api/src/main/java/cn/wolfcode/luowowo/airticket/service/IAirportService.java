package cn.wolfcode.luowowo.airticket.service;

import cn.wolfcode.luowowo.airticket.domain.AirPort;

import java.util.List;

/**
 * Created by Administrator on 2019/8/25.
 */
public interface IAirportService {

    List<AirPort> listByAirCode(String orgCity);
}
