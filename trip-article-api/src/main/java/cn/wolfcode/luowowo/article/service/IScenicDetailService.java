package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.ScenicDetail;

public interface IScenicDetailService {
    ScenicDetail queryDetailById(Long scenicId);
}
