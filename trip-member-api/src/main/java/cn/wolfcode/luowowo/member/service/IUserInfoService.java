package cn.wolfcode.luowowo.member.service;

import cn.wolfcode.luowowo.common.exception.LogicException;
import cn.wolfcode.luowowo.member.domain.UserInfo;

import java.util.List;

/**
 * 用户信息接口
 */
public interface IUserInfoService {

    /**
     * 根据id查询用户信息
     * @param id
     * @return
     */
    UserInfo get(long id);

    /**
     * 通过手机号码查询是否存在
     * @param phone
     * @return
     */
    boolean queryByPhone(String phone);

    /**
     * 发送验证码
     * @param phone
     */
    void sendVerifyCode(String phone);

    /**
     * 注册
     * 告诉dubbo不要处理我们抛出的异常类型
     * @param phone
     * @param nickname
     * @param password
     * @param rpassword
     * @param verifyCode
     */
    void regist(String phone, String nickname, String password, String rpassword, String verifyCode) throws LogicException;

    /**
     * 验证用户正确性
     * @param username
     * @param password
     * @return
     */
    String userLogin(String username, String password) throws LogicException;

    /**
     * 查询全部
     * @return
     */
    List<UserInfo> list();

    /**
     * 用户信息更改
     * @param userInfo
     */
    void updateUser(UserInfo userInfo);

    /**
     * 修改手机号
     * @param phone
     */
    void updatePhoneSendVerifyCode(String phone,Long uid) throws LogicException;

    /**
     * 确认验证码
     * @param verifyCode
     * @param phone
     */
    void checkoutVerfyCode(String verifyCode, String phone,Long id) throws  LogicException;

    /**
     * 新手机号发送验证码
     * @param phone
     */
    void newPhoneSendVerifyCode(String phone);

    /**
     * 修改头像
     * @param userInfo
     */
    void updateHeadImgUrl(UserInfo userInfo);

    /**
     * 修改密码
     * @param password
     * @param id
     */
    void updatePassword(String password, Long id,String verifyCode,String phone) throws LogicException;
}
