package cn.wolfcode.luowowo.airticket.mapper;

import cn.wolfcode.luowowo.airticket.domain.Plane;
import java.util.List;

public interface PlaneMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Plane record);

    Plane selectByPrimaryKey(Long id);

    List<Plane> selectAll();

    int updateByPrimaryKey(Plane record);
}