package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.DestTheme;
import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.mapper.DestThemeMapper;
import cn.wolfcode.luowowo.article.mapper.DestinationMapper;
import cn.wolfcode.luowowo.article.service.IDestThemeService;
import cn.wolfcode.luowowo.article.vo.ThemeDestVO;
import cn.wolfcode.luowowo.cache.service.IDestRedisService;
import cn.wolfcode.luowowo.hotel.domain.Hotel;
import cn.wolfcode.luowowo.hotel.service.IHotelPriceService;
import cn.wolfcode.luowowo.hotel.service.IHotelService;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhi on 2019/8/21.
 */
@Service
public class DestThemeServiceImpl implements IDestThemeService {
    @Autowired
    private DestThemeMapper destThemeMapper;
    @Autowired
    private DestinationMapper destinationMapper;
    @Autowired
    private DestinationServiceImpl destinationServicel;
    @Reference
    private IDestRedisService destRedisService;

    public List<DestTheme> queryThemeTop6() {

        return destThemeMapper.selectThemeTop6();
    }

    @Override
    public List<ThemeDestVO> queryByThemeid(Long id) {
        List<Destination> listDest = destinationMapper.selectByThemeId(id);
        List<ThemeDestVO> list = new ArrayList<>();
        ThemeDestVO vo = null;
        for (Destination d : listDest) {

            vo = destRedisService.getThemeDestVO(d.getId());
            if (vo == null){
                vo = new ThemeDestVO();
                List<Destination> toasts = destinationServicel.getToasts(d.getId());
                vo.setId(d.getId());
                vo.setName(d.getName());
                vo.setImg(d.getCoverUrl());
                if (toasts.size()>0){
                    vo.setShortName(toasts.get(0).getName());
                }
                destRedisService.setThemeDestVO(vo);
            }
            list.add(vo);

        }
        return list;
    }


}
