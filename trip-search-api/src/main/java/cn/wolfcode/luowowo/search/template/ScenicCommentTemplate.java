package cn.wolfcode.luowowo.search.template;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Document(indexName = "luowowo_scenic", type = "scenic")
public class ScenicCommentTemplate implements Serializable {
    public static final String INDEX_NAME = "luowowo_scenic";
    public static final String TYPE_NAME = "scenic";

    public static final int COMMENT_TYPE_COMMENT = 0; // 回复点评
    public static final int COMMENT_TYPE = 1; // 回复用户
    @Id
    @Field(store = true, index = true, type = FieldType.Long)
    private String id; // 评论id

    @Field(store = true, index = true, type = FieldType.Long)
    private Long scenicId; // 景点Id

    @Field(store = true, index = true, type = FieldType.Text)
    private String scenicName; // 景点名

    @Field(store = true, index = true, type = FieldType.Long)
    private Long userId; // 用户id

    @Field(store = true, index = true, type = FieldType.Text)
    private String username; // 用户名

    @Field(store = true, index = true, type = FieldType.Text)
    private String city; // 用户城市 暂时用不上

    @Field(store = true, index = true, type = FieldType.Long)
    private int level; // 等级

    @Field(store = true, index = true, type = FieldType.Text)
    private String headUrl; // 用户头像

    @Field(store = true, index = true, type = FieldType.Long)
    private int type = COMMENT_TYPE_COMMENT; //评论类别

    @Field(pattern = "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis", type=FieldType.Date, format = DateFormat.custom)
    private Date createTime; // 创建时间

    @Field(index = true, analyzer = "ik_max_word", store = true, searchAnalyzer = "ik_max_word", type = FieldType.Text)
    private String content; // 评论内容

    @Field(store = true, index = true, type = FieldType.Text)
    private String imgUrls; // 图片

    @Field(store = true, index = true, type = FieldType.Integer)
    private int hasImg; // 是否包含图片

    @Field(store = true, index = true, type = FieldType.Integer)
    private int starNum; // 评价星级

    @Field(store = true, index = true, type = FieldType.Integer)
    private int thumbupnum; // 点赞数

    @Field(store = true, type = FieldType.Long)
    private List<Long> thumbuplist = new ArrayList<>(); // 点赞列表
}
