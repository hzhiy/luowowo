package cn.wolfcode.luowowo.airticket.service.impl;

import cn.wolfcode.luowowo.airticket.domain.Flight;
import cn.wolfcode.luowowo.airticket.mapper.FlightMapper;
import cn.wolfcode.luowowo.airticket.query.FlightQuery;
import cn.wolfcode.luowowo.airticket.service.IFlightService;
import cn.wolfcode.luowowo.airticket.vo.FlightVO;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Administrator on 2019/8/24.
 */
@Service
public class FlightServiceImpl implements IFlightService {

    @Autowired
    private FlightMapper flightMapper;

    /**
     *  条件：
     *      出发城市
     *      目的地城市
     *      出发时间
     * @param qo
     * @return
     */

    @Override
    public List<FlightVO> queryForList1(FlightQuery qo) {

        /*return flightMapper.selectForList(qo);*/
        return null;
    }

    @Override
    public PageInfo<Flight> queryForList(FlightQuery qo) {
        if (qo.getOrderBy()!=null) {
            PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getOrderBy());
        }else{
            PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getOrderBy());
        }
        return new PageInfo<>(flightMapper.selectForList(qo));
    }
}
