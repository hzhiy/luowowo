package cn.wolfcode.luowowo.article.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DestinationThemeQuery extends QueryObject {
    private Long id=-1L;//月份的id
    private Long monthId=-1L;
    private Long themeId=-1L;
    private Long daysId=-1L;
}
