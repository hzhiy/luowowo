package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.TicketContent;

import java.util.List;

public interface TicketContentMapper {
    int deleteByPrimaryKey(Long id);

    int insert(TicketContent record);

    TicketContent selectByPrimaryKey(Long id);

    List<TicketContent> selectAll();

    int updateByPrimaryKey(TicketContent record);
}