package cn.wolfcode.luowowo.website.interceptor;

import cn.wolfcode.luowowo.cache.service.IUserInfoRedisService;
import cn.wolfcode.luowowo.common.util.Consts;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.website.annotation.RequireLogin;
import cn.wolfcode.luowowo.website.util.CookieUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 拦截器登录控制
 */
@Component
public class CheckLoginInterceptor implements HandlerInterceptor{
    @Reference
    private IUserInfoRedisService userInfoRedisService;

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 判断访问的方法类型是处理动态方法的
        if (handler instanceof HandlerMethod){
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            // 判断方法上是否贴有自定义注解
            if (handlerMethod.hasMethodAnnotation(RequireLogin.class)){
                String token = CookieUtil.getToken(request);
                    UserInfo userInfo = userInfoRedisService.getUserInfo(token);
                    if(userInfo == null){
                        response.sendRedirect("/login.html");
                        return false;
                    }
                    // 将失效时间重新计时
                    userInfoRedisService.setUserInfo(token, userInfo);
                    CookieUtil.addCookie(token, response);

            }
        }
        return true;
    }
}
