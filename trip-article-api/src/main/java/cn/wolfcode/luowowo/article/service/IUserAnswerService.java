package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.UserAnswer;

import java.util.List;

/**
 * Created by Administrator on 2019/8/25.
 */
public interface IUserAnswerService {
    List<UserAnswer> list();

}
