package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.DestTheme;
import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.vo.ThemeDestVO;

import java.util.List;
import java.util.Map;

/**
 * Created by zhi on 2019/8/21.
 */
public interface IDestThemeService {
    /**
     * 查询排名前6的酒店主题
     *
     * @return
     */
    List<DestTheme> queryThemeTop6();

    /**
     * 按酒店主题id查地区
     *
     * @param id
     * @return
     */
    List<ThemeDestVO> queryByThemeid(Long id);


}
