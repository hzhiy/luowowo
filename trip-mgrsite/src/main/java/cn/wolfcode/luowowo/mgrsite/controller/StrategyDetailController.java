package cn.wolfcode.luowowo.mgrsite.controller;

import cn.wolfcode.luowowo.article.domain.StrategyCatalog;
import cn.wolfcode.luowowo.article.domain.StrategyContent;
import cn.wolfcode.luowowo.article.domain.StrategyDetail;
import cn.wolfcode.luowowo.article.query.StrategyDetailQuery;
import cn.wolfcode.luowowo.article.service.*;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("strategyDetail")
public class StrategyDetailController {


    @Reference
    private IStrategyService strategyService;
    @Reference
    private IStrategyDetailService strategyDetailService;
    @Reference
    private IStrategyThemeService strategyThemeService;
    @Reference
    private IStrategyCatalogService strategyCatalogService;
    @Reference
    private IStrategyTagService strategyTagService;

    @RequestMapping("list")
    public String list(Model model, @ModelAttribute("qo")StrategyDetailQuery qo){
        model.addAttribute("pageInfo",  strategyDetailService.query(qo));
        return "strategyDetail/list";
    }

    @RequestMapping("input")
    public String input(Model model, Long id){
        if (id != null){
            StrategyDetail detail = strategyDetailService.get(id);
            StrategyContent content = strategyDetailService.getContent(detail.getId());
            detail.setStrategyContent(content);
            model.addAttribute("strategyDetail", detail);
            model.addAttribute("tags", strategyTagService.getTags(id));
        }
        model.addAttribute("themes", strategyThemeService.list());
        model.addAttribute("strategies", strategyService.list());
        return "strategyDetail/input";
    }


    @RequestMapping("getCatalogByStrategyId")
    @ResponseBody
    public Object getCatalogByStrategyId(Long strategyId){
        List<StrategyCatalog> strategyCatalogs = strategyCatalogService.queryCatalogByStrategyId(strategyId);
        return strategyCatalogs;
    }

    @RequestMapping("saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(StrategyDetail strategyDetail, String tags){
        strategyDetailService.saveOrUpdate(strategyDetail, tags);
        return AjaxResult.SUCCESS;
    }


    @RequestMapping("delete")
    @ResponseBody
    public Object delete(Long id){
        strategyDetailService.delete(id);
        return AjaxResult.SUCCESS;
    }

    @RequestMapping("changeState")
    @ResponseBody
    public Object changeState(Long id, int state){
        strategyDetailService.changeState(id, state);
        return AjaxResult.SUCCESS;
    }
}
