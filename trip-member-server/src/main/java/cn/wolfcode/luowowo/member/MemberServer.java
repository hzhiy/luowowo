package cn.wolfcode.luowowo.member;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by zhi on 2019/8/7.
 */
@SpringBootApplication
@EnableDubbo
@MapperScan("cn.wolfcode.luowowo.member.mapper")
public class MemberServer {
    public static void main(String[] args) {
        SpringApplication.run(MemberServer.class, args);
    }
}
