package cn.wolfcode.luowowo.mgrsite.listener;

import cn.wolfcode.luowowo.article.domain.StrategyDetail;
import cn.wolfcode.luowowo.article.domain.Ticket;
import cn.wolfcode.luowowo.article.domain.Travel;
import cn.wolfcode.luowowo.article.service.IStrategyDetailService;
import cn.wolfcode.luowowo.article.service.ITicketService;
import cn.wolfcode.luowowo.article.service.ITravelService;
import cn.wolfcode.luowowo.cache.emum.RedisKeys;
import cn.wolfcode.luowowo.cache.service.IStrategyStatisRedisService;
import cn.wolfcode.luowowo.cache.service.ITicketRedisService;
import cn.wolfcode.luowowo.cache.service.ITravelStatisRedisService;
import cn.wolfcode.luowowo.cache.vo.StrategyStatisVO;
import cn.wolfcode.luowowo.cache.vo.TravelStatisVO;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * 数据初始化
 */
@Component
public class RedisDataInitListener implements ApplicationListener<ContextRefreshedEvent> {
    @Reference
    private IStrategyDetailService strategyDetailService;
    @Reference
    private IStrategyStatisRedisService strategyStatisRedisService;
    @Reference
    private ITravelService travelService;
    @Reference
    private ITravelStatisRedisService travelStatisRedisService;
    @Reference
    private ITicketRedisService ticketRedisService;
    @Reference
    private ITicketService ticketService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        // 获取所有要存入的缓存数据 攻略
        List<StrategyDetail> strategyDetailList = strategyDetailService.list();
        StrategyStatisVO vo = null;
        // 遍历数据
        for (StrategyDetail detail : strategyDetailList) {
            // 判断数据是否存在
            vo = new StrategyStatisVO();
            boolean ret = strategyStatisRedisService.isVoExist(detail.getId());
            if (!ret) {
                // 不存在就存入reids中
                try {
                    BeanUtils.copyProperties(vo, detail);
                    vo.setStrategyId(detail.getId());
                    vo.setDestId(detail.getDest().getId());
                    vo.setDestName(detail.getDest().getName());
                    strategyStatisRedisService.setStrategyStatisVO(vo);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }

        // 游记初始化
        List<Travel> travelList = travelService.list();
        TravelStatisVO travelStatisVO = null;
        for (Travel travel : travelList) {
            travelStatisVO = new TravelStatisVO();
            boolean travelRet = travelStatisRedisService.isVoExist(travel.getId());
            if (!travelRet){
                try {
                    BeanUtils.copyProperties(travelStatisVO, travel);
                    travelStatisVO.setTravelId(travel.getId());
                    travelStatisVO.setDestId(travel.getDest().getId());
                    travelStatisVO.setDestName(travel.getDest().getName());
                    travelStatisRedisService.setTravelStatisVO(travelStatisVO);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }

        // 国内外攻略推荐初始化
        for (StrategyDetail detail : strategyDetailList) {
            // 查询攻略推荐中是否存在vo
            String commendKey = RedisKeys.STRATEGY_STATIS_COMMEND_SORT.getPrefix();
            String value = RedisKeys.STRATRGY_STATIS_VO.join(detail.getId().toString());
            if (!strategyStatisRedisService.isVoByStrategyExist(commendKey, value)) {
                String voStr = strategyStatisRedisService.get(value);
                StrategyStatisVO commendVO = JSON.parseObject(voStr, StrategyStatisVO.class);
                int scores = commendVO.getFavornum() + commendVO.getThumbsupnum();
                strategyStatisRedisService.addScore(commendKey, value, scores);
            }
        }

        // 热门攻略推荐初始化
        for (StrategyDetail detail : strategyDetailList) {
            // 查询攻略推荐中是否存在vo
            String commendKey = RedisKeys.STRATEGY_STATIS_HOT_SORT.getPrefix();
            String value = RedisKeys.STRATRGY_STATIS_VO.join(detail.getId().toString());
            if (!strategyStatisRedisService.isVoByStrategyExist(commendKey, value)) {
                String voStr = strategyStatisRedisService.get(value);
                StrategyStatisVO commendVO = JSON.parseObject(voStr, StrategyStatisVO.class);
                int scores = commendVO.getViewnum() + commendVO.getReplynum();
                strategyStatisRedisService.addScore(commendKey, value, scores);
            }
        }

        // 游记排行初始化
        for (Travel travel : travelList) {
            String prefix = RedisKeys.TRAVEL_STATIS_SORT.getPrefix();
            String value = RedisKeys.TRAVEL_STATIS_VO.join(travel.getId().toString());
            // 只是查询zset中是否存在该key
            if (!strategyStatisRedisService.isVoByStrategyExist(prefix, value)){
                // 先从redis中获取vo对象
                String voStr = travelStatisRedisService.get(value);
                // 添加每一条游记存到redis的游记排行中
                travelStatisRedisService.addScore(prefix, value, travel.getViewnum());
            }
        }
        /*System.out.println("----------------门票初始化start-----------------");
        //地区 门票初始化
        List<Ticket>tickets=ticketService.list();
        System.out.println(tickets);
        List<Ticket> list=new ArrayList<>();
        tickets.forEach(ts->{
            tickets.forEach(t->{
                if (ts.getDest().getId()==t.getDest().getId()){
                    list.add(t);
                }
                String key = RedisKeys.TICKET_STATIS_DEST.join(ts.getDest().getId().toString());
                //判断是有该地区的key和该key中是否有该门票,没有创建key或添加对象
                ticketRedisService.hasKey(key,list);
            });
        });
        System.out.println("----------------门票初始化end-----------------");*/
    }
}
