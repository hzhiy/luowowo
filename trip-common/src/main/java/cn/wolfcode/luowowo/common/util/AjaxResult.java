package cn.wolfcode.luowowo.common.util;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by zhi on 2019/8/7.
 */
@Setter@Getter
public class AjaxResult {
    private boolean success = true;
    private String msg;
    private Object Data;
    private int code;

    public AjaxResult() {}

    public AjaxResult(boolean success, String msg) {
        this.success = success;
        this.msg = msg;
    }

    public static final AjaxResult SUCCESS = new AjaxResult();

    public AjaxResult addObject(Object o){
        this.Data = o;
        return this;
    }
}
