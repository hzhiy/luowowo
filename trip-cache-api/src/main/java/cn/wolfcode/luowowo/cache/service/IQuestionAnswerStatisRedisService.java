package cn.wolfcode.luowowo.cache.service;

import cn.wolfcode.luowowo.cache.vo.QuestionAnswerStatisVO;
import cn.wolfcode.luowowo.cache.vo.QuestionStatisVO;

import java.util.List;

/**
 * 问答统计数据redis缓存操作
 */
public interface IQuestionAnswerStatisRedisService {

    /**
     * 增加回答数
     * @param qid
     * @param i
     */
    void answernumIncrease(Long qid, int i,int type);




    /**
     * 查询单个vo
     * @param qid
     * @return
     */
    QuestionStatisVO getQuestionStatisVO(Long qid);


    /**
     * 回答的点赞
     * @param aid
     * @param uid
     * @return
     */
    boolean answerThumbsup(String aid, Long uid);

    /**
     * 用户回答分值增加
     * @param id
     */
    void addAnswerScore(Long id,int score);

    /**
     * 用户点赞分值增加
     * @param id
     * @param score
     */
    void addThumbsupScore(Long id,int score);

    /**
     * 用户回答数增加
     * @param id
     * @param i
     */
    void userAnswersIncrease(Long id, int i);

    void userThumbsupIncrease(Long id,int i);

    List<QuestionAnswerStatisVO> queryForList(int type,int rank);

    /**
     * 数据初始化
     * @param daySortKey
     * @param join
     * @param daynum
     */
    void setRankList(String daySortKey, String join, Integer daynum);
}
