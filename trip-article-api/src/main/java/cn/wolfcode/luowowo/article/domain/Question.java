package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.article.vo.QuestionAnswerVO;
import cn.wolfcode.luowowo.common.domain.BaseDomain;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class Question extends BaseDomain{

    public static final int QUESTION_NORMAL_STATE = 0;
    public static final int QUESTION_RELEASE_STATE = 1;
    public static final int QUESTION_REFUSE_STATE = 2;


    private String title;

    private String content;

    private UserInfo questioner;

    private Destination destination;

    private Date createTime;

    private Date releaseTime;

    private int sharenum=0;

    private int answernum=0;

    private int viewnum=0;

    private int thumbsupnum=0;

    private int state=QUESTION_NORMAL_STATE;

    private QuestionAnswerVO questionAnswer;

}