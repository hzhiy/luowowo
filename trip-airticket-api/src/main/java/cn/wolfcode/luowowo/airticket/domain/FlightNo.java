package cn.wolfcode.luowowo.airticket.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FlightNo extends BaseDomain {

    private String no;

    private Long airline_id;

   // private Long flighttype_id;


}