package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

import java.util.prefs.BackingStoreException;

@Setter
@Getter
public class UserAnswer extends BaseDomain {
    private Long answererId;

    private Integer daynum;

    private Integer weeknum;

    private Integer monthnum;


}