package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.query.DestinationQuery;
import cn.wolfcode.luowowo.article.query.DestinationThemeQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 目的地管理
 */
public interface IDestinationService {
    /**
     * 查询目的地
     * @param deep
     * @return
     */
    List<Destination> getDestByDeep(int deep);

    /**
     * 查询关联地区
     * @param rid
     * @return
     */
    List<Destination> getDestByRegionId(Long rid);

    /**
     * 列表
     * @param qo
     * @return
     */
    PageInfo query(DestinationQuery qo);

    /**
     * 修改状态
     * @param id
     * @param hot
     */
    void changeHotValue(Long id, Integer hot);

    /**
     * 修改简介
     * @param id
     * @param info
     */
    void setInfo(Long id, String info);

    /**
     * 删除
     * @param id
     */
    void delete(Long id);

    /**
     * 地区导航 吐司
     * @param parentId
     * @return
     */
    List<Destination> getToasts(Long parentId);

    /**
     * 查询国内目的地
     * @param regionId
     */
    List<Destination> listByParentId(Long regionId);

    /**
     * 查询单条
     * @param id
     * @return
     */
    Destination getDestById(Long id);

    /**
     * 获得国家
     * @param id
     * @return
     */
    Destination getCountry(Long id);

    /**
     * 省份
     * @param id
     * @return
     */
    Destination getProvince(Long id);

    /**
     * 查询全部目的地
     * @return
     */
    List<Destination> list();

    /**
     * 查询酒店价格前6的城市
     * @return
     */
    List<Destination> queryHotelPriceCityTop6();

    /**
     * 查询国内城市 热门30
     * @return
     */
    List<Destination> queryChinaCity();

    /**
     * 海外地区 热门20
     * @return
     */
    List<Destination> queryOverseas();

    /**
     * 返回主题目的地
     * @param qo
     * @return
     */
    PageInfo<Destination> queryForList(DestinationThemeQuery qo);


    /**
     * 根据地区名查子地区
     * @param parentName
     * @return
     */
    List<Destination> queryDestByParentName(String parentName);

    /**
     * 根据地区名查地区
     * @param name
     * @return
     */
    Destination getDestByName(String name);

    /**
     * 查询国家除了指定的id
     * @param id
     * @return
     */
    List<Destination> getCountryNotId(Long id);
}
