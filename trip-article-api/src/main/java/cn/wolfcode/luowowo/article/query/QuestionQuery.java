package cn.wolfcode.luowowo.article.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Administrator on 2019/8/21.
 */
@Getter
@Setter
public class QuestionQuery extends QueryObject {
    private String order = "createTime desc";
}
