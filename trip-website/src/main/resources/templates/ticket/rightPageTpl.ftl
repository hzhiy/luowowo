<#--<ul class="recommend_tab_l">
    <li class="active subject_js" data="1">主题乐园<i class="ticket_icon"></i></li>
    <li class=" subject_js" data="2">温泉<i class="ticket_icon"></i></li>
    <li class=" subject_js" data="3">动植物园<i class="ticket_icon"></i></li>
    <li class=" subject_js" data="4">都市观光<i class="ticket_icon"></i></li>
    <li class=" subject_js" data="5">水乡古镇<i class="ticket_icon"></i></li>
    <li class=" subject_js" data="6">登山徒步<i class="ticket_icon"></i></li>
    <li class=" subject_js" data="7">湖光山色<i class="ticket_icon"></i></li>
    <li class=" subject_js" data="8">田园度假<i class="ticket_icon"></i></li>
</ul>
===-->
<div class="mod_bd" id="ticket_list">
    <#if list??>
        <#list list as t>
    <ul class="ticket_list_b area_list">
        <li>
            <a  href="https://piao.ctrip.com/dest/t1408240.html#ctm_ref=va_tkt1_s1012_prd1_p0_l1_1_img"  title="${t.title}">
                <img alt="" class="list_pic img-loaded" src="${t.coverUrl}"style="height: 160px" width="235px">
                <h3 class="list_title" title="${t.title}">${t.title}</h3>
                <#--<p class="list_time" title="参观大型鲸鲨展馆，挑战超长飞行过山车。">
                    参观大型鲸鲨展馆，挑战超长飞行过山车。 ...
                </p>-->
                <p>
                    <em class="base_price">
                        <dfn>¥</dfn><strong>
                        ${(t.price)!'查看详情'}
                    </strong>
                    </em>起
                </p>
            </a>
        </li>
    </ul>
        </#list>
    </#if>
</div>