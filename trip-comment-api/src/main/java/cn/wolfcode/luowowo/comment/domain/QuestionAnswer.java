package cn.wolfcode.luowowo.comment.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * 问题的回答
 */
@Getter
@Setter
@Document("question_answer")
public class QuestionAnswer implements Serializable{

    public static final int STATE_HIDDEN = 1;   // 隐藏评论
    public static final int STATE_SHOW = 0;   // 显示评论
    @Id
    private String id;

    private Long questionId;//问题id
    private Long answererId;//回答者
    private String nickname;
    private String headImgUrl;
    private int level;
    private int thumbsupnum;
    private String content;
    private Date createTime;
    private int state = STATE_SHOW;
    private boolean isgold = true;

}
