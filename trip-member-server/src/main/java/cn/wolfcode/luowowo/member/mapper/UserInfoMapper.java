package cn.wolfcode.luowowo.member.mapper;

import cn.wolfcode.luowowo.member.domain.UserInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserInfoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserInfo record);

    UserInfo selectByPrimaryKey(Long id);

    List<UserInfo> selectAll();

    int updateByPrimaryKey(UserInfo record);

    int selectCountByPhone(String phone);

    UserInfo selectByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

    void updateByUserInfo(UserInfo userInfo);

    String selectPhoneById(Long uid);

    void updateByPhone(UserInfo userInfo);

    void updateByHeadImgUrl(UserInfo userInfo);

    void updatePassword(UserInfo userInfo);
}