package cn.wolfcode.luowowo.article.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by zhi on 2019/8/8.
 */
@Setter
@Getter
public class RegionQuery extends QueryObject {
    private String order;
}
