package cn.wolfcode.luowowo.search.service;

import cn.wolfcode.luowowo.search.template.TravelTemplate;

import java.util.List;

/**
 * Created by zhi on 2019/8/17.
 */
public interface ITravelTemplateService {

    /**
     * 新增或者修改
     * @param travelTemplate
     */
    void saveOrUpdate(TravelTemplate travelTemplate);

    /**
     * 通过地区名称查询游记
     * @param destName
     * @return
     */
    List<TravelTemplate> findByDestName(String destName);
}

