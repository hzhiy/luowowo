package cn.wolfcode.luowowo.airticket.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Airline extends BaseDomain{
    private String name;

    private String logo;

}