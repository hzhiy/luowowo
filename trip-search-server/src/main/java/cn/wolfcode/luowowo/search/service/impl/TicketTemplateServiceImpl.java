package cn.wolfcode.luowowo.search.service.impl;

import cn.wolfcode.luowowo.article.domain.Ticket;
import cn.wolfcode.luowowo.article.query.TicketObject;
import cn.wolfcode.luowowo.search.repository.ITicketSearchRepository;
import cn.wolfcode.luowowo.search.service.ITicketTemplateService;
import cn.wolfcode.luowowo.search.template.TicketTemplate;
import cn.wolfcode.luowowo.search.template.TravelTemplate;
import com.alibaba.dubbo.config.annotation.Service;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Service
public class TicketTemplateServiceImpl implements ITicketTemplateService {

    @Autowired
    private ITicketSearchRepository ticketSearchRepository;

    @Override
    public List<TicketTemplate> queryByDestandThemeId(TicketObject qo) {
        Iterable<TicketTemplate> iterable = ticketSearchRepository.search(QueryBuilders.boolQuery()
                .must(QueryBuilders.matchQuery("destId", qo.getAjaxDestId()))
                .must(QueryBuilders.termQuery("ticketThemeId", qo.getAjaxSubjectId())));
        List<TicketTemplate>list=new ArrayList<>();
        iterable.forEach(t->{
            list.add(t);
        });
        return list;
    }

    @Override
    public void saveOrUpdate(TicketTemplate template) {
        ticketSearchRepository.save(template);
    }

    @Override
    public List<TicketTemplate> queryByDestId(Long ajaxDestId) {
        return ticketSearchRepository.findByDestId(ajaxDestId);
    }
}
