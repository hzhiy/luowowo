package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.DestTheme;
import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.query.DestinationQuery;
import cn.wolfcode.luowowo.article.query.DestinationThemeQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DestinationMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Destination record);

    Destination selectByPrimaryKey(Long id);

    List<Destination> selectAll();

    int updateByPrimaryKey(Destination record);

    List<Destination> selectDestByDeep(int deep);

    List<Destination> selectDestByRegionId(Long[] refIds);

    List<Destination> queryForList(DestinationQuery qo);

    void updateHotById(@Param("id") Long id, @Param("hot") Integer hot);

    void updateInfoById(@Param("id") Long id, @Param("info") String info);

    List<Destination> selectByParentId(Long id);

    List<Long> selectThemeDest(DestinationThemeQuery qo);

    List<Destination> selectByThemeId(Long themeId);

    List<Destination> selectHotelPriceCityTop6();

    List<Destination> selectHotelByPriceTop6();

    List<Destination> selectChinaCity();

    List<Destination> selectOverseas();

    List<Destination> selectDestByParentName(String parentName);

    Destination selectByName(String name);

    List<Destination> selectCountryNotId(Long id);

}