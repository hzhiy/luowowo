package cn.wolfcode.luowowo.article.vo;

import cn.wolfcode.luowowo.article.domain.Scenic;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class ScenicAndCommentVO implements Serializable{
    private Scenic scenic;
    private ScenicCommentVO scenicCommentVO;
}
