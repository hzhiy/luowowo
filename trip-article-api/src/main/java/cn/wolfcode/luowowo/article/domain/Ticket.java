package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Ticket extends BaseDomain{
    private String title;//门票名称
    private String coverUrl;//封面
    private Destination dest;//关联地点
    private TicketTheme ticketTheme;//关联主题
}
