<html lang="en">
<head>
    <title>游记管理</title>
    <#include "../common/header.ftl">
    <style>
        #contentModal img{
            width: 100%;
        }
    </style>
    <script>
        $(function () {
            $('.changeStateBtn').click(function () {
                var state = $(this).data('state');
                var id = $(this).data('id');
                $.get("/travel/updateState",{id:id,state:state},function (data) {
                    if (data.success){
                        location.reload();
                    }
                })
            });
            $('.lookBtn').click(function () {
                var id = $(this).data('id');
                $.get("/travel/lookContent",{id:id},function (data) {
                    console.log(data);
                    if(data!=null){
                        $('.modal-body').html(data.content);
                        $('#contentModal img').css('width','80%');
                        $('#contentModal').modal('show');
                    }

                })
            });

        })
    </script>
</head>
<body>
<!--左侧菜单回显变量设置-->
<#assign currentMenu="travel">

<div class="container-fluid " style="margin-top: 20px">
    <#include "../common/top.ftl">
    <div class="row">
        <div class="col-sm-2">
            <#include "../common/menu.ftl">
        </div>
        <div class="col-sm-10">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-head-line">游记管理</h1>
                </div>
            </div>
            <#setting number_format="#">
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/travel/list" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <select id="auditState" class="form-control" autocomplete="off" name="state">
                        <option value="-1">全部</option>
                        <option value="1">待审核</option>
                        <option value="2">发布</option>
                        <option value="3">已拒绝</option>
                    </select>
                    <script>
                        $("#auditState").val(${(qo.state)!});
                    </script>
                    <button id="query" type="submit" class="btn btn-success"><i class="icon-search"></i> 查询</button>
                </div>
            </form>

            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>编号</th>
                    <th>标题</th>
                    <th>封面</th>
                    <th>地点</th>
                    <th>作者</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <#list pageInfo.list as t>
                <tr>
                    <td>${t_index + 1}</td>
                    <td>${t.title}</td>
                    <td><img src="${t.coverUrl}" style="width: 50px;height: 50px"></td>
                    <td>${t.dest.name}</td>
                    <td>${t.author.nickname}</td>
                    <td>${t.stateDisplay}</td>
                    <td>
                        <#if t.state == 1>
                            <a href="javascript:void(0);" data-state="2" data-id="${t.id}" class="btn btn-success changeStateBtn"><span class="glyphicon glyphicon-ok"></span>发布</a>
                            |
                        </#if>
                        <#if t.state != 3>
                        <a href="javascript:void(0);" data-state="3" data-id="${t.id}" class="btn btn-danger changeStateBtn"><span class="glyphicon glyphicon-remove">拒绝</a>
                        </#if>
                    </td>
                    <td>
                        <a href="javascript:void(0);" class="lookBtn" data-id="${t.id}">查看</a>
                    </td>

                </tr>
                </#list>
            </table>
            <#--分页-->
            <#include "../common/page.ftl"/>
        </div>
    </div>
</div>


<div class="modal fade" id="contentModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">游记内容</h4>
            </div>
            <div class="modal-body">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</body>
</html>