<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link href="./styles/base.css" rel="stylesheet" type="text/css">
    <link href="./styles/reply.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="./js/jquery.js"></script>
    <script type="text/javascript" src="./js/travelfilter.js"></script>
    <script type="text/javascript" src="./js/common.js"></script>
    <script type="text/javascript" src="/js/plugins/jqPaginator/jq-paginator.min.js"></script>
</head>
<script>
    $(function () {
        $('.scenicClick').click(function () {
            var themeId = $(this).data("themeid");
            var destId = $(this).data('destid');
            $('.themeList li').removeClass('on');
            $('.scenic-list').empty();
            $(this).parent().addClass('on');
            $.get("/scenic/themeList",{themeId:themeId,destId:destId},function (data) {
                $('.scenic-list').append(data);
            });
        })
    })
</script>

<body>
    <div class="lww_header">
        <div class="header_wrap">
            <div class="header_logo">
                <a href="javascript:;" class="lww_logo"></a>
            </div>
            <ul class="header_nav">
                <li name="index"><a href="/">首页</a></li>
                <li  name="destination"><a href="/destination">目的地</a></li>
                <li  name="strategy" ><a href="/strategy">旅游攻略</a></li>
                <li  name="travel" ><a href="/travel">旅游日记</a></li>
                <li  name="hotel" ><a href="/hotel">订酒店</a></li>
                <li  name=""><a href="/ticket">门票</a></li>
                <li name=""><a href="javascript:;">社区<i class="icon_caret_down"></i></a></li>
            </ul>
            <div class="header_search">
                <input type="text" />
                <a class="icon_search"></a>
            </div>

            <#if userInfo??>
        <div class="login_info">
            <div class="head_user">
                <a href="/mine/home">
                    <img src="${(userInfo.headImgUrl)!'/images/default.jpg'}" />
                    <i class="icon_caret_down"></i>
                </a>
            </div>
            <div class="header_msg">
                消息<i class="icon_caret_down"></i>
            </div>
            <div class="header_daka">
                <a href="javascript:;">打卡</a>
            </div>
        </div>
            <#else>
        <div class="login-out">
            <a class="weibo-login" href="#" title="微博登录" rel="nofollow"></a>
            <a class="qq-login" href="#" title="QQ登录" rel="nofollow"></a>
            <a class="weixin-login" href="#" title="微信登录" rel="nofollow"></a>
            <a id="_j_showlogin" title="登录骡窝窝" href="/login.html" rel="nofollow">登录</a>
            <span class="split">|</span>
            <a href="/regist.html" title="注册帐号" rel="nofollow">注册</a>
        </div>
            </#if>
        </div>
        <div class="shadow"></div>
    </div>
    <div class="container">
        

	<div class="row row-placeTop" data-cs-p="面包屑">
    <div class="wrapper">
        <link href="http://css.mafengwo.net/css/cv/css+mdd+place-crumb:css+mdd+place-navbar^Z1U^1559788120.css"
              rel="stylesheet" type="text/css">
        <script language="javascript" src="http://js.mafengwo.net/js/hotel/sign/index.js?1552035728"
                type="text/javascript" crossorigin="anonymous"></script>
        <link href="http://css.mafengwo.net/css/mdd/place-crumb.css?1530619858" rel="stylesheet" type="text/css">

        <div class="crumb">

            <div class="item"><a href="/mdd/" target="_blank">目的地</a><em>&gt;</em></div>

            <div class="item">
                <div class="drop">
                    <span class="hd"><a href="/travel-scenic-spot/mafengwo/14674.html"
                                        target="_blank">广东<i></i></a></span>
                    <div class="bd">
                        <i class="arrow"><b></b></i>
                        <div class="col">
                            <h3>热门地区</h3>
                            <ul class="clearfix">
                                <li><a href="/travel-scenic-spot/mafengwo/10088.html" target="_blank">广州</a></li>
                                <li><a href="/travel-scenic-spot/mafengwo/10198.html" target="_blank">深圳</a></li>
                                <li><a href="/travel-scenic-spot/mafengwo/10269.html" target="_blank">珠海</a></li>
                                <li><a href="/travel-scenic-spot/mafengwo/11475.html" target="_blank">惠州</a></li>
                                <li><a href="/travel-scenic-spot/mafengwo/13394.html" target="_blank">佛山</a></li>
                            </ul>
                        </div>
                        <div class="more"><a href="/travel-scenic-spot/mafengwo/14674.html"
                                             target="_blank">&gt;&gt;更多地区</a></div>
                    </div>
                </div>
                <em>&gt;</em>
            </div>
            <div class="item">
                <div class="drop">
                    <span class="hd"><a href="/travel-scenic-spot/mafengwo/10088.html">广州<i></i></a></span>
                    <div class="bd">
                        <i class="arrow"><b></b></i>
                        <div class="col">
                            <!--h3>热门国家</h3-->
                            <ul class="clearfix">
                                <li><a href="/mdd/route/10088.html" target="_blank">广州行程线路</a></li>
                                <li><a href="/scenic/detail?scenicId=" target="_blank">广州景点</a></li>
                                <li><a href="/hotel/10088/?sFrom=mdd" target="_blank">广州酒店</a></li>
                                <li><a href="/cy/10088/gonglve.html" target="_blank">广州餐饮</a></li>
                                <li><a href="/gw/10088/gonglve.html" target="_blank">广州购物</a></li>
                                <li><a href="/yl/10088/gonglve.html" target="_blank">广州娱乐</a></li>
                                <li><a href="/localdeals/0-0-M10088-0-0-0-0-0.html" target="_blank">广州当地玩乐</a></li>
                                <li><a href="/gonglve/ziyouxing/mdd_10088/" target="_blank">广州自由行攻略</a></li>
                            </ul>
                        </div>
                        <!--div class="more"><a href="#">&gt;&gt;更多国家</a></div-->
                    </div>
                </div>
                <em>&gt;</em>
            </div>
            <div class="item cur"><strong>广州景点</strong></div>
        </div>
        <script language="JavaScript" type="text/javascript">
            $(function () {
                //面包屑
                $('.drop').mouseenter(function (ev) {
                    var target = $(ev.currentTarget);
                    clearTimeout(target.data('hideTimer'));
                    target.addClass('open');
                    target.children('bd').fadeIn(200);
                });
                $('.drop').mouseleave(function (ev) {
                    var target = $(ev.currentTarget);
                    target.data("hideTimer", setTimeout(function () {
                        target.removeClass('open');
                        target.children('bd').fadeOut(200);
                    }, 200));
                });
            });
        </script>

        <div class="place-navbar" id="_j_mdd_place_nav_bar_warper" style="border-top: 0;" data-cs-t="目的地导航">
            <div class="navbar-con">
                <ul class="navbar clearfix navbar-first-level-warper">
                    <li class="navbar-overview">
                        <a class="navbar-btn" href="/" data-cs-p="首页">
                            <i class="navbar-icon"></i><span>首页</span>

                        </a>
                    </li>
                    <li class="navbar-line">
                        <a class="navbar-btn" href="/destination/" data-cs-p="行程线路">
                            <i class="navbar-icon"></i><span>行程线路</span>

                        </a>
                    </li>
                    <li class="navbar-scenic">
                        <a class="navbar-btn" href="/scenic?destId=3261" data-cs-p="景点">
                            <i class="navbar-icon"></i><span>景点</span>

                        </a>
                    </li>
                    <li class="navbar-hotels">
                        <a class="navbar-btn" href="/hotel" data-cs-p="酒店">
                            <i class="navbar-icon"></i><span>酒店</span>

                        </a>
                    </li>
                    <li class="navbar-flight">
                        <a class="navbar-btn" href="/flight/" target="_blank" data-cs-p="机票">
                            <i class="navbar-icon"></i><span>机票</span>

                        </a>
                    </li>
                    
                </ul>
            </div>
        </div>
        <div id="fill_area" style="height: 75px; display: none;"></div>

    </div>
</div>


<div class="row row-summary row-bg">
    <div class="wrapper">
        <h2 class="title">景点概况</h2>
        <div>
            <p style="">
                ${(mainSingle.info)!}</span>
            </p>
        </div>
    </div>
	
</div>
<br>
<br>

<div class="row row-top5" data-cs-p="必游景点">
    <div class="wrapper">
        <h2 class="title">必游景点TOP5</h2>
        <#list top5Scenic as sc>
        <div class="item clearfix">
            <div class="info">
                <div class="middle">
                    <h3>
                        <span class="num">${sc_index + 1}</span>
                        <a href="/scenic/detail?scenicId=${sc.id}&destId=${(sc.dest_id)!}#toptop" target="_blank" title="${(sc.name)!}">${sc.name}
                        </a><a href="/scenic/detail?scenicId=${sc.id}&destId=${(sc.dest_id)!}#commentlist" target="_blank" title="${sc.name}">
                        <span class="rev-total"><em>${sc.commentNum}</em> 条点评</span>
                    </a>
                    </h3>
                    <p>${sc.info}</p>
                    <#if sc.includeScenicArr??>
                        <div class="links">这里还包含景点
                            <#list sc.includeScenicArr as isa>
                                <a href="/scenic/detail?scenicId=${isa.id}" target="_blank">${isa.name}</a>
                            </#list>
                        </div>
                    </#if>
                </div>
            </div>
            <div class="pic">
                <a href="/scenic/detail?scenicId=${sc.id}" target="_blank" title="${sc.name}">
                    <div class="large">
                        <img src="${sc.imgArr[0]}"
                             width="380" height="270">
                    </div>
                    <div>
                        <img src="${(sc.imgArr[1])!}"
                             width="185" height="130">
                    </div>
                    <div>
                        <img src="${(sc.imgArr[2])!}"
                             width="185" height="130">
                    </div>
                </a>
            </div>
        </div>
        </#list>

        <div class="item clearfix">
            <div class="info">
                <div class="middle">
                    <h3>
                        <span class="num">2</span>
                        <a href="/poi/443.html" target="_blank" title="沙面岛">沙面岛
                        </a><a href="/poi/443.html" target="_blank" title="沙面岛">
                        <span class="rev-total"><em>4661</em> 条点评</span>
                    </a>
                    </h3>
                    <p>广州租界最后的“世外桃源”</p>
                    <div class="links">这里还包含景点：
                        <a href="/poi/33590944.html" target="_blank">广东外事博物馆</a>
                        <a href="/poi/5168638.html" target="_blank">广东省基督教沙面堂</a>
                        <a href="/poi/25063.html" target="_blank">汇丰银行旧址</a>
                        <a href="/poi/25062.html" target="_blank">红楼</a>
                        <a href="/poi/25065.html" target="_blank">露德天主教圣母堂</a>
                        <a href="/poi/33590300.html" target="_blank">西固炮台抗英遗址</a>
                        <a href="/poi/34860544.html" target="_blank">沙面公园休闲园</a>
                        <a href="/poi/34860552.html" target="_blank">沙面公园舞台</a>
                        <a href="/poi/34860556.html" target="_blank">英国领事馆东副楼</a>
                        <a href="/poi/34860564.html" target="_blank">英国领事馆西副楼</a>
                        <a href="/poi/34860572.html" target="_blank">六柱亭</a>
                        <a href="/poi/34860576.html" target="_blank">乐韵悠悠</a>
                        <a href="/poi/34860584.html" target="_blank">雕塑园</a>
                        <a href="/poi/34860592.html" target="_blank">江边广场</a>
                        <a href="/poi/33593244.html" target="_blank">友好园</a>
                        <a href="/poi/34860600.html" target="_blank">广州水上巴士</a>
                        <a href="/poi/34860608.html" target="_blank">文化广场</a>
                        <a href="/poi/34860616.html" target="_blank">志愿驿站</a>
                        <a href="/poi/34860632.html" target="_blank">香港牛奶冰厂</a>
                        <a href="/poi/34860636.html" target="_blank">白鹅水景区</a>
                        <a href="/poi/34860640.html" target="_blank">清代城防古炮</a>
                    </div>
                </div>
            </div>
            <div class="pic">
                <a href="/poi/443.html" target="_blank" title="沙面岛">
                    <div class="large">
                        <img src="http://p2-q.mafengwo.net/s10/M00/A0/51/wKgBZ1m1GOaARk_EADxTppCJcxM97.jpeg?imageMogr2%2Fthumbnail%2F%21380x270r%2Fgravity%2FCenter%2Fcrop%2F%21380x270%2Fquality%2F100"
                             width="380" height="270">
                    </div>
                    <div>
                        <img src="http://p2-q.mafengwo.net/s10/M00/A0/32/wKgBZ1m1GK-Af6KwABEJxfvYVhU85.jpeg?imageMogr2%2Fthumbnail%2F%21185x130r%2Fgravity%2FCenter%2Fcrop%2F%21185x130%2Fquality%2F100"
                             width="185" height="130">
                    </div>
                    <div>
                        <img src="http://b1-q.mafengwo.net/s9/M00/0E/73/wKgBs1c6h8-AJnk8ABpthVbwhPo84.jpeg?imageMogr2%2Fthumbnail%2F%21185x130r%2Fgravity%2FCenter%2Fcrop%2F%21185x130%2Fquality%2F100"
                             width="185" height="130">
                    </div>
                </a>
            </div>
        </div>
        <div class="item clearfix">
            <div class="info">
                <div class="middle">
                    <h3>
                        <span class="num">3</span>
                        <a href="/poi/444.html" target="_blank" title="陈家祠">陈家祠
                        </a><a href="/poi/444.html" target="_blank" title="陈家祠">
                        <span class="rev-total"><em>2291</em> 条点评</span>
                    </a>
                    </h3>
                    <p>在此能感受建筑的精美，家族的兴衰，感受历史的传承</p>
                    <div class="links">这里还包含景点：
                        <a href="/poi/34888628.html" target="_blank">新景城广场</a>
                        <a href="/poi/34888632.html" target="_blank">鼻咽堂</a>
                        <a href="/poi/34888636.html" target="_blank">新光城市广场2期</a>
                        <a href="/poi/25306.html" target="_blank">光孝寺</a>
                        <a href="/poi/34888640.html" target="_blank">陈氏书院广场</a>
                    </div>
                </div>
            </div>
            <div class="pic">
                <a href="/poi/444.html" target="_blank" title="陈家祠">
                    <div class="large">
                        <img src="http://n2-q.mafengwo.net/s6/M00/67/52/wKgB4lMpk-KAdZgcAAUpgfKn-Y059.jpeg?imageMogr2%2Fthumbnail%2F%21380x270r%2Fgravity%2FCenter%2Fcrop%2F%21380x270%2Fquality%2F100"
                             width="380" height="270">
                    </div>
                    <div>
                        <img src="http://n4-q.mafengwo.net/s5/M00/9C/D7/wKgB3FEwT1GARj8SAAizrn9TMYY57.jpeg?imageMogr2%2Fthumbnail%2F%21185x130r%2Fgravity%2FCenter%2Fcrop%2F%21185x130%2Fquality%2F100"
                             width="185" height="130">
                    </div>
                    <div>
                        <img src="http://b1-q.mafengwo.net/s1/M00/1B/AE/wKgBm074JrrqOkk_AALJno4kk8Q18.jpeg?imageMogr2%2Fthumbnail%2F%21185x130r%2Fgravity%2FCenter%2Fcrop%2F%21185x130%2Fquality%2F100"
                             width="185" height="130">
                    </div>
                </a>
            </div>
        </div>
        <div class="item clearfix">
            <div class="info">
                <div class="middle">
                    <h3>
                        <span class="num">4</span>
                        <a href="/poi/466.html" target="_blank" title="白云山">白云山
                        </a><a href="/poi/466.html" target="_blank" title="白云山">
                        <span class="rev-total"><em>2224</em> 条点评</span>
                    </a>
                    </h3>
                    <p>南粤名山之一，广州有名的踏青胜地</p>
                    <div class="links">这里还包含景点：
                        <a href="/poi/5176874.html" target="_blank">麓湖公园</a>
                        <a href="/poi/25082.html" target="_blank">摩星岭</a>
                        <a href="/poi/5145291.html" target="_blank">云溪生态公园</a>
                        <a href="/poi/25137.html" target="_blank">桃花涧</a>
                        <a href="/poi/25081.html" target="_blank">山顶公园</a>
                        <a href="/poi/77855.html" target="_blank">云台花园</a>
                        <a href="/poi/8291485.html" target="_blank">能仁寺</a>
                        <a href="/poi/5185890.html" target="_blank">天南第一峰</a>
                        <a href="/poi/33590612.html" target="_blank">广州碑林</a>
                        <a href="/poi/33590616.html" target="_blank">鸣春谷</a>
                        <a href="/poi/29749454.html" target="_blank">明珠楼</a>
                        <a href="/poi/5185901.html" target="_blank">九龙泉</a>
                        <a href="/poi/34859392.html" target="_blank">祈福亭</a>
                        <a href="/poi/34859400.html" target="_blank">连理亭</a>
                        <a href="/poi/34859408.html" target="_blank">三人舞姬雕像</a>
                        <a href="/poi/34859416.html" target="_blank">跃云</a>
                        <a href="/poi/34859424.html" target="_blank">六祖殿</a>
                        <a href="/poi/34859432.html" target="_blank">浸绿浮香</a>
                        <a href="/poi/34859440.html" target="_blank">赏蝶轩</a>
                        <a href="/poi/34859444.html" target="_blank">叠水园</a>
                        <a href="/poi/34859452.html" target="_blank">生态木栈道</a>
                    </div>
                </div>
            </div>
            <div class="pic">
                <a href="/poi/466.html" target="_blank" title="白云山">
                    <div class="large">
                        <img src="http://n3-q.mafengwo.net/s5/M00/A7/DF/wKgB3FDGliOAbIEEACtJlr0zHzE05.jpeg?imageMogr2%2Fthumbnail%2F%21380x270r%2Fgravity%2FCenter%2Fcrop%2F%21380x270%2Fquality%2F100"
                             width="380" height="270">
                    </div>
                    <div>
                        <img src="http://b4-q.mafengwo.net/s1/M00/46/89/wKgIC1o6bHaAGyQfABDnPdkxeVI67.jpeg?imageMogr2%2Fthumbnail%2F%21185x130r%2Fgravity%2FCenter%2Fcrop%2F%21185x130%2Fquality%2F100"
                             width="185" height="130">
                    </div>
                    <div>
                        <img src="http://b2-q.mafengwo.net/s8/M00/F1/11/wKgBpVXIcguAPHHOAAM75wf1hl079.jpeg?imageMogr2%2Fthumbnail%2F%21185x130r%2Fgravity%2FCenter%2Fcrop%2F%21185x130%2Fquality%2F100"
                             width="185" height="130">
                    </div>
                </a>
            </div>
        </div>
        <div class="item clearfix">
            <div class="info">
                <div class="middle">
                    <h3>
                        <span class="num">5</span>
                        <a href="/poi/441.html" target="_blank" title="越秀公园">越秀公园
                        </a><a href="/poi/441.html" target="_blank" title="越秀公园">
                        <span class="rev-total"><em>1924</em> 条点评</span>
                    </a>
                    </h3>
                    <p>羊城八景之一，内有“五羊石雕”</p>
                    <div class="links">这里还包含景点：
                        <a href="/poi/28333.html" target="_blank">五羊石像</a>
                        <a href="/poi/26425.html" target="_blank">孙中山纪念碑</a>
                        <a href="/poi/33591540.html" target="_blank">四方炮台遗址</a>
                        <a href="/poi/28335.html" target="_blank">广州博物馆</a>
                        <a href="/poi/442.html" target="_blank">镇海楼</a>
                        <a href="/poi/33589816.html" target="_blank">明代古城墙</a>
                        <a href="/poi/5433700.html" target="_blank">广州美术馆</a>
                        <a href="/poi/34860336.html" target="_blank">冠斑犀鸟</a>
                        <a href="/poi/34860352.html" target="_blank">明绍武君臣冢</a>
                        <a href="/poi/34860368.html" target="_blank">伍廷芳墓</a>
                        <a href="/poi/34860376.html" target="_blank">观音山战斗遗址</a>
                        <a href="/poi/34860384.html" target="_blank">西汉南越国人字顶分室大墓</a>
                        <a href="/poi/34860404.html" target="_blank">夕阳红广场</a>
                        <a href="/poi/34860412.html" target="_blank">海东京畿园</a>
                        <a href="/poi/34860424.html" target="_blank">毓秀灵瀑</a>
                        <a href="/poi/34860444.html" target="_blank">成语寓言园</a>
                        <a href="/poi/34860448.html" target="_blank">越秀公园-光复纪念亭</a>
                        <a href="/poi/34860456.html" target="_blank">南秀湖</a>
                        <a href="/poi/33591648.html" target="_blank">海员亭</a>
                        <a href="/poi/34860468.html" target="_blank">小鹿广场</a>
                        <a href="/poi/34860472.html" target="_blank">古之楚庭石牌坊</a>
                    </div>
                </div>
            </div>
            <div class="pic">
                <a href="/poi/441.html" target="_blank" title="越秀公园">
                    <div class="large">
                        <img src="http://p3-q.mafengwo.net/s7/M00/C8/5C/wKgB6lR5o4KAc7l-AEk0iSpwfGg16.jpeg?imageMogr2%2Fthumbnail%2F%21380x270r%2Fgravity%2FCenter%2Fcrop%2F%21380x270%2Fquality%2F100"
                             width="380" height="270">
                    </div>
                    <div>
                        <img src="http://p3-q.mafengwo.net/s7/M00/CD/BE/wKgB6lR5qz6ANCK9AEo1lpM9Vbo54.jpeg?imageMogr2%2Fthumbnail%2F%21185x130r%2Fgravity%2FCenter%2Fcrop%2F%21185x130%2Fquality%2F100"
                             width="185" height="130">
                    </div>
                    <div>
                        <img src="http://p2-q.mafengwo.net/s7/M00/C8/B9/wKgB6lR5pBWAff1ZAEtQOIsxNF847.jpeg?imageMogr2%2Fthumbnail%2F%21185x130r%2Fgravity%2FCenter%2Fcrop%2F%21185x130%2Fquality%2F100"
                             width="185" height="130">
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<Br>
<Br>



<div class="row row-hotScenic row-bg" data-cs-p="热门景点">
    <div class="wrapper">
        <h2 class="title">热门景点</h2>
        <div class="bd">
            <div class="grid grid-two">
                <div class="figure">
                    <a href="/poi/5178221.html" target="_blank" title="${hotScenic[0].name}">
                        <img src="${hotScenic[0].imgArr[0]}"
                             width="485" height="320">
                        <h3 class="title">${hotScenic[0].name}</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>${hotScenic[0].name}</h3>
                                    <p>${hotScenic[0].info}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
            <div class="grid">
                <div class="figure">
                    <a href="/poi/6047483.html" target="_blank" title="${hotScenic[1].name}">
                        <img src="${hotScenic[1].imgArr[0]}"
                             width="242" height="155">
                        <h3 class="title">${hotScenic[1].name}</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>${hotScenic[1].name}</h3>
                                    <p>${hotScenic[1].info}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="figure">
                    <a href="/poi/1049.html" target="_blank" title="${hotScenic[2].name}">
                        <img src="${hotScenic[2].imgArr[0]}"
                             width="242" height="155">
                        <h3 class="title">${hotScenic[2].name}</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>${hotScenic[2].name}</h3>
                                    <p>${hotScenic[2].info}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
            <div class="grid">
                <div class="figure">
                    <a href="/poi/26336.html" target="_blank" title="${hotScenic[3].name}">
                        <img src="${hotScenic[3].imgArr[0]}"
                             width="242" height="155">
                        <h3 class="title">${hotScenic[3].name}</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>${hotScenic[3].name}</h3>
                                    <p>${hotScenic[3].info}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="figure">
                    <a href="/poi/928.html" target="_blank" title="${hotScenic[4].name}">
                        <img src="${hotScenic[4].imgArr[0]}"
                             width="242" height="155">
                        <h3 class="title">${hotScenic[4].name}</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>${hotScenic[4].name}</h3>
                                    <p>${hotScenic[4].info}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
            <div class="grid">
                <div class="figure">
                    <a href="/poi/5428004.html" target="_blank" title="${hotScenic[5].name}">
                        <img src="${hotScenic[5].imgArr[0]}"
                             width="242" height="155">
                        <h3 class="title">${hotScenic[5].name}</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>${hotScenic[5].name}</h3>
                                    <p>${hotScenic[5].info}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="figure">
                    <a href="/poi/5423346.html" target="_blank" title="${hotScenic[6].name}">
                        <img src="${hotScenic[6].imgArr[0]}"
                             width="242" height="155">
                        <h3 class="title">${hotScenic[6].name}</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>${hotScenic[6].name}</h3>
                                    <p>${hotScenic[6].info}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
            <div class="grid grid-two">
                <div class="figure">
                    <a href="/poi/28270.html" target="_blank" title="${hotScenic[7].name}">
                        <img src="${hotScenic[7].imgArr[0]}"
                             width="485" height="320">
                        <h3 class="title">${hotScenic[7].name}</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>${hotScenic[7].name}</h3>
                                    <p>${hotScenic[7].info}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
            <div class="grid">
                <div class="figure">
                    <a href="#" target="_blank" title="${hotScenic[8].name}">
                        <img src="${hotScenic[8].imgArr[0]}"
                             width="242" height="155">
                        <h3 class="title">${hotScenic[8].name}</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>${hotScenic[8].name}</h3>
                                    <p>${hotScenic[8].info}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="figure">
                    <a href="#" target="_blank" title="${hotScenic[9].name}">
                        <img src="${hotScenic[9].imgArr[0]}"
                             width="242" height="155">
                        <h3 class="title">${hotScenic[9].name}</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>${hotScenic[9].name}</h3>
                                    <p>${hotScenic[9].info}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>
</div>



<div class="row row-allScenic" data-cs-p="全部景点">
    <div class="wrapper">
        <h2 class="title">
            ${mainSingle.name}全部景点
            <a class="btn-add" href="#" target="_blank" title="推荐新的景点"><i>+</i>推荐新的景点</a>
        </h2>
        <ul class="nav themeList clearfix">
            <li class="on"><a class="scenicClick" data-themeId="-1" data-destId="${(mainSingle.dest_id)!}" title="全部景点">全部景点</a></li>
            <#list scenicThemes as themes>
            <li><a class="scenicClick" data-themeId="${themes.id}" data-destId="${(mainSingle.dest_id)!}" title="${themes.themeName}">${themes.themeName}</a></li>
            </#list>
        </ul>
        <div class="bd">
            <ul class="scenic-list clearfix" style="height: 378px">
                <#list pageInfo.list as sce>
                <li>
                    <a href="#" target="_blank" title="${sce.name}">
                        <div class="img"><img
                                src="${(sce.imgArr[0])!}"
                                width="192" height="130"></div>
                        <h3>${sce.name}</h3>
                    </a>
                </li>
                </#list>
            </ul>
        </div>
    </div>
    <#--<#include "../common/page.ftl">-->
</div>
        <#include "../common/footer.ftl">
</body>

</html>