package cn.wolfcode.luowowo.mgrsite.controller;

import cn.wolfcode.luowowo.article.query.DestinationQuery;
import cn.wolfcode.luowowo.article.service.IDestinationService;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by zhi on 2019/8/10.
 */
@Controller
@RequestMapping("destination")
public class DestinationController {
    @Reference
    private IDestinationService destinationService;

    @RequestMapping("list")
    public String list(Model model, @ModelAttribute("qo") DestinationQuery qo){
        model.addAttribute("toasts",destinationService.getToasts(qo.getParentId()));
        model.addAttribute("pageInfo", destinationService.query(qo));
        return "destination/list";
    }


    @RequestMapping("changeHotValue")
    @ResponseBody
    private Object changeHotValue(Long id, Integer hot){
        destinationService.changeHotValue(id, hot);
        return AjaxResult.SUCCESS;
    }



    @RequestMapping("setInfo")
    @ResponseBody
    private Object setInfo(Long id, String info){
        destinationService.setInfo(id, info);
        return AjaxResult.SUCCESS;
    }


    @RequestMapping("delete")
    @ResponseBody
    private Object delete(Long id){
        destinationService.delete(id);
        return AjaxResult.SUCCESS;
    }

}
