package cn.wolfcode.luowowo.cache.service;

import cn.wolfcode.luowowo.article.domain.DestinationTheme;

import java.util.List;

public interface IDestinationThemeRedisService {
    //拿到月份数据,并存到redis中
    List<DestinationTheme> getMonths();

    /**
     * 通过月份的Id拿到地区
     * @param month
     * @return
     */
    DestinationTheme getMothDest(Long month);

    /**
     * 通过主题的父Id拿到子的主题
     * @param id
     * @return
     */
    List<DestinationTheme> queryByThemeParendId(Long id);

    /**
     * 通过父id为空拿到大主题
     * @return
     */
    List<DestinationTheme> queryByParendTheme();

    /**
     *
     * @return 节日
     */
    List<DestinationTheme> getFestivals();
    /**
     *
     * @return 全年适宜
     */
    List<DestinationTheme> getRoundFit();
    /**
     *
     * @return 季节
     */
    List<DestinationTheme> getSeasons();
    /**
     *
     * @return 出行方式
     */
    List<DestinationTheme> getTripModes();

    /**
     *
     * @return 出行天数
     */
    List<DestinationTheme> getDays();
}
