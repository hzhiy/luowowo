package cn.wolfcode.luowowo.article.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScenicQuery extends QueryObject {
    private int state = -1;
    private Long scenic = -1L; // 景点id
    private Long destId = -1L; // 地区id
    private Long themeId= -1L; // 默认查询所有景点主题
    private String order = "commentNum Desc";
}
