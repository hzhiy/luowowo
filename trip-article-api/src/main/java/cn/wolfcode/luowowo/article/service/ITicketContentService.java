package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.TicketContent;

/**
 * 门票内容
 */
public interface ITicketContentService {


    TicketContent getByDetailId(Long id);
}
