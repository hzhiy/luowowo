package cn.wolfcode.luowowo.search.repository;

import cn.wolfcode.luowowo.search.template.DestinationTemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by zhi on 2019/8/17.
 */
@Repository
public interface IDestinationSearchRepository extends ElasticsearchRepository<DestinationTemplate, Long>{

    /**
     * 通过名称查询地区详细
     * @param name
     * @return
     */
    DestinationTemplate findByName(String name);
}
