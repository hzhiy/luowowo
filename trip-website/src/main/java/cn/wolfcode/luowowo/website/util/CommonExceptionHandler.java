package cn.wolfcode.luowowo.website.util;

import cn.wolfcode.luowowo.common.exception.LogicException;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import com.alibaba.fastjson.JSON;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 控制增强, 统一异常处理
 */
@ControllerAdvice
public class CommonExceptionHandler {

    // 在方法执行完之后
    @ExceptionHandler(LogicException.class)
    public void displayExp(Exception e, HttpServletResponse resp) throws IOException {
        e.printStackTrace();
        resp.setContentType("text/json;charset=utf-8");
        resp.getWriter().write(JSON.toJSONString(new AjaxResult(false, e.getMessage())));
    }
    // 不是自己业务代码出错时
    @ExceptionHandler(RuntimeException.class)
    public void runTimeExp(Exception e, HttpServletResponse resp) throws IOException {
        e.printStackTrace();
        resp.setContentType("text/json;charset=utf-8");
        resp.getWriter().write(JSON.toJSONString(new AjaxResult(false, "系统出现未知异常，请联系客服。")));
    }
}
