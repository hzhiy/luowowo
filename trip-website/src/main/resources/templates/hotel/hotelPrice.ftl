<#list hotelPrice as hp>
<div class="hotel-btns">

    <a class="btn-booking _j_booking_btn" href="javascript:" rel="nofollow" style="" >
        <div class="ota">
            <div class="name">
                <strong>${hp.info}</strong>
                <i class="icon-alipay" style=""></i>
                <i class="icon-wxpay" style=""></i>
            </div>
            <p class="tips" style="display:none;"></p>
        </div>
        <div class="price _j_booking_price">
            <strong>￥</strong><strong>${hp.price}</strong><strong style="font-size: 12px;color: #666;padding-left: 2px;vertical-align: 1px;">起</strong>
            <i class="arrow"></i>
        </div>
    </a>
</div>
</#list>