package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.Scenic;
import cn.wolfcode.luowowo.article.query.ScenicQuery;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ScenicMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Scenic record);

    Scenic selectByPrimaryKey(Long id);

    List<Scenic> selectAll();

    int updateByPrimaryKey(Scenic record);

    List<Scenic> selectTop5byDestId(Long destId);

    Scenic selectByDestId(Long destId);

    List<Scenic> selectHotScenic();

    List<Scenic> selectThemeScenics(ScenicQuery qo);

    List<Scenic> selectByParentId(Long parent_id);
}