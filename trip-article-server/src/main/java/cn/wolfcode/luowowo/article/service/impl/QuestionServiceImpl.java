package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.Question;
import cn.wolfcode.luowowo.article.mapper.QuestionMapper;
import cn.wolfcode.luowowo.article.query.QuestionQuery;
import cn.wolfcode.luowowo.article.service.IQuestionSevice;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2019/8/21.
 */
@Service
public class QuestionServiceImpl implements IQuestionSevice {

    @Autowired
    private QuestionMapper questionMapper;



    @Override
    public void saveOrUpdate(Question question) {
        question.setCreateTime(new Date());
        questionMapper.insert(question);
    }

    @Override
    public PageInfo<Question> queryForList(QuestionQuery qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize(),qo.getOrder());
        List<Question> list = questionMapper.selectForList(qo);
        return new PageInfo<>(list);
    }

    @Override
    public Question get(Long id) {
        return questionMapper.selectByPrimaryKey(id);
    }
}
