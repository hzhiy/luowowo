package cn.wolfcode.luowowo.comment.repository;

import cn.wolfcode.luowowo.comment.domain.ScenicComment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * 景点点评接口
 */
public interface IScenicCommentRepository extends MongoRepository<ScenicComment, String> {
    Page<ScenicComment> findByScenicId(Long scenicId, Pageable pageable);

    List<ScenicComment> findByUserId(Long uid);

    List<ScenicComment> findByScenicId(Long scenicId);

    long countByScenicId(Long scenicId);

    long countByHasImg(int hasImg);

    long countByStarNumBetween(int low, int high);

    long countByStarNum(int starNum);
}
