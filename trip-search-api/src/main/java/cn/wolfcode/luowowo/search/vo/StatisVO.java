package cn.wolfcode.luowowo.search.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by zhi on 2019/8/17.
 */
@Setter
@Getter
@ToString
public class StatisVO {
    public static final Integer ABROADS_STRATEGY = 0;     // 国外攻略
    public static final Integer CHINAS_STRATEGY = 1;      // 国内攻略
    public static final Integer THEMES_STRATEGY = 2;      // 主题攻略

    private Long id;
    private String name;
    private Long count;
}
