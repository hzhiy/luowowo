package cn.wolfcode.luowowo.comment.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by zhi on 2019/8/13.
 */
@Setter
@Getter
public class StrategyCommentQuery extends QueryObject {
    private int state = -1;
    private Long detailId = -1L;
    private String order = "createTime";

}
