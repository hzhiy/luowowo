package cn.wolfcode.luowowo.cache.service.impl;

import cn.wolfcode.luowowo.article.domain.Strategy;
import cn.wolfcode.luowowo.article.domain.StrategyDetail;
import cn.wolfcode.luowowo.article.service.IStrategyDetailService;
import cn.wolfcode.luowowo.cache.emum.RedisKeys;
import cn.wolfcode.luowowo.cache.service.IStrategyStatisRedisService;
import cn.wolfcode.luowowo.cache.vo.StrategyStatisVO;
import cn.wolfcode.luowowo.common.util.DateUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by zhi on 2019/8/7.
 */
@Service
public class StrategyStatisRedisServiceImpl implements IStrategyStatisRedisService {
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Reference
    private IStrategyDetailService strategyDetailService;

    private static final int STRATEGY_VIEWNUM = 1;      //点击数
    private static final int STRATEGY_REPLYNUM = 2;     //攻略评论数
    private static final int STRATEGY_FAVORNUM = 3;     //收藏数
    private static final int STRATEGY_SHARENUM = 4;     //分享数
    private static final int STRATEGY_THUMBSUPNUM = 5;  //点赞个数

    // 统一加+1操作
    public void numIncrease(Long sid, int type, int i){

        // 获得vo对象
        StrategyStatisVO vo = this.getStrategyStatisVO(sid);
        switch (type){
            case STRATEGY_VIEWNUM:
                vo.setViewnum(vo.getViewnum() + i);
                break;
            case STRATEGY_REPLYNUM:
                vo.setReplynum(vo.getReplynum() + i);
                break;
            case STRATEGY_FAVORNUM:
                vo.setFavornum(vo.getFavornum() + i);
                break;
            case STRATEGY_SHARENUM:
                vo.setSharenum(vo.getSharenum() + i);
                break;
            case STRATEGY_THUMBSUPNUM:
                vo.setThumbsupnum(vo.getThumbsupnum() + i);
                break;
        }
        this.setStrategyStatisVO(vo);
    }


    public StrategyStatisVO getStrategyStatisVO(Long id) {
        String key = RedisKeys.STRATRGY_STATIS_VO.join(id.toString());
        StrategyStatisVO vo = null;
        // 判断攻略对象存不存在
        if (redisTemplate.hasKey(key)) {
            // 存在就返回
            String voStr = redisTemplate.opsForValue().get(key);
            vo =  JSON.parseObject(voStr, StrategyStatisVO.class);
        }else {
            // 不存在则初始化
            StrategyDetail detail = strategyDetailService.get(id);
            vo = new StrategyStatisVO();
            try {
                BeanUtils.copyProperties(vo, detail);
                vo.setStrategyId(detail.getId());
                vo.setDestId(detail.getDest().getId());
                vo.setDestName(detail.getDest().getName());
                this.setStrategyStatisVO(vo);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return vo;
    }

    public void setStrategyStatisVO(StrategyStatisVO vo){
        redisTemplate.opsForValue().set(RedisKeys.STRATRGY_STATIS_VO.join(vo.getStrategyId().toString()), JSON.toJSONString(vo));
    }

    @Override
    public boolean favor(Long sid, Long uid) {
        // 此收藏功能是站在用户角度
        boolean flag = true;
        // 设计好针对该用户的攻略收藏集合并查询看是否存在
        String key = RedisKeys.USER_STRATEGY_STATIS_FAVOR.join(uid.toString());
        List<String> list = new ArrayList<>();
        if (redisTemplate.hasKey(key)) {
            // 存在  判断该攻略的id是否存在该用户的攻略收藏集合
            String listStr = redisTemplate.opsForValue().get(key);
            list = JSON.parseArray(listStr, String.class);
            if (list.contains(sid.toString())) {
                // 存在 做取消收藏操作  list剔除该攻略id  收藏-1
                list.remove(sid.toString());
                this.numIncrease(sid, STRATEGY_FAVORNUM, -1);
                flag = false;
            }else {
                // 不存在  做收藏操作   list添加该攻略id  收藏+1
                list.add(sid.toString());
                this.numIncrease(sid, STRATEGY_FAVORNUM, 1);
            }
        }else {
            // 不存在 新增
            list.add(sid.toString());
            this.numIncrease(sid, STRATEGY_FAVORNUM, 1);
        }
        redisTemplate.opsForValue().set(key, JSON.toJSONString(list));
        return flag;
    }

    @Override
    public boolean isUserFavorStrategy(Long uid, Long sid) {
        String key = RedisKeys.USER_STRATEGY_STATIS_FAVOR.join(uid.toString());
        if (redisTemplate.hasKey(key)) {
            String listStr = redisTemplate.opsForValue().get(key);
            List<String> list = JSON.parseArray(listStr, String.class);
            if (list.contains(sid.toString())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void viewNumIncrease(Long sid, int i) {
        this.numIncrease(sid, STRATEGY_VIEWNUM, i);
    }

    @Override
    public void replyNumIncrease(Long sid, int i) {
        this.numIncrease(sid, STRATEGY_REPLYNUM, i);
    }

    @Override
    public boolean isVoExist(Long id) {
        return redisTemplate.hasKey(RedisKeys.STRATRGY_STATIS_VO.join(id.toString()));
    }

    @Override
    public boolean strategyThumbup(Long sid, Long uid) {
        String key = RedisKeys.STRATEGY_STATIS_THUMBSUP.join(uid.toString(), sid.toString());
        // 存在表示该用户今天顶过该攻略
        if (redisTemplate.hasKey(key)){
            return false;
        }
        // 不存在表示该用户今天没有顶过该攻略
        Date date = new Date();
        this.numIncrease(sid, STRATEGY_THUMBSUPNUM, 1);
        redisTemplate.opsForValue().set(key, "1", DateUtil.getDateBetween(DateUtil.getEndDate(date), date), TimeUnit.SECONDS);
        return true;
    }

    @Override
    public Set<String> getKeys(String pattern) {
        Set<String> keys = redisTemplate.keys(pattern + "*");
        return keys;
    }

    @Override
    public String get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    @Override
    public boolean sharenum(Long sid, Long uid) {
        this.numIncrease(sid, STRATEGY_SHARENUM, 1);
        return true;
    }

    @Override
    public void addScore(String prefix, String value, int i) {
        redisTemplate.opsForZSet().incrementScore(prefix, value, i);
    }

    @Override
    public boolean isVoByStrategyExist(String prefix, String value) {
        Long rank = redisTemplate.opsForZSet().rank(prefix, value);
        return rank != null;
    }

    @Override
    public List<StrategyStatisVO> getAllZSetStrategyVO(String prefix) {
        // 根据传进来的key获取zset中key的值
        Set<String> set = redisTemplate.opsForZSet().reverseRange(prefix, 0, -1);
        List<StrategyStatisVO> list = new ArrayList<>();
        for (String s : set) {
            String voStr = redisTemplate.opsForValue().get(s);
            list.add(JSON.parseObject(voStr, StrategyStatisVO.class));
        }
        return list;
    }

    @Override
    public List<StrategyDetail> queryByUserIdToFavor(Long uid) {
        String FavorStr = redisTemplate.opsForValue().get(RedisKeys.USER_STRATEGY_STATIS_FAVOR.join(uid.toString()));
        if (StringUtils.hasLength(FavorStr)){
            List<StrategyDetail>list=new ArrayList<>();
            List<Long> sid = JSON.parseArray(FavorStr, Long.class);
            for (Long id : sid) {
                StrategyDetail detail = strategyDetailService.get(id);
                list.add(detail);
            }
            return list;
        }
        return null;
    }
}
