package cn.wolfcode.luowowo.cache.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 问答统计数据vo
 */
@Setter
@Getter
public class QuestionStatisVO implements Serializable {

    public static final int QUESTION_VIEWNUM = 1;      //点击数
    public static final int QUESTION_ANSWERNUM = 2;     //回答数
    public static final int QUESTION_SHARENUM = 3;     //分享数
    public static final int QUESTION_THUMBSUPNUM = 4;  //问题点赞个数
    private Long questionId;//用于数据持久化
    private int sharenum;

    private int answernum;

    private int viewnum;

    private int thumbsupnum;
}
