<ul class="recommend_tab_l">
        <li class="active subject_js" data="1">主题乐园<i class="ticket_icon"></i></li>
        <li class=" subject_js" data="2">温泉<i class="ticket_icon"></i></li>
        <li class=" subject_js" data="3">动植物园<i class="ticket_icon"></i></li>
        <li class=" subject_js" data="4">都市观光<i class="ticket_icon"></i></li>
        <li class=" subject_js" data="5">水乡古镇<i class="ticket_icon"></i></li>
        <li class=" subject_js" data="6">登山徒步<i class="ticket_icon"></i></li>
        <li class=" subject_js" data="7">湖光山色<i class="ticket_icon"></i></li>
        <li class=" subject_js" data="8">田园度假<i class="ticket_icon"></i></li>
    </ul>
===
<#if tickets??>
        <#list tickets as ticket>
    <#--推荐门票-->
               <li>
                  <a href="/ticket/detail?tid=${ticket.id}" target="_blank" onclick="cmcTag('门票频道页-PC-站点-P4-景点推荐-广州主题乐园-001-广州塔','PC门票频道页景点推荐');">
                     <div class="promotion_img_box">
                         <img src="${(ticket.coverUrl)!}" width="222" height="150" alt="">
                     </div>
                     <div class="promotion_footer">
                     <h5 title="广州塔">${(ticket.name)!}</h5>

                      <p><span>¥<dfn>${(ticket.price)!}</dfn></span><samp>起</samp></p>
                     </div>
                   </a>
               </li>
        </#list>
    </#if>