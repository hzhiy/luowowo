package cn.wolfcode.luowowo.airticket.service;

import cn.wolfcode.luowowo.airticket.domain.Flight;
import cn.wolfcode.luowowo.airticket.query.FlightQuery;
import cn.wolfcode.luowowo.airticket.vo.FlightVO;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * Created by Administrator on 2019/8/24.
 */
public interface IFlightService {
    /**
     * 查询航班数据
     * @param qo
     * @return
     */
    List<FlightVO> queryForList1(FlightQuery qo);

    PageInfo<Flight> queryForList(FlightQuery qo);
}
