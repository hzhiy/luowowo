package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 关于地区的主题
 */
@Getter
@Setter
public class DestinationTheme extends BaseDomain    {

    private Long parentId;//这个类的父类id

    private String name;//主题的名字

    private String imgUrl;//主题背景

    private Integer sequence;//排序序号

    private List<Destination> dests;//这个主题有的地区

}