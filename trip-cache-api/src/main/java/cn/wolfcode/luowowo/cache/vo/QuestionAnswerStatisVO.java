package cn.wolfcode.luowowo.cache.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 *社区问答排行榜
 */
@Setter
@Getter
public class QuestionAnswerStatisVO implements Serializable {

    private Long userId;

    private String headImgUrl;

    private int level;

    private String nickname;

    private int count;//回答数/金牌数/被顶数
}
