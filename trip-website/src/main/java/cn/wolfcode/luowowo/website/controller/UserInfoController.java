package cn.wolfcode.luowowo.website.controller;

import cn.wolfcode.luowowo.cache.service.IUserInfoRedisService;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import cn.wolfcode.luowowo.common.exception.LogicException;
import cn.wolfcode.luowowo.common.util.Consts;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.member.service.IUserInfoService;
import cn.wolfcode.luowowo.website.annotation.UserParam;
import cn.wolfcode.luowowo.website.util.CookieUtil;
import cn.wolfcode.luowowo.website.util.UploadUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeUnit;

/**
 * Created by zhi on 2019/8/7.
 */
@Controller
public class UserInfoController {

    @Reference
    private IUserInfoService userInfoService;
    @Reference
    private IUserInfoRedisService userInfoRedisService;

    @RequestMapping("hello")
    @ResponseBody
    public Object get() {
        return userInfoService.get(6L);
    }

    // 判断手机号是否存在
    @RequestMapping("checkPhone")
    @ResponseBody
    public boolean checkPhone(String phone) {
        return !userInfoService.queryByPhone(phone);
    }

    // 发送验证码
    @RequestMapping("sendVerifyCode")
    @ResponseBody
    public AjaxResult sendVerifyCode(String phone){
        userInfoService.sendVerifyCode(phone);
        return AjaxResult.SUCCESS;
    }

    // 注册
    @RequestMapping("userRegist")
    @ResponseBody
    public AjaxResult userRegist(String phone, String nickname, String password, String rpassword, String verifyCode){
        userInfoService.regist(phone, nickname, password, rpassword, verifyCode);
        return AjaxResult.SUCCESS;
    }

    // 登录

    @RequestMapping("userLogin")
    @ResponseBody
    public AjaxResult userLogin(String username, String password, HttpServletResponse response, HttpServletRequest request){
        String token = userInfoService.userLogin(username, password);
        CookieUtil.addCookie(token, response);
        if(token != null){
            UserInfo userInfo = userInfoRedisService.getUserInfo(token);
            request.getSession().setAttribute("userInfo", userInfo);
        }
        return AjaxResult.SUCCESS;
    }



    //-------------------个人信息修改-------------------------


    //用户信息更改
    @RequestMapping("/updateUser")
    public String updateUser(UserInfo userInfo){
        System.out.println(userInfo);
        userInfoService.updateUser(userInfo);
        return "mine/setting";
    }


    //修改手机号,发送验证码
    @RequestMapping("/updatePhoneSendVerifyCode")
    @ResponseBody
    public AjaxResult updatePhone(String phone,@UserParam UserInfo userInfo){
        userInfoService.updatePhoneSendVerifyCode(phone,userInfo.getId());
        return AjaxResult.SUCCESS;
    }


    //提交验证码
    @RequestMapping("/checkoutVerifyCode")
    @ResponseBody
    public AjaxResult checkoutVerifyCode(String verifyCode,String phone,Long id){
        userInfoService.checkoutVerfyCode(verifyCode,phone,id);
        return AjaxResult.SUCCESS;
    }


    //新手机号
    @RequestMapping("/newPhoneSendVerifyCode")
    @ResponseBody
    public AjaxResult newPhone(String phone){
        userInfoService.newPhoneSendVerifyCode(phone);
        return AjaxResult.SUCCESS;
    }

    //头像修改
    @RequestMapping("/updateHeadImgUrl")
    @ResponseBody
    public AjaxResult updateHeadImgUrl(UserInfo userInfo){
        userInfoService.updateHeadImgUrl(userInfo);
        return AjaxResult.SUCCESS;
    }


    //修改密码
    @RequestMapping("/updatePassword")
    @ResponseBody
    public AjaxResult updatePassword(String password,@UserParam UserInfo userInfo,String verifyCode,String phone){
        userInfoService.updatePassword(password,userInfo.getId(),verifyCode,phone);
        return AjaxResult.SUCCESS;
    }
}
