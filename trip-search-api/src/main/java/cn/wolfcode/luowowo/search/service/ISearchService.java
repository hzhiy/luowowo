package cn.wolfcode.luowowo.search.service;

import cn.wolfcode.luowowo.search.query.SearchQueryObject;
import cn.wolfcode.luowowo.search.template.DestinationTemplate;
import org.springframework.data.domain.Page;

/**
 * Created by zhi on 2019/8/17.
 */
public interface ISearchService {

    /**
     * 站内搜索
     * @param index
     * @param type
     * @param clz
     * @param qo
     * @param fields
     * @param <T>
     * @return
     */
    <T> Page<T> searchWithHighlight(String index, String type, Class<T> clz, SearchQueryObject qo, String... fields);

}

