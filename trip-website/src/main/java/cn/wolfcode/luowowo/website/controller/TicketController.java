package cn.wolfcode.luowowo.website.controller;

import cn.wolfcode.luowowo.article.domain.TicketContent;
import cn.wolfcode.luowowo.article.domain.TicketDetail;
import cn.wolfcode.luowowo.article.query.TicketObject;
import cn.wolfcode.luowowo.article.service.ITicketContentService;
import cn.wolfcode.luowowo.article.service.ITicketDetailService;
import cn.wolfcode.luowowo.article.service.ITicketService;
import cn.wolfcode.luowowo.cache.service.ITicketRedisService;
import cn.wolfcode.luowowo.search.service.ITicketTemplateService;
import cn.wolfcode.luowowo.search.template.TicketTemplate;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("ticket")
public class TicketController {

    @Reference
    private ITicketRedisService ticketRedisService;
    @Reference
    private ITicketService ticketService;
    @Reference
    private ITicketTemplateService ticketTemplateService;
    @Reference
    private ITicketDetailService ticketDetailService;
    @Reference
    private ITicketContentService ticketContentService;


    @RequestMapping("")
    public String list(Model model, TicketObject qo){
//        System.out.println(qo.getAjaxDestId());
//        List<Ticket> list = ticketRedisService.getByDestId(qo.getAjaxDestId());
//        System.out.println(list);
//        List<Ticket> tickets = ticketService.getALLDest();
//        List<Destination> dests=new ArrayList<>();//地区
//        List<TicketTheme> themes=new ArrayList<>();//主题
//        for (Ticket ticket : tickets) {
//            Destination dest = new Destination();
//            TicketTheme theme = new TicketTheme();
//            dest.setId(ticket.getDest().getId());
//            dest.setName(ticket.getName());
//            theme.setId(ticket.getTicketTheme().getId());
//            theme.setName(ticket.getName());
//            dests.add(dest);
//            themes.add(theme);
//        }
//        model.addAttribute("dests",dests);
//        model.addAttribute("themes",themes);

//        List<Ticket> list = ticketRedisService.getByDestId(3361L);
//        System.out.println(list);
//        model.addAttribute("tickets",list);
        return "ticket/index";
    }

    //地区
    @RequestMapping("/reloadSubAndProd")
    public String index(Model model, TicketObject qo){
        model.addAttribute("list",ticketTemplateService.queryByDestId(qo.getAjaxDestId()));
        return "ticket/allPageTpl";
    }

    //主题+地区
    @RequestMapping("/reloadProd")
    public String reloadProd(Model model, TicketObject qo){
        List<TicketTemplate> ticketTemplates = ticketTemplateService.queryByDestandThemeId(qo);
        model.addAttribute("list",ticketTemplates);
        return "ticket/rightPageTpl";
    }

    //详情
    @RequestMapping("/detail")
    public String detail(Model model,Long id){
        model.addAttribute("ticket",ticketService.getById(id));
        TicketDetail detail = ticketDetailService.getByTicketId(id);
        TicketContent content = ticketContentService.getByDetailId(detail.getId());
        detail.setTicketContent(content);
        model.addAttribute("ticketDetail",detail);
        return "ticket/detail";
    }

    //在线支付
    @RequestMapping("/addOrder")
    public String addOrder(Model model,Long tid){
        model.addAttribute("ticket",ticketService.getById(tid));
        model.addAttribute("ticketDetail",ticketDetailService.getByTicketId(tid));
        return "ticket/addOrder";
    }
}
