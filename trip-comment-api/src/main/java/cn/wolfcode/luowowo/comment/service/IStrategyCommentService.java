package cn.wolfcode.luowowo.comment.service;

import cn.wolfcode.luowowo.article.domain.StrategyCommend;
import cn.wolfcode.luowowo.article.query.StrategyDetailQuery;
import cn.wolfcode.luowowo.comment.domain.StrategyComment;
import cn.wolfcode.luowowo.comment.query.StrategyCommentQuery;
import com.github.pagehelper.PageInfo;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * 攻略评论接口
 */
public interface IStrategyCommentService {

    /**
     * 添加
     * @param comment
     */
    void saveOrUpdate(StrategyComment comment);

    /**
     * 查询攻略评论
     * @param qo
     */
    Page list(StrategyDetailQuery qo);

    /**
     * 点赞或者取消点赞
     * @param mongoId
     * @param userId
     */
    void commentThumbUp(String mongoId, Long userId);

    /**
     * 后台评论管理
     * @param qo
     * @return
     */
    Page queryForList(StrategyCommentQuery qo);

    /**
     * 修改状态
     * @param id
     * @param state
     */
    void updateState(String id, int state);

}
