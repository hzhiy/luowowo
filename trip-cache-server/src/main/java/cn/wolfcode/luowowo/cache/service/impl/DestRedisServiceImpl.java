package cn.wolfcode.luowowo.cache.service.impl;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.service.IDestinationService;
import cn.wolfcode.luowowo.article.vo.ThemeDestVO;
import cn.wolfcode.luowowo.cache.emum.RedisKeys;
import cn.wolfcode.luowowo.cache.service.IDestRedisService;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhi on 2019/8/22.
 */
@Service
public class DestRedisServiceImpl implements IDestRedisService {
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Reference
    private IDestinationService destinationService;

    @Override
    public ThemeDestVO getThemeDestVO(Long id) {
        String key = RedisKeys.THEME_DEST_VO.join(id.toString());
        String voStr = redisTemplate.opsForValue().get(key);
        if (StringUtils.hasLength(voStr)){
            ThemeDestVO vo = JSON.parseObject(voStr, ThemeDestVO.class);
            return vo;
        }
        return null;
    }

    @Override
    public void setThemeDestVO(ThemeDestVO vo) {
        String key = RedisKeys.THEME_DEST_VO.join(vo.getId().toString());
        redisTemplate.opsForValue().set(key, JSON.toJSONString(vo));
    }

    @Override
    public boolean favor(Long did, Long id) {
        // 此收藏功能是站在用户角度
        boolean flag = true;
        // 设计好针对该用户的游记收藏集合并查询看是否存在
        String key = RedisKeys.USER_DEST_FAVOR.join(id.toString());
        List<Long> list = new ArrayList<>();
        if (redisTemplate.hasKey(key)) {
            // 存在  判断该游记的id是否存在该用户的游记收藏集合
            String listStr = redisTemplate.opsForValue().get(key);
            list = JSON.parseArray(listStr, Long.class);
            if (list.contains(did)) {
                // 存在 做取消收藏操作  list剔除该游记id  收藏-1
                list.remove(did);
                flag = false;
            }else {
                // 不存在  做收藏操作   list添加该游记id  收藏+1
                list.add(did);
                flag=true;
            }
        }else {
            // 不存在 新增
            list.add(did);
            flag=true;
        }
        redisTemplate.opsForValue().set(key, JSON.toJSONString(list));
        return flag;
    }

    @Override
    public List<Destination> queryByUserIdFavor(Long id) {
        String key = RedisKeys.USER_DEST_FAVOR.join(id.toString());
        List<Destination>dests=new ArrayList<>();
        if (redisTemplate.hasKey(key)) {
            String listStr = redisTemplate.opsForValue().get(key);
            if (StringUtils.hasLength(listStr)) {
                List<Long> list = JSON.parseArray(listStr, Long.class);
                for (Long did : list) {
                    Destination dest = destinationService.getDestById(did);
                    dests.add(dest);
                }
            }
        }
        return dests;
    }
}
