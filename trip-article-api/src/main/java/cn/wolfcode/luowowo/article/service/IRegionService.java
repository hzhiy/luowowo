package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.domain.Region;
import cn.wolfcode.luowowo.article.query.RegionQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 区域
 */
public interface IRegionService {
    /**
     * 列表分页查询
     * @param qo
     * @return
     */
    PageInfo query(RegionQuery qo);

    /**
     * 查询关联地区
     * @param rid
     * @return
     */
    List<Destination> getDestByRegionId(Long rid);

    /**
     * 添加或修改
     * @param region
     */
    void saveOrUpdate(Region region);

    /**
     * 修改状态
     * @param id
     * @param hot
     */
    void changeHotValue(Long id, Integer hot);

    /**
     * 删除
     * @param id
     */
    void delete(Long id);

    /**
     * 查询热门区域
     * @return
     */
    List<Region> listAllHot();
}
