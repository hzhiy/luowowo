package cn.wolfcode.luowowo.airticket.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Setter
@Getter
public class Flight1 {
    private Long id;

    private Long flightno_id;

    private Integer depPort_id;

    private Date depTime;

    private Long arrAirport_id;

    private Date arrTime;

    private Integer price;

}