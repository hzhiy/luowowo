package cn.wolfcode.luowowo.comment.service.impl;

import cn.wolfcode.luowowo.comment.domain.HotelComment;
import cn.wolfcode.luowowo.comment.domain.StrategyComment;
import cn.wolfcode.luowowo.comment.repository.IHotelCommentRepository;
import cn.wolfcode.luowowo.comment.service.IHotelCommentService;
import cn.wolfcode.luowowo.comment.vo.HotelCommentVO;
import cn.wolfcode.luowowo.hotel.query.HotelCommentQuery;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by zhi on 2019/8/13.
 */
@Service
public class HotelCommentServiceImpl implements IHotelCommentService {

    @Autowired
    private IHotelCommentRepository hotelCommentRepository;
    @Autowired
    private MongoTemplate template;

    public HotelComment saveOrUpdate(HotelComment comment) {
        if (!StringUtils.hasLength(comment.getId())) {
            comment.setCreateTime(new Date());
        }
        if (comment.getRefComment() != null){
            Optional<HotelComment> optional = hotelCommentRepository.findById(comment.getRefComment().getId());
            if (optional.isPresent()) {
                HotelComment refComment = optional.get();
                comment.setRefComment(refComment);
            }
        }
        hotelCommentRepository.save(comment);

        return comment;
    }

    public Page list(HotelCommentQuery qo) {
        // 查询条件
        Query query = new Query();
        query.addCriteria(Criteria.where("hotelId").is(qo.getHotelId()));
        // 查总数
        long count = template.count(query, HotelComment.class);
        if (count == 0) {
            return Page.empty();
        }
        // 分页:PageRequest.of(当前页0开始, 每页显示条数, 排序规则)
        //Pageable request = PageRequest.of(qo.getCurrentPage() - 1, qo.getPageSize(), Sort.by(Sort.Direction.DESC, "createTime"));
        //query.with(request);
        // 查所有评论
        Pageable request = PageRequest.of(qo.getCurrentPage() - 1, qo.getPageSize(), Sort.by(Sort.Direction.DESC, "createTime"));
        query.with(request);
        // 查某一页
        List<HotelComment> list = template.find(query, HotelComment.class);
        PageImpl page = new PageImpl(list, request, count);
        return page;

    }

    public void commentThumbUp(String mongoId, Long userId) {
        Optional<HotelComment> comment = hotelCommentRepository.findById(mongoId);
        HotelComment hc = comment.get();
        if (hc != null){
            List<String> uidList = hc.getThumbuplist();
            if (uidList.contains(userId.toString())) {
                uidList.remove(userId.toString());
                hc.setThumbupnum(hc.getThumbupnum() - 1);
            }else {
                uidList.add(userId.toString());
                hc.setThumbupnum(hc.getThumbupnum() + 1);
            }
            hotelCommentRepository.save(hc);
        }
    }

    @Override
    public HotelCommentVO querySiatisInfo(Long id) {
        List<HotelComment> list = hotelCommentRepository.findByHotelId(id);
        Integer ensemble = 0;       // 总体评价
        Integer location = 0;       // 位置
        Integer cleanliness = 0;    // 清洁度
        Integer facility = 0;       // 设施
        Integer service = 0;        // 服务
        Integer comfort = 0;        // 舒适度
        Integer food = 0;           // 餐饮
        for (HotelComment hc : list) {
            ensemble += hc.getEnsemble();
            location += hc.getLocation();
            cleanliness += hc.getCleanliness();
            facility += hc.getFacility();
            service += hc.getService();
            comfort += hc.getComfort();
            food += hc.getFood();
        }
        HotelCommentVO vo = new HotelCommentVO();
        if (list.size() > 0){
            vo.setEnsemble(Double.parseDouble(ensemble * 2 / list.size() + ""));
            vo.setLocation(Double.parseDouble(location * 2 / list.size() + ""));
            vo.setCleanliness(Double.parseDouble(cleanliness * 2 / list.size() + ""));
            vo.setFacility(Double.parseDouble(facility * 2 / list.size() + ""));
            vo.setService(Double.parseDouble(service * 2 / list.size() + ""));
            vo.setComfort(Double.parseDouble(comfort * 2 / list.size() + ""));
            vo.setFood(Double.parseDouble(food * 2/ list.size() + ""));
            vo.setCount(Double.parseDouble(list.size() + ""));
        }
        return vo;
    }

    @Override
    public HotelComment reComment(String hcid, HotelComment hotelComment) {
        Optional<HotelComment> oComment = hotelCommentRepository.findById(hcid);
        HotelComment comment = oComment.get();
        if (comment != null){
            List<HotelComment> refCommentlist = comment.getRefCommentlist();
            hotelComment.setCreateTime(new Date());
            refCommentlist.add(hotelComment);
        }
        this.saveOrUpdate(comment);


        return hotelComment;
    }

    @Override
    public List<HotelComment> listAll() {
        return hotelCommentRepository.findAll();
    }

}
