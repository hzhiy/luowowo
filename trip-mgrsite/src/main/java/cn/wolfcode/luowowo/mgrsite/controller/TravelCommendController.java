package cn.wolfcode.luowowo.mgrsite.controller;

import cn.wolfcode.luowowo.article.domain.TravelCommend;
import cn.wolfcode.luowowo.article.query.TravelCommendQuery;
import cn.wolfcode.luowowo.article.service.ITravelCommendService;
import cn.wolfcode.luowowo.article.service.ITravelService;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by zhi on 2019/8/16.
 */
@Controller
@RequestMapping("travelCommend")
public class TravelCommendController {

    @Reference
    private ITravelCommendService travelCommendService;
    @Reference
    private ITravelService travelService;

    @RequestMapping("list")
    public String list(Model model, @ModelAttribute("qo") TravelCommendQuery qo){
        model.addAttribute("pageInfo", travelCommendService.queryForList(qo));
        model.addAttribute("details", travelService.list());
        return "travelCommend/list";
    }

    @RequestMapping("saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(TravelCommend travelCommend){
        travelCommendService.saveOrUpdate(travelCommend);
        return AjaxResult.SUCCESS;
    }
    @RequestMapping("delete")
    @ResponseBody
    public Object delete(Long id){
        travelCommendService.delete(id);
        return AjaxResult.SUCCESS;
    }



}
