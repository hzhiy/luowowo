package cn.wolfcode.luowowo.website.controller;

import cn.wolfcode.luowowo.article.domain.*;
import cn.wolfcode.luowowo.article.query.TravelQuery;
import cn.wolfcode.luowowo.article.service.*;
import cn.wolfcode.luowowo.cache.service.IUserInfoRedisService;
import cn.wolfcode.luowowo.comment.domain.HotelComment;
import cn.wolfcode.luowowo.comment.service.IHotelCommentService;
import cn.wolfcode.luowowo.hotel.domain.Hotel;
import cn.wolfcode.luowowo.hotel.service.IHotelService;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.member.service.IUserInfoService;
import cn.wolfcode.luowowo.search.query.SearchQueryObject;
import cn.wolfcode.luowowo.search.query.SearchResult;
import cn.wolfcode.luowowo.search.service.*;
import cn.wolfcode.luowowo.search.template.*;
import cn.wolfcode.luowowo.website.annotation.RequireLogin;
import cn.wolfcode.luowowo.website.annotation.UserParam;
import cn.wolfcode.luowowo.website.util.CookieUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhi on 2019/8/8.
 */
@Controller
public class IndexController {

    @Reference
    private IUserInfoRedisService userInfoRedisService;
    @Reference
    private IUserInfoTemplateService userInfoTemplateService;
    @Reference
    private IStrategyTemplateService strategyTemplateService;
    @Reference
    private IStrategyDetailService strategyDetailService;
    @Reference
    private IDestinationService destinationService;
    @Reference
    private ITravelCommendService travelCommendService;
    @Reference
    private IStrategyCommendService strategyCommendService;
    @Reference
    private ITravelService travelService;
    @Reference
    private ITravelTemplateService travelTemplateService;
    @Reference
    private IDestinationTemplateService destinationTemplateService;
    @Reference
    private IUserInfoService userInfoService;
    @Reference
    private ISearchService searchService;
    @Reference
    private IHotelService hotelService;
    @Reference
    private IHotelTemplateService hotelTemplateService;
    @Reference
    private IHotelCommentService hotelCommentService;
    @Reference
    private IHotelCommentSearchService hotelCommentSearchService;

    @RequestMapping("")
    public String index(Model model){
        List<TravelCommend> travelCommends = travelCommendService.getTop10();
        model.addAttribute("tcs", travelCommends.subList(0, travelCommends.size()>5?5:travelCommends.size()));
        List<StrategyCommend> scs = strategyCommendService.getTop5();
        model.addAttribute("scs", scs.subList(0, scs.size()>1?1:scs.size()));
        return "index/index";
    }

    // 游记
    @RequestMapping("index/travelPage")
    public String travelPage(Model model, @ModelAttribute("qo")TravelQuery qo){
        model.addAttribute("pageInfo", travelService.query(qo));
        return "index/travelPageTpl";
    }

    // 站内搜索
    @RequestMapping("q")
    public String search(Model model, @ModelAttribute("qo")SearchQueryObject qo){
        switch (qo.getType()){
            case SearchQueryObject.SEARCH_DEST:
                return destSearch(model, qo);
            case SearchQueryObject.SEARCH_STRATEGY:
                return strategySearch(model, qo);
            case SearchQueryObject.SEARCH_TRAVEL:
                return travelSearch(model, qo);
            case SearchQueryObject.SEARCH_USER:
                return userSearch(model, qo);
            default:
                return allSearch(model, qo);
        }
    }

    private String destSearch(Model model, SearchQueryObject qo){
        DestinationTemplate dest = destinationTemplateService.findByName(qo.getKeyword());

        SearchResult data = new SearchResult();
        if (dest != null){
            List<StrategyTemplate> ss = strategyTemplateService.findByDestName(dest.getName());
            List<TravelTemplate> ts = travelTemplateService.findByDestName(dest.getName());
            List<UserInfoTemplate> us = userInfoTemplateService.findByDestName(dest.getName());
            data.setTotal(Long.parseLong(ss.size() + ts.size() + us.size() + ""));
            data.setStrategys(ss.subList(0, ss.size()>5? 5 : ss.size()));
            data.setTravels(ts.subList(0, ts.size()>5? 5 : ts.size()));
            data.setUsers(us.subList(0, us.size()>5? 5 : us.size()));
            model.addAttribute("data", data);
        }
        model.addAttribute("dest", dest);
        return "index/searchDest";
    }
    private String strategySearch(Model model, SearchQueryObject qo){
        Page<StrategyTemplate> page = searchService.searchWithHighlight(StrategyTemplate.INDEX_NAME, StrategyTemplate.TYPE_NAME, StrategyTemplate.class, qo, "title", "subTitle", "summary");
        model.addAttribute("page", page);
        return "index/searchStrategy";
    }
    private String travelSearch(Model model, SearchQueryObject qo){
        Page<TravelTemplate> page = searchService.searchWithHighlight(TravelTemplate.INDEX_NAME, TravelTemplate.TYPE_NAME, TravelTemplate.class, qo, "title", "summary");
        model.addAttribute("page", page);
        return "index/searchTravel";
    }
    private String userSearch(Model model, SearchQueryObject qo){
        Page<UserInfoTemplate> page = searchService.searchWithHighlight(UserInfoTemplate.INDEX_NAME, UserInfoTemplate.TYPE_NAME, UserInfoTemplate.class, qo, "nickname", "destName");
        model.addAttribute("page", page);
        return "index/searchUser";
    }
    private String allSearch(Model model, SearchQueryObject qo){
        SearchResult data = new SearchResult();
        Page<DestinationTemplate> ds = searchService.searchWithHighlight(DestinationTemplate.INDEX_NAME, DestinationTemplate.TYPE_NAME, DestinationTemplate.class, qo, "name", "info");
        Page<StrategyTemplate> ss = searchService.searchWithHighlight(StrategyTemplate.INDEX_NAME, StrategyTemplate.TYPE_NAME, StrategyTemplate.class, qo, "title", "subTitle", "summary");
        Page<TravelTemplate> ts = searchService.searchWithHighlight(TravelTemplate.INDEX_NAME, TravelTemplate.TYPE_NAME, TravelTemplate.class, qo, "title", "summary");
        Page<UserInfoTemplate> us = searchService.searchWithHighlight(UserInfoTemplate.INDEX_NAME, UserInfoTemplate.TYPE_NAME, UserInfoTemplate.class, qo, "nickname", "destName");
        data.setTotal(ds.getTotalElements() + ss.getTotalElements() + ts.getTotalElements() + us.getTotalElements());
        data.setDests(    ds.getContent().subList(0, ds.getContent().size() > 5 ? 5 : ds.getContent().size()));
        data.setStrategys(ss.getContent().subList(0, ss.getContent().size() > 5 ? 5 : ss.getContent().size()));
        data.setTravels(  ts.getContent().subList(0, ts.getContent().size() > 5 ? 5 : ts.getContent().size()));
        data.setUsers(    us.getContent().subList(0, us.getContent().size() > 5 ? 5 : us.getContent().size()));
        model.addAttribute("data", data);
        return "index/searchAll";
    }

    @RequestMapping("addUser")
    @ResponseBody
    public String addUser(){
        UserInfoTemplate userInfoTemplate = new UserInfoTemplate();
        userInfoTemplate.setInfo("aaa");
        userInfoTemplateService.saveOrUpdate(userInfoTemplate);
        return "OK";
    }
    @RequestMapping("add")
    @ResponseBody
    public String addStrategy(){
        StrategyTemplate t = null;
        List<StrategyDetail> details = strategyDetailService.list();
        /*for (StrategyDetail detail : details) {
            t = new StrategyTemplate();
            t.setId(detail.getId());
            t.setTitle(detail.getTitle());
            t.setSubTitle(detail.getSubTitle());
            t.setDestId(detail.getDest().getId());
            t.setDestName(detail.getDest().getName());
            t.setThemeId(detail.getTheme().getId());
            t.setThemeName(detail.getTheme().getName());
            t.setSummary(detail.getSummary());
            t.setCreateTime(detail.getCreateTime());
            t.setViewnum(detail.getViewnum());
            t.setFavornum(detail.getFavornum());
            t.setReplynum(detail.getReplynum());
            t.setThumbsupnum(detail.getThumbsupnum());

            t.setCoverUrl(detail.getCoverUrl());

            //List<String> list = strategyTagService.getTagsBySDetailId(detail.getId());
            //t.setTags(list);

            Destination dest = destinationService.getCountry(detail.getDest().getId());

            t.setCountryId(dest.getId());
            t.setCountryName(dest.getName());
            dest = destinationService.getProvince(detail.getDest().getId());
            if(dest != null){
                t.setProvinceId(dest.getId());
                t.setProvinceName(dest.getName());
            }
            strategyTemplateService.saveOrUpdate(t);
        }*/

        //用户----------------------------------------------------------------------------
        /*List<UserInfo> us = userInfoService.list();
        for (UserInfo u : us) {
            UserInfoTemplate tt = new UserInfoTemplate();
            tt.setId(u.getId());
            tt.setDestName(u.getCity());
            tt.setFansnum(11);
            tt.setHeadUrl(u.getHeadImgUrl());
            tt.setInfo(u.getInfo());
            tt.setLevel(u.getLevel());
            tt.setNickname(u.getNickname());
            tt.setReplynum(10);
            tt.setTravelnum(2);
            userInfoTemplateService.saveOrUpdate(tt);
        }


        // 游记
        List<Travel> ts = travelService.list();

        for (Travel travel : ts) {
            TravelTemplate tt = new TravelTemplate();
            tt.setId(travel.getId());
            tt.setAuthorId(travel.getAuthor().getId());
            tt.setAuthorName(travel.getAuthor().getNickname());
            tt.setCoverUrl(travel.getCoverUrl());
            tt.setCreateTime(travel.getCreateTime());
            tt.setDestId(travel.getDest().getId());
            tt.setDestName(travel.getDest().getName());
            tt.setReplynum(travel.getReplynum());
            tt.setSummary(travel.getSummary());
            tt.setTitle(travel.getTitle());
            tt.setViewnum(travel.getViewnum());
            travelTemplateService.saveOrUpdate(tt);
        }

        //目的地----------------------------------------------------------------------------
        List<Destination> ds = destinationService.list();

        for (Destination d : ds) {

            DestinationTemplate tt = new DestinationTemplate();

            tt.setId(d.getId());
            tt.setCoverUrl(d.getCoverUrl());
            tt.setName(d.getName());
            tt.setInfo(d.getInfo());
            destinationTemplateService.saveOrUpdate(tt);
        }*/
        
        // 酒店
        /*List<Hotel> hotelList = hotelService.list();
        for (Hotel hotel : hotelList) {
            HotelTemplate hotelTemplate = new HotelTemplate();
            try {
                BeanUtils.copyProperties(hotelTemplate, hotel);
                if (hotel.getPosition() != null){
                    hotelTemplate.setPositionID(hotel.getPosition().getId());
                    hotelTemplate.setPositionName(hotel.getPosition().getName());
                    hotelTemplate.setPositionInfo(hotel.getPosition().getInfo());
                }
                if (hotel.getCity() != null){
                    hotelTemplate.setCityID(hotel.getCity().getId());
                    hotelTemplate.setCityName(hotel.getCity().getName());
                    hotelTemplate.setCityInfo(hotel.getCity().getInfo());
                }
                hotelTemplateService.saveOrUpdate(hotelTemplate);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        }*/

      /*  List<UserInfo> userInfos = userInfoService.list();
        for (UserInfo userInfo : userInfos) {
            String dayKey = RedisKeys.QUESTION_ANSWER_STATIS_VO_DAY.join(userInfo.getId().toString());

            String weekKey = RedisKeys.QUESTION_ANSWER_STATIS_VO_WEEK.join(userInfo.getId().toString());
            String monthKey = RedisKeys.QUESTION_ANSWER_STATIS_VO_MONTH.join(userInfo.getId().toString());
        }


        List<UserAnswer> userAnswers =  userAnswerService.list();
        String daySortKey = RedisKeys.QUESTION_ANSWER_STATIS_VO_DAY_SORT.getPrefix();
        String weekSortKey = RedisKeys.QUESTION_ANSWER_STATIS_VO_WEEK_SORT.getPrefix();
        String monthSortKey = RedisKeys.QUESTION_ANSWER_STATIS_VO_MONTH_SORT.getPrefix();
        for (UserAnswer userAnswer : userAnswers) {
            questionAnswerStatisRedisService.setRankList(daySortKey,RedisKeys.QUESTION_ANSWER_STATIS_VO_DAY.join(userAnswer.getAnswererId().toString()),userAnswer.getDaynum());
            questionAnswerStatisRedisService.setRankList(weekSortKey,RedisKeys.QUESTION_ANSWER_STATIS_VO_WEEK.join(userAnswer.getAnswererId().toString()),userAnswer.getWeeknum());
            questionAnswerStatisRedisService.setRankList(monthSortKey,RedisKeys.QUESTION_ANSWER_STATIS_VO_MONTH.join(userAnswer.getAnswererId().toString()),userAnswer.getMonthnum());

        }*/

      //酒店评论存es初始化
        List<HotelComment> hotelCommentList =  hotelCommentService.listAll();
        for (HotelComment comment : hotelCommentList) {
            HotelCommentTemplate template = new HotelCommentTemplate();
            try {
                BeanUtils.copyProperties(template, comment);
                hotelCommentSearchService.saveOrUpdate(template);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return "OK";
    }




}
