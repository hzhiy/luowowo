package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.domain.Region;
import cn.wolfcode.luowowo.article.mapper.RegionMapper;
import cn.wolfcode.luowowo.article.service.IRegionService;
import cn.wolfcode.luowowo.article.query.RegionQuery;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by zhi on 2019/8/8.
 */
@Service
public class RegionServiceImpl implements IRegionService {

    @Autowired
    private RegionMapper regionMapper;

    public PageInfo query(RegionQuery qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getOrder());
        List<Region> list = regionMapper.queryForList(qo);
        return new PageInfo<>(list);
    }

    @Override
    public List<Destination> getDestByRegionId(Long rid) {
        return null;
    }

    @Override
    public void saveOrUpdate(Region region) {
        if (region.getId() ==null) {
            regionMapper.insert(region);
        }else {
            regionMapper.updateByPrimaryKey(region);
        }
    }

    @Override
    public void changeHotValue(Long id, Integer hot) {
        regionMapper.updateHotById(id, hot);
    }

    @Override
    public void delete(Long id) {
        regionMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<Region> listAllHot() {
        return regionMapper.selectAllHot();
    }

}


