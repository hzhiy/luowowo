<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link href="/styles/base.css" rel="stylesheet" type="text/css">
    <link href="/styles/replyDetail.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/jquery.js"></script>

    <script type="text/javascript" src="/js/common.js"></script>

    <script>


    </script>
</head>

<body>
<div class="lww_header">
	<div class="header_wrap">
		<div class="header_logo">
			<a href="javascript:;" class="lww_logo"></a>
		</div>
		<ul class="header_nav">
			<li><a href="/index.html">首页</a></li>
			<li class="header_nav_active"><a href="/destination.html">目的地</a></li>
			<li><a href="/gonglve.html">旅游攻略</a></li>
			<li><a href="javascript:;">去旅行<i class="icon_caret_down"></i></a></li>
			<li><a href="javascript:;">机票</a></li>
			<li><a href="javascript:;">酒店</a></li>
			<li><a href="javascript:;">社区<i class="icon_caret_down"></i></a></li>
		</ul>
		<div class="header_search">
			<input type="text" />
			<a class="icon_search"></a>
		</div>
		<div class="login_info">
			<div class="head_user">
				<a href="javascript:;">
					<img src="./images/user.png" />
					<i class="icon_caret_down"></i>
				</a>
			</div>
			<div class="header_msg">
				消息<i class="icon_caret_down"></i>
			</div>
			<div class="header_daka">
				<a href="javascript:;">打卡</a>
			</div>
		</div>
	</div>
	<div class="shadow"></div>
</div>
    
<!----------------------------------------->

<div class="container" data-cs-t="景点详情页">

<div class="row row-top">
    <div class="wrapper">
        <div class="extra">
            <!-- 天气 S-->
            <div class="weather" data-cs-p="天气">
                <a href="/weather/10088.html" target="_blank">
                    <img src="http://images.mafengwo.net/images/mdd_weather/icon/icon34.png" width="25" height="25">
                    <span>中雨 27℃~34℃</span>
                </a>
            </div>
            <!-- 天气 E-->
            <!-- 收藏去过 S-->
            <div class="action _j_rside want-been">
                <div class="been-box">
                    <a class="_j_beenpoi btn-been _j_hovergo" href="/path/" target="_blank" title="添加至我的足迹"
                       data-cs-p="足迹">
                        <i class="icon"></i>
                        <span class="txt">去过</span>
                    </a>
                    <div class="rate-pop" style="display:none;">
                        <div class="rank-star">
                            <span class="s-star s-star0"></span>
                            <div class="click_star">
                                <a title="1星" rel="nofollow" data-num="1"></a>
                                <a title="2星" rel="nofollow" data-num="2"></a>
                                <a title="3星" rel="nofollow" data-num="3"></a>
                                <a title="4星" rel="nofollow" data-num="4"></a>
                                <a title="5星" rel="nofollow" data-num="5"></a>
                            </div>
                        </div>
                        <span class="rank-hint">必去推荐</span>
                    </div>
                </div>
                <a class="_j_favpoi btn-collect " href="/plan/fav.php?iMid=10088" target="_blank" title="添加收藏"
                   data-cs-p="收藏">
                    <i class="icon"></i>
                    <span class="txt">收藏</span>
                </a>
            </div>
            <!-- 收藏去过 E-->
        </div>
        <!-- 面包屑 S-->
       <div class="crumb" data-cs-p="面包屑">
            <div class="item"><a href="/mdd/" target="_blank">目的地</a><em>&gt;</em></div>
            <div class="item">
                <div class="drop">
                    <span class="hd"><a href="/travel-scenic-spot/mafengwo/10088.html">广州<i></i></a></span>
                    <div class="bd">
                        <i class="arrow"><b></b></i>
                        <div class="col">
                            <ul class="clearfix">
                                <li><a href="/jd/10088/gonglve.html" target="_blank">广州景点</a></li>
                                <li><a href="/hotel/10088/" target="_blank">广州酒店</a></li>
                                <li><a href="/cy/10088/gonglve.html" target="_blank">广州美食</a></li>
                                <li><a href="/gw/10088/gonglve.html" target="_blank">广州购物</a></li>
                                <li><a href="/yl/10088/gonglve.html" target="_blank">广州娱乐</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <em>&gt;</em>
            </div>
            <div class="item cur">广州景点</div>
        </div>
        <!-- 面包屑 E-->
		

        <!-- POI名称 S-->
        <div class="title">
            <h1>广州塔</h1>
            <div class="en">Canton Tower</div>
        </div>
        <!-- POI名称 E-->

        <!-- 快捷导航 S-->
        <div style="height: 60px;">
            <div class="r-nav" id="poi-navbar" data-cs-p="快捷导航">
                <ul class="clearfix">
                    <li data-scroll="overview" class="on">
                        <a title="概况">概况</a>
                    </li>
                    <li data-scroll="attractions" style="display: none">
                        <a title="景点亮点">景点亮点</a>
                    </li>
                    <li data-scroll="commentlist">
                        <a title="蜂蜂点评" href="#commentlist">蜂蜂点评<span>（5592条）</span></a>
                    </li>
                   

                    <li data-scroll="comment" class="nav-right">
                        <a class="btn-reviews" title="我要点评">我要点评</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- 快捷导航 E-->

    </div>
</div>

<script type="text/javascript">
    (function(){
        //面包屑
        $('.drop').hover(function(){
            var target = $(this);
            clearTimeout(target.data('hideTimer'));
            target.addClass('open');
            target.children('bd').fadeIn(200);
        },function(){
            var target = $(this);
            target.data("hideTimer", setTimeout(function() {
                target.removeClass('open');
                target.children('bd').fadeOut(200);
            }, 100));
        });
        //导航
        var $navbar = $('#poi-navbar'),
                offset_top,
                $lis = $navbar.find("li").not('.nav-right');
        $('<div/>').insertBefore($navbar).append($navbar).height($navbar.outerHeight(true));
        $(function(){
            offset_top = $navbar.offset().top;
            $(window).bind('scroll.poinav',setFixed).trigger("scroll.poinav");
        });
        $(document).delegate("[data-scroll]","click",function(){
            scrollTo($(this));
        });
        function setFixed(){
            var $rows = $('body >.container >[data-anchor]'),
                    scrolltop = $(document).scrollTop(),
                    h,
                    _lis=$lis.filter(":visible"),
                    currIndex;
            if(scrolltop  > offset_top){
                if(!$navbar.hasClass('fixed')){
                    $navbar.addClass('fixed');
                }
                h = $navbar.outerHeight(true);
                for (var $li, $row,top, i = 0, j = _lis.length; i < j; i++) {
                    $row = $rows.filter("[data-anchor=" + ($li = _lis.eq(i)).attr("data-scroll")+"]");
                    if ($row[0] && (top = $row.offset().top) && ( /*i == (j - 1) ||*/
                                    ((top - h <= scrolltop) && (top + $row.outerHeight() - h > scrolltop)))) {
                        currIndex = i;
                        break;
                    }
                }
                if(i==j){
                    _lis.removeClass("on");
                }else{
                    _lis.eq(currIndex || 0).addClass("on").siblings().removeClass("on");
                }
            }else{
                if($navbar.hasClass('fixed')){
                    $navbar.removeClass('fixed');
                }
                _lis.eq(0).addClass("on").siblings().removeClass("on");
            }
        }
})();
</script>
<div data-anchor="overview">
    <div class="row row-picture row-bg">
        <div class="wrapper">
            <a class="photo" data-cs-p="相册" href="/photo/poi/25091.html" target="_blank">
                <div class="bd">
                    <div class="pic-big"><img
                            src="http://p1-q.mafengwo.net/s11/M00/CE/1D/wKgBEFtr-IaAS0GtAFG11wwxAa051.jpeg?imageMogr2%2Fthumbnail%2F%21690x370r%2Fgravity%2FCenter%2Fcrop%2F%21690x370%2Fquality%2F100"
                            width="690" height="370"></div>
                    <div class="pic-small"><img
                            src="http://b4-q.mafengwo.net/s7/M00/C1/B1/wKgB6lR5mMeATyEUADGTU_1v3hM57.jpeg?imageMogr2%2Fthumbnail%2F%21305x183r%2Fgravity%2FCenter%2Fcrop%2F%21305x183%2Fquality%2F100"
                            width="305" height="183"></div>
                    <div class="pic-small"><img
                            src="http://p2-q.mafengwo.net/s7/M00/9D/16/wKgB6lPRuZKAf7FZABloWQE70uU04.jpeg?imageMogr2%2Fthumbnail%2F%21305x183r%2Fgravity%2FCenter%2Fcrop%2F%21305x183%2Fquality%2F100"
                            width="305" height="183"></div>
                    <span>34114张图片</span></div>
            </a></div>
    </div>

    <!-- 简介 S -->
    <div class="mod mod-detail" data-cs-p="概况">
        <div class="summary">
            ·广州塔是广州的地标，塔高600米，为国内第一高塔，可以俯瞰广州全景。<br>
            ·其头尾相当，腰身玲珑细长，又有“小蛮腰”之称，到晚上会亮灯，即使不游塔，也可来此拍摄外观。<br>
            ·包括摩天轮、珠江摄影观景平台、蜘蛛侠栈道等景点，大部分以观景摄影为主。<br>
            ·一般4点半-5点可以到达电视塔顶，观看日落及夜景。
        </div>

        <ul class="baseinfo clearfix">
            <li class="tel">
                <div class="label">电话</div>
                <div class="content">020-89338222</div>
            </li>
            <li class="item-site">
                <div class="label">网址</div>
                <div class="content"><a href="http://www.cantontower.com/" target="_blank" rel="nofollow">http://www.cantontower.com/</a>
                </div>
            </li>
            <li class="item-time">
                <div class="label">用时参考</div>
                <div class="content">1-3小时</div>
            </li>
        </ul>

        <dl>
            <dt>交通</dt>
            <dd>地铁：乘坐APM线或者3号线至赤岗塔站；<br>
                公交：乘坐121、121A、204、夜42、旅游2线至赤岗塔站。
            </dd>
        </dl>
        <dl>
            <dt>门票</dt>
            <dd>
                <div >
                    <div> 433米白云星空观光票:150人民币；450米塔顶游乐套票:228人民币；460米摩天轮游乐套票:298人民币；488米一塔倾城游乐套票:398人民币 (1月1日-12月31日
                        周一-周日)<br>
                        半票:1.2米至1.5米儿童实行半票优惠<br>
                        免票:6周岁及以下或1.2米及以下儿童免票（每名非免票成人限带一名免票儿童进场）<br>
                    </div>

                </div>
               
            </dd>
        </dl>
        <dl>
            <dt>开放时间</dt>
            <dd>
                <div>09:30-22:30；停止入场时间:22:00 (1月1日-12月31日 周一-周日)<br>
                    tips:<br>
                    摩天轮：10:00-22:30；逢周一：15:00-22:30；每月最后一周周一：17:00-22:30。<br>
                    极速云霄：10:00-22:30；逢周一：12:00-22:30；每月最后一周周一：17:00-22:30。<br>
                    广州塔实行分时段观光游览，门票以2小时为一个时段。入塔时间为:09:30、12:00、14:00、16:00、20:00
                </div>
               
            </dd>
        </dl>

        <div style="color:#999;font-size:12px;" data-cs-p="概况-感谢蜂蜂">
            *信息更新时间：2019-07-14&nbsp;&nbsp;&nbsp;&nbsp;
            感谢蜂蜂 <a href="/u/79650791.html" target="_blank">一点点的爱</a>
            参与了编辑
        </div>
    </div>
    <!-- 简介 E -->

    <!-- 内部景点 S -->
    <div data-anchor="subPoilist">
        <div id="pagelet-block-bb456ee5b764682811223f9b8d30739e" class="pagelet-block"
             data-api=":poi:pagelet:poiSubPoiApi" data-params="{&quot;poi_id&quot;:&quot;25091&quot;}" data-async="1"
             data-controller="/js/poi/ControllerPoiSubPoi">
            <div class="mod mod-innerScenic" data-cs-p="内部景点">
                <div class="mhd">内部景点</div>
                <div class="mbd">
                    <ul class="clearfix">
                        <li>
                            <a href="/poi/5174512.html" target="_blank" title="六榕寺">
                                <img src="http://p3-q.mafengwo.net/s2/M00/F2/34/wKgBpU8FGpeYFHvFAAJ5M5jQ6B099.jpeg?imageMogr2%2Fthumbnail%2F%21235x150r%2Fgravity%2FCenter%2Fcrop%2F%21235x150%2Fquality%2F100"
                                     width="235" height="150">
                                <span class="num num-top">1</span>
                                <div class="info">
                                    <h3>六榕寺</h3>
                                    <span><em>1341</em>人去过</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="/poi/7399291.html" target="_blank" title="琶洲塔">
                                <img src="http://p2-q.mafengwo.net/s7/M00/5B/5F/wKgB6lQ3szOAd5pVACDZmFf8kr895.jpeg?imageMogr2%2Fthumbnail%2F%21235x150r%2Fgravity%2FCenter%2Fcrop%2F%21235x150%2Fquality%2F100"
                                     width="235" height="150">
                                <span class="num num-top">2</span>
                                <div class="info">
                                    <h3>琶洲塔</h3>
                                    <span><em>125</em>人去过</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="/poi/6625097.html" target="_blank" title="雁塔公园">
                                <img src="http://b2-q.mafengwo.net/s6/M00/A8/AF/wKgB4lKJkimAI7UjAAEXaIrnncg59.jpeg?imageMogr2%2Fthumbnail%2F%21235x150r%2Fgravity%2FCenter%2Fcrop%2F%21235x150%2Fquality%2F100"
                                     width="235" height="150">
                                <span class="num num-top">3</span>
                                <div class="info">
                                    <h3>雁塔公园</h3>
                                    <span><em>40</em>人去过</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="/poi/6329296.html" target="_blank" title="仙姑塔">
                                <img src="http://n2-q.mafengwo.net/s6/M00/0A/E1/wKgB4lJIZAmAL65yAAGNysuGcYs30.jpeg?imageMogr2%2Fthumbnail%2F%21235x150r%2Fgravity%2FCenter%2Fcrop%2F%21235x150%2Fquality%2F100"
                                     width="235" height="150">
                                <span class="num num-top">4</span>
                                <div class="info">
                                    <h3>仙姑塔</h3>
                                    <span><em>22</em>人去过</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="/poi/33590056.html" target="_blank" title="广州塔摩天轮">
                                <img src="http://b3-q.mafengwo.net/s9/M00/00/12/wKgBs1gYsPOAbhHXAALo08HLVUg00.jpeg?imageMogr2%2Fthumbnail%2F%21235x150r%2Fgravity%2FCenter%2Fcrop%2F%21235x150%2Fquality%2F100"
                                     width="235" height="150">
                                <span class="num num-top">5</span>
                                <div class="info">
                                    <h3>广州塔摩天轮</h3>
                                    <span><em>54</em>人去过</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="/poi/33591408.html" target="_blank" title="水博苑">
                                <img src="http://p4-q.mafengwo.net/s12/M00/D1/BC/wKgED1vEjV-AAzpKAANYl1IXPDU06.jpeg?imageMogr2%2Fthumbnail%2F%21235x150r%2Fgravity%2FCenter%2Fcrop%2F%21235x150%2Fquality%2F100"
                                     width="235" height="150">
                                <span class="num num-top">6</span>
                                <div class="info">
                                    <h3>水博苑</h3>
                                    <span><em>1</em>人去过</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="/poi/34888780.html" target="_blank" title="450米观景平台">
                                <img src="http://p3-q.mafengwo.net/s12/M00/64/73/wKgED1v3ZrOAXNM4ADcYGZz2I_E99.jpeg?imageMogr2%2Fthumbnail%2F%21235x150r%2Fgravity%2FCenter%2Fcrop%2F%21235x150%2Fquality%2F100"
                                     width="235" height="150">
                                <span class="num num-top">7</span>
                                <div class="info">
                                    <h3>450米观景平台</h3>
                                    <span><em>1</em>人去过</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="/poi/34888832.html" target="_blank" title="广州塔广场">
                                <img src="http://n1-q.mafengwo.net/s12/M00/AC/6B/wKgED1xhh7yAbWq0ADWarQLHAqw52.jpeg?imageMogr2%2Fthumbnail%2F%21235x150r%2Fgravity%2FCenter%2Fcrop%2F%21235x150%2Fquality%2F100"
                                     width="235" height="150">
                                <span class="num num-top">8</span>
                                <div class="info">
                                    <h3>广州塔广场</h3>
                                    <span><em>34</em>人去过</span>
                                </div>
                            </a>
                        </li>

                    </ul>
                </div>
                <div class="more more-subpoi">
                    <a class="btn-subpoi" data-page="1">查看更多</a>
                </div>
            </div>
            <style>
                .mod-innerScenic .more {
                    margin-top: 20px;
                    text-align: center;
                }

                .mod-innerScenic .more a {
                    display: inline-block;
                    width: 160px;
                    height: 50px;
                    background-color: #fff;
                    border: 1px solid #fc9c27;
                    line-height: 50px;
                    color: #ff9d00;
                    font-size: 14px;
                    border-radius: 4px;
                    text-align: center;
                }

                .mod-innerScenic .num {
                    width: 40px;
                }
            </style>
        </div>
    </div>
    <!-- 内部景点 E -->

</div>

<!--评论-->
<div data-anchor="commentlist">

    <div id="pagelet-block-15f9d6d9ad9f6c363d2d27120e8a6198" class="pagelet-block"
         data-api=":poi:pagelet:poiCommentListApi" data-params="{&quot;poi_id&quot;:&quot;25091&quot;}" data-async="1"
         data-controller="/js/poi/ControllerPoiComment">
        <div class="mod mod-reviews" data-cs-p="评论列表">
            <div class="mhd mhd-large">蜂蜂点评<span>（共有<em>5592</em>条真实评价）</span></div>
            <div class="review-nav">
                <ul class="clearfix">
                    <li data-type="0" data-category="0" class="on"><span class="divide"></span><a
                            href="javascript:void(0);"><span>全部</span></a></li>
                    <li data-type="0" data-category="2" class="">
                        <span class="divide"></span>
                        <a href="javascript:void(0);"><span>有图</span><span class="num"> (1356条)</span></a>
                    </li>
                    <li data-type="1" data-category="13" class="">
                        <span class="divide"></span>
                        <a href="javascript:void(0);">
                            <span>好评</span>
                            <span class="num">（4749条）</span>
                        </a>
                    </li>
                    <li data-type="1" data-category="12" class="">
                        <span class="divide"></span>
                        <a href="javascript:void(0);">
                            <span>中评</span>
                            <span class="num">（750条）</span>
                        </a>
                    </li>
                    <li data-type="1" data-category="11" class="">
                        <span class="divide"></span>
                        <a href="javascript:void(0);">
                            <span>差评</span>
                            <span class="num">（93条）</span>
                        </a>
                    </li>
                    <li data-type="2" data-category="178487584" class="">
                        <span class="filter-word divide"></span>
                        <a href="javascript:void(0);">标志性建筑<span class="num">（535人提及）</span></a>
                    </li>
                    <li data-type="2" data-category="103125314" class="">
                        <span class="filter-word divide"></span>
                        <a href="javascript:void(0);">人很多<span class="num">（141人提及）</span></a>
                    </li>
                    <li data-type="2" data-category="183864017" class="">
                        <span class="filter-word divide"></span>
                        <a href="javascript:void(0);">值得去<span class="num">（118人提及）</span></a>
                    </li>
                    <li data-type="2" data-category="104277092" class="">
                        <span class="filter-word divide"></span>
                        <a href="javascript:void(0);">恐高<span class="num">（112人提及）</span></a>
                    </li>
                    <li data-type="2" data-category="176664633" class="">
                        <span class="filter-word divide"></span>
                        <a href="javascript:void(0);">交通方便<span class="num">（92人提及）</span></a>
                    </li>
                    <li data-type="0" data-category="1" class="">
                        <span class="divide"></span>
                        <a href="javascript:void(0);"><span>金牌点评</span><span class="num"> (33条)</span></a>
                    </li>
                </ul>
            </div>
            <div class="loading-img" style="display: none;"><img
                    src="http://images.mafengwo.net/images/weng/loading3.gif"> Loading...
            </div>
            <div class="_j_commentlist">
                <div class="rev-list">
                    <ul>

                        <#list commends as c>
                        <li class="rev-item comment-item clearfix">
                            <div class="user"><a class="avatar" href="${c.headUrl!}" target="_blank"><img
                                    src="http://b1-q.mafengwo.net/s10/M00/3D/07/wKgBZ1neKm2AcOhrAB5yLMoWpHI47.jpeg?imageMogr2%2Fthumbnail%2F%2148x48r%2Fgravity%2FCenter%2Fcrop%2F%2148x48%2Fquality%2F90"
                                    width="48" height="48"></a><span class="level">LV.${c.level}</span></div>
                            <a class="useful" data-id="191407415" title="点赞">
                                <i></i><span class="useful-num">5</span>
                            </a>
                            <a class="name" href="/u/52068941.html" target="_blank">${c.username!}</a>
                            <span class="s-star s-star"></span>
                            <p class="rev-txt">${c.content}
                            </p>


                            <div class="rev-img">
                                <#list s.imgS as img>
                                    <a href="/photo/poi/25091_440424016.html" target="_blank"><img
                                            src="${img}"
                                            width="200" height="120"></a>
                                </#list>
                            </div>

                            <div class="info clearfix">
                                <a class="btn-comment _j_comment" title="添加评论">评论</a>
                                <span class="time">${(c.createTime?string('yyyy-MM-dd HH:ss'))!}</span>
                                <span class="from">
                                此条点评来自游记<a href="/i/12601474.html" target="_blank">《南国花城正月中 ——2019年春节广州记（含长...》</a>
                            </span>
                            </div>

                        </li>
                        </#list>













                        <li class="rev-item comment-item clearfix">
                            <div class="user"><a class="avatar" href="/u/52068941.html" target="_blank"><img
                                    src="http://b1-q.mafengwo.net/s10/M00/3D/07/wKgBZ1neKm2AcOhrAB5yLMoWpHI47.jpeg?imageMogr2%2Fthumbnail%2F%2148x48r%2Fgravity%2FCenter%2Fcrop%2F%2148x48%2Fquality%2F90"
                                    width="48" height="48"></a><span class="level">LV.21</span></div>
                            <a class="useful" data-id="191407415" title="点赞">
                                <i></i><span class="useful-num">1</span>
                            </a>
                            <a class="name" href="/u/52068941.html" target="_blank">红袖·紫月</a>
                            <span class="s-star s-star4"></span>
                            <p class="rev-txt">这个广州地标建筑，外观还是挺漂亮的，小蛮腰！<br>
                                至于登塔后的景色，我觉得在高处看，所有城市的景色真的都差不多，所以并没有多少惊艳的感觉，塔顶的摩天轮可以坐坐，起码可以360度看看整个城市的景色，能看得到下面的珠江。<br>
                                至于什么白云观景大厅，星空观景大厅，真是挺无聊的，只不过是屋顶的天棚是蓝天白云的装饰风格和星空的装饰风格而已，人很多，闹哄哄的，然后必须吐槽一下照片，登塔的时候在绿幕前给拍张照片，然后电脑合成一下，一张要120元，2张160元，这种最简单的换背景PS技术，卖这个价格我也是服气的。吐糟吐糟！
                            </p>



                            <div class="rev-img">
                                <a href="/photo/poi/25091_440424016.html" target="_blank"><img
                                        src="http://b4-q.mafengwo.net/s13/M00/7F/60/wKgEaVzNb_-AETiXAAIHC_tV0Bk20.jpeg?imageMogr2%2Fthumbnail%2F%21200x120r%2Fgravity%2FCenter%2Fcrop%2F%21200x120%2Fquality%2F100"
                                        width="200" height="120"></a>
                                <a href="/photo/poi/25091_440424020.html" target="_blank"><img
                                        src="http://p1-q.mafengwo.net/s13/M00/7F/48/wKgEaVzNb-yAYCdiAAHGj47nch011.jpeg?imageMogr2%2Fthumbnail%2F%21200x120r%2Fgravity%2FCenter%2Fcrop%2F%21200x120%2Fquality%2F100"
                                        width="200" height="120"></a>
                                <a href="/photo/poi/25091_440424024.html" target="_blank"><img
                                        src="http://n1-q.mafengwo.net/s13/M00/7F/5B/wKgEaVzNb_yAQ4xpAAFC3Mtb4Tw62.jpeg?imageMogr2%2Fthumbnail%2F%21200x120r%2Fgravity%2FCenter%2Fcrop%2F%21200x120%2Fquality%2F100"
                                        width="200" height="120"></a>
                                <a href="/photo/poi/25091_440424028.html" target="_blank"><img
                                        src="http://n1-q.mafengwo.net/s13/M00/7F/5C/wKgEaVzNb_2AKAeeAAFvNsV4KkE69.jpeg?imageMogr2%2Fthumbnail%2F%21200x120r%2Fgravity%2FCenter%2Fcrop%2F%21200x120%2Fquality%2F100"
                                        width="200" height="120"></a>
                            </div>

                            <div class="info clearfix">
                                <a class="btn-comment _j_comment" title="添加评论">评论</a>
                                <span class="time">2019-05-04 19:48:40</span>
                                <span class="from">
                                此条点评来自游记<a href="/i/12601474.html" target="_blank">《南国花城正月中 ——2019年春节广州记（含长...》</a>
                            </span>
                            </div>

                            <div class="comment add-reply ">
                                <ul class="more_reply_box comment_list">
                                    <li>
                                        <a href="/u/63107989.html" target="_blank">
                                            <img src="http://b4-q.mafengwo.net/s9/M00/DA/0D/wKgBs1g919mAVdRVAACpgsqFw_Y38.jpeg?imageMogr2%2Fthumbnail%2F%2116x16r%2Fgravity%2FCenter%2Fcrop%2F%2116x16%2Fquality%2F90"
                                                 width="16" height="16">🍓辣辣辣辣王🍓
                                        </a>
                                        ：摩天轮一圈多长时间啊
                                        <a class="_j_reply re_reply" data-id="625204" data-uid="63107989"
                                           data-username="🍓辣辣辣辣王🍓" title="添加回复">回复</a>
                                        <br><span class="time">2019-06-18 22:44:18</span>
                                    </li>
                                    <li>
                                        <a href="/u/52068941.html" target="_blank">
                                            <img src="http://b1-q.mafengwo.net/s10/M00/3D/07/wKgBZ1neKm2AcOhrAB5yLMoWpHI47.jpeg?imageMogr2%2Fthumbnail%2F%2116x16r%2Fgravity%2FCenter%2Fcrop%2F%2116x16%2Fquality%2F90"
                                                 width="16" height="16">红袖·紫月
                                        </a>
                                        回复🍓辣辣辣辣王🍓：十几分钟吧
                                        <a class="_j_reply re_reply" data-id="638560" data-uid="52068941"
                                           data-username="红袖·紫月" title="添加回复">回复</a>
                                        <br><span class="time">2019-07-01 14:18:01</span>
                                    </li>
                                </ul>

                                <div class="add-comment hide reply-form">
                                    <textarea class="comment_reply" data-comment_id="191407415"
                                              data-comment_username="红袖·紫月" data-poi_id="25091" data-poi_name="广州塔"
                                              data-parent_id="" data-parent_uid="" data-parent_username=""
                                              style="overflow: hidden; color: rgb(204, 204, 204);"></textarea>
                                    <a class="btn btn_submit_reply">回复</a>
                                </div>
                            </div>
                        </li>
                        <li class="rev-item comment-item clearfix">
                            <div class="user"><a class="avatar" href="/u/36004548.html" target="_blank"><img
                                    src="http://n3-q.mafengwo.net/s12/M00/35/6C/wKgED1uqIpCARLIhAAAZUeRPlFM676.png?imageMogr2%2Fthumbnail%2F%2148x48r%2Fgravity%2FCenter%2Fcrop%2F%2148x48%2Fquality%2F90"
                                    width="48" height="48"></a><span class="level">LV.1</span></div>
                            <a class="useful" data-id="191246949" title="点赞">
                                <i></i><span class="useful-num">8</span>
                            </a>
                            <a class="name" href="/u/36004548.html" target="_blank">马蜂窝用户</a>
                            <span class="s-star s-star4"></span>
                            <p class="rev-txt">广州塔–<br>
                                广州多年的地标<br>
                                可先前往广州地铁博物馆参观<br>
                                再乘坐有轨电车🚃🚃🚃<br>
                                经过7.7公里长的草地轨道<br>
                                沿着江边看风景<br>
                                珠江新城CBD<br>
                                还有猎德大桥，会展中心，琶洲塔，琶洲大桥，广州塔…轻轨一程只要3块~</p>


                            <div class="rev-img">
                                <a href="/photo/poi/25091_436117704.html" target="_blank"><img
                                        src="http://p1-q.mafengwo.net/s13/M00/A2/04/wKgEaVy59ESAZ-2YAE20ErQS7n402.jpeg?imageMogr2%2Fthumbnail%2F%21200x120r%2Fgravity%2FCenter%2Fcrop%2F%21200x120%2Fquality%2F100"
                                        width="200" height="120"></a>
                                <a href="/photo/poi/25091_436117708.html" target="_blank"><img
                                        src="http://b3-q.mafengwo.net/s13/M00/A2/10/wKgEaVy59EqAa46nALaTkBmUrKM86.jpeg?imageMogr2%2Fthumbnail%2F%21200x120r%2Fgravity%2FCenter%2Fcrop%2F%21200x120%2Fquality%2F100"
                                        width="200" height="120"></a>
                                <a href="/photo/poi/25091_436117712.html" target="_blank"><img
                                        src="http://n3-q.mafengwo.net/s13/M00/A2/0A/wKgEaVy59EaACEUoAHTnueLvQJo60.jpeg?imageMogr2%2Fthumbnail%2F%21200x120r%2Fgravity%2FCenter%2Fcrop%2F%21200x120%2Fquality%2F100"
                                        width="200" height="120"></a>
                                <a href="/photo/poi/25091_436117716.html" target="_blank"><img
                                        src="http://n4-q.mafengwo.net/s13/M00/A1/E0/wKgEaVy59CmAfYX8AHRurxHChUc52.jpeg?imageMogr2%2Fthumbnail%2F%21200x120r%2Fgravity%2FCenter%2Fcrop%2F%21200x120%2Fquality%2F100"
                                        width="200" height="120"></a>
                            </div>

                            <div class="info clearfix">
                                <a class="btn-comment _j_comment" title="添加评论">评论</a>
                                <span class="time">2019-04-20 00:16:10</span>
                                <span class="from">
                                此条点评来自<a href="/app/intro/gonglve.php" target="_blank">马蜂窝旅游APP</a>
                            </span>
                            </div>

                            <div class="comment add-reply hide">
                                <ul class="more_reply_box comment_list">
                                </ul>

                                <div class="add-comment hide reply-form">
                                    <textarea class="comment_reply" data-comment_id="191246949"
                                              data-comment_username="马蜂窝用户" data-poi_id="25091" data-poi_name="广州塔"
                                              data-parent_id="" data-parent_uid="" data-parent_username=""
                                              style="overflow: hidden; color: rgb(204, 204, 204);"></textarea>
                                    <a class="btn btn_submit_reply">回复</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>

                <div align="right" class="m-pagination">
                    <span class="count">共<span>5</span>页 / <span>5592</span>条</span>

                    <span class="pg-current">1</span>
                    <a class="pi" data-page="2" rel="nofollow" title="第2页">2</a>
                    <a class="pi" data-page="3" rel="nofollow" title="第3页">3</a>
                    <a class="pi" data-page="4" rel="nofollow" title="第4页">4</a>
                    <a class="pi" data-page="5" rel="nofollow" title="第5页">5</a>

                    <a class="pi pg-next" data-page="2" rel="nofollow" title="后一页">后一页</a>
                    <a class="pi pg-last" data-page="5" rel="nofollow" title="末页">末页</a>
                </div>
            </div>
        </div>
    </div>
</div>


<div data-anchor="comment">
    <div class="row row-reviewForm" id="comment_20190714202243" data-cs-p="点评">
        <div class="wrapper">

            <div class="mfw-reviews">
                <div id="_j_commentform_cnt">
                    <h2>
                        <strong>广州塔</strong>
                        <em>*</em>为必填选项
                    </h2>
                    <form action="/scenic/addComment" method="post" class="_j_commentdialogform" data-typeid="3">
                        <input type="hidden" name="scenicId" value="${sc.id}">
                        <input type="hidden" name="replyComment.id" value="refCommentId">
                        <input type="hidden" name="currentPage" id="currentPage" value="1">

                        <div class="review-item item-star">
                            <div class="label"><em>*</em>总体评价</div>
                            <div class="review-star _j_rankblock" data-star="" name="rank">
                                <input type="hidden" name="rank" value="" essential="1" data-inputname="总体评价">
                                <span class="_j_starcount star0"></span>
                                <div class="click-star _j_starlist">
                                    <a role="button" title="千万别去" rel="nofollow"></a>
                                    <a role="button" title="不推荐" rel="nofollow"></a>
                                    <a role="button" title="一般般" rel="nofollow"></a>
                                    <a role="button" title="值得一去" rel="nofollow"></a>
                                    <a role="button" title="必须推荐" rel="nofollow"></a>
                                </div>

                            </div>
                            <span class="txt-tips _j_startip">点击星星打分</span>
                        </div>

                        <div class="review-item item-comment">
                            <div class="label"><em>*</em>评价</div>
                            <div class="content">
                                <textarea class="_j_commentarea" name="content" essential="1" data-inputname="点评内容"
                                          placeholder="详细、客观、真实，130字以上为佳！上传图片会加分哦！" data-minlen="15"
                                          data-maxlen="1000"></textarea>
                                <p class="_j_commentcounttip">15-1000个字</p>
                            </div>
                        </div>

                        <div class="review-item item-photo">
                            <div class="label">上传照片</div>
                            <div class="content">
                                <div>
                                    <input type="hidden"  class="form-control" id="coverUrl"  name="pic" >
                                    <img src="" width="100px" id="imgUrl">
                                </div>
                            </div>
                        </div>
                        <div class="review-item item-action">
                            <a class="btn-large _j_submit" role="button" id="commentBtn" title="提交点评">提交点评</a>
                        </div>
                    </form>
                </div
            </div>
			 <script type="text/javascript">
                // 星星数
			   $(function () {
                   $("._j_starlist a").mouseover(function () {
                        var index = $(this).index()+1;
                        var text = $(this).attr("title");

                       $(this).closest("div").prev().addClass("star"+index);
                       $(this).closest("div").parent().next().html(text);
                   }).mouseout(function () {
                       var index = $(this).index()+1;
                       var text = $(this).attr("title");
                       $(this).closest("div").prev().removeClass("star"+index);
                       $(this).closest("div").parent().next().html(text);

                       var x = $(this).closest("div").prev().prev().val();
                       if(x == index){
                           $(this).closest("div").prev().addClass("star"+x);
                       }


                   }).click(function () {
                       var index = $(this).index()+1;
                       var text = $(this).attr("title");
                       $(this).closest("div").prev().addClass("star"+index);
                       $(this).closest("div").parent().next().html(text);

                       $(this).closest("div").prev().prev().val(index);

                   })
               })

			</script>
           


            <div class="mfw-reviews have-reviews" style="display: none">
                <h2>
                    <strong>广州塔</strong>
                </h2>
                <div class="review-item item-star">
                    <div class="label">你已评价为</div>
                    <div class="review-star">
                        <span class="star0"></span>
                    </div>
                    <a class="edit-reviews" data-commentid="" title="修改评论"><i></i>我要修改</a>
                </div>
            </div>
        </div>
    </div>
    
</div>



</div>








<!----------------------------------------->
<div id="footer">
	<div class="ft-content" style="width: 1105px">
		<div class="ft-info clearfix">
			<dl class="ft-info-col ft-info-intro">
				<dt>马蜂窝旅游网</dt>
				<dd>叩丁狼是一家专注于培养高级IT技术人才，为学员提供定制化IT职业规划方案及</dd>
				<dd>意见咨询服务的教育科技公司，为您提供海量优质课程，以及创新的线上线下学</dd>
				<dd>习体验，帮助您获得全新的个人发展和能力提升。</dd>
			</dl>
			<dl class="ft-info-col ft-info-qrcode">
				<dd>
					<span class="ft-qrcode-tejia"></span>
				</dd>
				<dd>
					<span class="ft-qrcode-weixin"></span>
				</dd>
				<dd>
					<span class="ft-qrcode-weixin" style="background-image: url('https://p3-q.mafengwo.net/s10/M00/48/A9/wKgBZ1t_4sSAVJ6uAAAlzJ0PZgU881.png?imageMogr2%2Fthumbnail%2F%2194x90r%2Fgravity%2FCenter%2Fcrop%2F%2194x90%2Fquality%2F90')"></span>
				</dd>
			</dl>
			<dl class="ft-info-social">
				<dt>向崇尚自由的加勒比海盗致敬！</dt>
				<dd>
					<a class="ft-social-weibo" target="_blank" href="javascript:;" rel="nofollow"><i
							class="ft-social-icon"></i></a>
					<a class="ft-social-qqt" target="_blank" href="javascript:;" rel="nofollow"><i
							class="ft-social-icon"></i></a>
					<a class="ft-social-qzone" target="_blank" href="javascript:;" rel="nofollow"><i
							class="ft-social-icon"></i></a>
				</dd>
			</dl>
		</div>

		<div class="ft-links">
			<a target="_blank" href="http://china.makepolo.com/">马可波罗</a><a target="_blank" href="http://www.onlylady.com/">Onlylady女人志</a><a target="_blank" href="http://trip.elong.com/">艺龙旅游指南</a><a target="_blank" href="http://www.cncn.com">欣欣旅游网</a>
			<a target="_blank" href="http://www.8264.com/">户外运动</a><a target="_blank" href="http://www.yue365.com/">365音乐网</a><a target="_blank" href="http://ishare.iask.sina.com.cn/">爱问共享资料</a><a target="_blank" href="http://www.uzai.com/">旅游网</a>
			<a target="_blank" href="http://www.zongheng.com/">小说网</a>
			<a target="_blank" href="http://www.xuexila.com/">学习啦</a><a target="_blank" href="http://www.yododo.com">游多多自助游</a><a target="_blank" href="http://www.zhcpic.com/">问答</a><a target="_blank" href="http://huoche.mafengwo.cn/">火车时刻表</a>
			<a target="_blank" href="http://www.lvmama.com">驴妈妈旅游网</a>
			<a target="_blank" href="http://www.haodou.com/">好豆美食网</a><a target="_blank" href="http://www.taoche.com/">二手车</a><a target="_blank" href="http://www.lvye.cn">绿野户外</a><a target="_blank" href="http://www.tuniu.com/">途牛旅游网</a>
			<a target="_blank" href="http://www.mapbar.com/">图吧</a>
			<a target="_blank" href="http://www.chnsuv.com">SUV联合越野</a><a target="_blank" href="http://www.uc.cn/">手机浏览器</a><a target="_blank" href="http://sh.city8.com/">上海地图</a><a target="_blank" href="http://www.tianqi.com/">天气预报查询</a>
			<a target="_blank" href="http://www.ly.com/">同程旅游</a>
			<a target="_blank" href="http://www.tieyou.com/">火车票</a><a target="_blank" href="http://www.yunos.com/">YunOS</a><a target="_blank" href="http://you.ctrip.com/">携程旅游</a><a target="_blank" href="http://www.jinjiang.com">锦江旅游</a>
			<a target="_blank" href="http://www.huoche.net/">火车时刻表</a>
			<a target="_blank" href="http://www.tripadvisor.cn/">TripAdvisor</a><a target="_blank" href="http://www.tianxun.com/">天巡网</a><a target="_blank" href="http://www.mayi.com/">短租房</a><a target="_blank" href="http://www.zuzuche.com">租租车</a>
			<a target="_blank" href="http://www.5fen.com/">五分旅游网</a>
			<a target="_blank" href="http://www.zhuna.cn/">酒店预订</a><a target="_blank" href="http://www.ailvxing.com">爱旅行网</a><a target="_blank" href="http://360.mafengwo.cn/all.php">旅游</a><a target="_blank" href="http://vacations.ctrip.com/">旅游网</a>
			<a target="_blank" href="http://www.wed114.cn">wed114结婚网</a>
			<a target="_blank" href="http://www.chexun.com/">车讯网</a><a target="_blank" href="http://www.aoyou.com/">遨游旅游网</a><a target="_blank" href="http://www.91.com/">手机</a>
			<a href="javascript:;" target="_blank">更多友情链接&gt;&gt;</a>
		</div>
	</div>
</div>
<div class="mfw-toolbar" id="_j_mfwtoolbar" style="display: block;">
	<div class="toolbar-item-top" style="display: none;">
		<a role="button" class="btn _j_gotop">
			<i class="icon_top"></i>
			<em>返回顶部</em>
		</a>
	</div>
	<div class="toolbar-item-feedback">
		<a role="button" data-japp="feedback" class="btn">
			<i class="icon_feedback"></i>
			<em>意见反馈</em>
		</a>
	</div>
	<div class="toolbar-item-code">
		<a role="button" class="btn">
			<i class="icon_code"></i>
		</a>
		<a role="button" class="mfw-code _j_code">


			<img src="https://p1-q.mafengwo.net/s1/M00/6C/51/wKgIC1t_6TuASybrAADGUPUHjr021.jpeg?imageMogr2%2Fthumbnail%2F%21450x192r%2Fgravity%2FCenter%2Fcrop%2F%21450x192%2Fquality%2F90"
				width="450" height="192">
		</a>
		<!--<div class="wx-official-pop"><img src="http://images.mafengwo.net/images/qrcode-weixin.gif"><i class="_j_closeqrcode"></i></div>-->
	</div>

</div>


</body>

</html>