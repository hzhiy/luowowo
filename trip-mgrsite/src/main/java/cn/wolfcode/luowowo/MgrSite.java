package cn.wolfcode.luowowo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by zhi on 2019/8/8.
 */
@SpringBootApplication
@EnableScheduling // 启动定时任务支持
public class MgrSite {


    public static void main(String[] args) {
        SpringApplication.run(MgrSite.class, args);
    }
}
