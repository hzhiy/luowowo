package cn.wolfcode.luowowo.article.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserTravelStatisVo {
    private int TravelCount;
    private int ReplyNum;
    private int ViewNum;
}
