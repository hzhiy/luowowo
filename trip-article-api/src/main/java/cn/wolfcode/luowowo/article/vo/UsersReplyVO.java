package cn.wolfcode.luowowo.article.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * 用户回复VO
 */
@Getter
@Setter
public class UsersReplyVO {
    String toid; // 评论id
    Long fromId; // 传过来的被回复的用户id
    String fromName; // 被回复用户名
    String headUrl; // 被回复用户头像
    String content; // 传过来的回复内容
    int type; // 回复的类型
}
