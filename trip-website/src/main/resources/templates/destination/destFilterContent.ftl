<#list pageInfo.list as dest>
<li class="item">
    <div class="img">
        <a href="/destination/guide?id=${(dest.id)!}" target="_blank"><img height="200" width="320" src="${(dest.coverUrl)!}" style="display: inline;">
            <div class="title">${dest.name}</div>
        </a>
    </div>
    <div class="info">
        <p class="detail">${dest.info}</p>
        <div class="hot">
            <span class="label">TOP3</span>
            <a href="javascript:;" target="_blank">蓝梦岛</a>
            <span class="divide"></span>
            <a href="javascript:;" target="_blank">库塔海滩</a>
            <span class="divide"></span>
            <a href="javascript:;" target="_blank">情人崖</a>
        </div>

        <div class="line"><a href="javascript:;" target="_blank"><em>1</em>巴厘岛新晋网红5日线路</a></div>
    </div>
</li>
</#list>
