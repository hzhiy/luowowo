package cn.wolfcode.luowowo.cache.service.impl;

import cn.wolfcode.luowowo.article.domain.Question;
import cn.wolfcode.luowowo.article.service.IQuestionSevice;
import cn.wolfcode.luowowo.cache.emum.RedisKeys;
import cn.wolfcode.luowowo.cache.service.IQuestionAnswerStatisRedisService;
import cn.wolfcode.luowowo.cache.vo.QuestionAnswerStatisVO;
import cn.wolfcode.luowowo.cache.vo.QuestionStatisVO;
import cn.wolfcode.luowowo.comment.domain.QuestionAnswer;
import cn.wolfcode.luowowo.comment.service.IQuestionAnswerService;
import cn.wolfcode.luowowo.common.util.DateUtil;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.member.service.IUserInfoService;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import sun.awt.geom.AreaOp;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by Administrator on 2019/8/22.
 */
@Service
public class QuestionAnswerStatisRedisServiceImpl implements IQuestionAnswerStatisRedisService {

    public static final int DAY_TYPE = 0;
    public static final int WEEK_TYPE = 1;
    public static final int MONTH_TYPE = 2;

    public static final int RANK_TYPE_GOLD = 0;
    public static final int RANK_TYPE_ANSWER = 1;
    public static final int RANK_TYPE_THUMBSUP = 2;


    @Autowired
    private StringRedisTemplate template;

    @Reference
    private IQuestionSevice questionSevice;

    @Reference
    private IQuestionAnswerService questionAnswerService;

    @Reference
    private IUserInfoService userInfoService;

    @Override
    public void answernumIncrease(Long qid, int i,int type) {
      numIncrease(qid,i,type);
    }


    private void numIncrease(Long qid,int i,int type){
        String key = RedisKeys.QUESTION_STATIC_VO.join(qid.toString());
        QuestionStatisVO vo = this.getQuestionStatisVO(qid);
        switch (type){
            case QuestionStatisVO.QUESTION_VIEWNUM:
                vo.setViewnum(vo.getViewnum()+i);
                break;
            case QuestionStatisVO.QUESTION_ANSWERNUM:
                vo.setAnswernum(vo.getAnswernum()+i);
                break;
            case QuestionStatisVO.QUESTION_SHARENUM:
                vo.setSharenum(vo.getSharenum()+i);
                break;
            case QuestionStatisVO.QUESTION_THUMBSUPNUM:
                vo.setThumbsupnum(vo.getThumbsupnum()+i);
                break;
        }
        template.opsForValue().set(key,JSON.toJSONString(vo));
    }



    @Override
    public QuestionStatisVO getQuestionStatisVO(Long qid) {
        String key = RedisKeys.QUESTION_STATIC_VO.join(qid.toString());
        QuestionStatisVO vo = null;
        if (template.hasKey(key)){
            String voStr = template.opsForValue().get(key);
            vo = JSON.parseObject(voStr, QuestionStatisVO.class);

        } else{
            vo = new QuestionStatisVO();
            Question question = questionSevice.get(qid);

            try {
                BeanUtils.copyProperties(vo,question);
                vo.setQuestionId(question.getId());

            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return vo;
    }




    @Override
    public boolean answerThumbsup(String aid, Long uid) {
        String key = RedisKeys.ANSWER_STATIS_THUMBSUP.join(aid,uid.toString());
        if (template.hasKey(key)){
            return false;
        }else{
            //mongodb中点赞数+1
            QuestionAnswer answer = questionAnswerService.getById(aid);
            answer.setThumbsupnum(answer.getThumbsupnum()+1);
            questionAnswerService.saveOrUpdate(answer);
            Date now = new Date();
            template.opsForValue().set(key,JSON.toJSONString(now), DateUtil.getDateBetween(now,DateUtil.getEndDate(now)), TimeUnit.SECONDS);
            return true;
        }
    }

    @Override
    public void userAnswersIncrease(Long id, int i) {
        String dayKey = RedisKeys.QUESTION_ANSWER_STATIS_VO_DAY.join(id.toString());
        questionAnswerIncrease(dayKey,id,3);
        String weekKey = RedisKeys.QUESTION_ANSWER_STATIS_VO_WEEK.join(id.toString());
        questionAnswerIncrease(weekKey,id,5);
        String monthKey = RedisKeys.QUESTION_ANSWER_STATIS_VO_MONTH.join(id.toString());
        questionAnswerIncrease(monthKey,id,7);

    }

    @Override
    public void userThumbsupIncrease(Long id, int i) {
        String dayKey = RedisKeys.QUESTION_THUMBSUP_STATIS_VO_DAY.join(id.toString());
        questionAnswerIncrease(dayKey,id,i);
        String weekKey = RedisKeys.QUESTION_THUMBSUP_STATIS_VO_WEEK.join(id.toString());
        questionAnswerIncrease(weekKey,id,i);
        String monthKey = RedisKeys.QUESTION_THUMBSUP_STATIS_VO_MONTH.join(id.toString());
        questionAnswerIncrease(monthKey,id,i);
    }

    private void questionAnswerIncrease(String key, long id, int i){
        QuestionAnswerStatisVO vo = null;
        if (template.hasKey(key)){

            String voStr = template.opsForValue().get(key);
            vo = JSON.parseObject(voStr, QuestionAnswerStatisVO.class);
            vo.setCount(vo.getCount()+i);

        } else{
            vo = new QuestionAnswerStatisVO();
            UserInfo userInfo = userInfoService.get(id);
            vo.setUserId(id);
            vo.setHeadImgUrl(userInfo.getHeadImgUrl());
            vo.setNickname(userInfo.getNickname());
            vo.setLevel(userInfo.getLevel());
            vo.setCount(1);
        }
        template.opsForValue().set(key,JSON.toJSONString(vo));
    }

    @Override
    public void addAnswerScore(Long id,int score) {
       String dayKey = RedisKeys.QUESTION_ANSWER_STATIS_VO_DAY.join(id.toString());
       String weekKey = RedisKeys.QUESTION_ANSWER_STATIS_VO_WEEK.join(id.toString());
       String monthKey = RedisKeys.QUESTION_ANSWER_STATIS_VO_MONTH.join(id.toString());
       template.opsForZSet().incrementScore(RedisKeys.QUESTION_ANSWER_STATIS_VO_DAY_SORT.getPrefix(),dayKey,4);
       template.opsForZSet().incrementScore(RedisKeys.QUESTION_ANSWER_STATIS_VO_WEEK_SORT.getPrefix(),weekKey,10);
       template.opsForZSet().incrementScore(RedisKeys.QUESTION_ANSWER_STATIS_VO_MONTH_SORT.getPrefix(),monthKey,11);
    }

    @Override
    public void addThumbsupScore(Long id,int score) {
        String dayKey = RedisKeys.QUESTION_THUMBSUP_STATIS_VO_DAY.join(id.toString());
        String weekKey = RedisKeys.QUESTION_THUMBSUP_STATIS_VO_WEEK.join(id.toString());
        String monthKey = RedisKeys.QUESTION_THUMBSUP_STATIS_VO_MONTH.join(id.toString());
        template.opsForZSet().incrementScore(RedisKeys.QUESTION_THUMBSUP_STATIS_VO_DAY_SORT.getPrefix(),dayKey,1);
        template.opsForZSet().incrementScore(RedisKeys.QUESTION_THUMBSUP_STATIS_VO_WEEK_SORT.getPrefix(),weekKey,7);
        template.opsForZSet().incrementScore(RedisKeys.QUESTION_THUMBSUP_STATIS_VO_MONTH_SORT.getPrefix(),monthKey,9);
    }

    @Override
    public List<QuestionAnswerStatisVO> queryForList(int type, int rank) {
        List<QuestionAnswerStatisVO> list = new ArrayList<>();
        switch (type){
            case DAY_TYPE:
                switch (rank){
                    case RANK_TYPE_GOLD:
                        break;
                    case RANK_TYPE_ANSWER:
                        Set<String> strings = template.opsForZSet().reverseRange(RedisKeys.QUESTION_ANSWER_STATIS_VO_DAY_SORT.getPrefix(), 0, 9);
                        for (String string : strings) {
                            String voStr = template.opsForValue().get(string);
                            QuestionAnswerStatisVO vo = JSON.parseObject(voStr, QuestionAnswerStatisVO.class);
                            list.add(vo);
                        }
                        break;
                    case RANK_TYPE_THUMBSUP:
                        break;
                }
                break;
            case WEEK_TYPE:
                switch (rank){
                    case RANK_TYPE_GOLD:
                        break;
                    case RANK_TYPE_ANSWER:
                        Set<String> strings = template.opsForZSet().reverseRange(RedisKeys.QUESTION_ANSWER_STATIS_VO_WEEK_SORT.getPrefix(), 0, 9);
                        for (String string : strings) {
                            String voStr = template.opsForValue().get(string);
                            QuestionAnswerStatisVO vo = JSON.parseObject(voStr, QuestionAnswerStatisVO.class);
                            list.add(vo);
                        }
                        break;
                    case RANK_TYPE_THUMBSUP:
                        break;
                }
                break;
            case MONTH_TYPE:
                switch (rank){
                    case RANK_TYPE_GOLD:
                        break;
                    case RANK_TYPE_ANSWER:
                        Set<String> strings = template.opsForZSet().reverseRange(RedisKeys.QUESTION_ANSWER_STATIS_VO_MONTH_SORT.getPrefix(), 0, 9);
                        for (String string : strings) {
                            String voStr = template.opsForValue().get(string);
                            QuestionAnswerStatisVO vo = JSON.parseObject(voStr, QuestionAnswerStatisVO.class);
                            list.add(vo);
                        }
                        break;
                    case RANK_TYPE_THUMBSUP:
                        break;
                }
                break;
        }
        return list;

    }

   /* private List<QuestionAnswerStatisVO> getRankByAnswers(int rank,String key) {
        List<QuestionAnswerStatisVO> list = new ArrayList<>();
        switch (rank){
            case RANK_TYPE_GOLD:
                return null;
            case RANK_TYPE_ANSWER:
                Set<String> strings = template.opsForZSet().reverseRange(key, 0, 9);
                for (String string : strings) {
                    String voStr = template.opsForValue().get(string);
                    QuestionAnswerStatisVO vo = JSON.parseObject(voStr, QuestionAnswerStatisVO.class);
                   list.add(vo);
                }
                return list;
            case RANK_TYPE_THUMBSUP:
                return null;
        }

    }
    */

    @Override
    public void setRankList(String key, String value, Integer daynum) {
       template.opsForZSet().add(key,value,daynum);

    }
}
