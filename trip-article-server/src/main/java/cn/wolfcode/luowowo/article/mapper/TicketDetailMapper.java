package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.TicketDetail;

import java.util.List;

public interface TicketDetailMapper {
    int deleteByPrimaryKey(Long id);

    int insert(TicketDetail record);

    TicketDetail selectByPrimaryKey(Long id);

    List<TicketDetail> selectAll();

    int updateByPrimaryKey(TicketDetail record);

    TicketDetail selectByTicketId(Long id);
}