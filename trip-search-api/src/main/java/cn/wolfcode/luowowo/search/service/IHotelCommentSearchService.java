package cn.wolfcode.luowowo.search.service;

import cn.wolfcode.luowowo.hotel.query.HotelCommentQuery;
import cn.wolfcode.luowowo.search.query.SearchQueryObject;
import cn.wolfcode.luowowo.search.template.HotelCommentTemplate;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

/**
 * Created by zhi on 2019/8/17.
 */
public interface IHotelCommentSearchService {

    /**
     * 站内搜索
     * @param index
     * @param type
     * @param clz
     * @param qo
     * @param fields
     * @param <T>
     * @return
     */
    <T> Page<T> searchWithHighlight(String index, String type, Class<T> clz, SearchQueryObject qo, String... fields);

    /**
     * 保存或者修改
     * @param hotelCommentTemplate
     * @return
     */
    HotelCommentTemplate saveOrUpdate(HotelCommentTemplate hotelCommentTemplate);

    /**
     * 搜索词频
     * @param id
     * @return
     */
    List<Map<String,Object>> searchCiPin(Long id);

    /**
     * 按照词频搜索
     * @param qo
     * @return
     */
    Page list(HotelCommentQuery qo);
}

