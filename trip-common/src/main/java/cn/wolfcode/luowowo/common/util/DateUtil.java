package cn.wolfcode.luowowo.common.util;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by zhi on 2019/8/14.
 */
public class DateUtil {

    /**
     * 两日期相差的秒数
     * @param d1
     * @param d2
     * @return
     */
    public static Long getDateBetween(Date d1, Date d2){
        return Math.abs((d1.getTime() - d2.getTime()) / 1000);
    }

    /**
     * 获得当天的最后一秒的时间
     * @param date
     * @return
     */
    public static Date getEndDate(Date date){
        if (date == null){
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23); // HOUR是12小时进制的
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return calendar.getTime();
    }


    public static Date getDate(Date date,int hour,int minnute,int second){
        if (date == null){
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY,hour); // HOUR是12小时进制的
        calendar.set(Calendar.MINUTE, minnute);
        calendar.set(Calendar.SECOND, second);
        return calendar.getTime();
    }

}
