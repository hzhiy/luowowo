<ul class="rank-list _j_rank_list">

    <#list ranks as r>
    <li class="r-top r-top1 clearfix">
        <em class="num">${r_index+1}</em>
        <div class="user no_qid">
            <a class="avatar" href="javascript:;" target="_blank" rel="nofollow"><img
                    src="${r.headImgUrl}"></a>
            <span class="name"><a href="javascript:;" target="_blank"
                                  rel="nofollow">${r.nickname}</a></span>
            <span class="level"><a href="javascript:;" target="_blank"
                                   rel="nofollow">LV.${r.level}</a></span>
        </div>
        <span class="num">${r.count}</span>
    </li>
    </#list>
</ul>