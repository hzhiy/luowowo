package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

/**
 * 景点主题
 */
@Setter
@Getter
public class ScenicTheme extends BaseDomain{

    private String themeName;

    private Long dest_id;

    private String imgUrl;

    private Integer sequence;

}