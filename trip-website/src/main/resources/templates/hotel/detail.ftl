<!DOCTYPE html>
<html>
<head>
    <link href="/styles/base.css" rel="stylesheet" type="text/css">
    <link href="/styles/hotel.css" rel="stylesheet" type="text/css">
    <link href="/styles/travelspot.css" rel="stylesheet" type="text/css">
    <link href="/styles/travelwiki.css" rel=" stylesheet " type="text/css ">
    <link href="/styles/hoteldetail.css" rel=" stylesheet " type="text/css ">

    <#--多图片上传的样式-->
    <link href="/js/plugins/uploadfiy/css/common.css" rel=" stylesheet " type="text/css ">
    <link href="/js/plugins/uploadfiy/css/index.css" rel=" stylesheet " type="text/css ">
    <link href="/js/plugins/jqPaginator/jqPagination.css" rel="stylesheet" type="text/css">
    <script type="text/javascript " src="/js/jQuery-2.1.4/jquery-2.1.4.js"></script>
    <script type="text/javascript" src="/js/system/guide.js"></script>
    <script type="text/javascript" src="/js/plugins/jquery-form/jquery.form.js"></script>
    <script type="text/javascript" src="/js/plugins/jqPaginator/jq-paginator.min.js"></script>
    <script type="text/javascript" src="/js/system/common.js"></script>

    <script>
        $(function () {
            // 加载评论
            $('#searchForm').ajaxForm(function (data) {
                $('#hotelComment').html(data);
            })
            $("#searchForm").submit();


            // 判断图片上传到阿里云了没
            var flag = false;


            // 多图片上传相关
            var delParent;
            var defaults = {
                fileType         : ["jpg","png","bmp","jpeg"],   // 上传文件的类型
                fileSize         : 1024 * 1024 * 10                  // 上传文件的大小 10M
            };
            /*点击图片的文本框*/
            $(".file").change(function(){

                $('#fileForm').ajaxSubmit(function (data) {
                    if (data.status){
                        console.log(data.url);
                        $('input[name=imgstr]').val(data.url);
                    }
                });
                var idFile = $(this).attr("id");
                var file = document.getElementById(idFile);
                var imgContainer = $(this).parents(".z_photo"); //存放图片的父亲元素
                var dd = $('._j_picitem_btn').closest('dd');
                var fileList = file.files; //获取的图片文件
                var input = $(this).parent();//文本框的父亲元素
                var imgArr = [];
                //遍历得到的图片文件
                var numUp = imgContainer.find(".up-section").length;
                var totalNum = numUp + fileList.length;  //总的数量
                if(fileList.length > 20 || totalNum > 20 ){
                    alert("上传图片数目不可以超过20个，请重新选择");  //一次选择上传超过20个 或者是已经上传和这次上传的到的总数也不可以超过20个
                }
                else if(numUp < 20){
                    fileList = validateUp(fileList);
                    for(var i = 0;i<fileList.length;i++){
                        var imgUrl = window.URL.createObjectURL(fileList[i]);
                        imgArr.push(imgUrl);
                        /*var $section = $("<dd class='_j_picitem' >");
                            imgContainer.prepend($section);

                        var $span = $("<span class='up-span'>");
                            $span.appendTo($section);

                        var $img0 = $("<img class='close-upimg'>").on("click",function(event){
                               event.preventDefault();
                               event.stopPropagation();
                               $(".works-mask").show();
                               delParent = $(this).parent();
                           });
                           $img0.attr("src","/images/a7.png").appendTo($section);
                        var $img = $("<img class='up-img up-opcity'>");
                            $img.attr("src",imgArr[i]);
                            $img.appendTo($section);
                        var $p = $("<p class='img-name-p'>");
                            $p.html(fileList[i].name).appendTo($section);
                        var $input = $("<input id='taglocation' name='taglocation' value='' type='hidden'>");
                            $input.appendTo($section);
                        var $input2 = $("<input id='tags' name='tags' value='' type='hidden'/>");
                            $input2.appendTo($section);*/
                        $(dd).after('' +
                                '<dd class="_j_picitem" id="ddremove">' +
                                '<div class="place">' +
                                '<div class="img">' +
                                '<img class="_j_edit_src" src="'+imgArr[i]+'" style="width:120px;height:120px"> ' +
                                '</div> ' +
                                '<div class="title">' +
                                '<h4 class="_j_edit_title"></h4>' +
                                '</div> <div class="mask-operate"> ' +
                                '<a class="btn-remove _j_remove_dd"></a> ' +
                                '</div> ' +
                                '</div> ' +
                                '</dd>')
                    }
                }
                setTimeout(function(){
                    $(".up-section").removeClass("loading");
                    $(".up-img").removeClass("up-opcity");
                },450);
                numUp = imgContainer.find(".up-section").length;
                if(numUp >= 20){
                    $(this).parent().hide();
                }
            });
            /*$(".z_photo").delegate(".close-upimg","click",function(){
                $(".works-mask").show();
                delParent = $(this).parent();
            });
            $(".wsdel-ok").click(function(){
                $(".works-mask").hide();
                var numUp = delParent.siblings().length;
                if(numUp < 6){
                    delParent.parent().find(".z_file").show();
                }
                delParent.remove();
            });
            $(".wsdel-no").click(function(){
                $(".works-mask").hide();
            });*/
            function validateUp(files){
                var arrFiles = [];//替换的文件数组
                for(var i = 0, file; file = files[i]; i++){
                    //获取文件上传的后缀名
                    var newStr = file.name.split("").reverse().join("");
                    if(newStr.split(".")[0] != null){
                        var type = newStr.split(".")[0].split("").reverse().join("");
                        if(jQuery.inArray(type, defaults.fileType) > -1){
                            // 类型符合，可以上传
                            if (file.size >= defaults.fileSize) {
                                alert(file.size);
                                alert('您这个"'+ file.name +'"文件大小过大');
                            } else {
                                // 在这里需要判断当前所有文件中
                                arrFiles.push(file);
                            }
                        }else{
                            alert('您这个"'+ file.name +'"上传类型不符合');
                        }
                    }else{
                        alert('您这个"'+ file.name +'"没有类型, 无法识别');
                    }
                }
                return arrFiles;
            }
            // 收显全部
            $('#_j_facility_info_expand_trigger').click(function () {
                if ($('#_j_facility_info_expand_trigger span').html() == "收起全部") {
                    $('#_j_facility_info_expand_trigger i').removeClass('icon-less');
                    $('#_j_facility_info_expand_trigger i').addClass('icon-more');
                    $('#_j_facility_info_expand_trigger span').html('展开全部');
                    document.getElementById('_j_facility_info').style.height = '72px';

                } else {
                    $('#_j_facility_info_expand_trigger i').removeClass('icon-more');
                    $('#_j_facility_info_expand_trigger i').addClass('icon-less');
                    $('#_j_facility_info_expand_trigger span').html('收起全部');
                    document.getElementById('_j_facility_info').style.height = '570px';

                }
            })
            // 写点评弹出窗口
            $('.btn-add').click(function () {
                <#if userInfo??>
                $('body').css({overflow:"hidden"});
                $('#_j_layer_1').css({display:"block"});
                <#else >
                popup('请先登录')
                </#if>

            })
            // 点评窗口关闭
            $('#popup_close').click(function () {
                // 星星样式初始化
                $('._j_starcount').removeClass().addClass('_j_starcount').addClass('star');
                $("input[name=rank_star]").val('');      // 整体评价
                $("input[name=location]").val('');       // 位置
                $("input[name=cleanliness]").val('');    // 清洁度
                $("input[name=facility]").val('');       // 设施
                $("input[name=service]").val('');        // 服务
                $("input[name=comfort]").val('');        // 舒适度
                $("input[name=food]").val('');           // 餐饮
                $('._j_commentarea').val('');            // 评论
                $('input[name=imgstr]').val('');
                $('dd').remove('#ddremove');

                $('body').css({overflow:"auto"});
                $('#_j_layer_1').css({display:"none"});


            })

            // 评论提交
            $('._j_submit').click(function () {
                <#if userInfo?? >

                    $("input[name=content]").val($('._j_commentarea').val());
                    $('#commentForm').ajaxSubmit(function (data) {
                        $('#hotelComment').prepend(data);
                    })
                    $('#popup_close').click(); // 窗口关闭
                <#else >
                    popup('请先登录');
                </#if>

            })

            // 多图片上传
            $('.add-place').click(function () {
                $('.file').click();
            })


            $("._j_starlist a").mouseover(function () {
                var index = $(this).index()+1;
                var text = $(this).attr("title");
                $(this).closest("div").prev().addClass("star"+index);
                $(this).closest("div").parent().next().html(text);
            }).mouseout(function () {
                var index = $(this).index()+1;
                var text = $(this).attr("title");
                $(this).closest("div").prev().removeClass("star"+index);
                $(this).closest("div").parent().next().html(text);
                var x = $(this).closest("div").prev().prev().val();
                if(x == index){
                    $(this).closest("div").prev().addClass("star"+x);
                }
            }).click(function () {
                var index = $(this).index()+1;
                var text = $(this).attr("title");
                $(this).closest("div").prev().addClass("star"+index);
                $(this).closest("div").parent().next().html(text);

                $(this).closest("div").prev().prev().val(index);

            })


            // 收藏
            $('#_j_fav_trigger').click(function () {
                var _this = $(this);
                <#if userInfo??>
                    var hid = $(_this).data('hid');
                    var uid = $(_this).data('uid');
                    $.get('/hotel/favor', {hid:hid, uid:uid}, function (data) {
                        if (data.success){
                            $(_this).addClass('on');
                        }else if (data.code == 102){
                            popup('收藏错误');
                        }else {
                            $(_this).removeClass('on');
                        }
                    })
                <#else >
                    popup('请先登录');
                </#if>
            })


            // 全部评论点击
            $('#dianping').click(function () {
                $('.comm-tab li').removeClass('on');
                $(this).closest('li').addClass('on');
                $('#searchForm input[name=name]').val('');
                $("#searchForm").submit();


            })
            // 词频点击

            $('#cipin').click(function () {
                console.log(1);
                $('.comm-tab li').removeClass('on');
                $(this).closest('li').addClass('on');
                var name = $(this).data('name');
                var hid = $(this).data('hid');
                $('#searchForm input[name=name]').val(name);
                $("#searchForm").submit();
            })
        })
    </script>

</head>
<body style="position: relative;">
<form id="fileForm" action="/images" style="display: none" method="post" enctype="multipart/form-data">
    <input type="file" name="file" id="file" class="file" accept="image/jpg,image/jpeg,image/png,image/bmp" multiple>
</form>
<#assign currentNav="hotel">
<#include "../common/navbar.ftl">
<div class="container">
<#--吐司-->
    <div class="top-info clearfix" id="_j_crumb">
        <div class="crumb">
            <span class="tit">您在这里：</span>
            <div class="item">
                <div class="drop">
                    <span class="hd"><a href="/hotel/" target="_blank">酒店<i></i></a></span>
                    <div class="bd" style="width:auto">
                        <i class="arrow"><b></b></i>
                        <div class="col">
                            <h3>周边国家和地区</h3>
                            <ul class="clearfix">
                            <#list countryList as c>
                                <li><a href="#" target="_blank">${c.name}<span> ${c.english}</span></a></li>
                            </#list>
                            <#--<li><a href="/hotel/10184/" target="_blank" title="韩国酒店预订">韩国<span>Korea</span></a></li>
                            <li><a href="/hotel/14293/" target="_blank" title="蒙古酒店预订">蒙古<span>Mongolia</span></a></li>
                            <li><a href="/hotel/10183/" target="_blank" title="日本酒店预订">日本<span>Japan</span></a></li>
                            <li><a href="/hotel/10300/" target="_blank" title="俄罗斯酒店预订">俄罗斯<span>Russia</span></a></li>
                            <li><a href="/hotel/10820/" target="_blank" title="老挝酒店预订">老挝<span>Laos</span></a></li>-->
                            </ul>
                        </div>
                        <div class="more"><a href="/hotel/" target="_blank">&gt;&gt;更多国家和地区</a></div>
                    </div>
                </div>
                <em>&gt;</em></div>
            <div class="item">
                <div class="drop">
                    <span class="hd"><a href="/hotel/21536/" target="_blank">${country.name}<i></i></a></span>
                    <div class="bd" style="width:auto">
                        <i class="arrow"><b></b></i>
                        <div class="col">
                            <h3>${country.name}其他城市</h3>
                            <ul class="clearfix">
                            <#list provinceList as pl>
                                <li><a href="/hotel/10065/" target="_blank">${pl.name}<span> ${pl.english}</span></a>
                                </li>
                            </#list>
                            <#--<li><a href="/hotel/10099/" target="_blank" title="上海閰掑簵鎺ㄨ崘">上海<span>Shanghai</span></a></li>
                            <li><a href="/hotel/10195/" target="_blank" title="西安閰掑簵鎺ㄨ崘">西安<span>Xi'an</span></a></li>
                            <li><a href="/hotel/10198/" target="_blank" title="深圳閰掑簵鎺ㄨ崘">深圳<span>Shenzhen</span></a></li>
                            <li><a href="/hotel/10208/" target="_blank" title="重庆閰掑簵鎺ㄨ崘">重庆<span>ChongQing</span></a></li>
                            <li><a href="/hotel/10035/" target="_blank" title="成都閰掑簵鎺ㄨ崘">成都<span>Chengdu</span></a></li>-->
                            </ul>
                        </div>
                        <div class="more"><a href="/hotel/21536/" target="_blank">&gt;&gt;更多城市</a></div>
                    </div>
                </div>
                <em>&gt;</em></div>
            <div class="item"><a href="/hotel/h?name=广州&checkIn=${(qo.checkIn)!}&checkOut=${(qo.checkOut)!}" target="_blank" title="广州酒店查询">${hotel.city.name}</a><em>&gt;</em>
            </div>
            <div class="item cur"><strong>${hotel.name}预订</strong></div>
        </div>
        <div class="weather-wrapper">


            <link href="http://css.mafengwo.net/weather/littleWeather.css?1530619858" rel="stylesheet" type="text/css">

            <div class="littleWeather">
                <a href="/weather/10088.html" target="_blank">
                    <span></span>
                </a>
            </div>
        </div>
    </div>

<#--封面-->
    <div class="hotel-intro">

        <div class="intro-hd">
            <div class="intro-extra">
                <span class="score"><em>${hotel.grade}</em>分</span>
                <span class="divide"></span>
                <span class="from"><strong class="t">
                <#if hotel.grade?eval gte 8.5>
                    非常好
                <#else >
                    很好
                </#if>
                </strong><br><i class="icon-bg icon-m"></i>200评论</span>

            </div>
            <div class="main-title">
                <h1>${hotel.name}</h1>
            </div>
            <div class="en-title"><span>${hotel.internationalName}</span></div>
            <div class="location"><span title="${hotel.address}">地址：${hotel.address}</span>
                <a class="a-maps" href="/hotel/list_map.php?poiid=5136442">
                    <iclass="icon-bg"></i>查看地图
                </a>
            </div>
        </div>


        <div class="intro-bd clearfix" data-cs-t="酒店详情页">
            <div class="img-big _j_album_trigger clickstat " data-id="169448403" data-is-top-album="1" data-cs-p="图片"
                 data-cs-l="大图" data-cs-d="大图">
                <img src="${hotel.coverUrl}">
                <span class="num"><em>${(hotel.images?size+1)!22}</em> 张图片</span>
                <div class="exp">
                    <p>${hotel.info}</p>
                </div>
            </div>
            <ul class="img-small">
            <#if hotel.imgs??>
                <#list hotel.images as img>
                    <li><img class="_j_album_trigger clickstat" src="${img}" data-id="169447941" data-cs-p="图片"
                             data-cs-l="小图" data-cs-d="小图"></li>
                </#list>
            <#else >
                <li><img class="_j_album_trigger clickstat"
                         src="http://b2-q.mafengwo.net/s10/M00/2B/7F/wKgBZ1mMvSCAWIbZAB582eRClY418.jpeg?imageMogr2%2Fthumbnail%2F%21300x240r%2Fgravity%2FCenter%2Fcrop%2F%21300x240%2Fquality%2F90"
                         data-id="169447941" data-cs-p="图片" data-cs-l="小图" data-cs-d="小图"></li>
                <li><img class="_j_album_trigger clickstat"
                         src="http://b3-q.mafengwo.net/s10/M00/2B/6F/wKgBZ1mMvR6ATRj_AAUyEvkzM8s73.jpeg?imageMogr2%2Fthumbnail%2F%21300x240r%2Fgravity%2FCenter%2Fcrop%2F%21300x240%2Fquality%2F90"
                         data-id="169447986" data-cs-p="图片" data-cs-l="小图" data-cs-d="小图"></li>
                <li><img class="_j_album_trigger clickstat"
                         src="http://n4-q.mafengwo.net/s10/M00/2B/94/wKgBZ1mMvSKAM0vOAAEwxT8kYtM99.jpeg?imageMogr2%2Fthumbnail%2F%21300x240r%2Fgravity%2FCenter%2Fcrop%2F%21300x240%2Fquality%2F90"
                         data-id="169448598" data-cs-p="图片" data-cs-l="小图" data-cs-d="小图"></li>
                <li><img class="_j_album_trigger clickstat"
                         src="http://p4-q.mafengwo.net/s11/M00/67/C4/wKgBEFtyZguAP_kdAEPUel-6pQg14.jpeg?imageMogr2%2Fthumbnail%2F%21300x240r%2Fgravity%2FCenter%2Fcrop%2F%21300x240%2Fquality%2F90"
                         data-id="253010224" data-cs-p="图片" data-cs-l="小图" data-cs-d="小图"></li>
                <li><img class="_j_album_trigger clickstat"
                         src="http://b2-q.mafengwo.net/s11/M00/7E/23/wKgBEFz32VWAc5rtAAIOfT7GLLw05.jpeg?imageMogr2%2Fthumbnail%2F%21300x240r%2Fgravity%2FCenter%2Fcrop%2F%21300x240%2Fquality%2F90"
                         data-id="863853650" data-cs-p="图片" data-cs-l="小图" data-cs-d="小图"></li>
                <li><img class="_j_album_trigger clickstat"
                         src="http://b1-q.mafengwo.net/s11/M00/67/78/wKgBEFtyZe2AUvB0AC2DajzsCqQ37.jpeg?imageMogr2%2Fthumbnail%2F%21300x240r%2Fgravity%2FCenter%2Fcrop%2F%21300x240%2Fquality%2F90"
                         data-id="253009880" data-cs-p="图片" data-cs-l="小图" data-cs-d="小图"></li>
            </#if>
            </ul>
        </div>
    </div>

<#--中部导航栏-->
    <div class="hotel-navbar" id="_j_navbar" data-cs-t="酒店详情页" style="transform: translateY(0px);">
        <div class="navbar-content clearfix">
            <ul class="nav">
                <li class="_j_nav_item" data-type="bookingInfo"><a href="javascript:;">预订</a></li>
                <li class="_j_nav_item" data-type="map"><a href="javascript:;">位置</a></li>
                <li class="_j_nav_item" data-type="hotelInfo"><a href="javascript:;">攻略</a></li>
                <li class="_j_nav_item" data-type="comment"><a href="#dianping">点评</a></li>
                <li class="_j_nav_item" style="" data-type="nearbyHotels"><a href="javascript:;">周边酒店</a></li>
            </ul>
            <div class="navbar-r">
                <div class="r-a">

                    <a class="" href="#dianping"><i class="icon-bg icon-mod"></i>点评</a>

                    <a class=<#if isFavor??>"on"<#else >""</#if> id="_j_fav_trigger" href="javascript:;" data-hid="${(hotel.id)!}" data-uid="${(userInfo.id)!-1}"><i class="icon-bg icon-fav"></i>收藏</a>
                </div>
                <div class="r-b"><span style="">每晚<strong>¥${(hotel.prices[0].price)!299}</strong></span><a
                        class="r-btn _j_nav_item" href="javascript:;" data-type="bookingInfo" data-cs-p="顶部导航"
                        data-cs-l="预订button" data-cs-d="预订button">预订</a></div>
            </div>
        </div>
    </div>

<#--酒店信息-->

    <div class="hotel-info" id="_j_hotel_info">
        <div class="info-section">
            <dl class="clearfix">
                <dt>基本信息</dt>
                <dd class="clearfix">

                    <div class="cell">
                        <span class="label">入住时间: </span>
                        <span class="content"><strong>${hotel.checkin}</strong>之后</span>
                    </div>
                    <div class="cell">
                        <span class="label">离店时间: </span>
                        <span class="content"><strong>${hotel.departure}</strong>之前</span>
                    </div>
                    <div class="cell">
                        <span class="label">建成于: </span>
                        <span class="content"><strong>${hotel.history}</strong>年</span>
                    </div>
                    <div class="cell">
                        <span class="label">翻修于: </span>
                        <span class="content"><strong>${hotel.history}</strong>年</span>
                    </div>
                    <div class="cell">
                        <span class="label">酒店规模: </span>
                        <span class="content"><strong>${hotel.scale}</strong>间客房</span>
                    </div>
                </dd>
            </dl>
        </div>
        <div class="info-section">
            <div class="expand-wrap" id="_j_facility_info" style="height:72px;">
                <dl class="clearfix">
                    <dt>主要设施</dt>
                    <dd>
                        <ul class="facility-item clearfix">
                            <li><i class="icon-bg icon-wifi"></i>wifi服务</li>
                            <li><i class="icon-bg icon-park"></i>免费停车场</li>
                            <li><i class="icon-bg icon-elevator"></i>电梯</li>
                            <li class="off"><i class="icon-bg icon-restaurant"></i>餐厅</li>
                            <li><i class="icon-bg icon-consign"></i>行李寄存</li>
                            <li><i class="icon-bg icon-24hours"></i>24小时服务</li>
                            <li><i class="icon-bg icon-thermos"></i>热水壶</li>
                            <li><i class="icon-bg icon-blower"></i>吹风机</li>
                        </ul>
                    </dd>
                </dl>
                <dl class="clearfix">
                    <dt>酒店服务</dt>
                    <dd>
                        <ul class="facility-item clearfix">
                            <li><i class="icon-bg icon-ok"></i>洗衣服务</li>
                            <li><i class="icon-bg icon-ok"></i>邮政服务</li>
                            <li><i class="icon-bg icon-ok"></i>门卫</li>
                            <li><i class="icon-bg icon-ok"></i>旅游服务</li>
                            <li><i class="icon-bg icon-ok"></i>代客泊车</li>
                            <li><i class="icon-bg icon-ok"></i>客房服务</li>
                            <li><i class="icon-bg icon-ok"></i>快速入住服务</li>
                            <li><i class="icon-bg icon-ok"></i>叫车服务</li>
                            <li><i class="icon-bg icon-ok"></i>熨衣服务</li>
                            <li><i class="icon-bg icon-ok"></i>接站服务</li>
                            <li><i class="icon-bg icon-ok"></i>礼宾服务</li>
                            <li><i class="icon-bg icon-ok"></i>旅游交通图</li>
                            <li><i class="icon-bg icon-ok"></i>一次性账单结算</li>
                            <li><i class="icon-bg icon-ok"></i>信用卡结算</li>
                            <li><i class="icon-bg icon-ok"></i>行李服务</li>
                            <li><i class="icon-bg icon-ok"></i>24小时大堂经理</li>
                        </ul>
                    </dd>
                </dl>
                <dl class="clearfix">
                    <dt>酒店设施</dt>
                    <dd>
                        <ul class="facility-item clearfix">
                            <li><i class="icon-bg icon-ok"></i>商务中心</li>
                            <li><i class="icon-bg icon-ok"></i>中餐厅</li>
                            <li><i class="icon-bg icon-ok"></i>吸烟区</li>
                            <li><i class="icon-bg icon-ok"></i>休息区</li>
                            <li><i class="icon-bg icon-ok"></i>传真/复印</li>
                            <li><i class="icon-bg icon-ok"></i>公共区域监控</li>
                            <li><i class="icon-bg icon-ok"></i>多功能厅</li>
                            <li><i class="icon-bg icon-ok"></i>多媒体演示系统</li>
                        </ul>
                    </dd>
                </dl>
                <dl class="clearfix">
                    <dt>房间设施</dt>
                    <dd>
                        <ul class="facility-item clearfix">
                            <li><i class="icon-bg icon-ok"></i>叫醒服务</li>
                            <li><i class="icon-bg icon-ok"></i>24小时热水</li>
                            <li><i class="icon-bg icon-ok"></i>保险箱</li>
                            <li><i class="icon-bg icon-ok"></i>多种规格电源插座</li>
                            <li><i class="icon-bg icon-ok"></i>小冰箱／迷你吧</li>
                            <li><i class="icon-bg icon-ok"></i>无烟房</li>
                            <li><i class="icon-bg icon-ok"></i>宽带上网</li>
                            <li><i class="icon-bg icon-ok"></i>国际长途电话</li>
                            <li><i class="icon-bg icon-ok"></i>空调</li>
                            <li><i class="icon-bg icon-ok"></i>电视</li>
                            <li><i class="icon-bg icon-ok"></i>独立卫浴间</li>
                            <li><i class="icon-bg icon-ok"></i>咖啡壶／茶具</li>
                            <li><i class="icon-bg icon-ok"></i>沙发</li>
                            <li><i class="icon-bg icon-ok"></i>拖鞋</li>
                            <li><i class="icon-bg icon-ok"></i>衣柜/衣橱</li>
                            <li><i class="icon-bg icon-ok"></i>110V电压插座</li>
                            <li><i class="icon-bg icon-ok"></i>220V电压插座</li>
                            <li><i class="icon-bg icon-ok"></i>免费洗漱用品</li>
                            <li><i class="icon-bg icon-ok"></i>遮光窗帘</li>
                            <li><i class="icon-bg icon-ok"></i>鸭绒被</li>
                            <li><i class="icon-bg icon-ok"></i>语音留言</li>
                            <li><i class="icon-bg icon-ok"></i>针线包</li>
                            <li><i class="icon-bg icon-ok"></i>书桌</li>
                            <li><i class="icon-bg icon-ok"></i>手动窗帘</li>
                            <li><i class="icon-bg icon-ok"></i>电话</li>
                            <li><i class="icon-bg icon-ok"></i>化妆镜</li>
                            <li><i class="icon-bg icon-ok"></i>报纸</li>
                        </ul>
                    </dd>
                </dl>
            </div>
            <a class="expand-more" id="_j_facility_info_expand_trigger" href="javascript:;" data-is-expand="0"><i
                    class="icon-bg icon-more"></i><span>展开全部</span></a>
        </div>
        <div class="info-section">
            <a style="color: #666;font-weight: bold;text-decoration: none;cursor: default;" target="_blank"
               href="/hotel/license?hotel_id=5136442">骡窝窝酒店平台合作伙伴</a>
        </div>
        <div class="info-section">
            <div class="expand-wrap" id="_j_description" style="height: auto;">
                <dl class="clearfix">
                    <dt>酒店攻略</dt>
                    <dd>必读<br>${hotel.info}<br>酒店时尚温馨、安逸舒适、环境优美，拥有标准房、豪华房、圆床房、豪华套房等多种房型。<br><br>贴士<br>酒店设有停车场，方便选择自驾游出行的窝窝们停车。<br>酒店配套有汤家养生餐厅、300平米多功能会议室，设施一应俱全。<br>酒店可提供行李寄存和24小时前台服务，方便为有需要的窝窝服务。
                    </dd>
                </dl>
            </div>
            <a class=" expand-more" id="_j_description_expand_trigger" href="javascript:;" style="display:none;"
               data-is-expand="0"><i class="icon-bg icon-more"></i><span>展开全部</span></a>
        </div>

    </div>


<#--评论-->
    <div class="hotel-comment">
        <h3 class="title">${(vo.count)!10}条真实用户评论</h3>
        <dl class="hotel-score clearfix">
            <dt>
            <div class="num"><em>${vo.ensemble}</em>分</div>
            非常好
            </dt>
            <dd>
                <div class="num p1"><em>${vo.location}</em></div>
                位置
            </dd>
            <dd>
                <div class="num p1"><em>${vo.service}</em></div>
                服务
            </dd>
            <dd>
                <div class="num p3"><em>${vo.cleanliness}</em></div>
                清洁度
            </dd>
            <dd>
                <div class="num p2"><em>${vo.comfort}</em></div>
                舒适度
            </dd>
            <dd>
                <div class="num p2"><em>${vo.facility}</em></div>
                设施
            </dd>
            <dd>
                <div class="num p2"><em>${vo.food}</em></div>
                餐饮
            </dd>
        </dl>
        <div class="comm-bar">
            <ul class="comm-tab">
                <li class="on">
                    <a id="dianping" class="_j_comment_type" href="javascript:;">全部点评${vo.count}条</a>
                </li>
                <#if cipin??>
                    <#list cipin as c>
                        <li class="ccc">
                            <a id="cipin" class="_j_comment_type" data-hid="${hotel.id}" data-name="${c.name}" href="javascript:;">${(c.name)!}(${c.num})</a>
                        </li>
                    </#list>
                </#if>
            </ul>
            <a class="btn-add"><i class="icon-bg icon-plus"></i>写点评</a>
        </div>
        <#--放评论的地方-->
        <div class="comm-list sign-font-family" id="_j_comment_list">
            <style>
                .comm-meta .comm-report {
                    display: none;
                    font-size: 12px;
                    color: #999;
                    vertical-align: middle;
                }

                .comm-item:hover .comm-report {
                    display: inline-block;
                }

                .comm-report:hover, .comm-reply-report:hover {
                    color: #FF8A00;
                }

                .reply-box .comm-reply-report {
                    display: none;
                    font-size: 12px;
                    color: #999;
                    margin-left: 10px;
                }

                .comment_reply_item:hover .comm-reply-report {
                    display: inline-block;
                }

                .hotel-report-dialog label {
                    margin: 0 3px;
                }
            </style>
            <form id="searchForm" action="/hotel/comment" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <input type="hidden" name="hotelId" value="${hotel.id!}">
                <input type="hidden" name="name">

                <div class="com-box " id="hotelComment">

                </div>
            </form>

        </div>


    </div>

</div>



<#--点评窗口-->
<div id="_j_layer_1" class="layer _j_layer"
     style="position: fixed; width: 100%; height: 100%; top: 0px; left: 0px; z-index: 1000; display: none;">
    <div class="layer_mask _j_mask"
         style="position: fixed;width: 100%;height: 100%;top: 0px;left: 0px;background: rgb(0, 0, 0);opacity: 0.7;z-index: -1;"></div>
    <div class="layer_content _j_content"
         style="position: fixed; width: 100%; height: 100%; top: 0px; left: 0px; z-index: 0; overflow: hidden auto;">
        <div class="popup-box layer_dialog _j_dialog pop_no_margin pop-reviews dialog_overflow"
             style="position: absolute; opacity: 1; background: rgb(255, 255, 255); z-index: 1; left: 495px; top: 20px;">
            <div class="dialog_title" style="display:none">
                <div class="_j_title title"></div>
            </div>
            <div class="dialog_body _j_content">
                <div class="_j_weng_form_cnt">
                    <style type="text/css">
                        .pop-upload-weng {
                            width: 300px;
                        }
                        .pop-upload-weng ._j_piccomment {
                            display: none;
                        }
                    </style>
                    <div class="mfw-reviews">
                        <div id="_j_wengform_cnt_20190824091756_466">
                            <h2>
                                <strong>广州四季酒店(Four Seasons Hotel Guangzhou)</strong>
                                <em>*</em>为必填选项
                            </h2>
                            <form id="commentForm" action="/hotel/commentAdd" method="post" class="_j_commentdialogform">
                                <input type="hidden" name="hotelId" value="${hotel.id}">
                                <input type="hidden" name="hotelName" value="${hotel.name}">
                                <input type="hidden" name="positionId" value="${hotel.position.id}">
                                <input type="hidden" name="positionName" value="${hotel.position.name}">
                                <input type="hidden" name="cityId" value="${hotel.city.id}">
                                <input type="hidden" name="cityName" value="${hotel.city.name}">
                                <input type="hidden" name="imgstr" >



                                <div class="review-item item-star">
                                    <div class="label"><em>*</em>总体评价</div>
                                    <div class="review-star _j_rankblock" data-star="" data-name="rank_star">
                                        <input type="hidden" name="ensemble" value="" essential="1"
                                               data-inputname="总体评价">
                                        <span class="_j_starcount star"></span>
                                        <div class="click-star _j_starlist">
                                            <a role="button" title="不建议" rel="nofollow"></a>
                                            <a role="button" title="有待改善" rel="nofollow"></a>
                                            <a role="button" title="还可以" rel="nofollow"></a>
                                            <a role="button" title="值得一去" rel="nofollow"></a>
                                            <a role="button" title="强烈推荐" rel="nofollow"></a>
                                        </div>
                                    </div>
                                    <span class="txt-tips _j_startip">点击星星打分</span>
                                </div>

                                <div class="review-group">
                                    <div class="review-item item-rating">
                                        <div class="label"><em>*</em>位置</div>
                                        <div class="review-score _j_rankblock" data-star="" data-name="location">
                                            <input type="hidden" name="location" value=""
                                                   essential="1" data-inputname="给位置的评分">
                                            <span class="_j_starcount star0"></span>
                                            <div class="click-star _j_starlist">
                                                <a role="button" title="不满意" rel="nofollow"></a>
                                                <a role="button" title="待改善" rel="nofollow"></a>
                                                <a role="button" title="还可以" rel="nofollow"></a>
                                                <a role="button" title="很不错" rel="nofollow"></a>
                                                <a role="button" title="非常满意" rel="nofollow"></a>
                                            </div>
                                        </div>
                                        <span class="txt-tips _j_startip">给位置打分</span>
                                    </div>
                                    <div class="review-item item-rating">
                                        <div class="label"><em>*</em>清洁度</div>
                                        <div class="review-score _j_rankblock" data-star="" data-name="cleanliness">
                                            <input type="hidden" name="cleanliness" value=""
                                                   essential="1" data-inputname="给清洁度的评分">
                                            <span class="_j_starcount star0"></span>
                                            <div class="click-star _j_starlist">
                                                <a role="button" title="不满意" rel="nofollow"></a>
                                                <a role="button" title="待改善" rel="nofollow"></a>
                                                <a role="button" title="还可以" rel="nofollow"></a>
                                                <a role="button" title="很不错" rel="nofollow"></a>
                                                <a role="button" title="非常满意" rel="nofollow"></a>
                                            </div>
                                        </div>
                                        <span class="txt-tips _j_startip">给清洁度打分</span>
                                    </div>
                                    <div class="review-item item-rating">
                                        <div class="label"><em>*</em>设施</div>
                                        <div class="review-score _j_rankblock" data-star="" data-name="facility">
                                            <input type="hidden" name="facility" value=""
                                                   essential="1" data-inputname="给设施的评分">
                                            <span class="_j_starcount star0"></span>
                                            <div class="click-star _j_starlist">
                                                <a role="button" title="不满意" rel="nofollow"></a>
                                                <a role="button" title="待改善" rel="nofollow"></a>
                                                <a role="button" title="还可以" rel="nofollow"></a>
                                                <a role="button" title="很不错" rel="nofollow"></a>
                                                <a role="button" title="非常满意" rel="nofollow"></a>
                                            </div>
                                        </div>
                                        <span class="txt-tips _j_startip">给设施打分</span>
                                    </div>
                                    <div class="review-item item-rating">
                                        <div class="label"><em>*</em>服务</div>
                                        <div class="review-score _j_rankblock" data-star="" data-name="service">
                                            <input type="hidden" name="service" value="" essential="1"
                                                   data-inputname="给服务的评分">
                                            <span class="_j_starcount star0"></span>
                                            <div class="click-star _j_starlist">
                                                <a role="button" title="不满意" rel="nofollow"></a>
                                                <a role="button" title="待改善" rel="nofollow"></a>
                                                <a role="button" title="还可以" rel="nofollow"></a>
                                                <a role="button" title="很不错" rel="nofollow"></a>
                                                <a role="button" title="非常满意" rel="nofollow"></a>
                                            </div>
                                        </div>
                                        <span class="txt-tips _j_startip">给服务打分</span>
                                    </div>
                                    <div class="review-item item-rating">
                                        <div class="label"><em>*</em>舒适度</div>
                                        <div class="review-score _j_rankblock" data-star="" data-name="comfort">
                                            <input type="hidden" name="comfort" value="" essential="1"
                                                   data-inputname="给舒适度的评分">
                                            <span class="_j_starcount star0"></span>
                                            <div class="click-star _j_starlist">
                                                <a role="button" title="不满意" rel="nofollow"></a>
                                                <a role="button" title="待改善" rel="nofollow"></a>
                                                <a role="button" title="还可以" rel="nofollow"></a>
                                                <a role="button" title="很不错" rel="nofollow"></a>
                                                <a role="button" title="非常满意" rel="nofollow"></a>
                                            </div>
                                        </div>
                                        <span class="txt-tips _j_startip">给舒适度打分</span>
                                    </div>
                                    <div class="review-item item-rating">
                                        <div class="label"><em>*</em>餐饮</div>
                                        <div class="review-score _j_rankblock" data-star="" data-name="food">
                                            <input type="hidden" name="food" value="" essential="1"
                                                   data-inputname="给餐饮的评分">
                                            <span class="_j_starcount star0"></span>
                                            <div class="click-star _j_starlist">
                                                <a role="button" title="不满意" rel="nofollow"></a>
                                                <a role="button" title="待改善" rel="nofollow"></a>
                                                <a role="button" title="还可以" rel="nofollow"></a>
                                                <a role="button" title="很不错" rel="nofollow"></a>
                                                <a role="button" title="非常满意" rel="nofollow"></a>
                                            </div>
                                        </div>
                                        <span class="txt-tips _j_startip">给餐饮打分</span>
                                    </div>
                                </div>

                                <div class="review-item item-comment">
                                    <div class="label"><em>*</em>内容</div>
                                    <div class="content">
                                        <textarea class="_j_commentarea" name="content" essential="1"
                                                  data-inputname="内容" placeholder="100字+3图，有机会评为优质点评！" data-minlen="1"
                                                  data-maxlen="10000"></textarea>
                                        <p class="_j_commentcounttip">内容不超过10000字</p>
                                    </div>
                                </div>

                                <div class="review-item item-photo">
                                    <div class="label">上传照片</div>
                                    <div class="content">
                                        <dl class="upload-box _j_piclist">
                                            <dd data-wengid="" class="_j_picitem_btn" id="_j_pluplader_btn_container_8"
                                                style="position: relative;">
                                                <a class="add-place"><i></i></a>
                                                <div id="html5_1dj0j73e41071pvigqm1p1m50f16_container"
                                                     class="moxie-shim moxie-shim-html5"
                                                     style="position: absolute; top: 0px; left: 0px; width: 120px; height: 120px; overflow: hidden; z-index: -1;">

                                                </div>
                                            </dd>

                                        </dl>
                                        <p style="clear:both;">图片不超过20张</p>
                                    </div>
                                </div>
                                <div class="review-item item-action">
                                    <a class="btn-large _j_submit" role="button" title="提交">提交</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <a id="popup_close" class="close-btn _j_close"><i></i></a>
        </div>
    </div>
</div>

<#include "../common/footer.ftl">
</body>
</html>