package cn.wolfcode.luowowo.article.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by zhi on 2019/8/13.
 */
@Setter
@Getter
public class TravelDetailQuery extends QueryObject {
    private Long travelId = -1L;
}
