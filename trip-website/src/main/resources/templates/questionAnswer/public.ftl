<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link href="/styles/base.css" rel="stylesheet" type="text/css">
    <link href="/styles/public.css" rel="stylesheet" type="text/css">
    <link href="/styles/addtravelnote.css" rel="stylesheet" type="text/css">
    <link href="/js/ueditor/themes/default/css/umeditor.css" type="text/css" rel="stylesheet">
    <link href="/js/plugins/datepicker/datepicker.css" rel="stylesheet">
    <script type="text/javascript" src="/js//jquery/jquery.js"></script>
    <script src="/js/plugins/jquery-form/jquery.form.js"></script>
    <script type="text/javascript" src="/js/system/addtravelnote.js"></script>
    <script type="text/javascript" charset="utf-8" src="/js/ueditor/umeditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/js/ueditor/umeditor.min.js"></script>
    <script src="/js/plugins/datepicker/datepicker.js"></script>
    <script type="text/javascript" src="/js/ueditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript" src="/js/system/common.js"></script>
    <script type="text/javascript" src="/js/system/public.js"></script>

    <script>
        var ue;
        $(function () {

                        // 初始化富文本编辑器
                        ue = UM.getEditor('editor',{
                            textarea:"content",
                            imageUrl:"/travel/contentImage",
                            imageFieldName:"upfile",
                            //UMEDITOR_HOME_URL:"/",
                            imagePath:''
                        });


            $('.qt-post-btn').click(function () {
                //console.log(ue.getContent());
                $('#content').val(ue.getContent());
                $('#editForm').ajaxSubmit(function (data) {
                    if (data.success){
                        window.location.href="/questionAnswer/list";
                    } else{
                        alert(data.msg);
                    }
                })
            })
        })
    </script>
</head>

<body>
<div class="lww_header">
    <div class="header_wrap">
        <div class="header_logo">
            <a href="javascript:;" class="lww_logo"></a>
        </div>
        <ul class="header_nav">
            <li><a href="/index.html">首页</a></li>
            <li><a href="/destination.html">目的地</a></li>
            <li><a href="/gonglve.html">旅游攻略</a></li>
            <li><a href="javascript:;">去旅行<i class="icon_caret_down"></i></a></li>
            <li><a href="javascript:;">游记</a></li>
            <li><a href="javascript:;">酒店</a></li>
            <li><a href="javascript:;">社区<i class="icon_caret_down"></i></a></li>
        </ul>
        <div class="header_search">
            <input type="text" />
            <a class="icon_search"></a>
        </div>
        <div class="login_info">
            <div class="head_user">
                <a href="javascript:;">
                    <img src="./images/user.png" />
                    <i class="icon_caret_down"></i>
                </a>
            </div>
            <div class="header_msg">
                消息<i class="icon_caret_down"></i>
            </div>
            <div class="header_daka">
                <a href="javascript:;">打卡</a>
            </div>
        </div>
    </div>
    <div class="shadow"></div>
</div>
<div class="wrapper">
    <div class="qt-container clearfix">
        <div class="qt-main">
            <div class="crumb">
                <a href="/wenda/">旅游问答</a> &gt; <span>我要提问</span>
            </div>

            <div class="qt-tit">
                <h5>问题标题</h5>
                <div class="qt-con">
                    <form class="forms" action="/questionAnswer/saveOrUpdate" method="post" id="editForm">
                    <input type="text" placeholder="标题不小于10字哦" class="_j_title" name="title">
                    <textarea name="content" id="content" style="display: none"></textarea>
                    <span class="count"><span class="_j_title_num">0</span>/80 字</span>
                    <span class="_j_min_num hide">10</span>
                    <span class="error err-tips _j_title_error">标题不能少于10字</span>
                </div>
            </div>
            <div class="qt-details">
                <h5><a title="添加问答内容" class="icon active" id="_j_show_content"></a>问题详细内容</h5>
                <script id="editor" type="text/plain" style="width:100%;height:500px;"></script>
                </div>
                <div class="publish_question">
                        <a class="qt-post-btn _j_publish" title="发布问题">发布问题</a>
                        </div>
                        </div>
                        </form>
                        <div class="qt-sider">
                        <div class="qts-tit">提问的正确姿势</div>
                        <div class="qts-con">
                        <p>1.问题要【具体】【真实】【诚恳】，问题较多，需全面阐述时，可以添加问题补充。结伴/交易/与旅行无关的提问将被删除。</p>
                <p>2.给问题添加目的地，并打上正确的标签将有助于更快地获得回答。</p>
                </div>
                </div>
                </div>
                </div>
                </body>

                </html>