

<div class="gonglve_wrap">
    <#list page.content as h>
    <div class="hotel-item clearfix _j_hotel_item" >
        <div class="hotel-pic">
            <a href="/hotel/detail?id=${h.id}" class="_j_hotel_info_link" >
                <img src="${h.coverUrl}" alt="${h.name}" style="width: 330px;">
            </a>
        </div>
        <div class="hotel-title">
            <div class="title">
                <h3><a href="/hotel/detail?id=${h.id}" class="_j_hotel_info_link" >${h.name}</a></h3>
                <br/>
                <span>${h.internationalName}</span>
            </div>
        </div>
        <div class="hotel-info id${h.id}">
            <ul class="nums clearfix">
                <li class="rating rating2">
                    <em>${h.grade}</em>分
                    <br/>
                    <strong>
                        <#if h.grade?eval gte 8.5>
                            非常好
                        <#else >
                            很好
                        </#if>
                    </strong>
                </li>
                <li>
                    <a href="#" class="_j_hotel_info_link">
                        <em>300条</em>
                        <br/>
                        窝窝评价
                    </a>
                </li>
                <li class="spilt"></li>
                <li>
                    <a href="#" class="_j_hotel_info_link">
                        <em>30篇</em>
                        <br/>
                        游记提及
                    </a>
                </li>
            </ul>
            <p class="summary" title="${h.info}">${h.info}</p>
            <div class="location">
                                <span>
                                    <i class="icon-location"></i>
                                    位于:
                                    <a href="#" data-id="${h.positionID}" >${h.positionName}</a>
                                </span>
                <i class="icon-subway"></i>
            </div>
        </div>
        <script>
            $.get('/hotel/get?id=${h.id}',function (data) {
                $('.id${h.id}').after(data);
            })
        </script>
        <#--<div class="hotel-btns">

            <a class="btn-booking _j_booking_btn" href="javascript:" rel="nofollow" style="" >
                <div class="ota">
                    <div class="name">
                        <strong><img src="http://images.mafengwo.net/images/hotel/newlogo/mafengwo_2018@2x.png" height="20" width="100"></strong>
                        <i class="icon-alipay" style=""></i>
                        <i class="icon-wxpay" style=""></i>
                    </div>
                    <p class="tips" style="display:none;"></p>
                </div>
                <div class="price _j_booking_price">
                    <strong>￥</strong><strong>3019</strong><strong style="font-size: 12px;color: #666;padding-left: 2px;vertical-align: 1px;">起</strong>
                    <i class="arrow"></i>
                </div>
                <div class="price _j_booking_sold_out" style="display:none;">
                    <span>已售罄</span>
                </div>
            </a>
        </div>-->

    </div>
</#list>
    <div style="float: right">
        <div style="float: left;" ><span style="line-height:30px"> 共${page.totalPages!}页 / ${page.totalElements}条&nbsp;&nbsp;&nbsp;</span></div>
        <div id="pagination" class="jq-pagination" style="display: inline;"></div>
    </div>

    <#-- <div class="page-hotel" align="right" id="list_paginator" rel="nofollow">
         <span class="count">共<span>${page.totalPages!}</span>页 / <span>${page.totalElements}</span>家旅店</span>
         <div id="pagination" class="jq-pagination" style="display: inline;"></div>
         <a class="ti first _j_pageitem" data-value="1" rel="nofollow">首页</a>
         <span class="this-page" data-value="1" rel="nofollow">1</span>
         <a class="ti _j_pageitem" data-value="2" rel="nofollow">2</a>
         <a class="ti _j_pageitem" data-value="3" rel="nofollow">3</a>
         <a class="ti _j_pageitem" data-value="4" rel="nofollow">4</a>
         <a class="ti _j_pageitem" data-value="5" rel="nofollow">5</a>
         <a class="ti _j_pageitem prev" data-value="2" rel="nofollow">后一页</a>
         <a class="ti first _j_pageitem" data-value="272" rel="nofollow">末页</a>
     </div>-->
    <script>
        $("#pagination").jqPaginator({
            totalPages: ${page.totalPages!0},
            visiblePages: 5,
            currentPage: ${page.number+1}||1,
                prev: '<a class="prev" href="javascript:void(0);">上一页<\/a>',
                next: '<a class="next" href="javascript:void(0);">下一页<\/a>',
                page: '<a href="javascript:void(0);">{{page}}<\/a>',
                last: '<a class="last" href="javascript:void(0);" >尾页<\/a>',
                onPageChange: function(page, type) {
                    if(type == 'change'){
                        $("#currentPage").val(page);
                        $("#searchForm").submit();
                    }
                 }
        })
    </script>
</div>

