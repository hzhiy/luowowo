package cn.wolfcode.luowowo.cache.service;

/**
 * Created by zhi on 2019/8/7.
 */

import cn.wolfcode.luowowo.article.domain.Travel;
import cn.wolfcode.luowowo.cache.vo.TravelStatisVO;

import java.util.List;
import java.util.Set;

/**
 * 攻略缓存数据
 */
public interface ITravelStatisRedisService {

    /**
     * 获取单个
     */
    TravelStatisVO getTravelStatisVO(Long id);

    /**
     * 阅读数增加
     * @param sid
     * @param i
     */
    void viewNumIncrease(Long sid, int i);

    /**
     * 评论数增加
     * @param sid
     * @param i
     */
    void replyNumIncrease(Long sid, int i);

    /**
     * 判断该数据是否存在
     * @param id
     * @return true: 存在  false: 不存在
     */
    boolean isVoExist(Long id);

    /**
     * 将vo对象存入redis
     * @param vo
     */
    void setTravelStatisVO(TravelStatisVO vo);

    /**
     * 收藏操作
     * @param sid
     * @param uid
     * @return true: 收藏成功  false: 取消收藏
     */
    boolean favor(Long sid, Long uid);

    /**
     * 用户是否收藏指定攻略
     * @param uid
     * @param sid
     * @return true: 收藏  false: 没有收藏
     */
    boolean isUserFavorTravel(Long uid, Long sid);

    /**
     * 顶操作
     * @param sid
     * @param uid
     * @return true: 顶成功  false: 今天已经顶过了
     */
    boolean travelThumbup(Long sid, Long uid);

    /**
     * 根据前缀获得reids中的key
     * @param pattern
     * @return
     */
    Set<String> getKeys(String pattern);

    /**
     * 获取值
     * @param key
     * @return
     */
    String get(String key);

    /**
     * 分享操作
     * @param sid
     * @param uid
     * @return true: 分享成功  false: 分享失败
     */
    boolean sharenum(Long sid, Long uid);

    /**
     * 游记排行分数加1
     * @param prefix
     * @param value
     * @param i
     */
    void addScore(String prefix, String value, int i);

    /**
     * 游记排行前10 (阅读量)
     * @return
     */
    List<TravelStatisVO> getTravelViewNumTop10();

    /**
     * 根据用户Id查询所收藏的游记Id
     * @param id
     * @return
     */
    List<Travel> queryByUserIdToFavor(Long id);
}
