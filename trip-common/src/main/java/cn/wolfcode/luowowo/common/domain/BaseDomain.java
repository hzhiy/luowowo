package cn.wolfcode.luowowo.common.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by zhi on 2019/8/7.
 */
@Setter
@Getter
public class BaseDomain implements Serializable{
    // 作为主键ID
    protected Long id;
}
