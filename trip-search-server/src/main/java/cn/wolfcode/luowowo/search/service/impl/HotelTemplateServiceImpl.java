package cn.wolfcode.luowowo.search.service.impl;

import cn.wolfcode.luowowo.search.query.SearchQueryObject;
import cn.wolfcode.luowowo.search.repository.IHotelSearchRepository;
import cn.wolfcode.luowowo.search.service.IHotelTemplateService;
import cn.wolfcode.luowowo.search.template.HotelTemplate;
import com.alibaba.dubbo.config.annotation.Service;
import org.apache.lucene.search.TermQuery;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.util.StringUtils;

/**
 * Created by zhi on 2019/8/22.
 */
@Service
public class HotelTemplateServiceImpl implements IHotelTemplateService {
    @Autowired
    private IHotelSearchRepository repository;
    @Autowired
    private ElasticsearchTemplate template;

    @Override
    public void saveOrUpdate(HotelTemplate hotelTemplate) {
        repository.save(hotelTemplate);
    }

    @Override
    public Page queryHotelByCityName(SearchQueryObject qo) {
        NativeSearchQueryBuilder query = new NativeSearchQueryBuilder();
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        if (StringUtils.hasLength(qo.getCityName())){
            boolQueryBuilder.must(QueryBuilders.termQuery("cityName", qo.getCityName()));
        }
        if (StringUtils.hasLength(qo.getPositionName())){
            boolQueryBuilder.must(QueryBuilders.termQuery("positionName", qo.getPositionName()));
        }
        if (StringUtils.hasLength(qo.getKeyword())){
            boolQueryBuilder.must(QueryBuilders.multiMatchQuery(qo.getKeyword(), "name"));
        }
        query.withQuery(boolQueryBuilder);
        query.withPageable(qo.getPageableNoSort());

        Page<HotelTemplate> page = repository.search(query.build());

        return page;
    }
}
