package cn.wolfcode.luowowo;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Administrator on 2019/8/24.
 */
@SpringBootApplication
@EnableDubbo
@MapperScan("cn.wolfcode.luowowo.airticket.mapper")
public class AirTicketServer {


    public static void main(String[] args) {
        SpringApplication.run(AirTicketServer.class,args);
    }
}
