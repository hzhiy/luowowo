package cn.wolfcode.luowowo.hotel.vo;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.domain.Travel;
import cn.wolfcode.luowowo.hotel.domain.Hotel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@ToString
public class MineTravelStatisVO {
    private Travel travel;
    private String country;
    private String city;
    private Destination dest;
    private List<Hotel> hotel=new ArrayList<>();
    private int hotelCount;
}
