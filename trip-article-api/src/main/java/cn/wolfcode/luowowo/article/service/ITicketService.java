package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.domain.StrategyDetail;
import cn.wolfcode.luowowo.article.domain.Ticket;

import java.util.List;

/**
 * 门票
 */
public interface ITicketService {

    /**
     * 查询所有 门票
     * @return
     */
    List<Ticket> list();

    /**
     * 获取所有地区
     * @return
     */
    List<Ticket> getALLDest();

    /**
     * 查单个
     * @param id
     * @return
     */
    Ticket getById(Long id);
}
