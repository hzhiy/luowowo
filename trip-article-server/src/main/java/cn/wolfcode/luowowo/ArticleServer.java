package cn.wolfcode.luowowo;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by zhi on 2019/8/8.
 */
@SpringBootApplication
@EnableDubbo
@MapperScan("cn.wolfcode.luowowo.article.mapper")
public class ArticleServer {
    public static void main(String[] args) {
        SpringApplication.run(ArticleServer.class, args);
    }
}
