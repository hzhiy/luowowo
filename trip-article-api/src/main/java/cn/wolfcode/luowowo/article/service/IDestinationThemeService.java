package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.DestinationTheme;
import cn.wolfcode.luowowo.article.query.DestFitlerQuery;
import cn.wolfcode.luowowo.article.query.DestinationThemeQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 攻略推荐服务
 */
public interface IDestinationThemeService {
    /**
     * 分页
     * @param qo
     * @return
     */
    PageInfo queryForList(DestinationThemeQuery qo);

    /**
     * 增加或更新
     * @param destinationTheme
     */
    void saveOrUpdate(DestinationTheme destinationTheme);

    /**
     * 删除
     * @param id
     */
    void delete(Long id);



    /**
     * 根据主题的大标题查找下面的子标题
     * @param parentId
     * @return
     */
    List<DestinationTheme> queryByThemeParendId(Long parentId);

    /**
     * 热门主题的大标题
     * @return
     */
    List<DestinationTheme> queryByParendTheme();

    /**
     * 找月份集合
     * @return
     */
    List<DestinationTheme> queryMonth();

    /**
     * 根据月份的id找
     * @param monthId
     * @return
     */
    DestinationTheme queryByThemeId(Long monthId);


}
