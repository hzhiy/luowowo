package cn.wolfcode.luowowo.airticket.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AirPort extends BaseDomain {
    private String name;

    private String aircode;

}