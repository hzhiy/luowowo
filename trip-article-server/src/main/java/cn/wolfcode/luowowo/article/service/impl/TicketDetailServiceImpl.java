package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.TicketDetail;
import cn.wolfcode.luowowo.article.mapper.TicketDetailMapper;
import cn.wolfcode.luowowo.article.service.ITicketDetailService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 *
 */
@Service
public class TicketDetailServiceImpl implements ITicketDetailService{

    @Autowired
    private TicketDetailMapper ticketDetailMapper;


    @Override
    public TicketDetail getByTicketId(Long id) {
        return ticketDetailMapper.selectByTicketId(id);
    }

    @Override
    public List<TicketDetail> list() {
        return ticketDetailMapper.selectAll();
    }
}
