package cn.wolfcode.luowowo.cache.service.impl;

import cn.wolfcode.luowowo.cache.emum.RedisKeys;
import cn.wolfcode.luowowo.cache.service.IHotelRedisService;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by zhi on 2019/8/24.
 */
@Service
public class HotelRedisServiceImpl implements IHotelRedisService {

    @Autowired
    private StringRedisTemplate template;

    @Override
    public boolean favor(Long hid, Long uid) {
        String key = RedisKeys.USER_HOTEL_STATIS_FAVOR.join(uid.toString());
        String hids = template.opsForValue().get(key);
        List<String> hidList = new ArrayList<>();
        if (hids != null){
            hidList = JSON.parseArray(hids, String.class);
            if (hidList.contains(hid.toString())){
                hidList.remove(hid.toString());
                template.opsForValue().set(key, JSON.toJSONString(hidList));
                return false;
            }
        }
        hidList.add(hid.toString());
        template.opsForValue().set(key, JSON.toJSONString(hidList));
        return true;
    }

    @Override
    public List<String> get(Long id) {
        String key = RedisKeys.USER_HOTEL_STATIS_FAVOR.join(id.toString());
        String s = template.opsForValue().get(key);
        if (s == null){
            return Collections.EMPTY_LIST;
        }
        return JSON.parseArray(s, String.class);
    }
}
