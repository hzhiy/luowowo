<html lang="en">
<head>
    <title>攻略评论</title>
<#include "../common/header.ftl">
    <script type="text/javascript" src="/js/plugins/jqPaginator/jq-paginator.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#pagination").jqPaginator({
                        totalPages: ${page.totalPages!0},
                        visiblePages: 5,
                        currentPage: ${page.number+1}||1,
                    prev: '<a class="prev" href="javascript:void(0);">上一页<\/a>',
                    next: '<a class="next" href="javascript:void(0);">下一页<\/a>',
                    page: '<a href="javascript:void(0);">{{page}}<\/a>',
                    last: '<a class="last" href="javascript:void(0);" >尾页<\/a>',
                    onPageChange: function(page, type) {
                if(type == 'change'){
                    $("#currentPage").val(page);
                    $("#searchForm").submit();
                    }
                }
            })



            $('.updateState').click(function () {
                var item = $(this);
                var id = item.data('id');
                var state = item.data('state');

                $.get("/strategyComment/updateState", {id: id, state: state}, function (data) {
                    if (data.success){
                        location.reload();
                    }
                })
            })

        })

    </script>

</head>
<body>
<!--左侧菜单回显变量设置-->
<#assign currentMenu="strategyComment">

<div class="container-fluid " style="margin-top: 20px">
<#include "../common/top.ftl">
    <div class="row">
        <div class="col-sm-2">
        <#include "../common/menu.ftl">
        </div>
        <div class="col-sm-10">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-head-line">攻略评论</h1>
                </div>
            </div>
        <#setting number_format="#">
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/strategyComment/list" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <select name="state" class="form-control"  >
                    <option value="-1">全部</option>
                    <option value="0">公开</option>
                    <option value="1">隐藏</option>
                </select>
                <script>
                    $('select[name=state]').val(${qo.state});
                </script>
                <button type="submit">查询</button>
            </form>

            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>序号</th>
                    <th>文章标题</th>
                    <th>评语</th>
                    <th>评论人</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>
            <#list page.content as entity>
                <tr>
                    <td>${entity_index+1}</td>
                    <td>${entity.detailTitle!}</td>
                    <td>
                        <#if entity.content?length gt 10>
                        ${entity.content?substring(0,30)}...
                        <#else>${entity.content!}
                        </#if>
                    </td>
                    <td>${entity.username!}</td>
                    <td>${entity.stateName!}</td>

                    <td>
                        <a type="button" class="btn btn-info btn-xs updateState" data-state="0" data-id="${entity.id}">
                            <span class="glyphicon glyphicon-edit"></span> 公开
                        </a>

                        <a type="button" class="btn btn-danger btn-xs updateState" data-state="1" data-id="${entity.id}">
                            <span class="glyphicon glyphicon-trash"></span> 隐藏
                        </a>
                    </td>
                </tr>
            </#list>
            </table>
        <#--分页-->
            <div style="float: center">
                <div id="pagination" class="jq-pagination" style="display: inline;"></div>
                <div style="float: right;"><span style="line-height:30px"> 共${page.totalPages!}页 / ${page.totalElements}条&nbsp;&nbsp;&nbsp;</span>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="editModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">攻略主题添加/编辑</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/strategyTheme/saveOrUpdate" method="post" id="editForm">
                    <input type="hidden" value="" name="id">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">名称：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="name" name="name" placeholder="请输入攻略主题名称">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">状态：</label>
                        <div class="col-sm-6">
                            <select class="form-control" id="state" name="state">
                                <option value="0">正常</option>
                                <option value="1">禁用</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">排序：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="sequence" name="sequence" placeholder="请输入序号">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary submitBtn">保存</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</body>
</html>