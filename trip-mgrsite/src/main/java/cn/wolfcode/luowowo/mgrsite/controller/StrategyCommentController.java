package cn.wolfcode.luowowo.mgrsite.controller;

import cn.wolfcode.luowowo.comment.domain.StrategyComment;
import cn.wolfcode.luowowo.comment.query.StrategyCommentQuery;
import cn.wolfcode.luowowo.comment.service.IStrategyCommentService;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by zhi on 2019/8/13.
 */
@Controller
@RequestMapping("strategyComment")
public class StrategyCommentController {

    @Reference
    private IStrategyCommentService strategyCommentService;

    @RequestMapping("list")
    public String list(Model model, @ModelAttribute("qo") StrategyCommentQuery qo){

        Page page = strategyCommentService.queryForList(qo);
        model.addAttribute("page", page);
        return "strategyComment/list";
    }

    @RequestMapping("updateState")
    @ResponseBody
    public Object updateState(String id, int state){
        strategyCommentService.updateState(id, state);
        return AjaxResult.SUCCESS;
    }

}
