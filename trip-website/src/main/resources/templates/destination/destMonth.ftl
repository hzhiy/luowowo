
<div class="tiles tiles-${monthId}">

    <#list destMonthUp as dest>
    <div class="item col3">
        <a href="/destination/guide?id=${(dest.id)!}"  target="_blank"><img src="${(dest.coverUrl)!}" width="323" height="220">
            <div class="title">${(dest.name)!}</div>
        </a>
    </div>
    </#list>
    <#if destMonthDown??>
     <#list destMonthDown as dest>
    <div class="item col4">
        <a href="/destination/guide?id=${(dest.id)!}" target="_blank"><img src="${(dest.coverUrl)!}" width="238" height="220">
            <div class="title"><span>${(dest.name)!}<i></i></span></div>
        </a>
    </div>
     </#list>
    </#if>
    <div class="item col4">
        <a href="/destination/destFilter" target="_blank"><img src="http://p2-q.mafengwo.net/s10/M00/FF/E0/wKgBZ1jnHBCAeMgOAA5o9Lrjij428.jpeg?imageMogr2%2Fthumbnail%2F%21476x440r%2Fgravity%2FCenter%2Fcrop%2F%21476x440%2Fquality%2F100" width="238" height="220">
            <div class="more"><span>更多<i></i></span></div>
        </a>
    </div>
</div>
