package cn.wolfcode.luowowo.hotel.service.impl;

import cn.wolfcode.luowowo.hotel.domain.Hotel;
import cn.wolfcode.luowowo.hotel.mapper.HotelMapper;
import cn.wolfcode.luowowo.hotel.service.IHotelService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhi on 2019/8/22.
 */
@Service
public class HotelServiceImpl implements IHotelService {
    @Autowired
    private HotelMapper hotelMapper;

    @Override
    public List<Hotel> queryById(List<Long> hotelId) {
        return hotelMapper.selectListId(hotelId);
    }

    @Override
    public List<Hotel> queryByCityId(Long cityId) {
        List<Hotel> hotels = hotelMapper.selectByCityId(cityId);
        return hotels.subList(0, hotels.size()>8? 8 : hotels.size());
    }

    @Override
    public List<Hotel> list() {
        return hotelMapper.selectAll();
    }

    @Override
    public Hotel get(Long id) {
        Hotel hotel = hotelMapper.selectByPrimaryKey(id);
        if (hotel.getImgs() != null){
            List<String> list = new ArrayList<>();
            for (String s : hotel.getImgs().split(",")) {
                list.add(s);
            }
            hotel.setImages(list);
        }
        return hotel;
    }

    @Override
    public List<Hotel> getByDestId(Long id) {
        return hotelMapper.selectByDestId(id);
    }


}
