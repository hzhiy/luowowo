package cn.wolfcode.luowowo.comment.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by zhi on 2019/8/13.
 */
@Setter
@Getter
public class QuestionAnswerQuery extends QueryObject {
    private int state = -1;
    private Long questionId = -1L;
    private String order = "createTime";

    private int type;
    private int rank;

}
