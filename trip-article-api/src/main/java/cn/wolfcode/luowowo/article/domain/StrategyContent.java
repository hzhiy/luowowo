package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by zhi on 2019/8/10.
 */
@Setter
@Getter
public class StrategyContent extends BaseDomain {
    private String content;

}
