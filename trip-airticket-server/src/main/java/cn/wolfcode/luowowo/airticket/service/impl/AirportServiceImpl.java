package cn.wolfcode.luowowo.airticket.service.impl;

import cn.wolfcode.luowowo.airticket.domain.AirPort;
import cn.wolfcode.luowowo.airticket.mapper.AirPortMapper;
import cn.wolfcode.luowowo.airticket.service.IAirportService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Administrator on 2019/8/25.
 */
@Service
public class AirportServiceImpl implements IAirportService {

    @Autowired
    private AirPortMapper airPortMapper;

    @Override
    public List<AirPort> listByAirCode(String orgCity) {
        return airPortMapper.selectByAircode(orgCity);
    }
}
