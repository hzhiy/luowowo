package cn.wolfcode.luowowo.website.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("packageTour")
public class PackageTourController {


    @RequestMapping("")
    public String list(){
        return "packageTour/list";
    }


}
