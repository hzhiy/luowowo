package cn.wolfcode.luowowo.search.service;

import cn.wolfcode.luowowo.search.template.DestinationTemplate;
import cn.wolfcode.luowowo.search.template.UserInfoTemplate;
import com.github.pagehelper.Page;

import java.util.List;

/**
 * Created by zhi on 2019/8/17.
 */
public interface IUserInfoTemplateService {

    /**
     * 新增或者修改
     * @param userInfoTemplate
     */
    void saveOrUpdate(UserInfoTemplate userInfoTemplate);

    /**
     * 通过地区名称查询
     * @param destName
     * @return
     */
    List<UserInfoTemplate> findByDestName(String destName);
}

