package cn.wolfcode.luowowo.article.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by zhi on 2019/8/21.
 */
@Setter
@Getter
public class HotelQuery extends QueryObject {
    private Long destThemeId;

    private String name;
    private String checkIn;
    private String checkOut;
}
