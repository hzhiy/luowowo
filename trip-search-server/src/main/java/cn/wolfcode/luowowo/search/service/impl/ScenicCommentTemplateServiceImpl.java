package cn.wolfcode.luowowo.search.service.impl;

import cn.wolfcode.luowowo.search.repository.IScenicCommentSearchRepository;
import cn.wolfcode.luowowo.search.service.IScenicCommentTemplateService;
import cn.wolfcode.luowowo.search.template.ScenicCommentTemplate;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

/**
 * 景点评论查询 服务
 */
@Service
public class ScenicCommentTemplateServiceImpl implements IScenicCommentTemplateService {

    @Autowired
    private IScenicCommentSearchRepository dao;

    @Autowired
    private ElasticsearchTemplate template;

    @Override
    public void save(ScenicCommentTemplate template) {
        dao.save(template);
    }
}
