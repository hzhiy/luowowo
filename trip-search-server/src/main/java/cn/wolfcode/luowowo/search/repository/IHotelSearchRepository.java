package cn.wolfcode.luowowo.search.repository;

import cn.wolfcode.luowowo.search.template.HotelTemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by zhi on 2019/8/17.
 */
@Repository
public interface IHotelSearchRepository extends ElasticsearchRepository<HotelTemplate, Long>{

}
