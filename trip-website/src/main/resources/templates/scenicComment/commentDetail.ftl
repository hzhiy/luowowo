<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>景点详细</title>
    <link href="/styles/base.css" rel="stylesheet" type="text/css">
    <link href="/styles/travelnotedetail.css" rel="stylesheet" type="text/css">
    <link href="/styles/myComment.css" rel="stylesheet" type="text/css">
    <link href="/styles/upload.css" rel="stylesheet" type="text/css">
    <link href="https://css.mafengwo.net/css/cv/css+base:css+jquery.suggest:css+plugins:css+plugins+jquery.jgrowl:css+other+popup:css+mfw-header.2015^YlVS^1559526017.css" rel="stylesheet" type="text/css"/>
    <link href="https://css.mafengwo.net/css/cv/css+mdd+scenic-v2:css+mdd+mfw-reviews^YFI^1530619858.css" rel="stylesheet" type="text/css"/>
    <link href="https://css.mafengwo.net/css/mdd/mfw-reviews.css?1530619858" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/js/jquery/jquery.js"></script>
    <script type="text/javascript" src="/js/plugins/jquery-form/jquery.form.js"></script>
    <script type="text/javascript" src="/js/system/travelnotedetail.js"></script>
    <script type="text/javascript" src="/js/system/emoji.js"></script>
    <script type="text/javascript" src="/js/plugins/jqPaginator/jq-paginator.min.js"></script>
    <script type="text/javascript" src="/js/system/common.js"></script>
    <script type="text/javascript" src="/js/plugins/comment/jQuery.upload.js"></script>
    <script>
        $(function () {

            //点评点击事件
            $(".btn-reviews").click(function () {
                var scroll_offset = $(".mfw-reviews").offset(); //得到pos这个div层的offset，包含两个值，top和left
                $("body,html").animate({
                    scrollTop: scroll_offset.top //让body的scrollTop等于pos的top，就实现了滚动
                }, 0);
            });


            /*//显示点评
            $(".btn-commentview").click(function () {
                var scroll_offset = $(".commentview").offset();
                $("body,html").animate({
                    scrollTop: scroll_offset.top
                }, 0);
            });*/

            //星星点击事件
            var num;
            $(".click-star a").click(function () {

                //获取星星的数值
                num = $(this).data('num');

                $(".star").removeClass().addClass('star _j_starcount star' + num);
            });
            //发表点评
            $(".commentBtn").click(function () {
                <#--<#if userInfo?? ><#else ></#if>-->
                if (!$("#commentContent").val()) {
                    alert("评论不能为空");
                    return;
                }

                $("#star_num").val(num);

                $("#commentForm").ajaxSubmit(function (data) {
                    $('._j_commentlist').prepend(data);
                    $('#clickTop').click();
                });
            });
            //点赞
            $(".useful").click(function () {
                var commentId = $(this).data("id"); // 文章id
                var t = $(this);

                $.get("/scenic/scenicThumbUp", {toid: commentId}, function (data) {
                    if (data.success) {
                        t.children('.useful-num').html(data.data.thumbupnum);
                        popup("你很6哦");
                    } else {
                        popup("没登录");
                        var number = (data.data.thumbupnum);
                        t.children('.useful-num').html(number)
                    }
                });
            });



            // 多图片上传相关
            var delParent;
            var defaults = {
                fileType         : ["jpg","png","bmp","jpeg"],   // 上传文件的类型
                fileSize         : 1024 * 1024 * 10                  // 上传文件的大小 10M
            };
            /*点击图片的文本框*/
            $(".file").change(function(){

                $('#fileForm').ajaxSubmit(function (data) {
                    if (data.status){
                        console.log(data.url);
                        $('input[name=imgUrls]').val(data.url);
                    }
                });
                var idFile = $(this).attr("id");
                var file = document.getElementById(idFile);
                var imgContainer = $(this).parents(".z_photo"); //存放图片的父亲元素
                var dd = $('#_j_addpicbtns').closest('dd');
                var fileList = file.files; //获取的图片文件
                var input = $(this).parent();//文本框的父亲元素
                var imgArr = [];
                //遍历得到的图片文件
                var numUp = imgContainer.find(".up-section").length;
                var totalNum = numUp + fileList.length;  //总的数量
                if(fileList.length > 20 || totalNum > 20 ){
                    alert("上传图片数目不可以超过20个，请重新选择");  //一次选择上传超过20个 或者是已经上传和这次上传的到的总数也不可以超过20个
                }
                else if(numUp < 20){
                    fileList = validateUp(fileList);
                    for(var i = 0;i<fileList.length;i++){
                        var imgUrl = window.URL.createObjectURL(fileList[i]);
                        imgArr.push(imgUrl);
                        $(dd).after('' +
                                '<dd class="_j_picitem" >' +
                                '<div class="place">' +
                                '<div class="img">' +
                                '<img class="_j_edit_src" src="'+imgArr[i]+'" style="width:120px;height:120px"> ' +
                                '</div> ' +
                                '<div class="title">' +
                                '<h4 class="_j_edit_title"></h4>' +
                                '</div> <div class="mask-operate"> ' +
                                '<a class="btn-remove _j_remove_dd"></a> ' +
                                '</div> ' +
                                '</div> ' +
                                '</dd>')
                    }
                }
                setTimeout(function(){
                    $(".up-section").removeClass("loading");
                    $(".up-img").removeClass("up-opcity");
                },450);
                numUp = imgContainer.find(".up-section").length;
                if(numUp >= 20){
                    $(this).parent().hide();
                }
            });
            function validateUp(files){
                var arrFiles = [];//替换的文件数组
                for(var i = 0, file; file = files[i]; i++){
                    //获取文件上传的后缀名
                    var newStr = file.name.split("").reverse().join("");
                    if(newStr.split(".")[0] != null){
                        var type = newStr.split(".")[0].split("").reverse().join("");
                        if(jQuery.inArray(type, defaults.fileType) > -1){
                            // 类型符合，可以上传
                            if (file.size >= defaults.fileSize) {
                                alert(file.size);
                                alert('您这个"'+ file.name +'"文件大小过大');
                            } else {
                                // 在这里需要判断当前所有文件中
                                arrFiles.push(file);
                            }
                        }else{
                            alert('您这个"'+ file.name +'"上传类型不符合');
                        }
                    }else{
                        alert('您这个"'+ file.name +'"没有类型, 无法识别');
                    }
                }
                return arrFiles;
            }

            $('.add-place').click(function () {
                console.log(1);
                $('.file').click();
            })

        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#case").upload(
                    //该函数为点击放大镜的回调函数，如没有该函数，则不显示放大镜
                    function (_this, data) {  } );
        })
    </script>
</head>

<body>

<form id="fileForm" action="/trussImgs" style="display: none" method="post" enctype="multipart/form-data">
    <input type="file" name="file" id="file" class="file" accept="image/jpeg,image/jpeg,image/png,image/bmp" multiple>
</form>
<#--<div class="lww_header">
	<div class="header_wrap">
		<div class="header_logo">
			<a href="javascript:;" class="lww_logo"></a>
		</div>
		<ul class="header_nav">
			<li><a href="./index.html">首页</a></li>
			<li class="header_nav_active"><a href="./destination.html">目的地</a></li>
			<li><a href="./gonglve.html">旅游攻略</a></li>
			<li><a href="javascript:;">去旅行<i class="icon_caret_down"></i></a></li>
			<li><a href="javascript:;">机票</a></li>
			<li><a href="javascript:;">酒店</a></li>
			<li><a href="javascript:;">社区<i class="icon_caret_down"></i></a></li>
		</ul>
		<div class="header_search">
			<input type="text" />
			<a class="icon_search"></a>
		</div>
		<div class="login_info">
			<div class="head_user">
				<a href="javascript:;">
					<img src="./images/user.png" />
					<i class="icon_caret_down"></i>
				</a>
			</div>
			<div class="header_msg">
				消息<i class="icon_caret_down"></i>
			</div>
			<div class="header_daka">
				<a href="javascript:;">打卡</a>
			</div>
		</div>
	</div>
	<div class="shadow"></div>
</div>-->
<div>
    <div class="lww_header">
        <div class="header_wrap">
            <div class="header_logo">
                <a href="javascript:;" class="lww_logo"></a>
            </div>
            <ul class="header_nav">
                <li name="index"><a href="/">首页</a></li>
                <li  name="destination"><a href="/destination">目的地</a></li>
                <li  name="strategy" ><a href="/strategy">旅游攻略</a></li>
                <li  name="travel" ><a href="/travel">旅游日记</a></li>
                <li  name="hotel" ><a href="/hotel">订酒店</a></li>
                <li  name=""><a href="/ticket">门票</a></li>
                <li name=""><a href="javascript:;">社区<i class="icon_caret_down"></i></a></li>
            </ul>
            <div class="header_search">
                <input type="text" />
                <a class="icon_search"></a>
            </div>

            <#if userInfo??>
        <div class="login_info">
            <div class="head_user">
                <a href="/mine/home">
                    <img src="${(userInfo.headImgUrl)!'/images/default.jpg'}" />
                    <i class="icon_caret_down"></i>
                </a>
            </div>
            <div class="header_msg">
                消息<i class="icon_caret_down"></i>
            </div>
            <div class="header_daka">
                <a href="javascript:;">打卡</a>
            </div>
        </div>
            <#else>
        <div class="login-out">
            <a class="weibo-login" href="#" title="微博登录" rel="nofollow"></a>
            <a class="qq-login" href="#" title="QQ登录" rel="nofollow"></a>
            <a class="weixin-login" href="#" title="微信登录" rel="nofollow"></a>
            <a id="_j_showlogin" title="登录骡窝窝" href="/login.html" rel="nofollow">登录</a>
            <span class="split">|</span>
            <a href="/regist.html" title="注册帐号" rel="nofollow">注册</a>
        </div>
            </#if>
        </div>
        <div class="shadow"></div>
    </div>
</div>

<!----------------------------------------->

<div class="container" data-cs-t="景点详情页">

<div class="row row-top">

    <div class="wrapper">
        <div class="extra">
            <!-- 收藏去过 S-->
            <div class="action _j_rside want-been">
                <div class="been-box">
                    <!-- 天气 S-->
                    <a class="weather" data-cs-p="天气">
                        <iframe width="400" scrolling="no" height="50" frameborder="0" allowtransparency="true" src="//i.tianqi.com/index.php?c=code&id=12&icon=1&num=5&site=12"></iframe>
                    </a>
                    <a class="_j_beenpoi btn-been _j_hovergo" href="/path/" target="_blank" title="添加至我的足迹"
                       data-cs-p="足迹">
                        <i class="icon"></i>
                        <span class="txt">去过</span>
                    </a>
                </div>
                <a class="_j_favpoi btn-collect " href="/plan/fav.php?iMid=10088" target="_blank" title="添加收藏"
                   data-cs-p="收藏">
                    <i class="icon"></i>
                    <span class="txt">收藏</span>
                </a>
            </div>
            <!-- 收藏去过 E-->
        </div>
        <!-- 面包屑 S-->
       <div class="crumb" data-cs-p="面包屑">
            <div class="item"><a href="/mdd/" target="_blank">目的地</a><em>&gt;</em></div>
            <div class="item">
                <div class="drop">
                    <span class="hd"><a href="/travel-scenic-spot/mafengwo/10088.html">${(toast.name)!}<i></i></a></span>
                    <div class="bd">
                        <i class="arrow"><b></b></i>
                        <div class="col">
                            <ul class="clearfix">
                                <li><a href="/jd/10088/gonglve.html" target="_blank">${(toast.name)!}景点</a></li>
                                <li><a href="/hotel/10088/" target="_blank">${(toast.name)!}酒店</a></li>
                                <li><a href="/cy/10088/gonglve.html" target="_blank">${(toast.name)!}美食</a></li>
                                <li><a href="/gw/10088/gonglve.html" target="_blank">${(toast.name)!}购物</a></li>
                                <li><a href="/yl/10088/gonglve.html" target="_blank">${(toast.name)!}娱乐</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <em>&gt;</em>
            </div>
            <div class="item cur">${(detail.name)!}景点</div>
        </div>
        <!-- 面包屑 E-->


        <!-- POI名称 S-->
        <div class="title" id="toptop">
            <h1>${(detail.name)!}</h1>
            <div class="en">Canton Tower</div>
        </div>
        <!-- POI名称 E-->

        <!-- 快捷导航 S-->
        <div style="height: 60px;">
            <div class="r-nav" id="poi-navbar" data-cs-p="快捷导航">
                <ul class="clearfix">
                    <li data-scroll="overview" class="on">
                        <a href="#toptop" title="概况">概况</a>
                    </li>
                    <li data-scroll="commentlist">
                        <a title="蜂蜂点评" id="clickTop" href="#commentlist">蜂蜂点评<span>（${totalNum!}）</span></a>
                    </li>


                    <li data-scroll="comment" class="nav-right">
                        <a class="btn-reviews" title="我要点评">我要点评</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- 快捷导航 E-->

    </div>
</div>

<script type="text/javascript">
    $(function() {
        //面包屑
        $('.drop').hover(function () {
            var target = $(this);
            clearTimeout(target.data('hideTimer'));
            target.addClass('open');
            target.children('bd').fadeIn(200);
        }, function () {
            var target = $(this);
            target.data("hideTimer", setTimeout(function () {
                target.removeClass('open');
                target.children('bd').fadeOut(200);
            }, 100));
        });
        //导航
        var $navbar = $('#poi-navbar'),
                offset_top,
                $lis = $navbar.find("li").not('.nav-right');
        $('<div/>').insertBefore($navbar).append($navbar).height($navbar.outerHeight(true));
        $(function () {
            offset_top = $navbar.offset().top;
            $(window).bind('scroll.poinav', setFixed).trigger("scroll.poinav");
        });
        $(document).delegate("[data-scroll]", "click", function () {
            scrollTo($(this));
        });

        function setFixed() {
            var $rows = $('body >.container >[data-anchor]'),
                    scrolltop = $(document).scrollTop(),
                    h,
                    _lis = $lis.filter(":visible"),
                    currIndex;
            if (scrolltop > offset_top) {
                if (!$navbar.hasClass('fixed')) {
                    $navbar.addClass('fixed');
                }
                h = $navbar.outerHeight(true);
                for (var $li, $row, top, i = 0, j = _lis.length; i < j; i++) {
                    $row = $rows.filter("[data-anchor=" + ($li = _lis.eq(i)).attr("data-scroll") + "]");
                    if ($row[0] && (top = $row.offset().top) && (/*i == (j - 1) ||*/
                                    ((top - h <= scrolltop) && (top + $row.outerHeight() - h > scrolltop)))) {
                        currIndex = i;
                        break;
                    }
                }
                if (i == j) {
                    _lis.removeClass("on");
                } else {
                    _lis.eq(currIndex || 0).addClass("on").siblings().removeClass("on");
                }
            } else {
                if ($navbar.hasClass('fixed')) {
                    $navbar.removeClass('fixed');
                }
                _lis.eq(0).addClass("on").siblings().removeClass("on");
            }
        }
    })
</script>
<div data-anchor="overview">
    <div class="row row-picture row-bg">
        <div class="wrapper">
            <a class="photo" data-cs-p="相册" href="/photo/poi/25091.html" target="_blank">

                <div class="bd">
                    <div class="pic-big"><img
                            src="${(detail.imgArr[0])!}"
                            width="690" height="370"></div>
                    <div class="pic-small"><img
                            src="${(detail.imgArr[1])!}"
                            width="305" height="183"></div>
                    <div class="pic-small"><img
                            src="${(detail.imgArr[2])!}"
                            width="305" height="183"></div>
                    <span>34114张图片</span>
                </div>
            </a>
        </div>
    </div>

    <!-- 简介 S -->
    <div class="mod mod-detail" data-cs-p="概况">
        <div class="summary">
            ${detail.info}
        </div>

        <ul class="baseinfo clearfix">
            <li class="tel">
                <div class="label">电话</div>
                <div class="content">${detail.tel}</div>
            </li>
            <li class="item-site">
                <div class="label">网址</div>
                <div class="content"><a href="http://www.cantontower.com/" target="_blank" rel="nofollow">${(detail.url)!}</a>
                </div>
            </li>
            <li class="item-time">
                <div class="label">用时参考</div>
                <div class="content">${detail.takeTime}</div>
            </li>
        </ul>

        <dl>
            <dt>交通</dt>
            <dd>${detail.arrivalWay}
            </dd>
        </dl>
        <dl>
            <dt>门票</dt>
            <dd>
                <div >
                    ${detail.ticketing}
                </div>
            </dd>
        </dl>
        <dl>
            <dt>开放时间</dt>
            <dd>
                <div>${detail.opentime}
                </div>

            </dd>
        </dl>

        <div style="color:#999;font-size:12px;" data-cs-p="概况-感谢蜂蜂">
            *信息更新时间：2019-07-14&nbsp;&nbsp;&nbsp;&nbsp;
            感谢蜂蜂 <a href="/u/79650791.html" target="_blank">一点点的爱</a>
            参与了编辑
        </div>
    </div>
    <!-- 简介 E -->

    <!-- 内部景点 S -->
    <div data-anchor="subPoilist">
        <div id="pagelet-block-bb456ee5b764682811223f9b8d30739e" class="pagelet-block"
             data-api=":poi:pagelet:poiSubPoiApi" data-params="{&quot;poi_id&quot;:&quot;25091&quot;}" data-async="1"
             data-controller="/js/poi/ControllerPoiSubPoi">
            <div class="mod mod-innerScenic" data-cs-p="内部景点">
                <div class="mhd">内部景点</div>
                <div class="mbd">
                    <ul class="clearfix">

                        <#list includeS as inc >
                        <li>
                            <a href="/scenic/detail?scenicId=${inc.id}&destId=${inc.dest_id}" target="_blank" title="${inc.name}">
                                <img src="${inc.imgArr[0]}"
                                     width="235" height="150">
                                <span class="num num-top">${inc_index + 1}</span>
                                <div class="info">
                                    <h3>${(inc.name)!}</h3>
                                    <span><em>1341</em>人去过</span>
                                </div>
                            </a>
                        </li>
                        </#list>

                    </ul>
                </div>
                <div class="more more-subpoi">
                    <a class="btn-subpoi" data-page="1">查看更多</a>
                </div>
            </div>
            <style>
                .mod-innerScenic .more {
                    margin-top: 20px;
                    text-align: center;
                }

                .mod-innerScenic .more a {
                    display: inline-block;
                    width: 160px;
                    height: 50px;
                    background-color: #fff;
                    border: 1px solid #fc9c27;
                    line-height: 50px;
                    color: #ff9d00;
                    font-size: 14px;
                    border-radius: 4px;
                    text-align: center;
                }

                .mod-innerScenic .num {
                    width: 40px;
                }
            </style>
        </div>
    </div>
    <!-- 内部景点 E -->

</div>

<!--评论-->
<div data-anchor="commentlist" id="commentlist">

    <div id="pagelet-block-15f9d6d9ad9f6c363d2d27120e8a6198" class="pagelet-block"
         data-api=":poi:pagelet:poiCommentListApi" data-params="{&quot;poi_id&quot;:&quot;25091&quot;}" data-async="1"
         data-controller="/js/poi/ControllerPoiComment">
        <div class="mod mod-reviews" data-cs-p="评论列表">
            <div class="mhd mhd-large">蜂蜂点评<span>（共有<em>${totalNum!}</em>条真实评价）</span></div>
            <div class="review-nav">
                <ul class="clearfix">
                    <li data-type="0" data-category="0" class="on"><span class="divide"></span>
                        <a href="javascript:void(0);"><span>全部</span></a>
                    </li>
                    <li data-type="0" data-category="2" class="">
                        <span class="divide"></span>
                        <a href="javascript:void(0);"><span>有图</span><span class="num"> (${tagsCount.top4Tags[0]}条)</span></a>
                    </li>
                    <li data-type="1" data-category="13" class="">
                        <span class="divide"></span>
                        <a href="javascript:void(0);">
                            <span>好评</span>
                            <span class="num">（${tagsCount.top4Tags[1]} 条 ）</span>
                        </a>
                    </li>
                    <li data-type="1" data-category="12" class="">
                        <span class="divide"></span>
                        <a href="javascript:void(0);">
                            <span>中评</span>
                            <span class="num">（${tagsCount.top4Tags[2]} 条）</span>
                        </a>
                    </li>
                    <li data-type="1" data-category="11" class="">
                        <span class="divide"></span>
                        <a href="javascript:void(0);">
                            <span>差评</span>
                            <span class="num">（${tagsCount.top4Tags[3]} 条）</span>
                        </a>
                    </li>

                    <#--ES模板拼接-->

                    <li data-type="2" data-category="178487584" class="">
                        <span class="filter-word divide"></span>
                        <a href="javascript:void(0);">标志性建筑<span class="num">（535人提及）</span></a>
                    </li>
                    <li data-type="2" data-category="103125314" class="">
                        <span class="filter-word divide"></span>
                        <a href="javascript:void(0);">人很多<span class="num">（141人提及）</span></a>
                    </li>
                    <li data-type="2" data-category="183864017" class="">
                        <span class="filter-word divide"></span>
                        <a href="javascript:void(0);">值得去<span class="num">（118人提及）</span></a>
                    </li>
                    <li data-type="2" data-category="104277092" class="">
                        <span class="filter-word divide"></span>
                        <a href="javascript:void(0);">恐高<span class="num">（112人提及）</span></a>
                    </li>
                    <li data-type="2" data-category="176664633" class="">
                        <span class="filter-word divide"></span>
                        <a href="javascript:void(0);">交通方便<span class="num">（92人提及）</span></a>
                    </li>
                    <li data-type="0" data-category="1" class="">
                        <span class="divide"></span>
                        <a href="javascript:void(0);"><span>金牌点评</span><span class="num"> (33条)</span></a>
                    </li>
                </ul>
            </div>
            <div class="loading-img" style="display: none;">
                <img src="http://images.mafengwo.net/images/weng/loading3.gif"> Loading...
            </div>
            <div class="_j_commentlist">
            <#list page.content as comment>
                    <div class="mfw-cmt rev-list">
                        <ul>
                             <li class="rev-item comment-item clearfix">

                                <script>
                                            $(function () {
                                                //回复评论
                                                $("#_j_reply_list${(comment.id)!}").on("click", ".replyBtn", function () {
                                                    var username = $(this).data("username");
                                                    var toid = $(this).data("toid");
                                                    var formid = $(this).data("formid");
                                                    var headurl = $(this).data("headurl");

                                                    $("#toid").val(toid);
                                                    $("#formid").val(formid);
                                                    $("#headurl").val(headurl);
                                                    $("#username").val(username);

                                                    $("#reply${(comment.id)!}").attr("placeholder", "回复：" + username);

                                                });


                                                //回复用户
                                                $("#_j_reply_item${(comment.id)!}").on("click", ".replyBtnTypeOne", function () {
                                                    var username = $(this).data("username");
                                                    var toid = $(this).data("toid");
                                                    var formid = $(this).data("formid");
                                                    var headurl = $(this).data("headurl");

                                                    $("#toid").val(toid);
                                                    $("#formid").val(formid);
                                                    $("#headurl").val(headurl);
                                                    $("#username").val(username);
                                                    $("#type").val(1);

                                                    $("#reply${(comment.id)!}").attr("placeholder", "回复：" + username);

                                                });
                                                //发表回复
                                                $(".btn_submit_reply${(comment.id)!}").click(function () {
                                                    if (!$("#reply${(comment.id)!}").val()) {
                                                        alert("评论不能为空");
                                                        return;
                                                    }
                                                    $("#replyForm${(comment.id)!}").ajaxSubmit(function (data) {
                                                        $("#reply${(comment.id)!}").val("");
                                                        $("#reply${(comment.id)!}").attr("placeholder", "添加回复");
                                                        $("#comment_list${comment.id}").append(data);
                                                        $("#type").val("0");
                                                    })
                                                });

                                                //点击评论
                                                $("#pinglun${comment.id!}").click(function () {
                                                    $("#addcomment${(comment.id)!}").toggle();
                                                });

                                            })
                                        </script>

                                        <!-- 点评 -->
                                        <div class="mfw-cmt _j_comment_item" id="_j_reply_list${(comment.id)!}">
                                            <div class="user"><a class="avatar" href="#" target="_blank"><img
                                                    src="${comment.headUrl!}"
                                                    width="48" height="48"></a><span
                                                    class="level">LV.${comment.level!}</span>
                                            </div>
                                            <a class="useful" data-id="${comment.id!}"
                                               title="点赞">
                                                <i></i><span id="uNum" class="useful-num">${comment.thumbupnum!}</span>
                                            </a>
                                            <a class="name" href="#" target="_blank">${comment.username!}</a>
                                            <span class="s-star s-star${comment.starNum!}"></span>
                                            <p class="rev-txt">${comment.content!}</p>


                                            <div class="rev-img">
                                                <#list comment.imgArr! as img>
                                                    <a href="#" target="_blank"><img src="${img}" width="200" height="120"></a>
                                                </#list>
                                            </div>

                                            <div class="info clearfix">
                                                <a role="button" class="_j_reply re_reply replyBtn"
                                                   data-toid="${(comment.id)!}"
                                                   data-formid="${(comment.userId)!}"
                                                   data-headurl="${(comment.headUrl)!}"
                                                   data-username="${(comment.username)!}" title="添加回复">回复
                                                </a>


                                                <a class="btn-comment _j_comment commentReplyBtn" title="评论"
                                                   data-toid="${comment.id!}" id="pinglun${comment.id!}">评论
                                                </a>
                                                <span class="time">${(comment.createTime?string("yyyy-MM-dd"))!}</span>
                                            </div>
                                        </div>


                                        <!-- 回复模板 -->
                                        <div class="mfw-cmt _j_reply_item" id="_j_reply_item${(comment.id)!}">
                                            <div class="comment add-reply ">

                                                <ul class="more_reply_box comment_list" id="comment_list${comment.id}">

                                                    <#if comment.usersReply??>

                                                        <#list comment.usersReply as ur>

                                                        <#if ur.type = 0>
                                                            <li>
                                                                <a href="/u/63107989.html" target="_blank">
                                                                    <img src="http:${ur.headUrl}"
                                                                         width="16" height="16">🍓${ur.refComment.username}🍓
                                                                </a>
                                                                ：${ur.refComment.content}
                                                                <a role="button" class="_j_reply re_reply replyBtnTypeOne" data-toid="${ur.id}" data-formid="${ur.userId}" data-headurl="${ur.headUrl}"
                                                                   data-username="${ur.username}" title="添加回复">回复</a>
                                                                <br><span
                                                                    class="time">${(ur.refComment.createTime?string("yyyy-MM-dd"))!}
                                                            </span>
                                                            </li>
                                                        <#else >
                                                            <li>
                                                                <a href="/u/52068941.html" target="_blank">
                                                                    <img src="${ur.refComment.headUrl}"
                                                                         width="16" height="16">${ur.refComment.username}
                                                                </a>
                                                                回复${ur.username}：${ur.refComment.content}
                                                                <a role="button" class="_j_reply re_reply replyBtnTypeOne" data-toid="${ur.id}" data-formid="${ur.userId}" data-headurl="${ur.headUrl}"
                                                                   data-username="${ur.username}" title="添加回复">回复</a>
                                                                <br><span
                                                                    class="time">${(ur.refComment.createTime?string("yyyy-MM-dd"))!}
                                                            </span>
                                                            </li>
                                                        </#if>

                                                        <div class="mcmt-quote">
                                                        </div>
                                                        <div class="mcmt-word">

                                                        </div>
                                                        </#list>
                                                    </#if>
                                                </ul>


                                                <div id="addcomment${(comment.id)!}" class="add-comment hide reply-form">
                                                    <form action="/scenic/usersReply" method="post" id="replyForm${(comment.id)!}">

                                                        <input type="hidden" name="toid"  id="toid">
                                                        <input type="hidden" name="fromId"  id="formid">
                                                        <input type="hidden" name="headUrl"  id="headurl">
                                                        <input type="hidden" name="fromName"  id="username">
                                                        <input type="hidden" name="type" value="0" id="type">

                                                        <textarea class="comment_reply" style="color: #000000;" id="reply${(comment.id)!}" name="content"></textarea>
                                                    </form>
                                                    <a class="btn btn_submit_reply${(comment.id)!}">发送</a>
                                                </div>
                                            </div>
                                        </div>
                             </li>
                        </ul>
                    </div>
            </#list>

            <script>
                $(function () {
                    $("#pagination").jqPaginator({
                        totalPages: ${page.totalPages!0},
                        visiblePages: 5,
                        currentPage: ${page.number+1}||1,
                            prev: '<a class="prev" href="javascript:void(0);">上一页<\/a>',
                            next: '<a class="next" href="javascript:void(0);">下一页<\/a>',
                            page: '<a href="javascript:void(0);">{{page}}<\/a>',
                            last: '<a class="last" href="javascript:void(0);" >尾页<\/a>',
                            onPageChange: function(page, type) {
                        if(type == 'change'){
                            $("#currentPage").val(page);
                            $("#searchForm").submit();
                        }

                    }
                })
            </script>
                <#--<div align="right" class="m-pagination">
                    <span class="count">共<span>5</span>页 / <span>${totalNum!}</span>条</span>

                    <span class="pg-current">1</span>
                    <a class="pi" data-page="2" rel="nofollow" title="第2页">2</a>
                    <a class="pi" data-page="3" rel="nofollow" title="第3页">3</a>
                    <a class="pi" data-page="4" rel="nofollow" title="第4页">4</a>
                    <a class="pi" data-page="5" rel="nofollow" title="第5页">5</a>

                    <a class="pi pg-next" data-page="2" rel="nofollow" title="后一页">后一页</a>
                    <a class="pi pg-last" data-page="5" rel="nofollow" title="末页">末页</a>
                </div>-->

                <div style="float: right">
                    <div style="float: left;" ><span style="line-height:30px"> 共${page.totalPages!}页 / ${page.totalElements}条&nbsp;&nbsp;&nbsp;</span></div>
                    <div id="pagination" class="jq-pagination" style="display: inline;"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div data-anchor="comment">
    <div class="row row-reviewForm" id="comment_20190714202243" data-cs-p="点评">
        <div class="wrapper">

            <!-- 点评 S -->
            <div data-anchor="comment">
                <div class="row row-reviewForm" id="comment_20190718192433" data-cs-p="点评">
                    <div class="wrapper">

                        <div class="mfw-reviews">
                            <div id="_j_commentform_cnt">
                                <h2>
                                    <strong>${(detail.name)!}</strong>
                                    <em>*</em>为必填选项
                                </h2>
                                <form action="/scenic/addComment" method="post" class="_j_commentdialogform" id="commentForm">
                                    <input type="hidden" name="scenicId" value="1"/>
                                    <input type="hidden" name="scenicName" value="${detail.name}"/>
                                    <input type="hidden" id="star_num" name="starNum" value=""/>
                                    <input type="hidden" id="comment_type" name="type" value="0"/>


                                    <div class="review-item item-star">
                                        <div class="label"><em>*</em>总体评价</div>
                                        <div class="review-star _j_rankblock" data-star="" name="rank">
                                            <!-- 星星数量 -->
                                            <span class="star _j_starcount"></span>
                                            <div class="click-star _j_starlist">
                                                <a role="button" title="千万别去" rel="nofollow" data-num="1"></a>
                                                <a role="button" title="不推荐" rel="nofollow" data-num="2"></a>
                                                <a role="button" title="一般般" rel="nofollow" data-num="3"></a>
                                                <a role="button" title="值得一去" rel="nofollow" data-num="4"></a>
                                                <a role="button" title="必须推荐" rel="nofollow" data-num="5"></a>
                                            </div>

                                        </div>
                                        <span class="txt-tips _j_startip">点击星星打分</span>
                                    </div>

                                    <div class="review-item item-comment">
                                        <div class="label"><em>*</em>评价</div>
                                        <div class="content">
                                        <textarea class="_j_commentarea" name="content" essential="1"
                                                  data-inputname="点评内容" placeholder="详细、客观、真实，130字以上为佳！上传图片会加分哦！"
                                                  data-minlen="15" data-maxlen="1000" id="commentContent"></textarea>
                                            <p class="_j_commentcounttip">15-1000个字</p>
                                        </div>
                                    </div>
                                    <div class="review-item item-photo">
                                        <div class="label">上传照片</div>
                                        <div class="content">
                                            <dl class="upload-box _j_piclist">
                                                <dd data-commentid="" id="_j_addpicbtns" ids="0" style="position: relative;">
                                                    <a class="add-place"><i></i></a>
                                                    <div id="html5_1dfo6usgd1maqh5812ivtkf1n223_container"
                                                         class="moxie-shim moxie-shim-html5"
                                                         style="position: absolute; top: 0px; left: 0px; width: 120px; height: 120px; overflow: hidden; z-index: -1;">
                                                        <input name="imgUrls" id="imgUrls" type="text" style="display: none">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </div>
                                    <div class="review-item item-action">
                                        <a class="btn-large commentBtn" role="button" title="提交点评">提交点评</a>
                                    </div>
                                </form>
                            </div>
                        </div>

                </div>
            </div>
			 <script type="text/javascript">
                // 星星数
			   $(function () {
                   $("._j_starlist a").mouseover(function () {
                        var index = $(this).index()+1;
                        var text = $(this).attr("title");

                       $(this).closest("div").prev().addClass("star"+index);
                       $(this).closest("div").parent().next().html(text);
                   }).mouseout(function () {
                       var index = $(this).index()+1;
                       var text = $(this).attr("title");
                       $(this).closest("div").prev().removeClass("star"+index);
                       $(this).closest("div").parent().next().html(text);

                       var x = $(this).closest("div").prev().prev().val();
                       if(x == index){
                           $(this).closest("div").prev().addClass("star"+x);
                       }


                   }).click(function () {
                       var index = $(this).index()+1;
                       var text = $(this).attr("title");
                       $(this).closest("div").prev().addClass("star"+index);
                       $(this).closest("div").parent().next().html(text);

                       $(this).closest("div").prev().prev().val(index);

                   })
               })
			</script>


        </div>
    </div>

</div>


</div>


<!----------------------------------------->
<#include "../common/footer.ftl">
</body>

</html>