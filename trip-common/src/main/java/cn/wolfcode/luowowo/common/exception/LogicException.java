package cn.wolfcode.luowowo.common.exception;

/**
 * Created by zhi on 2019/8/7.
 */

/**
 * 自定义异常
 */
public class LogicException extends RuntimeException {
    public LogicException(String message) {
        super(message);
    }
}
