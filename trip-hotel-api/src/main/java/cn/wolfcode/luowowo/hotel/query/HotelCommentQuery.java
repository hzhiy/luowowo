package cn.wolfcode.luowowo.hotel.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by zhi on 2019/8/24.
 */
@Setter
@Getter
public class HotelCommentQuery extends QueryObject {
    private Long hotelId;
    private String name;


}
