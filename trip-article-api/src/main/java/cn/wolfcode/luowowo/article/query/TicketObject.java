package cn.wolfcode.luowowo.article.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TicketObject extends QueryObject{
    private Long ajaxDestId;
    private Long ajaxSubjectId;
}
