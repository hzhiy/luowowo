<#--回复模板-->
<#--需要 评论Id, 被回复用户id, 被回复用户名name, 被回复用户头像headurl 回复内容content-->
<#list usersReply as re >

<#--回复点评-->
    <#if re.type = 0>
<li>
    <a href="/u/63107989.html" target="_blank">
        <img src="${re.headUrl}"
             width="16" height="16">🍓${re.refComment.username}🍓
    </a>
    ：${re.refComment.content}
    <a role="button" class="_j_reply re_reply replyBtnTypeOne" data-toid="${re.id}" data-formid="${re.userId}" data-headurl="${re.headUrl}"
       data-username="${re.username}" title="添加回复">回复</a>
    <br><span
        class="time">${(re.refComment.createTime?string("yyyy-MM-dd"))!}
</span>
</li>


    <#--回复用户-->
    <#else >
<li>
    <a href="/u/52068941.html" target="_blank">
        <img src="${re.refComment.headUrl}"
             width="16" height="16">${re.refComment.username}
    </a>
    回复${re.username}：${re.refComment.content}
    <a role="button" class="_j_reply re_reply replyBtnTypeOne" data-toid="${re.id}" data-formid="${re.userId}" data-headurl="${re.headUrl}"
       data-username="${re.username}" title="添加回复">回复</a>
    <br><span
        class="time">${(re.refComment.createTime?string("yyyy-MM-dd"))!}
</span>
</li>
    </#if>
</#list>