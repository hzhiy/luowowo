package cn.wolfcode.luowowo.search.repository;

import cn.wolfcode.luowowo.search.template.DestinationTemplate;
import cn.wolfcode.luowowo.search.template.UserInfoTemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by zhi on 2019/8/17.
 */
@Repository
public interface IUserInfoSearchRepository extends ElasticsearchRepository<UserInfoTemplate, Long>{

    /**
     * 通过地区名称查询
     * @param destName
     * @return
     */
    List<UserInfoTemplate> findByDestName(String destName);
}
