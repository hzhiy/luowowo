package cn.wolfcode.luowowo.cache.service.impl;

import cn.wolfcode.luowowo.article.domain.DestinationTheme;
import cn.wolfcode.luowowo.article.service.IDestinationThemeService;
import cn.wolfcode.luowowo.cache.emum.RedisKeys;
import cn.wolfcode.luowowo.cache.service.IDestinationThemeRedisService;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class DestinationThemeRedisServiceImpl implements IDestinationThemeRedisService {
    @Reference
    private IDestinationThemeService destinationThemeService;
    @Autowired
    private StringRedisTemplate redisTemplate;


    //拿到月份数据,并存到redis中  //拿到月份数据,并存到redis中
    @Override
    public List<DestinationTheme> getMonths() {
        String key = RedisKeys.THEME_MONTH_LIST.getPrefix();
        if (redisTemplate.hasKey(key)){
            String s = redisTemplate.opsForValue().get(key);
            List<DestinationTheme> monthTheme = JSON.parseArray(s, DestinationTheme.class);
            return monthTheme;
        }
        List<DestinationTheme> destTheme=destinationThemeService.queryMonth();
        redisTemplate.opsForValue().set(key,JSON.toJSONString(destTheme));
        return destTheme;
    }

    @Override
    public DestinationTheme getMothDest(Long monthId) {
        String key =RedisKeys.DEST_THEME_MONTH.join(monthId.toString());
        if (redisTemplate.hasKey(key)){
            String s = redisTemplate.opsForValue().get(key);
            DestinationTheme destinationTheme = JSON.parseObject(s, DestinationTheme.class);
            return destinationTheme;
        }
        DestinationTheme destinationTheme= destinationThemeService.queryByThemeId(monthId);
        redisTemplate.opsForValue().set(key,JSON.toJSONString(destinationTheme));
        return destinationTheme;
    }

    @Override
    public List<DestinationTheme> queryByThemeParendId(Long id) {
        String key =RedisKeys.DEST_THEME.join(id.toString());
        if (redisTemplate.hasKey(key)){
            String s = redisTemplate.opsForValue().get(key);
            List<DestinationTheme> destinationThemes = JSON.parseArray(s, DestinationTheme.class);
            return destinationThemes;
        }
        List<DestinationTheme> destinationThemes = destinationThemeService.queryByThemeParendId(id);
        redisTemplate.opsForValue().set(key,JSON.toJSONString(destinationThemes));
        return destinationThemes;

    }

    @Override
    public List<DestinationTheme> queryByParendTheme() {
        String key =RedisKeys.DEST_THEME.getPrefix();
        if (redisTemplate.hasKey(key)){
            String s = redisTemplate.opsForValue().get(key);
            List<DestinationTheme> monthTheme = JSON.parseArray(s, DestinationTheme.class);
            return monthTheme;
        }
        List<DestinationTheme> destTheme=destinationThemeService.queryByParendTheme();
        redisTemplate.opsForValue().set(key,JSON.toJSONString(destTheme));
        return destTheme;
    }

    @Override
    public List<DestinationTheme> getFestivals() {
        return this.queryByThemeParendId(4L);
    }

    @Override
    public List<DestinationTheme> getRoundFit() {
        return this.queryByThemeParendId(1L);
    }

    @Override
    public List<DestinationTheme> getSeasons() {
        return this.queryByThemeParendId(2L);
    }

    @Override
    public List<DestinationTheme> getTripModes() {
        return this.queryByThemeParendId(3L);
    }

    @Override
    public List<DestinationTheme> getDays() {
        return this.queryByThemeParendId(69L);
    }
}
