package cn.wolfcode.luowowo.airticket.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2019/8/25.
 */
@Setter
@Getter
public class TicketCondition {

    public static final Map<Integer, TicketCondition> TIME_CONDITON = new HashMap<>();


    static {
        TIME_CONDITON.put(1, new TicketCondition(0,0,0,9,59,59));
        TIME_CONDITON.put(2, new TicketCondition(10,0,0,13,59,59));
        TIME_CONDITON.put(3, new TicketCondition(14,0,0,189,59,59));
        TIME_CONDITON.put(4, new TicketCondition(19,0,0,24,0,0));
    }


    private Date beginDate;
    private Date endDate;

    private int hour;
    private int minute;
    private int second;

    private int ehour;
    private int eminute;
    private int esecond;

    public TicketCondition(int bhour, int bminute, int bsecond, int ehour, int eminute, int esecond) {
        this.hour = bhour;
        this.minute = bminute;
        this.second = bsecond;
        this.ehour = ehour;
        this.eminute = eminute;
        this.esecond = esecond;
    }


}
