package cn.wolfcode.luowowo.search;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by zhi on 2019/8/7.
 */
@SpringBootApplication
@EnableDubbo
public class SearchServer {
    public static void main(String[] args) {
        SpringApplication.run(SearchServer.class, args);
    }
}
