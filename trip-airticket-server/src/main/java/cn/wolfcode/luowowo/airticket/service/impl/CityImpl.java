package cn.wolfcode.luowowo.airticket.service.impl;

import cn.wolfcode.luowowo.airticket.domain.City;
import cn.wolfcode.luowowo.airticket.mapper.CityMapper;
import cn.wolfcode.luowowo.airticket.service.ICityService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Administrator on 2019/8/24.
 */
@Service
public class CityImpl implements ICityService {


    @Autowired
    private CityMapper cityMapper;

    @Override
    public List<City> getHotCity() {

        return cityMapper.selectHotCity();
    }

    @Override
    public List<City> getNormalCity(int i) {
        return cityMapper.selectNormalCity(i);
    }
}
