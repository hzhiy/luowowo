package cn.wolfcode.luowowo.airticket.mapper;

import cn.wolfcode.luowowo.airticket.domain.Airline;
import java.util.List;

public interface AirlineMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Airline record);

    Airline selectByPrimaryKey(Long id);

    List<Airline> selectAll();

    int updateByPrimaryKey(Airline record);
}