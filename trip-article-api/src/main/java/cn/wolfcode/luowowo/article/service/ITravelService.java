package cn.wolfcode.luowowo.article.service;


import cn.wolfcode.luowowo.article.domain.Travel;
import cn.wolfcode.luowowo.article.domain.TravelContent;
import cn.wolfcode.luowowo.article.query.TravelQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 游记服务
 */
public interface ITravelService {

    /**
     * 分页查询
     * @param qo
     * @return
     */
    PageInfo query(TravelQuery qo);


    /**
     * 添加/更新
     * @param travel
     */
    Long saveOrUpdate(Travel travel);

    /**
     * 查询所有
     * @return
     */
    List<Travel> list();

    /**
     * 查单个
     * @param id
     * @return
     */
    Travel get(Long id);

    /**
     * 查询阅读量前3
     * @param destId
     * @return
     */
    List<Travel> getTravelTop3(Long destId);

    /**
     * 查内容
     * @param id
     * @return
     */
    TravelContent getContent(Long id);

    /**
     * 后台游记管理分页
     * @param qo
     * @return
     */
    PageInfo queryMgr(TravelQuery qo);

    /**
     * 修改状态
     * @param travel
     */
    void updateState(Travel travel);

    /**
     * 根据用户id查询游记
     * @param userInfoId
     * @return
     */
    List<Travel> queryByUserId(Long userInfoId);
}
