package cn.wolfcode.luowowo.comment.repository;

import cn.wolfcode.luowowo.comment.domain.HotelComment;
import cn.wolfcode.luowowo.comment.domain.TravelComment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * monogDB 攻略评论接口
 */
@Repository // 贴上@Repository注解，底层会创建出动态代理对象，交给Spring管理
public interface IHotelCommentRepository extends MongoRepository<HotelComment, String>{

    /**
     * 通过酒店id查询
     * @param id
     * @return
     */
    List<HotelComment> findByHotelId(Long id);
}
