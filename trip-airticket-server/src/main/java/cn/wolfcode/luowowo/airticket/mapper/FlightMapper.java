package cn.wolfcode.luowowo.airticket.mapper;

import cn.wolfcode.luowowo.airticket.domain.Flight;
import cn.wolfcode.luowowo.airticket.query.FlightQuery;

import java.util.List;

public interface FlightMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Flight record);

    Flight selectByPrimaryKey(Long id);

    List<Flight> selectAll();

    int updateByPrimaryKey(Flight record);

    List<Flight> selectForList(FlightQuery qo);
}