package cn.wolfcode.luowowo.search.service;

import cn.wolfcode.luowowo.search.template.ScenicCommentTemplate;

public interface IScenicCommentTemplateService {
    void save(ScenicCommentTemplate template);
}
