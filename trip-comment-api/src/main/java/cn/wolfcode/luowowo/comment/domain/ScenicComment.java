package cn.wolfcode.luowowo.comment.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 景点点评
 */
@Setter
@Getter
@ToString
@Document("scenic_comment")
public class ScenicComment implements Serializable {

    public static final int COMMENT_TYPE_COMMENT = 0; // 回复点评
    public static final int COMMENT_TYPE = 1; // 回复用户
    public static final int CONDITION_HAS_IMG = 0; // 是否包含图片
    @Id
    private String id; // id
    private Long scenicId; // 景点Id
    private String scenicName; // 景点名
    private Long userId; // 用户id
    private String username; // 用户名
    private String city; // 用户城市 暂时用不上
    private int level; // 等级
    private String headUrl; // 用户头像
    private int type = COMMENT_TYPE_COMMENT; //评论类别
    private Date createTime; // 创建时间
    private String content; // 评论内容

    private String imgUrls; // 图片
    private int hasImg; // 是否包含图片

    private int starNum; // 评价星级
    private int thumbupnum; // 点赞数
    private List<Long> thumbuplist = new ArrayList<>(); // 点赞列表

    private ScenicComment refComment; // 装回复者用的

    private List<ScenicComment> usersReply = new ArrayList<>(); // 用户之间的回复

    public String[] getImgArr() {
        if (StringUtils.hasLength(imgUrls)) {
            return imgUrls.split(";");
        }
        return null;
    }

    public List<String> getCommentImgArr() {
        if (StringUtils.hasLength(imgUrls)) {
            return new ArrayList<>(Arrays.asList(imgUrls.split(";")));
        }
        return null;
    }

    // 星星转文字
    public String getStarCommend() {
        if (starNum == 1) {
            return "千万别去";
        } else if (starNum == 2) {
            return "不推荐";
        } else if (starNum == 3) {
            return "一般般";
        } else if (starNum == 4) {
            return "值得一去";
        } else if (starNum == 5) {
            return "必须推荐";
        }
        return null;
    }


}
