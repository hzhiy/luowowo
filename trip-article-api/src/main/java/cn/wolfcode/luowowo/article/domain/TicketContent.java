package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TicketContent extends BaseDomain{
    private String shouldKnow;
    private String introduce;
}
