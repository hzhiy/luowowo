package cn.wolfcode.luowowo.search.template;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;

@Document(indexName = "luowowo_ticket", type = "ticket")
@Getter
@Setter
@ToString
public class TicketTemplate implements Serializable {


    public static final String INDEX_NAME = "luowowo_ticket";
    public static final String TYPE_NAME = "ticket";
    @Id
    //@Field 每个文档的字段配置（类型、是否分词、是否存储、分词器 ）
    @Field(store = true, index = false, type = FieldType.Long)
    private Long id;  //门票id
    @Field(index = true, store = true, type = FieldType.Keyword)
    private Long destId;  //地区id
    @Field(index = true, store = true, type = FieldType.Keyword)
    private String title;
    @Field(index = true, store = true, type = FieldType.Keyword)
    private String price;
    @Field(index = true, store = true, type = FieldType.Keyword)
    private Long ticketThemeId;
    @Field(index = true, store = true, type = FieldType.Keyword)
    private String coverUrl;
    @Field(index = true, store = true, type = FieldType.Keyword)
    private String address;
    @Field(index = true, store = true, type = FieldType.Keyword)
    private String businessHours;
}