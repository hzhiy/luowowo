package cn.wolfcode.luowowo.mgrsite.controller;

import cn.wolfcode.luowowo.article.domain.Travel;
import cn.wolfcode.luowowo.article.query.TravelQuery;
import cn.wolfcode.luowowo.article.service.ITravelService;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by zhi on 2019/8/11.
 */
@Controller
@RequestMapping("travel")
public class TravelController {
    @Reference
    private ITravelService travelService;

    @RequestMapping("list")
    public String list(Model model, @ModelAttribute("qo")TravelQuery qo){

        model.addAttribute("pageInfo", travelService.queryMgr(qo));
        return "travel/list";
    }

    @RequestMapping("lookContent")
    @ResponseBody
    public Object lookContent(Long id){
        return travelService.getContent(id);
    }

    @RequestMapping("updateState")
    @ResponseBody
    public Object updateState(Travel travel){
        travelService.updateState(travel);
        return AjaxResult.SUCCESS;
    }
}
