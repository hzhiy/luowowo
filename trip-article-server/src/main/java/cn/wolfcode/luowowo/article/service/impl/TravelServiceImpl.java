package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.Travel;
import cn.wolfcode.luowowo.article.domain.TravelContent;
import cn.wolfcode.luowowo.article.mapper.TravelContentMapper;
import cn.wolfcode.luowowo.article.mapper.TravelMapper;
import cn.wolfcode.luowowo.article.query.TravelQuery;
import cn.wolfcode.luowowo.article.service.ITravelService;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

/**
 * Created by zhi on 2019/8/11.
 */
@Service
public class TravelServiceImpl implements ITravelService {
    @Autowired
    private TravelMapper travelMapper;
    @Autowired
    private TravelContentMapper travelContentMapper;

    @Override
    public PageInfo query(TravelQuery qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getOrderBy());
        return new PageInfo<Travel>(travelMapper.selectForList(qo));
    }

    @Override
    public Long saveOrUpdate(Travel travel) {
        // summary
        TravelContent content = travel.getTravelContent();
        travel.setSummary(content.getContent().length() > 200 ? content.getContent().substring(0, 200) : content.getContent());
        // lastUpdateTime
        travel.setLastUpdateTime(new Date());
        if (travel.getId() == null){
            // createTime
            travel.setCreateTime(new Date());
            travelMapper.insert(travel);
            content.setId(travel.getId());
            travelContentMapper.insert(content);
        }else {
            travelMapper.updateByPrimaryKey(travel);
            content.setId(travel.getId());
            travelContentMapper.updateByPrimaryKey(content);
        }

        return travel.getId();
    }

    @Override
    public List<Travel> list() {
        return travelMapper.selectAll();
    }

    @Override
    public Travel get(Long id) {
        return travelMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Travel> getTravelTop3(Long destId) {
        return travelMapper.selectTravelTop3(destId);
    }

    @Override
    public TravelContent getContent(Long id) {
        return travelContentMapper.get(id);
    }

    @Override
    public PageInfo queryMgr(TravelQuery qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getLastUpdateTimeOrderBy());
        return new PageInfo<Travel>(travelMapper.selectForList(qo));
    }

    @Override
    public void updateState(Travel travel) {
        if (travel.getState() == 2){
            travel.setReleaseTime(new Date());
        }else {
            travel.setReleaseTime(null);
        }
        travelMapper.updateState(travel);
    }

    @Override
    public List<Travel> queryByUserId(Long userInfoId) {
        return travelMapper.selectByUserId(userInfoId);
    }
}
