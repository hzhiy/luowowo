package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.StrategyDetail;
import cn.wolfcode.luowowo.article.query.StrategyDetailQuery;
import cn.wolfcode.luowowo.article.vo.StrategyPersistenceStatisVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StrategyDetailMapper {
    int deleteByPrimaryKey(Long id);

    int insert(StrategyDetail record);

    StrategyDetail selectByPrimaryKey(Long id);

    List<StrategyDetail> selectAll();

    int updateByPrimaryKey(StrategyDetail record);

    List<StrategyDetail> queryForList(StrategyDetailQuery qo);

    void insertRelation( @Param("tagId") Long tagId,@Param("DetailId") Long DetailId);

    void deleteRelation(Long detailId);

    List<StrategyDetail> selectDetailTop3(Long destId);

    void updateState(@Param("id") Long id, @Param("state") int state);

    void updateReplynum(@Param("id") Long id, @Param("replynum") int replynum);

    void updateStatisData(StrategyPersistenceStatisVO vo);


}