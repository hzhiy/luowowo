package cn.wolfcode.luowowo.cache.service;

/**
 * Created by zhi on 2019/8/7.
 */

import cn.wolfcode.luowowo.article.domain.Strategy;
import cn.wolfcode.luowowo.article.domain.StrategyDetail;
import cn.wolfcode.luowowo.article.domain.Travel;
import cn.wolfcode.luowowo.cache.vo.StrategyStatisVO;
import cn.wolfcode.luowowo.member.domain.UserInfo;

import java.util.List;
import java.util.Set;

/**
 * 攻略缓存数据
 */
public interface IStrategyStatisRedisService {

    /**
     * 获取单个
     */
    StrategyStatisVO getStrategyStatisVO(Long id);

    /**
     * 阅读数增加
     * @param sid
     * @param i
     */
    void viewNumIncrease(Long sid, int i);

    /**
     * 评论数增加
     * @param sid
     * @param i
     */
    void replyNumIncrease(Long sid, int i);

    /**
     * 判断该数据是否存在
     * @param id
     * @return true: 存在  false: 不存在
     */
    boolean isVoExist(Long id);

    /**
     * 将vo对象存入redis
     * @param vo
     */
    void setStrategyStatisVO(StrategyStatisVO vo);

    /**
     * 收藏操作
     * @param sid
     * @param uid
     * @return true: 收藏成功  false: 取消收藏
     */
    boolean favor(Long sid, Long uid);

    /**
     * 用户是否收藏指定攻略
     * @param uid
     * @param sid
     * @return true: 收藏  false: 没有收藏
     */
    boolean isUserFavorStrategy(Long uid, Long sid);

    /**
     * 顶操作
     * @param sid
     * @param uid
     * @return true: 顶成功  false: 今天已经顶过了
     */
    boolean strategyThumbup(Long sid, Long uid);

    /**
     * 根据前缀获得reids中的key
     * @param pattern
     * @return
     */
    Set<String> getKeys(String pattern);

    /**
     * 获取值
     * @param key
     * @return
     */
    String get(String key);

    /**
     * 分享操作
     * @param sid
     * @param uid
     * @return true: 分享成功  false: 分享失败
     */
    boolean sharenum(Long sid, Long uid);

    /**
     * 攻略推荐分数加或减
     * @param prefix 识别是攻略推荐还是攻略热门
     * @param value value
     * @param i 加减值
     */
    void addScore(String prefix, String value, int i);

    /**
     * 查询zset中是否存在vo
     * @param prefix 识别是攻略推荐还是攻略热门
     * @param value value
     * @return
     */
    boolean isVoByStrategyExist(String prefix, String value);

    /**
     * 获取zset中全部攻略对象的vo
     * @param prefix 识别是攻略推荐还是攻略热门
     * @return
     */
    List<StrategyStatisVO> getAllZSetStrategyVO(String prefix);

    /**
     * 根据用户id查询收藏的攻略
     * @param uid
     * @return
     */
    List<StrategyDetail> queryByUserIdToFavor(Long uid);
}
