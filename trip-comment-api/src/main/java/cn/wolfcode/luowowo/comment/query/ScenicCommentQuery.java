package cn.wolfcode.luowowo.comment.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class ScenicCommentQuery extends QueryObject {
    private int state = -1;
    private Long scenicId = -1L;
    private Long destId = -1L;
    private String order = "createTime";

}
