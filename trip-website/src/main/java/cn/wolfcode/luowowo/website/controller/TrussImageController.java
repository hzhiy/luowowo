package cn.wolfcode.luowowo.website.controller;

import cn.wolfcode.luowowo.website.util.AliyunUploadUtil;
import cn.wolfcode.luowowo.website.util.TrussAliyunUploadUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

@RestController

public class TrussImageController {

    /**
     * 资源路径: /images
     * 动作设计: post 从无到有 新增
     * 请求参数: file
     * 返回结果: JsonResult
     * 因为后面的插件是指定返回值的 所以现在先修改 免得以后再改
     */
    @RequestMapping("trussImgs")
    public Map<String, Object> send(MultipartFile[] file) {
        Map<String, Object> map = new HashMap<>();
        StringBuilder sb = new StringBuilder();
        try {
            for (int i = 0; i < file.length; i++) {
                String url = TrussAliyunUploadUtil.uploadAli(file[i]);
                System.out.println(url);
                if (i == file.length-1){
                    sb.append(url);
                }else {
                    sb.append(url).append(";");
                }
            }
            map.put("url", sb.toString());
            map.put("status", 1);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("msg", "上传错误!");
            map.put("status", 0);
        }
        return map;

    }
}
