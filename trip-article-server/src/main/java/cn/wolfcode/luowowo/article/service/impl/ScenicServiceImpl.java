package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.Scenic;
import cn.wolfcode.luowowo.article.domain.ScenicTheme;
import cn.wolfcode.luowowo.article.mapper.ScenicMapper;
import cn.wolfcode.luowowo.article.mapper.ScenicThemeMapper;
import cn.wolfcode.luowowo.article.query.ScenicQuery;
import cn.wolfcode.luowowo.article.service.IScenicService;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 *
 */
@Service
public class ScenicServiceImpl implements IScenicService {

    @Autowired
    private ScenicMapper scenicMapper;

    @Autowired
    private ScenicThemeMapper scenicThemeMapper;

    @Override
    public Scenic getById(Long scenicId) {
        return scenicMapper.selectByPrimaryKey(scenicId);
    }

    @Override
    public List<Scenic> queryTop5ByDestId(ScenicQuery qo) {
        return scenicMapper.selectTop5byDestId(qo.getDestId());
    }

    @Override
    public Scenic getByDestId(Long destId) {
        return scenicMapper.selectByDestId(destId);
    }

    @Override
    public List<Scenic> queryHotScenic() {
        return scenicMapper.selectHotScenic();
    }

    @Override
    public List<ScenicTheme> queryThemebyDestId(Long destId) {
        return scenicThemeMapper.selectByDestId(destId);
    }

    @Override
    public PageInfo<Scenic> query(ScenicQuery qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        return new PageInfo<>(scenicMapper.selectThemeScenics(qo));
    }

    @Override
    public List<Scenic> queryInScenicByScenicId(Long scenicId) {
        Scenic scenic = scenicMapper.selectByPrimaryKey(scenicId);
        return scenicMapper.selectByParentId(scenic.getParent_id());
    }
}
