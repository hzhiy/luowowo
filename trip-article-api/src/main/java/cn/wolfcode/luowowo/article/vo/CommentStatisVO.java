package cn.wolfcode.luowowo.article.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentStatisVO {
    private int count;//评论数
    private int thumbupnum;//点赞数
}
