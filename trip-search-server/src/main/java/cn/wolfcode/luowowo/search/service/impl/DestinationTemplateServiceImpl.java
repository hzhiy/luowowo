package cn.wolfcode.luowowo.search.service.impl;

import cn.wolfcode.luowowo.search.repository.IDestinationSearchRepository;
import cn.wolfcode.luowowo.search.service.IDestinationTemplateService;
import cn.wolfcode.luowowo.search.template.DestinationTemplate;
import com.alibaba.dubbo.config.annotation.Service;
import org.elasticsearch.client.ElasticsearchClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

/**
 * Created by zhi on 2019/8/17.
 */
@Service
public class DestinationTemplateServiceImpl implements IDestinationTemplateService {

    @Autowired
    private IDestinationSearchRepository destinationSearchRepository;
    @Autowired
    private ElasticsearchTemplate template;
    @Autowired
    private ElasticsearchClient client;

    public void saveOrUpdate(DestinationTemplate destinationTemplate) {
        destinationSearchRepository.save(destinationTemplate);
    }

    @Override
    public DestinationTemplate findByName(String name) {
        return destinationSearchRepository.findByName(name);
    }
}
