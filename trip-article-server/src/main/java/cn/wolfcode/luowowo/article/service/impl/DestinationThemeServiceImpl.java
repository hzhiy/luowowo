package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.DestinationTheme;
import cn.wolfcode.luowowo.article.mapper.DestinationThemeMapper;
import cn.wolfcode.luowowo.article.query.DestFitlerQuery;
import cn.wolfcode.luowowo.article.query.DestinationThemeQuery;
import cn.wolfcode.luowowo.article.service.IDestinationThemeService;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by zhi on 2019/8/16.
 */
@Service
public class DestinationThemeServiceImpl implements IDestinationThemeService {

    @Autowired
    private DestinationThemeMapper destinationThemeMapper;

    @Override
    public PageInfo queryForList(DestinationThemeQuery qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        return new PageInfo(destinationThemeMapper.selectForList(qo));
    }

    @Override
    public void saveOrUpdate(DestinationTheme destinationTheme) {
        if (destinationTheme.getId() == null) {
            destinationThemeMapper.insert(destinationTheme);
        }else {
            destinationThemeMapper.updateByPrimaryKey(destinationTheme);
        }
    }

    @Override
    public void delete(Long id) {
        destinationThemeMapper.deleteByPrimaryKey(id);
    }


    @Override
    public List<DestinationTheme> queryByThemeParendId(Long parentId) {

        return destinationThemeMapper.selectByParendId(parentId);
    }

    @Override
    public List<DestinationTheme> queryByParendTheme() {
        return destinationThemeMapper.selectByParendTheme();
    }

    @Override
    public List<DestinationTheme> queryMonth() {

        return destinationThemeMapper.selectByParendId(50L);//月份的父类id
    }

    @Override
    public DestinationTheme queryByThemeId(Long monthId) {

        return destinationThemeMapper.selectByPrimaryKey(monthId);
    }


}
