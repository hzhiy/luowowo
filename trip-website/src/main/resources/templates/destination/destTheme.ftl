<div class="tiles">
    <#list destThemes as dt>
    <div class="item col4">
        <a href="/destination/destFilter" target="_blank"><img src="${(dt.imgUrl)!}" width="238" height="220">
            <div class="title">${(dt.name)!}</div>
        </a>
    </div>
    </#list>
</div>