package cn.wolfcode.luowowo.cache.service;


import java.util.List;

/**
 * 酒店缓存
 */
public interface IHotelRedisService {


    /**
     * 收藏与取消收藏
     * @param hid
     * @param uid
     * @return
     */
    boolean favor(Long hid, Long uid);

    /**
     * 获得收藏集合
     * @param id
     * @return
     */
    List<String> get(Long id);
}
