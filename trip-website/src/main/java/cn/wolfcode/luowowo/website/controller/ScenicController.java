package cn.wolfcode.luowowo.website.controller;

import cn.wolfcode.luowowo.article.domain.Scenic;
import cn.wolfcode.luowowo.article.domain.ScenicTheme;
import cn.wolfcode.luowowo.article.query.ScenicQuery;
import cn.wolfcode.luowowo.article.service.IDestinationService;
import cn.wolfcode.luowowo.article.service.IScenicDetailService;
import cn.wolfcode.luowowo.article.service.IScenicService;
import cn.wolfcode.luowowo.article.vo.UsersReplyVO;
import cn.wolfcode.luowowo.comment.domain.ScenicComment;
import cn.wolfcode.luowowo.comment.query.ScenicCommentQuery;
import cn.wolfcode.luowowo.comment.service.IScenicCommentService;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.website.annotation.UserParam;
import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 景点和点评跳转控制
 */
@Controller
@RequestMapping("/scenic")
public class ScenicController {

    @Reference
    private IScenicCommentService scenicCommentService;

    @Reference
    private IScenicService scenicService;

    @Reference
    private IScenicDetailService scenicDetailService;

    @Reference
    private IDestinationService destinationService;

    // 景点页面
    @RequestMapping("")
    public String index(Model model, @ModelAttribute("qo") ScenicQuery qo) {
        // 查地区的单个景点
        Scenic mainSingle = scenicService.getByDestId(qo.getDestId());
        model.addAttribute("mainSingle", mainSingle);
        List<Scenic> top5Scenic = scenicService.queryTop5ByDestId(qo);
        model.addAttribute("top5Scenic", top5Scenic);
        // 热门景点 以最喜欢排序 暂时, 有时间可以 在用户那里装一个景点的集合, 通过点击去的景然后判断哪个最多排序
        List<Scenic> hotScenic = scenicService.queryHotScenic();
        model.addAttribute("hotScenic", hotScenic);
        // 地区所有景点主题
        List<ScenicTheme> scenicThemes = scenicService.queryThemebyDestId(qo.getDestId());
        model.addAttribute("scenicThemes", scenicThemes);
        // 默认查询 地区全部景点
        PageInfo<Scenic> page = scenicService.query(qo);
        model.addAttribute("pageInfo", page);
        return "scenicComment/comment-list";
    }

    /**
     * 景点主题模板
     *
     * @param qo
     * @param model
     * @return
     */
    @RequestMapping("/themeList")
    public String themeList(@ModelAttribute("qo") ScenicQuery qo, Model model) {
        System.out.println(qo.getThemeId());
        System.out.println(qo.getDestId());
        model.addAttribute("pageInfo", scenicService.query(qo));
        return "scenicComment/scenicThemeTpl";
    }


    /**
     * 景点详细,评论回显
     *
     * @param model
     * @param qo
     * @return
     */
    @RequestMapping("/detail")
    public String scenic(Model model, @ModelAttribute("qo") ScenicCommentQuery qo) {
        // 回显景点详细
        // 回显评论, 查所有本景点的评论
        qo.setPageSize(3);
        Page<ScenicComment> page = scenicCommentService.queryCommentByScenicId(qo);
        model.addAttribute("page", page);

        // 景点详细, 通过景点Id查详细
        model.addAttribute("detail", scenicDetailService.queryDetailById(qo.getScenicId()));

        // 导航吐司 toast
        model.addAttribute("toast", destinationService.getDestById(qo.getDestId()));

        // 内部景点
        List<Scenic> includeS = scenicService.queryInScenicByScenicId(qo.getScenicId());
        model.addAttribute("includeS", includeS.subList(0, includeS.size()>12? 12: includeS.size()));


        // 固定的前面四个 tags 总评论数(单独查) 有图, 好评, 中评, 差评的总数
        model.addAttribute("totalNum", scenicCommentService.totalCommentNum());
        // 查有图的
        Map<String, List> tagsCount = scenicCommentService.tagsCount();
        model.addAttribute("tagsCount", tagsCount);

        return "scenicComment/commentDetail";
    }


    /**
     * 景点点评 点评增加
     *
     * @param scenicComment 点评对象
     * @param userInfo      用户对象
     * @param model
     * @return
     */
    @RequestMapping("/addComment")
    public String addComment(ScenicComment scenicComment, @UserParam UserInfo userInfo, Model model) {
        if (userInfo == null) {
            return null;
        }
        System.out.println(scenicComment.getImgUrls());
        scenicComment.setUserId(userInfo.getId());
        scenicComment.setUsername(userInfo.getNickname());
        scenicComment.setHeadUrl(userInfo.getHeadImgUrl());
        scenicComment.setLevel(userInfo.getLevel());
        scenicComment.setCity(userInfo.getCity());
        scenicComment.setCreateTime(new Date());

        if (scenicComment.getImgUrls() != null) {
            scenicComment.setHasImg(1);
        }

        model.addAttribute("sct", scenicCommentService.addComment(scenicComment));
        return "scenicComment/addCommentTpl";
    }


    // 评论点赞
    @RequestMapping("/scenicThumbUp")
    @ResponseBody
    public AjaxResult commentThumbUp(String toid, @UserParam UserInfo userInfo) {
        AjaxResult result = new AjaxResult();
        if (toid != null) {
            scenicCommentService.commentThumbUp(toid, userInfo.getId());
            result.setData(scenicCommentService.getscenicCommentById(toid));
        }
        return result;
    }


    /**
     * @param replyVO  用户回复对象
     * @param userInfo
     * @param model
     * @return
     */
    @RequestMapping("/usersReply")
    public String usersReply(UsersReplyVO replyVO, @UserParam UserInfo userInfo, Model model) {
        List<ScenicComment> usersReply = scenicCommentService.usersReply(replyVO, userInfo);
        model.addAttribute("usersReply", usersReply);
        return "scenicComment/replyTpl";
    }


    // 收藏, 去过




    // 评论普通回复, 废弃
    @RequestMapping("/addReply")
    @ResponseBody
    @Deprecated
    public AjaxResult addReply(String toid, String content, @UserParam UserInfo userInfo) {
        if (userInfo != null && toid != null) {
            // 增加点评的评论
            ScenicComment comment = scenicCommentService.queryByCommentId(toid);
            ScenicComment refComment = new ScenicComment();
            Long id = userInfo.getId();
            refComment.setUserId(userInfo.getId());
            refComment.setUsername(userInfo.getNickname());
            refComment.setHeadUrl(userInfo.getHeadImgUrl());
            refComment.setLevel(userInfo.getLevel());
            refComment.setCity(userInfo.getCity());
            refComment.setCreateTime(new Date());
            comment.setRefComment(refComment);
            scenicCommentService.addComment(comment);
            return AjaxResult.SUCCESS;
        }
        AjaxResult result = new AjaxResult();
        result.setSuccess(false);
        result.setMsg("你没有选择任何评论");
        return result;
    }

}
