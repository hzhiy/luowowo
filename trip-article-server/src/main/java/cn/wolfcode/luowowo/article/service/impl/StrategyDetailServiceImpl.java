package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.*;
import cn.wolfcode.luowowo.article.mapper.*;
import cn.wolfcode.luowowo.article.query.StrategyDetailQuery;
import cn.wolfcode.luowowo.article.service.IDestinationService;
import cn.wolfcode.luowowo.article.service.IStrategyDetailService;
import cn.wolfcode.luowowo.article.vo.StrategyPersistenceStatisVO;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

/**
 * Created by zhi on 2019/8/10.
 */
@Service
public class StrategyDetailServiceImpl implements IStrategyDetailService {

    @Autowired
    private StrategyDetailMapper strategyDetailMapper;
    @Autowired
    private StrategyMapper strategyMapper;
    @Autowired
    private IDestinationService destinationService;
    @Autowired
    private StrategyContentMapper strategyContentMapper;
    @Autowired
    private StrategyTagMapper strategyTagMapper;

    public PageInfo query(StrategyDetailQuery qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List<StrategyDetail> list = strategyDetailMapper.queryForList(qo);
        return new PageInfo<>(list);
    }

    public void saveOrUpdate(StrategyDetail strategyDetail, String tags) {
        // 根据传过来的攻略id查询目的地
        Strategy strategy = strategyMapper.selectByPrimaryKey(strategyDetail.getStrategy().getId());
        strategyDetail.setDest(strategy.getDest());
        // 简介
        String content = strategyDetail.getStrategyContent().getContent();
        strategyDetail.setSummary(content.length() > 200 ? content.substring(0, 200) : content);
        // 是国内还是外
        List<Destination> toasts = destinationService.getToasts(strategy.getDest().getId());
        strategyDetail.setIsabroad(toasts.get(0).getId() != 1L);
        // 作者
        UserInfo userInfo = new UserInfo();
        userInfo.setId(1L);
        strategyDetail.setAuthor(userInfo);
        // 内容
        StrategyContent strategyContent = new StrategyContent();
        strategyContent.setContent(content);
        if (strategyDetail.getId() == null){
            // 设置创建时间
            strategyDetail.setCreateTime(new Date());
            strategyDetailMapper.insert(strategyDetail);
            // 添加内容
            strategyContent.setId(strategyDetail.getId());
            strategyContentMapper.insert(strategyContent);

        }else {
            strategyDetailMapper.updateByPrimaryKey(strategyDetail);
            // 修改内容
            strategyContent.setId(strategyDetail.getId());
            strategyContentMapper.updateByPrimaryKey(strategyContent);
            // 删除标签
            strategyDetailMapper.deleteRelation(strategyDetail.getId());

        }
        // 添加标签
        if (tags != null){
            for (String s : tags.split(",")) {
                // 避免标签重复
                StrategyTag strategyTag = strategyTagMapper.selectByName(s);
                if (strategyTag == null) {
                    strategyTag = new StrategyTag();
                    strategyTag.setName(s);
                    strategyTagMapper.insert(strategyTag);
                }
                // 添加中间表
                strategyDetailMapper.insertRelation(strategyTag.getId(), strategyDetail.getId());
            }
        }
    }

    public List<StrategyDetail> list() {
        return strategyDetailMapper.selectAll();
    }

    public void changeState(Long id, int state) {
        strategyDetailMapper.updateState(id, state);
    }

    public StrategyDetail get(Long id) {
        return strategyDetailMapper.selectByPrimaryKey(id);
    }

    public StrategyContent getContent(Long id) {
        return strategyContentMapper.selectByPrimaryKey(id);
    }

    public List<StrategyDetail> getDetailTop3(Long destId) {
        return strategyDetailMapper.selectDetailTop3(destId);
    }

    public void updateStatisData(StrategyPersistenceStatisVO vo) {
        strategyDetailMapper.updateStatisData(vo);
    }

    @Override
    public void delete(Long id) {
        strategyDetailMapper.deleteByPrimaryKey(id);
        // 删除关系
        strategyDetailMapper.deleteRelation(id);

    }

    @Override
    public void updateReplynum(Long id, int replynum) {
        strategyDetailMapper.updateReplynum(id, replynum);
    }

}
