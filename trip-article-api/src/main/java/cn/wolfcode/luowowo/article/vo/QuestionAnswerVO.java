package cn.wolfcode.luowowo.article.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 问题的回答
 */
@Getter
@Setter
public class QuestionAnswerVO implements Serializable{

    public static final int STATE_HIDDEN = 1;   // 隐藏评论
    public static final int STATE_SHOW = 0;   // 显示评论
    private String id;

    private Long questionId;//问题id
    private Long answererId;//回答者
    private String nickname;
    private String headImgUrl;
    private int level;
    private int thumbsupnum;
    private String content;
    private Date createTime;
    private int state = STATE_SHOW;
    private boolean isgold = true;

}
