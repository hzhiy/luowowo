package cn.wolfcode.luowowo.website.controller;

import cn.wolfcode.luowowo.article.domain.*;
import cn.wolfcode.luowowo.article.query.DestFitlerQuery;
import cn.wolfcode.luowowo.article.query.DestinationQuery;
import cn.wolfcode.luowowo.article.query.DestinationThemeQuery;
import cn.wolfcode.luowowo.article.query.TravelQuery;
import cn.wolfcode.luowowo.article.service.*;
import cn.wolfcode.luowowo.cache.service.IDestRedisService;
import cn.wolfcode.luowowo.cache.service.IDestinationThemeRedisService;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.website.annotation.UserParam;
import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by zhi on 2019/8/10.
 */
@Controller
@RequestMapping("destination")
public class DestinationController {

    @Reference
    private IRegionService regionService;
    @Reference
    private IDestinationService destinationService;
    @Reference
    private IStrategyCatalogService strategyCatalogService;
    @Reference
    private IStrategyDetailService strategyDetailService;
    @Reference
    private ITravelService travelService;

    @Reference
    private IDestinationThemeRedisService destinationThemeRedisService;
    @Reference
    private IDestRedisService destRedisService;
    @Reference
    private IDestinationThemeService destinationThemeService;

    @RequestMapping("")
    public String list(Model model){
        model.addAttribute("hotRegions", regionService.listAllHot());
        model.addAttribute("themes",destinationThemeRedisService.queryByParendTheme());
        model.addAttribute("months",destinationThemeRedisService.getMonths());

        return "destination/index";
    }

    @RequestMapping("getHotDestByRegionId")
    public String getHotDestByRegionId(Model model,@ModelAttribute("regionId") Long regionId){
        List<Destination> list = new ArrayList<>();
        // leftDests  rightDests
        // 把其区域的目的地还有子目的地查询出来
        if (regionId == -1L){
            list = destinationService.listByParentId(1L);
        }else {
            list = destinationService.getDestByRegionId(regionId);
        }
        // 拆分为左右两个list 传到前端
        int size = list.size();
        model.addAttribute("leftDests", list.subList(0, size / 2 + 1));
        model.addAttribute("rightDests", list.subList(size/2 + 1, size));

        // 前端使用js拼接起来
        return "destination/hotdestTpl";
    }

    @RequestMapping("guide")
    public String guide(Model model, Long id){
        // 攻略分类
        model.addAttribute("catalogs", strategyCatalogService.queryCatalogByDestId(id));
        List<Destination> toasts = destinationService.getToasts(id);
        Destination destination = toasts.remove(toasts.size() - 1);
        model.addAttribute("toasts", toasts);
        model.addAttribute("dest", destination);
        // strategyDetails 获得攻略top 按照点击量分 攻略
        List<StrategyDetail> strategyDetails = strategyDetailService.getDetailTop3(id);
        model.addAttribute("strategyDetails", strategyDetails);


        return "destination/guide";
    }


    @RequestMapping("surveyPage")
    public String surveyPage(Model model,@ModelAttribute("qo") DestinationQuery qo){
        // toasts
        List<Destination> toasts = destinationService.getToasts(qo.getDestId());
        model.addAttribute("dest", toasts.remove(toasts.size() - 1));
        model.addAttribute("toasts", toasts);
        return "destination/survey";
    }
    @RequestMapping("survey")
    public String survey(Model model,@ModelAttribute("qo") DestinationQuery qo){
        // catalogs
        List<StrategyCatalog> catalogs = strategyCatalogService.queryCatalogByDestId(qo.getDestId());
        model.addAttribute("catalogs", catalogs);
        // catalog
        StrategyCatalog catalog = strategyCatalogService.get(qo.getCatalogId());
        model.addAttribute("catalog", catalog);
        // detail
        StrategyDetail detail = catalog.getDetails().get(0);
        StrategyContent content = strategyDetailService.getContent(detail.getId());
        detail.setStrategyContent(content);
        model.addAttribute("detail", detail);
        return "destination/surveyTpl";
    }

    // 目的地游记  异步分页  带范围的条件查询
    @RequestMapping("travels")
    public String travels(Model model, @ModelAttribute("qo")TravelQuery qo){

        qo.setState(Travel.STATE_RELEASE);
        model.addAttribute("pageInfo",  travelService.query(qo));
        return "destination/travelTpl";
    }

    /**
     *  通过月份的id,拿到本月推荐的地区
     * @param model
     * @param month
     * @return
     */
    @RequestMapping("destMonth")
    public String destMonth(Model model,Long month){
        if (month==null || month==-1){
            month=64L;
        }
        model.addAttribute("monthId",month);
        DestinationTheme mothDest = destinationThemeRedisService.getMothDest(month);
        List<Destination> dests = mothDest.getDests();
        if(dests.size()>=3){
            model.addAttribute("destMonthUp",dests.subList(0,3));
            if (dests.size()<=6) {
                model.addAttribute("destMonthDown", dests.subList(3, dests.size()));
            }else {
                model.addAttribute("destMonthDown", dests.subList(3,6));
            }
        }else {
            model.addAttribute("destMonthUp",dests);
        }
     return "destination/destMonth";
    }

    @RequestMapping("/destTheme")
    public String destTheme(Model model,Long id){

        List<DestinationTheme> destinationThemes = destinationThemeRedisService.queryByThemeParendId(id);
        if (destinationThemes.size()>=12){
            model.addAttribute("destThemes",destinationThemes.subList(0,12));
        }else {
            model.addAttribute("destThemes",destinationThemes);
        }

        return "destination/destTheme";
    }
    @RequestMapping("/destFilter")
    public String destFilter(Model model,@ModelAttribute("qo")DestinationThemeQuery qo){

        model.addAttribute("months",destinationThemeRedisService.getMonths());
        model.addAttribute("festivals",destinationThemeRedisService.getFestivals());
        model.addAttribute("roundFit",destinationThemeRedisService.getRoundFit());
        model.addAttribute("seasons",destinationThemeRedisService.getSeasons());
        model.addAttribute("tripModes",destinationThemeRedisService.getTripModes());
        model.addAttribute("days",destinationThemeRedisService.getDays());

        return "destination/destFilter";
    }
    @RequestMapping("/destFilterContent")
    public String destFilterContent(Model model, @ModelAttribute("qo")DestinationThemeQuery qo){
        PageInfo<Destination> pageInfo = destinationService.queryForList(qo);

        model.addAttribute("pageInfo",pageInfo);

        return "destination/destFilterContent";
    }

    //收藏
    @RequestMapping("/favor")
    @ResponseBody
    public AjaxResult favor(Long did, @UserParam UserInfo userInfo){
        AjaxResult result = new AjaxResult();
        if (userInfo == null){
            result.setSuccess(false);
            result.setCode(102);
            result.setMsg("请先登录");
            return result;
        }
        boolean ret = destRedisService.favor(did,userInfo.getId());
        if (ret){
            return AjaxResult.SUCCESS;
        }
        result.setSuccess(ret);
        return result;

    }
}
