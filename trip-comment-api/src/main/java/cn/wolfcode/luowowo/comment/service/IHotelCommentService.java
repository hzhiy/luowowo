package cn.wolfcode.luowowo.comment.service;

import cn.wolfcode.luowowo.comment.domain.HotelComment;
import cn.wolfcode.luowowo.comment.vo.HotelCommentVO;
import cn.wolfcode.luowowo.hotel.query.HotelCommentQuery;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * 酒店评论接口
 */
public interface IHotelCommentService {

    /**
     * 添加
     * @param comment
     */
    HotelComment saveOrUpdate(HotelComment comment);

    /**
     * 查询攻略评论
     * @param qo
     */
    Page list(HotelCommentQuery qo);

    /**
     * 点赞或者取消点赞
     * @param mongoId
     * @param userId
     */
    void commentThumbUp(String mongoId, Long userId);


    /**
     * 获取统计信息
     * @param id
     * @return
     */
    HotelCommentVO querySiatisInfo(Long id);

    /**
     * 评论回复
     * @param hcid
     * @param hotelComment
     * @return
     */
    HotelComment reComment(String hcid, HotelComment hotelComment);

    /**
     * 所有酒店评论对象
     * @return
     */
    List<HotelComment> listAll();
}
