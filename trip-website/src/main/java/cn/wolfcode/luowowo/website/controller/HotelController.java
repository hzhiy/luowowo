package cn.wolfcode.luowowo.website.controller;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.query.HotelQuery;
import cn.wolfcode.luowowo.article.service.IDestThemeService;
import cn.wolfcode.luowowo.article.service.IDestinationService;
import cn.wolfcode.luowowo.cache.service.IHotelRedisService;
import cn.wolfcode.luowowo.comment.domain.HotelComment;
import cn.wolfcode.luowowo.comment.domain.TravelComment;
import cn.wolfcode.luowowo.comment.service.IHotelCommentService;
import cn.wolfcode.luowowo.comment.vo.HotelCommentVO;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import cn.wolfcode.luowowo.hotel.domain.Hotel;
import cn.wolfcode.luowowo.hotel.query.HotelCommentQuery;
import cn.wolfcode.luowowo.hotel.service.IHotelPriceService;
import cn.wolfcode.luowowo.hotel.service.IHotelService;
import cn.wolfcode.luowowo.hotel.vo.HotelAvgPriceVO;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.search.query.SearchQueryObject;
import cn.wolfcode.luowowo.search.service.IHotelCommentSearchService;
import cn.wolfcode.luowowo.search.service.IHotelSearchService;
import cn.wolfcode.luowowo.search.service.IHotelTemplateService;
import cn.wolfcode.luowowo.search.service.ISearchService;
import cn.wolfcode.luowowo.search.template.HotelCommentTemplate;
import cn.wolfcode.luowowo.search.template.HotelTemplate;
import cn.wolfcode.luowowo.website.annotation.RequireLogin;
import cn.wolfcode.luowowo.website.annotation.UserParam;
import com.alibaba.dubbo.config.annotation.Reference;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by zhi on 2019/8/21.
 */
@Controller
@RequestMapping("hotel")
public class HotelController {

    @Reference
    private IDestThemeService destThemeService;
    @Reference
    private IDestinationService destinationService;
    @Reference
    private IHotelService hotelService;
    @Reference
    private IHotelTemplateService hotelTemplateService;
    @Reference
    private IHotelPriceService hotelPriceService;
    @Reference
    private IHotelCommentService hotelCommentService;
    @Reference
    private IHotelRedisService hotelRedisService;
    @Reference
    private ISearchService searchService;
    @Reference
    private IHotelSearchService hotelSearchService;
    @Reference
    private IHotelCommentSearchService hotelCommentSearchService;

    @RequestMapping("")
    public String list(Model model, @ModelAttribute("qo") HotelQuery qo) {
        // 国内地区
        model.addAttribute("dest", destinationService.queryChinaCity());
        // 海外地区
        model.addAttribute("overseas", destinationService.queryOverseas());
        // 主题酒店
        model.addAttribute("hotelTags", destThemeService.queryThemeTop6());
        // 特价酒店
        model.addAttribute("hotelCity", destinationService.queryHotelPriceCityTop6());
        return "hotel/hotel";
    }

    @RequestMapping("theme")
    public String theme(Model model, Long id) {
        model.addAttribute("list", destThemeService.queryByThemeid(id));
        return "hotel/hotelTpl";
    }

    @RequestMapping("theme1")
    public String theme1(Model model, Long id) {
        model.addAttribute("list", hotelService.queryByCityId(id));
        return "hotel/hotelTpl1";
    }

    @RequestMapping("h")
    public String h(Model model, @ModelAttribute("qo") HotelQuery qo) {
        // 国内地区
        model.addAttribute("dests", destinationService.queryChinaCity());
        // 海外地区
        model.addAttribute("overseas", destinationService.queryOverseas());
        // 城市内地区
        model.addAttribute("destChild", destinationService.queryDestByParentName(qo.getName()));
        // 精准地区
        Destination dest = destinationService.getDestByName(qo.getName());
        model.addAttribute("dest", dest);
        // 吐司

        List<Destination> toasts = destinationService.getToasts(dest.getId());
        model.addAttribute("country", toasts.get(0));
        List<Destination> countryList = destinationService.getCountryNotId(toasts.get(0).getId());
        model.addAttribute("countryList", countryList.subList(0, countryList.size() > 5 ? 5 : countryList.size()));
        List<Destination> provinceList = destinationService.listByParentId(toasts.get(0).getId());
        model.addAttribute("provinceList", provinceList.subList(0, provinceList.size() > 5 ? 5 : provinceList.size()));
        // 均价
        List<HotelAvgPriceVO> priceList = hotelPriceService.queryStarAvg(dest.getId());
        model.addAttribute("starPrice", priceList);
        return "hotel/list";
    }


    @RequestMapping("get")
    public String get(Model model, Long id) {
        model.addAttribute("hotelPrice", hotelPriceService.getByHotelId(id));
        return "hotel/hotelPrice";

    }

    @RequestMapping("searchPage")
    public String searchPage(Model model, @ModelAttribute("qo") SearchQueryObject qo) {
        qo.setPageSize(3);
        qo.setOrderBy("price");
        Page hotelTemplates = hotelSearchService.searchWithHighlight(HotelTemplate.INDEX_NAME, HotelTemplate.TYPE_NAME, HotelTemplate.class, qo, "name");
        Page page = hotelTemplateService.queryHotelByCityName(qo);
        model.addAttribute("page", hotelTemplates);
        return "hotel/searchPageTpl";
    }


    @RequestMapping("detail")
    public String detail(Model model, Long id, @UserParam UserInfo userInfo) {
        // 吐司
        Hotel hotel = hotelService.get(id);
        List<Destination> toasts = destinationService.getToasts(hotel.getCity().getId());
        model.addAttribute("country", toasts.get(0));
        List<Destination> countryList = destinationService.getCountryNotId(toasts.get(0).getId());
        model.addAttribute("countryList", countryList.subList(0, countryList.size() > 5 ? 5 : countryList.size()));
        List<Destination> provinceList = destinationService.listByParentId(toasts.get(0).getId());
        model.addAttribute("provinceList", provinceList.subList(0, provinceList.size() > 5 ? 5 : provinceList.size()));
        // 当前酒店信息
        model.addAttribute("hotel", hotel);
        HotelCommentVO vo = hotelCommentService.querySiatisInfo(id);
        model.addAttribute("vo", vo);
        if (userInfo != null) {
            List<String> list = hotelRedisService.get(userInfo.getId());
            if (list.contains(id.toString())) {
                model.addAttribute("isFavor", "111");
            }
        }
        // 词频相关
        List<Map<String, Object>> searchCiPin = hotelCommentSearchService.searchCiPin(hotel.getId());
        model.addAttribute("cipin", searchCiPin);
        System.out.println(searchCiPin);
        return "hotel/detail";
    }

    @RequireLogin
    @PostMapping("commentAdd")
    public String commentAdd(Model model, HotelComment comment, @UserParam UserInfo userInfo) {
        comment.setUserId(userInfo.getId());
        comment.setUsername(userInfo.getNickname());
        comment.setCity(userInfo.getCity());
        comment.setLevel(userInfo.getLevel());
        comment.setHeadUrl(userInfo.getHeadImgUrl());

        HotelComment travelComment = hotelCommentService.saveOrUpdate(comment);
        HotelCommentTemplate hotelCommentTemplate = new HotelCommentTemplate();
        hotelCommentTemplate.setCreateTime(new Date());
        hotelCommentTemplate.setUserId(userInfo.getId());
        hotelCommentTemplate.setUsername(userInfo.getNickname());
        hotelCommentTemplate.setLevel(userInfo.getLevel());
        hotelCommentTemplate.setHeadUrl(userInfo.getHeadImgUrl());
        model.addAttribute("hc", travelComment);
        try {
            BeanUtils.copyProperties(hotelCommentTemplate, comment);
            hotelCommentSearchService.saveOrUpdate(hotelCommentTemplate);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        // model.addAttribute("floor", floor + 1);

        return "hotel/addComment";
    }

    @RequestMapping("comment")
    public String comment(Model model, @ModelAttribute("qo") HotelCommentQuery qo) {
        qo.setPageSize(3);
        if (StringUtils.hasLength(qo.getName())){
            Page page = hotelCommentSearchService.list(qo);
            model.addAttribute("page", page);

        }else {
            Page page = hotelCommentService.list(qo);
            model.addAttribute("page", page);
        }

        return "hotel/searchCommentTpl";
    }

    @RequestMapping("commentThumbUp")
    @ResponseBody
    public Object commentThumbUp(String hcid, Long uid) {
        hotelCommentService.commentThumbUp(hcid, uid);
        return AjaxResult.SUCCESS;
    }

    @RequestMapping("favor")
    @ResponseBody
    public Object favor(Long hid, Long uid) {
        AjaxResult result = new AjaxResult();
        boolean ret = true;
        try {
            ret = hotelRedisService.favor(hid, uid);
            result.setSuccess(ret);
        } catch (Exception e) {
            e.printStackTrace();
            result.setCode(102);
            result.setSuccess(false);
        }
        return result;

    }

    // 评论回复
    @RequestMapping("reComment")
    public String reComment(HotelComment hotelComment, @ModelAttribute("hcid") String hcid, String reusername, Model model) {
        if (hotelComment.getType() == 1) {
            HotelComment reComment = new HotelComment();
            reComment.setUsername(reusername);
            hotelComment.setRefComment(reComment);
        }
        HotelComment comment = hotelCommentService.reComment(hcid, hotelComment);
        model.addAttribute("r", comment);
        return "hotel/reComment";
    }

    // 词频
    @RequestMapping("cipin")
    public String cipin(Model model, Long hid, String name){

        return "";

    }


}
