package cn.wolfcode.luowowo.article.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DestFitlerQuery extends QueryObject {
    private Long monthId=-1L;
    private Long themeId=-1L;
    private Long daysId=-1L;
}
