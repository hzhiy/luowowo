<html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>骡窝窝国内机票查询 - 骡窝窝</title>
    <meta name="renderer" content="webkit">

    <link href="/styles/flight/css+base_css+jquery.suggest_css+plugins_css+plugins+jquery.jgrowl_css+other+popup_css+mfw-header.2015^YlVS^1564104263.css"
          rel="stylesheet" type="text/css">
    <link href="/styles/flight.css" rel="stylesheet" type="text/css">

    <link href="/styles/flight/index.2ef6f77c.css" rel="stylesheet"
          type="text/css">
    <link href="/styles/flight/css+hotel+datepicker-range^a1w^1552035728.css" rel="stylesheet" type="text/css">
    <link href="/styles/flight/css+mfw-header.2015^ylvs^1559526017.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="/js/jQuery-2.1.4/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/js/plugins/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="/js/plugins/jquery-form/jquery.form.js"></script>
    <script type="text/javascript" src="/js/plugins/jqPaginator/jq-paginator.min.js"></script>
    <script type="text/javascript" src="/js/plugins/jrender/jrender.min.js"></script>
    <style>
        .v-list-item-ticket {
            padding-top:20px;
            margin:auto 0;
        }
    </style>
    <script>
        $(function () {

            //搜索请求
            $.get('/flight/searchResult', {'orgCity': '${qo.orgCity}', 'dstCity': '${qo.dstCity}', 'depTime': '${qo.depTime?string("yyyy-MM-dd")}'}, function (data) {
                searchResult(data);
            });

        })

        $(function () {
            var timeType;//出发时间
            var depPortId;//出发机场
            var arrPortId;//降落机场
            var planeType;//飞机机型
            var airlineId;//航空公司
            //出发城市三字码
            var orgCity;
            //到达城市三字码
            var dstCity;

            var deleteHtml = "<span><i></i></spane>";
            //起飞时间点击事件
            $('.timeType').click(function () {
                timeType=$(this).data('time');
                console.log($(this).html());
                $('#depTimeType').val($(this).html());
               // $('#selectedTime').data('time');
            //  $('#selectedTime').attr("data-time",timeType);
                $('#selectedTime').css("display","block");
                $('#theSelected').css("display","block");
                $('#clearAll').css("display","block");
                $('#selectedTime .close').css('display','block');
                $('#timeName').data('code','timeName');
                $('#timeName').html($(this).html()+deleteHtml);
                var deptTime = $("#depTime").val();
                if ($('#orgCity').val()){
                    orgCity = $('#orgCity').val();
                }
                if($('#dstCity').val()){
                    dstCity = $('#dstCity').val();
                }
                $.get('/flight/searchResult2', {'orgCity': orgCity, 'dstCity': dstCity, 'depTime': deptTime,'timeType':timeType,'depPortId':depPortId,'arrPortId':arrPortId,'airlineId':airlineId,'planeType':planeType}, function (data) {
                    searchResult2(data);
                });

            })
            //机型
            $('.planeType').click(function () {
                planeType=$(this).data('type');
                $('#planeType').val($(this).html());
                $('#selectedPlaneType').css("display","block");
                $('#theSelected').css("display","block");
                $('#clearAll').css("display","block");
                $('#selectedPlaneType .close').css('display','block');
                $('#planeName').data('code','planeName');
                $('#planeName').html($(this).html()+deleteHtml);
                var deptTime = $("#depTime").val();
                if ($('#orgCity').val()){
                    orgCity = $('#orgCity').val();
                }
                if($('#dstCity').val()){
                    dstCity = $('#dstCity').val();
                }
                $.get('/flight/searchResult2', {'orgCity': orgCity, 'dstCity': dstCity, 'depTime': deptTime,'timeType':timeType,'depPortId':depPortId,'arrPortId':arrPortId,'airlineId':airlineId,'planeType':planeType}, function (data) {
                    searchResult2(data);
                });

            })

            //起飞机场
            $('#depport').on('click','.depport',function () {
                depPortId = $(this).data('depport');
                $('#depPort').val($(this).html());
                $('#selectedDepPort').css("display","block")
                $('#theSelected').css("display","block");
                $('#clearAll').css("display","block");
                $('#selectedDepPort .close').css('display','block');
                $('#depportName').data('code','depportName');
                $('#depportName').html($(this).html()+deleteHtml);
                var deptTime = $("#depTime").val();
                if ($('#orgCity').val()){
                    orgCity = $('#orgCity').val();
                }
                if($('#dstCity').val()){
                    dstCity = $('#dstCity').val();
                }
                $.get('/flight/searchResult2', {'orgCity': orgCity, 'dstCity': dstCity, 'depTime': deptTime,'timeType':timeType,'depPortId':depPortId,'arrPortId':arrPortId,'airlineId':airlineId,'planeType':planeType}, function (data) {
                    searchResult2(data);
                });

            })

            //降落机场
            $('#arrport').on('click','.arrport',function () {
                arrPortId = $(this).data('arrport');
                $('#arrPort').val($(this).html());
                $('#selectedArrPort').css("display","block");
                $('#theSelected').css("display","block");
                $('#clearAll').css("display","block");
                $('#selectedArrPort .close').css('display','block');
                $('#arrportName').data('code','arrportName');
                $('#arrportName').html($(this).html()+deleteHtml);
                var deptTime = $("#depTime").val();
                if ($('#orgCity').val()){
                    orgCity = $('#orgCity').val();
                }
                if($('#dstCity').val()){
                    dstCity = $('#dstCity').val();
                }
                $.get('/flight/searchResult2', {'orgCity': orgCity, 'dstCity': dstCity, 'depTime': deptTime,'timeType':timeType,'depPortId':depPortId,'arrPortId':arrPortId,'airlineId':airlineId,'planeType':planeType}, function (data) {
                    searchResult2(data);
                });
            })

            //航空公司
            $('#airline').on('click','.airline',function () {
                airlineId = $(this).data('airline');
                $('#airLine').val($(this).html());
                $('#selectedAirline').css("display","block");
                $('#theSelected').css("display","block");
                $('#clearAll').css("display","block");
                $('#airlineName').data('code','airlineName');
                $('#airlineName').html($(this).html()+deleteHtml);
                var deptTime = $("#depTime").val();
                if ($('#orgCity').val()){
                    orgCity = $('#orgCity').val();
                }
                if($('#dstCity').val()){
                    dstCity = $('#dstCity').val();
                }
                $('#selectedAirline .close').css('display','block');
                $.get('/flight/searchResult2', {'orgCity': orgCity, 'dstCity': dstCity, 'depTime': deptTime,'timeType':timeType,'depPortId':depPortId,'arrPortId':arrPortId,'airlineId':airlineId,'planeType':planeType}, function (data) {
                    searchResult2(data);
                });
            })


            //点击删除已选中

            $('.close').on('click',"i",function () {
                var closest = $(this).closest("div");
                closest.css("display","none");
                var code = closest.data('code');
                if (code=='timeName'){
                    timeType=null;
                } else if(code=='depportName'){
                    depPortId=null;
                } else if(code=='arrportName'){
                    arrPortId=null;
                } else if(code=='airlineName'){
                    airlineId=null;
                } else if(code=='planeName'){
                    planeType=null;
                }
                var deptTime = $("#depTime").val();
                if ($('#orgCity').val()){
                    orgCity = $('#orgCity').val();
                }
                if($('#dstCity').val()){
                    dstCity = $('#dstCity').val();
                }
                $('#selectedAirline .close').css('display','block');
                $.get('/flight/searchResult2', {'orgCity': orgCity, 'dstCity': dstCity, 'depTime': deptTime,'timeType':timeType,'depPortId':depPortId,'arrPortId':arrPortId,'airlineId':airlineId,'planeType':planeType}, function (data) {
                    searchResult2(data);
                });

            });

          /*    $('.close i').click(function () {
               $(this).closest('div').css("display","none");
               if ($('clo'))
            var flags = $('.close').map(function (i,v) {
                   console.log(i);
                  // console.log($(v).css());
                   if($(v).css('display')=='block') {
                       return true;
                   }
               }).get();

               console.log(flags);

               for(var i=0;i<flags.length;i++){
                  if (flags[i]){
                     // console.log(flags[i]);
                      return;
                  }
              }
              /!*  alert("heheh");
                $('#theSelected').css('display','none');
                $('#clearAll').css('display','none');

              //  console.log(flag);
            }) */




            //出发航班城市的标签点击事件 (为文本框赋值)
            $(".departGroupLayer a").click(function () {
                $("#departCity").val('');
                var flight = $(this).data('name');
                orgCity = $(this).data('code');

                $("#departCity").val(flight);
                $("#departCityLayer").css('display', 'none');
            });

            //航班城市选择框的显示
            $("#departCity").click(function () {
                $("#departCityLayer").css('display', 'block');
                //显示国内热门城市
            });
            /*  //航班城市选择框的隐藏
              $("#departCity").blur(function () {
                  $("#departCityLayer").css('display', 'none');
                  //显示国内热门城市
              });*/

            //到达航班城市的标签点击事件 (为文本框赋值)
            $(".destGroupLayer a").click(function () {
                console.log("in");
                $("#destCity").val('');
                var flight = $(this).data('name');
                dstCity = $(this).data('code');
                $("#destCity").val(flight);
                $("#destCityLayer").css('display', 'none');
            });

            //航班城市选择框的显示
            $("#destCity").click(function () {
                $("#destCityLayer").css('display', 'block');
            });


            var tab = '#hot';

            //城市选择款的nav栏
            $(".hcl-sort").mouseenter(function () {
                $(this).attr('class', 'hcl-sort on');
                $(tab).css('display', 'none');
                tab = '#' + $(this).data('tab');
                $(tab).css('display', 'block');
            });

            //城市选择款nae栏的颜色改变
            $(".hcl-sort").mouseleave(function () {
                $(this).attr('class', 'hcl-sort');
            });

            //' 换'按钮的点击事件
            $("#citySwitc").click(function () {
                //值切换
                var temp = $("#departCity").val();
                var destval = $("#destCity").val();
                $("#departCity").val(destval);
                $("#destCity").val(temp);
                //三字码
                var temp2 = orgCity;
                orgCity = dstCity;
                dstCity = temp2;
            });

            //查询按键点击处理
            $("#search").click(function () {
                console.log(orgCity);
                console.log(dstCity);
                var deptTime = $("#depTime").val();
                if ($('#orgCity').val()){
                    orgCity = $('#orgCity').val();
                }
                if($('#dstCity').val()){
                    dstCity = $('#dstCity').val();
                }

                timeType=null;
                depPortId=null;
                arrPortId=null;
                airlineId=null;
                planeType=null;

                $('.close').css('display',"none");

                $.get('/flight/searchResult', {'orgCity': orgCity, 'dstCity': dstCity, 'depTime': deptTime,'timeType':timeType,'depPortId':depPortId,'arrPortId':arrPortId,'airlineId':airlineId,'planeType':planeType}, function (data) {
                    searchResult(data);
                });
            });

            /*订票机票点击事件*/
            /*  $("#btn_search").on("click", function () {
                  var r = confirm("确认是否订购该机票？");
                  if (r == true)
                  {
                      alert("预订成功!");
                  } else
                  {
                      alert("取消预订!");
                  }
              })*/


            //搜索
            /*  $('#search').click(function () {
                  //orgCity和dstCity
                  $("#toSearch").submit();
              })*/

        });

        //searchResult
        function searchResult2(data){
            if (data.success) {
                console.log(data);
                var result = data.data;
                //航程列表
                $("#airinfo").renderValues({list:result.flights.list}, {
                    beginFly: function (item, value) {
                        if (value){
                            $(item).html(value.substr(11, 5));
                        }
                    },
                    endFly: function (item, value) {
                        if(value){
                            $(item).html(value.substr(11, 5));
                        }
                    }
                });
            } else {
                alert(data.msg);
            }
        }


        function searchResult(data){
            if (data.success) {
                console.log(data);
                //  console.log(data);
                var result = data.data;
                // console.log(flightInfo);
                // var a = JSON.parse(flightInfo);
                // console.log(a);
                //var b = a.result.output.result;
                /* {list: flightInfo.list}*/
                //console.log(flightInfo);
                //console.log(flightInfo.list);

                //航程列表
                $("#airinfo").renderValues({list:result.flights.list}, {
                    beginFly: function (item, value) {
                        if (value){
                            $(item).html(value.substr(11, 5));
                        }
                    },
                    endFly: function (item, value) {
                        if(value){
                            $(item).html(value.substr(11, 5));
                        }
                    }
                });

                //起飞机场列表
                $('#depport').html('');
               $.each(data.data.depPorts,function (index,ele) {
                   //id name,
                   var id = ele.id;
                   var name = ele.name;
                   $('#depport').append("<li class='depport' data-depport='"+id+"'>"+name+"</>");
               })
                //降落机场列表
                $('#arrport').html('');
                $.each(data.data.arrPorts,function (index,ele) {
                    //id name,
                    var id = ele.id;
                    var name = ele.name;
                    $('#arrport').append("<li class='arrport' data-arrport='"+id+"'>"+name+"</>");
                })


                //航空公司
                $('#airline').html('');
                $.each(data.data.airlines,function (index,ele) {
                    //id name,
                    var id = ele.id;
                    var name = ele.name;
                    $('#airline').append("<li class='airline' data-airline='"+id+"'>"+name+"</>");
                })
                //airline
                //   console.log(b);
            } else {
                alert(data.msg);
            }
        }
    </script>
</head>

<body style="position: relative;">


<div id="header" xmlns="http://www.w3.org/1999/html">
    <div class="mfw-header">
        <div class="header-wrap clearfix" id="_j_head_nav_warper">
            <div class="head-logo"><a class="mfw-logo" title="马蜂窝自由行" href="http://www.mafengwo.cn/"></a></div>
            <ul class="head-nav" data-cs-t="headnav" id="_j_head_nav">
                <li class="head-nav-index _j_head_nav_index" data-cs-p="index" data-tab-item="首页"><a
                        href="http://www.mafengwo.cn/">首页</a></li>
                <li class="head-nav-place _j_head_nav_mdd" data-cs-p="mdd" data-tab-item="目的地"><a
                        href="http://www.mafengwo.cn/mdd/" title="目的地">目的地</a></li>
                <li class="head-nav-gonglve _j_head_nav_gonglve" data-cs-p="gonglve" data-tab-item="旅游攻略"><a
                        href="http://www.mafengwo.cn/gonglve/" title="旅游攻略">旅游攻略</a></li>
                <li class="head-nav-sales _j_head_nav_sales head-nav-dropdown _j_sales_nav_show" id="_j_nav_sales"
                    data-cs-p="sales" data-tab-item="去旅行">
                    <a class="drop-toggle" href="http://www.mafengwo.cn/sales/"
                       style="cursor: pointer;display: block;border-bottom:0 none;" data-sales-nav="去旅行">
                        <span>去旅行<i class="icon-caret-down"></i></span>
                    </a>
                    <div class="dropdown-menu dropdown-sales hide _j_sales_top" id="_j_sales_panel"
                         data-cs-t="sales_nav">
                        <ul>
                            <li><a target="_blank" href="http://www.mafengwo.cn/sales/" data-sales-nav="自由行">自由行<i
                                    class="i-hot">hot</i></a></li>
                            <li><a target="_blank" href="http://www.mafengwo.cn/sales/0-0-0-0-0-0-0-0.html?group=4"
                                   data-sales-nav="跟团游">跟团游</a></li>
                            <li><a target="_blank" href="http://www.mafengwo.cn/localdeals/"
                                   data-sales-nav="当地游">当地游</a></li>
                            <li><a target="_blank" href="http://www.mafengwo.cn/sales/0-0-0-5-0-0-0-0.html"
                                   data-sales-nav="邮轮">邮轮</a></li>
                            <li><a target="_blank" href="http://www.mafengwo.cn/sales/visa/" data-sales-nav="签证">签证</a>
                            </li>

                        </ul>
                    </div>
                </li>
                <li class="head-nav-flight _j_head_nav_flight head-nav-active" data-cs-p="flight" data-tab-item="机票"><a
                        href="http://www.mafengwo.cn/flight/" title="国内机票">机票</a></li>
                <li class="head-nav-hotel _j_head_nav_hotel" data-cs-p="hotel" data-tab-item="酒店"><a
                        href="http://www.mafengwo.cn/hotel/" title="酒店">订酒店</a></li>
                <li class="head-nav-community _j_head_nav_community head-nav-dropdown _j_shequ_nav_show"
                    id="_j_nav_community" data-cs-p="community" data-tab-item="社区">
                    <div class="drop-toggle"><span>社区<i class="icon-caret-down"></i></span></div>
                    <!-- 社区下拉面板 begin -->
                    <div class="dropdown-panel dropdown-community hide _j_shequ_top no-image" id="_j_community_panel"
                         data-cs-t="community_nav">
                        <div class="panel-wrapper">
                            <ul class="nav-list clearfix">
                                <li class="h"><a href="http://www.mafengwo.cn/wenda/" target="_blank" title="问答"
                                                 data-cs-p="wenda">问答<i class="i-hot">hot</i></a></li>
                                <li><a href="http://www.mafengwo.cn/mall/things.php" target="_blank" title="马蜂窝周边"
                                       data-cs-p="things">马蜂窝周边<i class="i-new">new</i></a></li>
                                <li><a href="http://www.mafengwo.cn/club/" target="_blank" title="蜂首俱乐部"
                                       data-cs-p="club">蜂首俱乐部</a></li>
                                <li><a href="http://www.mafengwo.cn/together/" target="_blank" title="结伴"
                                       data-cs-p="together">结伴</a></li>
                            </ul>
                            <ul class="nav-list-sub clearfix">

                                <li><a href="http://www.mafengwo.cn/group/" target="_blank" title="马蜂窝旅行家"
                                       data-cs-p="group">小组论坛</a></li>
                                <li><a href="http://www.mafengwo.cn/rudder/" target="_blank" title="分舵同城"
                                       data-cs-p="rudder">分舵同城</a></li>
                                <li><a href="http://www.mafengwo.cn/auction/" target="_blank" title="马蜂窝拍卖行"
                                       data-cs-p="paimai">马蜂窝拍卖行</a></li>

                                <!--<li><a href="http://www.mafengwo.cn/postal/" target="_blank" title="游记纪念工厂" data-cs-p="postal">游记纪念工厂</a></li>-->
                                <li><a href="http://www.mafengwo.cn/photo_pk/prev.php" target="_blank" title="照片PK"
                                       data-cs-p="photo_pk">照片PK</a></li>
                                <li><a href="http://www.mafengwo.cn/focus/" target="_blank" title="真人兽"
                                       data-cs-p="focus">真人兽</a></li>
                                <li><a href="http://www.mafengwo.cn/mall/virtual_goods.php" target="_blank" title="道具商店"
                                       data-cs-p="virtual">道具商店</a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- 社区下拉面板 end -->
                </li>
                <li class="head-nav-app _j_head_nav_app" data-cs-p="app" data-tab-item="APP"><a
                        href="http://www.mafengwo.cn/app/intro/gonglve.php" title="APP">APP</a></li>
                <li class="head-nav-choice _j_head_nav_choice" data-cs-p="choice" data-tab-item="马蜂窝旅行者之选"><a
                        href="http://www.mafengwo.cn/poi/choice/group">
                    <div class="mfwzx-logo"></div>
                </a></li>
            </ul>
            <div class="head-search" data-online="1">
                <div class="head-search-wrapper">
                    <div class="head-searchform">
                        <input name="q" type="text" id="_j_head_search_input" autocomplete="off">
                        <a role="button" href="javascript:" class="icon-search" id="_j_head_search_link"></a>
                    </div>
                </div>
            </div>
            <div id="pagelet-block-48134057e2b13772b2c8c26380e7dc52" class="pagelet-block"
                 data-api="apps:user:pagelet:pageViewHeadInfo" data-params="{&quot;type&quot;:1}" data-async="1"
                 data-controller="/js/pageletcommon/pageHeadUserInfoWWWNormal">
                <div class="login-info">
                    <div class="head-user" id="_j_head_user">
                        <a class="drop-trigger" href="http://www.mafengwo.cn/u/51906092.html" title="Irving的窝"
                           rel="nofollow">
                            <div class="user-image"><img
                                    src="http://n1-q.mafengwo.net/s14/M00/AD/45/wKgE2l1VBu-AB3aJAALvCqtVYj001.jpeg?imageMogr2%2Fthumbnail%2F%2132x32r%2Fgravity%2FCenter%2Fcrop%2F%2132x32%2Fquality%2F90"
                                    height="32" width="32" alt="Irving的窝"></div>
                            <i class="icon-caret-down"></i>
                        </a>
                    </div>
                    <div class="head-msg" id="_j_head_msg">
                        <a class="drop-trigger" href="javascript:" rel="nofollow">
                            <i class="icon-msg"></i>
                            消息
                            <i class="icon-caret-down"></i>
                            <span class="head-msg-new hide" style="display: none;"></span>
                        </a>
                    </div>
                    <div class="head-daka ">
                        <a class="btn head-btn-daka" href="javascript:" rel="nofollow" id="head-btn-daka" title="打卡"
                           data-japp="daka">打卡</a>
                        <!-- <a class="btn-active head-btn-daka" href="javascript:" rel="nofollow" id="head-btn-daka-active" title="打卡推荐" data-japp="daka">打卡推荐</a> -->
                        <a class="btn-active head-btn-daka" href="javascript:" rel="nofollow" id="head-btn-daka-active"
                           title="已打卡" data-japp="daka">已打卡</a>
                    </div>
                </div>
                <div class="dropdown-group">
                    <!-- 消息下拉菜单 begin -->
                    <div class="dropdown-menu dropdown-msg hide" id="_j_msg_panel" style="z-index:10;">
                        <ul>

                            <li><a href="http://www.mafengwo.cn/msg/sms/index" target="_blank" rel="nofollow">私信</a>
                            </li>
                            <li><a href="http://www.mafengwo.cn/msg/entry/group" target="_blank" rel="nofollow">小组消息</a>
                            </li>
                            <li><a href="http://www.mafengwo.cn/msg/entry/sys" target="_blank" rel="nofollow">系统通知</a>
                            </li>
                            <li><a href="http://www.mafengwo.cn/msg/entry/ask" target="_blank" rel="nofollow">问答消息</a>
                            </li>
                            <li><a href="http://www.mafengwo.cn/msg/entry/reply" target="_blank" rel="nofollow">回复消息</a>
                            </li>
                            <li><a href="http://www.mafengwo.cn/msg/entry/fav" target="_blank" rel="nofollow">喜欢与收藏</a>
                            </li>
                            <li><a href="http://www.mafengwo.cn/msg/entry/friends" target="_blank"
                                   rel="nofollow">好友动态</a></li>
                        </ul>
                    </div>
                    <div class="dropdown-menu dropdown-msg hide" id="_j_msg_float_panel" style="display: none;">
                        <ul></ul>
                        <a href="javascript:" class="close-newmsg">×</a>
                    </div>
                    <!-- 消息下拉菜单 end -->
                    <!-- 用户下拉菜单 begin -->
                    <div class="dropdown-menu dropdown-user hide" id="_j_user_panel" data-cs-t="user_nav">
                        <div class="user-info">
                            <a class="coin" href="http://www.mafengwo.cn/g/i/2947856.html" target="_blank"
                               id="head-my-honey" rel="nofollow" data-cs-p="coin">蜂蜜 0</a> / <a class="coin"
                                                                                                href="http://www.mafengwo.cn/user/lv.php#coin"
                                                                                                target="_blank"
                                                                                                id="head-my-coin"
                                                                                                rel="nofollow"
                                                                                                data-cs-p="coin">金币
                            856</a>
                        </div>
                        <ul>
                            <li><a href="http://www.mafengwo.cn/u/51906092.html" target="_blank" title="我的马蜂窝"
                                   rel="nofollow" data-cs-p="wo"><i class="icon-wo"></i>我的马蜂窝<span
                                    class="level">LV.3</span> </a></li>
                            <li><a href="http://www.mafengwo.cn/note/create_index.php" target="_blank"
                                   class="drop-write" title="写游记" rel="nofollow" data-cs-p="writenotes"><i
                                    class="icon-writenotes"></i>写游记</a></li>
                            <li><a href="https://w.mafengwo.cn/activity_reserve_note/index" target="_blank"
                                   class="drop-write" title="预约游记" rel="nofollow" data-cs-p="appointnotes"><i
                                    class="icon-ordernotes"></i>预约游记</a></li>
                            <li data-cs-t="足迹_首页" data-cs-p="页头_我的足迹"><a
                                    href="http://www.mafengwo.cn/path/51906092.html" target="_blank" title="我的足迹"
                                    rel="nofollow"><i class="icon-path"></i>我的足迹</a></li>
                            <li><a href="http://www.mafengwo.cn/wenda/u/51906092/answer.html" target="_blank"
                                   title="我的问答" rel="nofollow" data-cs-p="wenda"><i class="icon-wenda"></i>我的问答</a></li>
                            <li><a href="http://www.mafengwo.cn/friend/index/follow" target="_blank" title="我的好友"
                                   rel="nofollow" data-cs-p="friend"><i class="icon-friend"></i>我的好友</a></li>
                            <li><a href="http://www.mafengwo.cn/plan/fav_type.php" title="我的收藏" target="_blank"
                                   rel="nofollow" data-cs-p="collect"><i class="icon-collect"></i>我的收藏</a></li>
                            <li><a href="http://www.mafengwo.cn/plan/route.php" title="我的路线" target="_blank"
                                   rel="nofollow" data-cs-p="route"><i class="icon-route"></i>我的路线</a></li>
                            <li><a href="http://www.mafengwo.cn/order_center/" title="我的订单" target="_blank"
                                   rel="nofollow" data-cs-p="order"><i class="icon-order"></i>我的订单</a></li>
                            <li><a href="http://www.mafengwo.cn/sales/coupon.php" title="我的优惠券" target="_blank"
                                   rel="nofollow" data-cs-p="coupon"><i class="icon-coupon"></i>我的优惠券</a></li>
                            <li><a href="https://www.mafengwo.cn/kol/home" title="创作者开放平台" target="_blank"
                                   rel="nofollow"><i class="icon-gonglve"></i>创作者开放平台</a></li>
                            <li><a href="https://passport.mafengwo.cn/setting/" title="我的设置" target="_blank"
                                   rel="nofollow" data-cs-p="settings"><i class="icon-settings"></i>设置</a></li>
                            <li><a href="https://passport.mafengwo.cn/logout.html" title="退出登录" rel="nofollow"><i
                                    class="icon-logout" data-cs-p="logout"></i>退出</a></li>
                        </ul>
                    </div>
                    <!-- 用户下拉菜单 end -->
                </div>
            </div>
        </div>
        <div class="shadow"></div>
    </div>

    <!-- 新自由行菜单 begin -->
    <div class="dropdown-bar" style="display: none">
        <div class="content">
            <ul class="clearfix ul-dropdown-bar" id="Js_middleNav">
                <li data-type="sales"><a href="http://www.mafengwo.cn/sales/">自由行</a></li>
                <li data-type="freewalker"><a href="http://www.mafengwo.cn/sales/0-0-0-0-0-0-0-0.html?group=4">跟团游</a>
                </li>
                <li data-type="localdeals"><a href="http://www.mafengwo.cn/localdeals/">当地游</a></li>

                <li data-type="visa"><a href="http://www.mafengwo.cn/sales/visa/">签证</a></li>
                <li data-type="wifi"><a href="http://www.mafengwo.cn/localdeals/0-0-0-21-0-0-0-0.html">全球WiFi</a></li>
                <li data-type="cruise"><a href="http://www.mafengwo.cn/sales/0-0-0-5-0-0-0-0.html">邮轮</a></li>


            </ul>
        </div>

    </div>

</div>


<link rel="stylesheet" href="/styles/flight/swiper-4.4.2.min.css">


<div id="app"><!---->
    <div class="fltlist"><!----><!---->
        <div class="fltlist-searchbox">
            <div class="v-flightpc-container">
                <div class="fltlist-search width72">
                    <div class="v-select">
                        <div class="v-select-wrapper"><#--<input type="text" readonly="readonly" placeholder=""><i
                                class="v-select-arrow-down"></i><i class="v-select-arrow-up"></i>--></div>
                       <#-- <ul class="v-select-list">
                            <li class="current">单程</li>
                            <li class="">往返</li>
                        </ul>-->
                    </div>
                </div>
                <div class="fltlist-search margin-left14"><input placeholder="出发城市" class="v-search-input" id="departCity" value="${qo.orgCityName}">
                    <input type="hidden" id="orgCity" value="${qo.orgCity}">
                    <!---->
                    <div class="hot-city-layer" id="departCityLayer" style="width: 490px; display: none;">
                        <!--航班城市分类导航栏-->
                        <div class="hcl-nav">
                            <a class="hcl-sort on" id="departHotTab" data-tab="hot" href="javascript:;">国内热门</a>
                            <a href="javascript:;" data-tab="ABCDE" class="hcl-sort">ABCDE</a>
                            <a href="javascript:;" data-tab="FGHJ" class="hcl-sort">FGHJ</a>
                            <a href="javascript:;" data-tab="KLMNP" class="hcl-sort">KLMNP</a>
                            <a href="javascript:;" data-tab="QRSTW" class="hcl-sort">QRSTW</a>
                            <a href="javascript:;" data-tab="XYZ" class="hcl-sort">XYZ</a>
                        </div>
                        <!--航班城市列表-->
                        <div class="hcl-list" id="startFlight">
                            <div class="departGroupLayer ng-scope" id="hot" style="display: block;">
                                <dl class="ng-scope">
                                    <dd>
                                    <#list hotFlights as ele>
                                        <a href="javascript:;" class="ng-binding ng-scope" data-id="${ele.id}"
                                           data-name="${ele.name}" data-code="${ele.aircode}">${ele.name}</a>
                                    </#list>
                                    </dd>
                                </dl>
                            </div>
                            <div class="departGroupLayer ng-scope" id="ABCDE" style="display: none;">
                                <dl class="ng-scope">
                                    <dd>
                                    <#list initialA as ele>
                                        <a href="javascript:;" class="ng-binding ng-scope" data-id="${ele.id}"
                                           data-name="${ele.name}" data-code="${ele.aircode}">${ele.name}</a>
                                    </#list>
                                    </dd>
                                </dl>
                            </div>
                            <div class="departGroupLayer ng-scope" id="FGHJ" style="display: none;">
                                <dl class="ng-scope">
                                    <dd>
                                    <#list initialF as ele>
                                        <a href="javascript:;" class="ng-binding ng-scope" data-id="${ele.id}"
                                           data-name="${ele.name}" data-code="${ele.aircode}">${ele.name}</a>
                                    </#list>
                                    </dd>
                                </dl>
                            </div>
                            <div class="departGroupLayer ng-scope" id="KLMNP" style="display: none;">
                                <dl class="ng-scope">

                                    <dd>
                                    <#list initialK as ele>
                                        <a href="javascript:;" class="ng-binding ng-scope" data-id="${ele.id}"
                                           data-name="${ele.name}" data-code="${ele.aircode}">${ele.name}</a>
                                    </#list>
                                    </dd>
                                </dl>
                            </div>
                            <div class="departGroupLayer ng-scope" id="QRSTW" style="display: none;">
                                <dl class="ng-scope">
                                    <dd>
                                    <#list initialQ as ele>
                                        <a href="javascript:;" class="ng-binding ng-scope" data-id="${ele.id}"
                                           data-name="${ele.name}" data-code="${ele.aircode}">${ele.name}</a>
                                    </#list>
                                    </dd>
                                </dl>
                            </div>
                            <div class="departGroupLayer ng-scope" id="XYZ" style="display: none;">
                                <dl class="ng-scope">
                                    <dd>
                                    <#list initialX as ele>
                                        <a href="javascript:;" class="ng-binding ng-scope" data-id="${ele.id}"
                                           data-name="${ele.name}" data-code="${ele.aircode}">${ele.name}</a>
                                    </#list>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                    <!----></div>
                <!-not mafengwo-->


                <!--not mafengwo-->
                <div class="fltlist-search fltlist-change">
                    换
                </div>
                <div class="fltlist-search"><input placeholder="到达城市" class="v-search-input" id="destCity" value="${qo.dstCityName}">
                    <input type="hidden" id="dstCity" value="${qo.dstCity}">
                    <!---->
                    <div class="hot-city-layer" id="destCityLayer" style="width: 490px; display: none;">
                        <!--航班城市分类导航栏-->
                        <div class="hcl-nav">
                            <a class="hcl-sort on" id="departHotTab" data-tab="hot2" href="javascript:;">国内热门</a>
                            <a href="javascript:;" data-tab="ABCDE2" class="hcl-sort">ABCDE</a>
                            <a href="javascript:;" data-tab="FGHJ2" class="hcl-sort">FGHJ</a>
                            <a href="javascript:;" data-tab="KLMNP2" class="hcl-sort">KLMNP</a>
                            <a href="javascript:;" data-tab="QRSTW2" class="hcl-sort">QRSTW</a>
                            <a href="javascript:;" data-tab="XYZ2" class="hcl-sort">XYZ</a>
                        </div>
                        <!--航班城市列表-->
                        <div class="hcl-list" id="startFlight">
                            <div class="destGroupLayer ng-scope" id="hot2" style="display: block;">
                                <dl class="ng-scope">
                                    <dd>
                                    <#list hotFlights as ele>
                                        <a href="javascript:;" class="ng-binding ng-scope" data-id="${ele.id}"
                                           data-name="${ele.name}" data-code="${ele.aircode}">${ele.name}</a>
                                    </#list>
                                    </dd>
                                </dl>
                            </div>
                            <div class="destGroupLayer ng-scope" id="ABCDE2" style="display: none;">
                                <dl class="ng-scope">
                                    <dd>
                                    <#list initialA as ele>
                                        <a href="javascript:;" class="ng-binding ng-scope" data-id="${ele.id}"
                                           data-name="${ele.name}" data-code="${ele.aircode}">${ele.name}</a>
                                    </#list>
                                    </dd>
                                </dl>
                            </div>
                            <div class="destGroupLayer ng-scope" id="FGHJ2" style="display: none;">
                                <dl class="ng-scope">
                                    <dd>
                                    <#list initialF as ele>
                                        <a href="javascript:;" class="ng-binding ng-scope" data-id="${ele.id}"
                                           data-name="${ele.name}" data-code="${ele.aircode}">${ele.name}</a>
                                    </#list>
                                    </dd>
                                </dl>
                            </div>

                            <div class="destGroupLayer ng-scope" id="KLMNP2" style="display: none;">
                                <dl class="ng-scope">
                                    <dd>
                                    <#list initialK as ele>
                                        <a href="javascript:;" class="ng-binding ng-scope" data-id="${ele.id}"
                                           data-name="${ele.name}" data-code="${ele.aircode}">${ele.name}</a>
                                    </#list>
                                    </dd>
                                </dl>
                            </div>
                            <div class="destGroupLayer ng-scope" id="QRSTW2" style="display: none;">
                                <dl class="ng-scope">
                                    <dd>
                                    <#list initialQ as ele>
                                        <a href="javascript:;" class="ng-binding ng-scope" data-id="${ele.id}"
                                           data-name="${ele.name}" data-code="${ele.aircode}">${ele.name}</a>
                                    </#list>
                                    </dd>
                                </dl>
                            </div>
                            <div class="destGroupLayer ng-scope" id="XYZ2" style="display: none;">
                                <dl class="ng-scope">
                                    <dd>
                                    <#list initialX as ele>
                                        <a href="javascript:;" class="ng-binding ng-scope" data-id="${ele.id}"
                                           data-name="${ele.name}" data-code="${ele.aircode}">${ele.name}</a>
                                    </#list>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                    <!----></div>
                <div class="fltlist-search margin-left14">
                    <div class="fltlist-input width200 padding-left48"><input type="text"
                                                                              placeholder="yyyy-mm-dd" onclick="WdatePicker()" value="${qo.depTime?string("yyyy-MM-dd")}" id="depTime" ><span
                            class="date-desc">去程</span></div>
                    <div class="fltlist-input width200 padding-left48 padding-right0"><input type="text"
                                                                                             readonly="readonly"
                                                                                             placeholder="yyyy-mm-dd"><span
                            class="date-desc">返程</span><!----></div><!----><!----><!----></div>
                <div class="fltlist-search">
                    <button id="search"><i></i><span>搜索</span></button>
                </div>
            </div>
        </div>
        <div class="v-flightpc-container clearfix">
            <div class="fltlist-result">
                <div class="fltlist-selected-plane">选择航班：上海 ⇀ 广州 08月25日 周日</div><!---->
                <div class="fltlist-select-date">
                    <div class="fltlist-datebar">
                        <div class="fltlist-date">
                            <div class="fltlist-date-arrow-left">
                                <div class="fltlist-date-arrow-icon"><span><i></i></span></div>
                            </div>
                            <div class="fltlist-date-middle">
                                <div class="v-nav-bar-nav-bar v-nav-bar-nav-bar_horizontal">
                                    <div class="v-nav-bar-wrapper">
                                        <div class="v-nav-bar-content" style="transform: translate(0px, 0px);">
                                            <div class="v-nav-bar-list-wrapper">
                                                <div class="v-nav-bar-nav-bar-items">
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>752
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">08-24 周六</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item v-nav-bar-nav-bar-item_active">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>672
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">08-25 周日</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>662
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">08-26 周一</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>660
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">08-27 周二</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>586
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">08-28 周三</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>660
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">08-29 周四</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>660
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">08-30 周五</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>510
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">08-31 周六</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>540
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-01 周日</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>602
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-02 周一</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>604
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-03 周二</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>663
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-04 周三</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>624
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-05 周四</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>661
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-06 周五</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>545
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-07 周六</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>684
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-08 周日</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>585
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-09 周一</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>623
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-10 周二</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>864
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-11 周三</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>844
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-12 周四</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>351
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-13 周五</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>258
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-14 周六</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>552
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-15 周日</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>607
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-16 周一</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>607
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-17 周二</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>566
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-18 周三</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>586
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-19 周四</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>662
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-20 周五</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>546
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-21 周六</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>632
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-22 周日</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>672
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-23 周一</div>
                                                    </div>
                                                    <div class="v-nav-bar-nav-bar-item last">
                                                        <div class="v-nav-bar-nav-bar-item-price"><span>￥</span>510
                                                        </div>
                                                        <div class="v-nav-bar-nav-bar-item-day">09-24 周二</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="fltlist-date-arrow-right">
                                <div class="fltlist-date-arrow-icon"><span><i class="icon-right"></i></span></div>
                            </div>
                        </div>
                    </div>
                    <div class="fltlist-calendar">
                        <div class="fltlist-calendar-detail"><span><i></i></span>
                            <div class="fltlist-calendar-text">低价日历</div>
                        </div>
                        <p class="fltlist-calendar-close" style="display: none;"><i></i></p></div>
                </div><!---->
                <div class="filter">
                    <div class="v-list-dialog" style="display: none;">
                        <div class="v-list-mask-loading">
                            <div class="v-list-mask-loading-icon"></div>
                        </div>
                    </div>
                    <div class="fliter-wrapper ">
                        <div class="filter-desc">筛选：</div>
                        <div class="filter-desc width100 margin-left14">
                            <div class="v-select">
                                <div class="v-select-wrapper"><input type="text" id="depTimeType"  placeholder="" value="起飞时间"><i
                                        class="v-select-arrow-down"></i><i class="v-select-arrow-up"></i></div>
                               <ul class="v-select-list">
                                    <li  data-time="1" class="timeType">00:00-09:59</li>
                                    <li  data-time="2" class="timeType">10:00-13:59</li>
                                    <li data-time="3" class="timeType">14:00-18:59</li>
                                    <li data-time="4" class="timeType">19:00-24:00</li>
                                </ul>
                            </div>
                        </div>
                        <div class="filter-desc width100 margin-left8">
                            <div class="v-select">
                                <div class="v-select-wrapper"><input type="text"  placeholder="" value="起飞机场" id="depPort"><i
                                        class="v-select-arrow-down"></i><i class="v-select-arrow-up"></i></div>
                                <ul class="v-select-list " id="depport">
                                   <#-- <li class="">浦东机场</li>
                                    <li class="">虹桥机场</li>-->
                                </ul>
                            </div>
                        </div>
                        <div class="filter-desc width100 margin-left8">
                            <div class="v-select">
                                <div class="v-select-wrapper"><input type="text" placeholder="" value="降落机场" id="arrPort"><i
                                        class="v-select-arrow-down"></i><i class="v-select-arrow-up"></i></div>
                                <ul class="v-select-list " id="arrport">
                                    <#--<li class="">白云机场</li>-->
                                </ul>
                            </div>
                        </div>
                        <div class="filter-desc width100 margin-left8">
                            <div class="v-select">
                                <div class="v-select-wrapper"><input type="text"  placeholder="" value="航空公司" id="airLine"><i
                                        class="v-select-arrow-down"></i><i class="v-select-arrow-up"></i></div>
                                <ul class="v-select-list " id="airline">
                                    <#--<li class="">春秋航空</li>
                                    <li class="">吉祥航空</li>
                                    <li class="">南方航空</li>
                                    <li class="">厦门航空</li>
                                    <li class="">深圳航空</li>
                                    <li class="">东方航空</li>
                                    <li class="">四川航空</li>
                                    <li class="">中国国际航空</li>
                                    <li class="">海南航空</li>
                                    <li class="">上海航空</li>
                                    <li class="">金鹏航空</li>-->
                                </ul>
                            </div>
                        </div>
                        <div class="filter-desc width100 margin-left8">
                            <div class="v-select">
                                <div class="v-select-wrapper"><input type="text" readonly="readonly" placeholder="" id="planeType" value="机型"><i
                                        class="v-select-arrow-down"></i><i class="v-select-arrow-up"></i></div>
                                <ul class="v-select-list">
                                    <li class="planeType" data-type="1">大型机</li>
                                    <li class="planeType" data-type="2">中型机</li>
                                    <li class="planeType" data-type="3">小型机</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!---->
                <div class="filter">
                    <div class="fliter-wrapper">
                        <div class="filter-desc" style="display: none" id="theSelected">已选：</div>
                        <div class="filter-desc margin-left14" style="display: none" id="selectedTime"><div class="close" display: none; id="timeName" data-code=""><span><i></i></span></div></div>
                        <div class="filter-desc margin-left14" style="display: none" id="selectedDepPort"><div class="close" style="display: none;" id="depportName" data-code=""><span><i></i></span></div></div>
                        <div class="filter-desc margin-left14" style="display: none" id="selectedArrPort"><div class="close" display: none; id="arrportName" data-code=""><span><i></i></span></div></div>
                        <div class="filter-desc margin-left14" style="display: none" id="selectedAirline"><div class="close" display: none; id="airlineName" data-code=""><span><i></i></span></div></div><!---->
                        <div class="filter-desc margin-left14" style="display: none" id="selectedPlaneType"><div class="close" display: none; id="planeName" data-code=""><span><i></i></span></div></div><!---->
                        <div class="filter-desc width100 margin-left8" style="display: none" id="clearAll"><button>全部清空</button></div></div></div>
                <!---->
                <script>
                    //点击下拉框时，将对应的div显示，并设置data-xx值。

                    //点击xx时，将div隐藏
                    //点击全部清空时将div全部隐藏。
                </script>

                <div class="fltlist-airline" id="airinfo">
                    <div class="fltlist-airline-title">
                        <div class="fltlist-airline-col padding-left53">航空信息</div>
                        <div class="fltlist-airline-col padding-left112"><span
                                class="fltlist-airline-col-wrapper">起飞时间<i class="fltlist-airline-up"></i><i
                                class="fltlist-airline-down"></i></span></div>
                        <div class="fltlist-airline-col padding-left106"><span
                                class="fltlist-airline-col-wrapper">到达时间<i class="fltlist-airline-up"></i><i
                                class="fltlist-airline-down"></i></span></div>
                        <div class="fltlist-airline-col padding-left75"><span class="fltlist-airline-col-wrapper">价格<i
                                class="fltlist-airline-up active"></i><i class="fltlist-airline-down"></i></span></div>
                    </div><!---->
                    <div render-loop="list">
                        <div class="fltlist-list-wrapper">
                        <div class="fltlist-list-cache-item"><!----></div>
                        <div class="fltlist-list-item">
                            <div class="fltlist-list-roundway-line" style="display: none;">
                                <button>收起<i class="fltlist-airline-up"></i></button>
                            </div>
                            <div class="v-list-item-content">
                                <div class="v-list-item-icon"><#--<i class="fltlist-logo fltlist-logo-9C"></i><span
                                        class="v-list-item-tag"></span>--></div>
                                <div class="v-list-item-name">
                                    <div class="v-list-item-desc"><span render-html="list.airline.name">春秋航空</span><span render-html="list.flightNo.no">9C8541</span></div>
                                    <div class="v-list-item-info"><span render-html="list.plane.name">空客A320</span><!----></div>
                                </div>
                                <div class="v-list-item-time">
                                    <div class="v-list-item-time-time" render-key="list.depTime" render-fun="beginFly">23:35</div>
                                    <div class="v-list-item-info" render-html="list.depPort.name">浦东机场T2</div>
                                </div>
                                <div class="v-list-item-line">
                                    <div class="line-wrapper">
                                        <div class="line-time">2小时45分钟</div>
                                        <div class="line"></div><!----></div>
                                </div>
                                <div class="v-list-item-time v-list-item-time-to">
                                    <div class="v-list-item-time-time" render-key="list.arrTime" render-fun="endFly">02:20</div>
                                    <div class="v-list-item-info" render-html="list.arrPort.name">白云机场T1</div>
                                   <#-- <span class="plus-day">+1</span>--></div>
                                <div class="v-list-item-price">
                                    <div class="v-list-item-price-desc">
                                        <div><span>￥</span><div render-html="list.price" style="display: inline-block">672</div><span class="gray">起</span></div><!----></div>
                                </div>
                                <div class="v-list-item-ticket" >
                                    <button class="v-list-item-button ticket" >订票</button>
                                </div>
                                <div class="v-list-item-ticket" style="display: none;">
                                    <button class="v-list-item-fold">收起<i class="fltlist-airline-up"></i></button>
                                </div>
                            </div>
                        </div><!----><!----><!----></div>
                    </div>
                   <#-- <div class="fltlist-list-wrapper">
                        <div class="fltlist-list-cache-item"><!--&ndash;&gt;</div>
                        <div class="fltlist-list-item">
                            <div class="fltlist-list-roundway-line" style="display: none;">
                                <button>收起<i class="fltlist-airline-up"></i></button>
                            </div>
                            <div class="v-list-item-content">
                                <div class="v-list-item-icon"><i class="fltlist-logo fltlist-logo-HO"></i><span
                                        class="v-list-item-tag"></span></div>
                                <div class="v-list-item-name">
                                    <div class="v-list-item-desc">吉祥航空<span>HO1287</span></div>
                                    <div class="v-list-item-info"><span>波音787-9(大)</span><!--&ndash;&gt;</div>
                                </div>
                                <div class="v-list-item-time">
                                    <div class="v-list-item-time-time">21:35</div>
                                    <div class="v-list-item-info">虹桥机场T2</div>
                                </div>
                                <div class="v-list-item-line">
                                    <div class="line-wrapper">
                                        <div class="line-time">2小时15分钟</div>
                                        <div class="line"></div><!--&ndash;&gt;</div>
                                </div>
                                <div class="v-list-item-time v-list-item-time-to">
                                    <div class="v-list-item-time-time">23:50</div>
                                    <div class="v-list-item-info">白云机场T1</div><!--&ndash;&gt;</div>
                                <div class="v-list-item-price">
                                    <div class="v-list-item-price-desc">
                                        <div><span>￥</span>807<span class="gray">起</span></div><!--&ndash;&gt;</div>
                                </div>
                                <div class="v-list-item-ticket">
                                    <button class="v-list-item-button">订票</button>
                                </div>
                                <div class="v-list-item-ticket" style="display: none;">
                                    <button class="v-list-item-fold">收起<i class="fltlist-airline-up"></i></button>
                                </div>
                            </div>
                        </div><!--&ndash;&gt;<!--&ndash;&gt;<!--&ndash;&gt;</div>-->


                   </div>
                <div class="fltlist-pagination">
                    <div class="fltlist-pagination-num">共6页 / 106条</div>
                    <div class="fltlist-pagination-gray fltlist-pagination-btn">1</div>
                    <div class="fltlist-pagination-gray">2</div>
                    <div class="fltlist-pagination-gray">3</div><!----><!----><!---->
                    <div class="fltlist-pagination-btn">下一页 &gt;&gt;</div>
                </div>
            </div>
            <div class="fltlist-sidebar">
                <div class="fltlist-intro">
                    <ul class="fltlist-intro-info clearfix">
                        <li class="fltlist-intro-detail">
                            <div class="fltlist-intro-icon"><i></i></div>
                            <div class="fltlist-intro-desc">航协认证</div>
                        </li>
                        <li class="fltlist-intro-detail">
                            <div class="fltlist-intro-icon"><i class="flightpc-sure"></i></div>
                            <div class="fltlist-intro-desc">出行保证</div>
                        </li>
                        <li class="fltlist-intro-detail">
                            <div class="fltlist-intro-icon"><i class="flightpc-tel"></i></div>
                            <div class="fltlist-intro-desc">7X24</div>
                        </li>
                    </ul>
                    <div class="fltlist-intro-tel">免费客服电话：4006345678</div>
                </div>
                <div class="fltlist-history">
                    <div class="fltlist-history-title">历史查询</div>
                    <ul class="fltlist-history-list">
                        <li class="fltlist-history-item">
                            <div class="fltlist-history-city"><span>上海</span><span>⇀</span><span>广州</span>
                                <div class="fltlist-history-city-price">查看</div>
                            </div>
                            <div class="fltlist-history-date"><span>2019-08-25</span><span class="week">周日</span></div>
                            <!----><!----></li>
                        <li class="fltlist-history-item">
                            <div class="fltlist-history-city"><span>广州</span><span>⇀</span><span>上海</span>
                                <div class="fltlist-history-city-price"><span>￥</span>814</div>
                            </div>
                            <div class="fltlist-history-date"><span>2019-08-25</span><span class="week">周日</span></div>
                            <!----><!----></li>
                        <li class="fltlist-history-item">
                            <div class="fltlist-history-city"><span>广州</span><span>⇀</span><span>上海</span>
                                <div class="fltlist-history-city-price">查看</div>
                            </div>
                            <div class="fltlist-history-date"><span>2019-08-24</span><span class="week">周六</span></div>
                            <!----><!----></li>
                        <li class="fltlist-history-item">
                            <div class="fltlist-history-city"><span>绥芬河</span><span>⇀</span><span>上海</span>
                                <div class="fltlist-history-city-price">查看</div>
                            </div>
                            <div class="fltlist-history-date"><span>2019-08-24</span><span class="week">周六</span></div>
                            <!----><!----></li>
                    </ul>
                </div>
                <div class="fltlist-slider">
                    <div class="v-banner">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div><img
                                        src="http://b3-q.mafengwo.net/s14/M00/1B/6B/wKgE2l0IoqiADHpKAAItDErw7lk257.png?imageMogr2%2Fthumbnail%2F%21620x350r%2Fgravity%2FCenter%2Fcrop%2F%21620x350%2Fquality%2F100"
                                        width="100%" height="auto"></div>
                            </div>
                        </div><!----><!---->
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<link href="http://css.mafengwo.net/css/mfw-footer.css?1558532347" rel="stylesheet" type="text/css">

<div id="footer">
    <div class="ft-content" style="width: 1105px">
        <div class="ft-info clearfix">
            <dl class="ft-info-col ft-info-intro">
                <dt>马蜂窝旅游网</dt>
                <dd>中国年轻一代用得更多的旅游网站</dd>
                <dd>上亿旅行者共同打造的<strong>"旅行神器"</strong></dd>
                <dd><strong>60,000</strong> 多个全球旅游目的地</dd>
                <dd><strong>600,000</strong> 个细分目的地新玩法</dd>
                <dd><strong>760,000,000</strong> 次攻略下载</dd>
                <dd><strong>38,000</strong> 家旅游产品供应商</dd>
            </dl>
            <dl class="ft-info-col ft-info-about">
                <dt>关于我们</dt>
                <dd><a href="http://www.mafengwo.cn/s/about.html" rel="nofollow">关于马蜂窝</a><a
                        href="http://www.mafengwo.cn/s/contact.html" class="m_l_10" rel="nofollow">联系我们</a></dd>
                <dd><a href="http://www.mafengwo.cn/s/private.html" rel="nofollow">隐私政策</a><a
                        href="http://www.mafengwo.cn/s/logo.html" rel="nofollow" class="m_l_10">商标声明</a></dd>
                <dd><a href="http://www.mafengwo.cn/s/agreement.html" rel="nofollow">服务协议</a><a
                        href="http://www.mafengwo.cn/s/rules.html" rel="nofollow" class="m_l_10">游记协议</a></dd>
                <dd><a href="http://www.mafengwo.cn/s/salesagreement.html" rel="nofollow">商城平台服务协议</a></dd>
                <dd><a href="http://www.mafengwo.cn/s/property.html" rel="nofollow">网络信息侵权通知指引</a></dd>
                <dd style="white-space: nowrap;"><a href="http://www.mafengwo.cn/s/monitor.html" rel="nofollow">马蜂窝旅游网服务监督员</a>
                </dd>
                <dd><a href="http://www.mafengwo.cn/s/sitemap.html" target="_blank">网站地图</a><a
                        class="joinus highlight m_l_10" title="马蜂窝团队招聘" target="_blank"
                        href="https://app.mokahr.com/apply/mafengwo1/173#/page/社会招聘?_k=j4j3ux" rel="nofollow">加入马蜂窝</a>
                </dd>
            </dl>
            <dl class="ft-info-col ft-info-service">
                <dt>旅行服务</dt>
                <dd>
                    <ul class="clearfix">
                        <li><a target="_blank" href="http://www.mafengwo.cn/gonglve/">旅游攻略</a></li>
                        <li><a target="_blank" href="http://www.mafengwo.cn/hotel/">酒店预订</a></li>
                        <li><a target="_blank" href="http://www.mafengwo.cn/sales/">旅游特价</a></li>
                        <li><a target="_blank" href="http://zuche.mafengwo.cn/">国际租车</a></li>
                        <li><a target="_blank" href="http://www.mafengwo.cn/wenda/">旅游问答</a></li>
                        <li><a target="_blank" href="http://www.mafengwo.cn/insure/">旅游保险</a></li>
                        <li><a target="_blank" href="http://z.mafengwo.cn">旅游指南</a></li>
                        <li><a target="_blank" href="http://huoche.mafengwo.cn">订火车票</a></li>
                        <li><a target="_blank" href="http://www.mafengwo.cn/travel-news/">旅游资讯</a></li>
                        <li><a target="_blank" href="http://www.mafengwo.cn/app/intro/gonglve.php">APP下载</a></li>
                        <li style="width: 120px;"><a target="_blank" href="http://www.mafengwo.cn/sales/alliance.php"
                                                     class="highlight">旅行商城全球商家入驻</a></li>
                    </ul>
                </dd>
            </dl>
            <dl class="ft-info-col ft-info-qrcode">
                <dd>
                    <span class="ft-qrcode-tejia"></span>
                    <p>马蜂窝良品<br>官方服务号</p>
                </dd>
                <dd>
                    <span class="ft-qrcode-weixin"></span>
                    <p>马蜂窝旅游<br>订阅号</p>
                </dd>
                <dd>
                    <span class="ft-qrcode-weixin"
                          style="background-image: url('https://p3-q.mafengwo.net/s10/M00/48/A9/wKgBZ1t_4sSAVJ6uAAAlzJ0PZgU881.png?imageMogr2%2Fthumbnail%2F%2194x90r%2Fgravity%2FCenter%2Fcrop%2F%2194x90%2Fquality%2F90')"></span>
                    <p>马蜂窝APP<br>扫描立即下载</p>
                </dd>
            </dl>
            <dl class="ft-info-social">
                <dt>旅游之前，先上马蜂窝！</dt>
                <dd>
                    <a class="ft-social-weibo" target="_blank" href="http://weibo.com/mafengwovip" rel="nofollow"><i
                            class="ft-social-icon"></i></a>
                    <a class="ft-social-qqt" target="_blank" href="http://t.qq.com/mafengwovip" rel="nofollow"><i
                            class="ft-social-icon"></i></a>
                    <a class="ft-social-qzone" target="_blank" href="http://1213600479.qzone.qq.com/" rel="nofollow"><i
                            class="ft-social-icon"></i></a>
                </dd>
            </dl>
        </div>

        <div class="ft-copyright">
            <a href="http://www.mafengwo.cn"><i class="ft-mfw-logo"></i></a>
            <p>© 2019 Mafengwo.cn
                <a href="http://www.miibeian.gov.cn/" target="_blank" rel="nofollow">京ICP备11015476号</a>
                <a href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=11010502013401"
                   target="_blank"><img src="http://images.mafengwo.net/images/footer/police_record.png" width="12"
                                        style="margin:0 2px 4px 0;">京公网安备11010502013401号</a>
                <a href="https://n2-q.mafengwo.net/s14/M00/CB/D1/wKgE2l1eMEaAY8n4AA4wSFefaLc94.jpeg" target="_blank"
                   rel="nofollow">京ICP证110318号</a>
                <span class="m_l_10">违法和不良信息举报电话: 010-83416877 举报邮箱: mfwjubao@mafengwo.com</span>
            </p>
            <p>
                <a href="https://n2-q.mafengwo.net/s14/M00/CB/4C/wKgE2l1eMB-AFs0sAB7jJAiDaNA17.jpeg" target="_blank"
                   rel="nofollow" class="m_l_10">网络出版服务许可证</a>
                <a href="https://b4-q.mafengwo.net/s14/M00/CB/FA/wKgE2l1eMFGAQI4LABmNJYlBasQ00.jpeg" target="_blank"
                   rel="nofollow" class="m_l_10">增值电信业务经营许可证</a>
                <a href="https://p1-q.mafengwo.net/s14/M00/B0/74/wKgE2l1CgbCAXc97ACZLiibfY8M62.jpeg" target="_blank"
                   rel="nofollow" class="m_l_10">营业执照</a>
                <a href="https://p1-q.mafengwo.net/s14/M00/CC/28/wKgE2l1eMF2AcAOpABhXGZtsUJ820.jpeg" target="_blank"
                   rel="nofollow" class="m_l_10">广播电视节目制作经营许可证</a>
                <a href="/sales/uhelp/doc" target="_blank" rel="nofollow" class="m_l_10">帮助中心</a>
                <span class="m_l_10">马蜂窝客服：国内</span><span class="highlight">4006-345-678</span>
                <span class="m_l_10">海外</span> <span class="highlight">+86-10-8341-6888</span>
            </p>
        </div>
        <div class="ft-safety">
            <a class="s-a" target="_blank" href="https://search.szfw.org/cert/l/CX20140627008255008321"
               id="___szfw_logo___"></a>
            <a class="s-b"
               href="https://ss.knet.cn/verifyseal.dll?sn=e130816110100420286o93000000&amp;ct=df&amp;a=1&amp;pa=787189"
               target="_blank" rel="nofollow"></a>
            <a class="s-c" href="http://www.itrust.org.cn/Home/Index/itrust_certifi/wm/1669928206.html" target="_blank"
               rel="nofollow"></a>
            <a class="s-d" href="http://www.itrust.org.cn/Home/Index/satification_certificate/wm/MY2019051501.html"
               target="_blank" rel="nofollow"></a>
        </div>

    </div>
</div>
<style>
    #banner-con-gloable {
        display: block;
        position: fixed;
        bottom: 0;
        left: -100%;
        z-index: 110;
        width: 100%;
        height: 179px;
        overflow-x: hidden;
    }

    #banner-con-gloable .banner-btn-con {
        width: 100%;
        height: 162px;
        background: rgba(30, 15, 8, 0.95);
        position: absolute;
        bottom: 0;
    }

    #banner-con-gloable .banner-btn-con .close-btn {
        position: absolute;
        right: 35px;
        top: 24px;
        z-index: 120;
        height: 24px;
        width: 24px;
        cursor: pointer;
    }

    #banner-con-gloable .banner-image-con {
        position: absolute;
        right: 50%;
        bottom: 0;
        width: 1000px;
        margin-right: -500px;
    }

    #float-pannel-gloable {
        padding-left: 28px;
        padding-bottom: 20px;
        display: block;
        position: fixed;
        bottom: 0;
        z-index: 110;
        left: -230px;
    }

    #float-pannel-gloable .float-btn {
        width: 24px;
        height: 24px;
        position: absolute;
        right: 0;
        top: 0;
        z-index: 100;
    }

    #closed {
        height: 24px;
        width: 24px;
        vertical-algin: top;
        border: none;
        cursor: pointer;
    }
</style>

<div id="float-pannel-gloable">
    <img class="float-image" src="https://p4-q.mafengwo.net/s14/M00/BB/8C/wKgE2l0r2W-ALHaeAACAt6lqXyA464.png"
         style="width:178px;">
    <div class="float-btn">
        <img id="closed" src="https://n4-q.mafengwo.net/s13/M00/46/AC/wKgEaVy2xHeAZJhRAAADGY-wozY871.png"></div>
</div>





<link href="http://css.mafengwo.net/css/mfw-toolbar.css?1537192876" rel="stylesheet" type="text/css">

<div class="mfw-toolbar" id="_j_mfwtoolbar" style="display: block;">
    <div class="toolbar-item-top" style="display: none;">
        <a role="button" class="btn _j_gotop">
            <i class="icon_top"></i>
            <em>返回顶部</em>
        </a>
    </div>
    <div class="toolbar-item-feedback">
        <a role="button" data-japp="feedback" class="btn">
            <i class="icon_feedback"></i>
            <em>意见反馈</em>
        </a>
    </div>
    <div class="toolbar-item-code">
        <a role="button" class="btn">
            <i class="icon_code"></i>
        </a>
        <a role="button" class="mfw-code _j_code">


            <img src="https://p1-q.mafengwo.net/s1/M00/6C/51/wKgIC1t_6TuASybrAADGUPUHjr021.jpeg?imageMogr2%2Fthumbnail%2F%21450x192r%2Fgravity%2FCenter%2Fcrop%2F%21450x192%2Fquality%2F90"
                 width="450" height="192">
        </a>
        <!--<div class="wx-official-pop"><img src="http://images.mafengwo.net/images/qrcode-weixin.gif"><i class="_j_closeqrcode"></i></div>-->
    </div>

</div>




</body>

</html>