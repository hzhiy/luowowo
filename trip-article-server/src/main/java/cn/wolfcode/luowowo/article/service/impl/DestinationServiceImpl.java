package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.domain.Region;
import cn.wolfcode.luowowo.article.mapper.DestinationMapper;
import cn.wolfcode.luowowo.article.mapper.RegionMapper;
import cn.wolfcode.luowowo.article.query.DestinationQuery;
import cn.wolfcode.luowowo.article.query.DestinationThemeQuery;
import cn.wolfcode.luowowo.article.service.IDestinationService;
import cn.wolfcode.luowowo.hotel.domain.Hotel;
import cn.wolfcode.luowowo.hotel.service.IHotelPriceService;
import cn.wolfcode.luowowo.hotel.service.IHotelService;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by zhi on 2019/8/10.
 */
@Service
public class DestinationServiceImpl implements IDestinationService {

    @Autowired
    private RegionMapper regionMapper;
    @Autowired
    private DestinationMapper destinationMapper;
    @Reference
    private IHotelPriceService hotelPriceService;
    @Reference
    private IHotelService hotelService;

    @Override
    public List<Destination> getDestByDeep(int deep) {
        return destinationMapper.selectDestByDeep(deep);
    }

    @Override
    public List<Destination> getDestByRegionId(Long rid) {
        Region region = regionMapper.selectByPrimaryKey(rid);
        return destinationMapper.selectDestByRegionId(region.getRefIds());
    }

    @Override
    public PageInfo query(DestinationQuery qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List<Destination> list = destinationMapper.queryForList(qo);
        return new PageInfo<>(list);

    }

    @Override
    public void changeHotValue(Long id, Integer hot) {
        destinationMapper.updateHotById(id, hot);
    }

    @Override
    public void setInfo(Long id, String info) {
        destinationMapper.updateInfoById(id, info);
    }

    @Override
    public void delete(Long id) {
        destinationMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<Destination> getToasts(Long parentId) {
        List<Destination> list = new ArrayList<>();
        toasts(parentId, list);
        // 反转
        Collections.reverse(list);
        return list;
    }

    @Override
    public List<Destination> listByParentId(Long regionId) {
        return destinationMapper.selectByParentId(regionId);
    }

    @Override
    public Destination getDestById(Long id) {
        return destinationMapper.selectByPrimaryKey(id);
    }

    @Override
    public Destination getCountry(Long id) {
        List<Destination> toasts = this.getToasts(id);
        if (toasts.size() > 0) {
            return toasts.get(0);
        }
        return null;
    }

    @Override
    public Destination getProvince(Long id) {
        List<Destination> toasts = this.getToasts(id);
        if (toasts.size() > 1) {
            if (toasts.get(0).getId() == 1){    // 国内
                return toasts.get(1);
            }

        }
        return null;
    }

    @Override
    public List<Destination> list() {
        return destinationMapper.selectAll();
    }

    private void toasts(Long id, List<Destination> list) {
        if (id == null) {
            return;
        }
        Destination destination = destinationMapper.selectByPrimaryKey(id);
        if (destination != null) {
            list.add(destination);
        }
        if (destination != null && destination.getParent() != null) {
            toasts(destination.getParent().getId(), list);
        }

    }

    @Override
    public PageInfo<Destination> queryForList(DestinationThemeQuery qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<Long> ids = destinationMapper.selectThemeDest(qo);
        List<Destination> dests = new ArrayList<>();
        for (Long id : ids) {
            Destination destination = destinationMapper.selectByPrimaryKey(id);
            dests.add(destination);
        }
        return new PageInfo<Destination>(dests);
    }

    public List<Destination> queryHotelPriceCityTop6() {
        return destinationMapper.selectHotelByPriceTop6();
    }

    @Override
    public List<Destination> queryChinaCity() {

        return destinationMapper.selectChinaCity();
    }

    @Override
    public List<Destination> queryOverseas() {
        return destinationMapper.selectOverseas();
    }

    @Override
    public List<Destination> queryDestByParentName(String parentName) {
        return destinationMapper.selectDestByParentName(parentName);
    }

    @Override
    public Destination getDestByName(String name) {
        return destinationMapper.selectByName(name);
    }

    @Override
    public List<Destination> getCountryNotId(Long id) {
        return destinationMapper.selectCountryNotId(id);
    }
}
