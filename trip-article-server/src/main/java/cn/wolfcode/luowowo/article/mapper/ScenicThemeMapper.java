package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.ScenicTheme;
import java.util.List;

public interface ScenicThemeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ScenicTheme record);

    ScenicTheme selectByPrimaryKey(Long id);

    List<ScenicTheme> selectAll();

    int updateByPrimaryKey(ScenicTheme record);

    List<ScenicTheme> selectByDestId(Long destId);
}