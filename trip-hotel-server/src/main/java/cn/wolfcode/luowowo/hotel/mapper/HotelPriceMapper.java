package cn.wolfcode.luowowo.hotel.mapper;

import cn.wolfcode.luowowo.hotel.domain.HotelPrice;
import cn.wolfcode.luowowo.hotel.vo.HotelAvgPriceVO;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface HotelPriceMapper {
    int deleteByPrimaryKey(Long id);

    int insert(HotelPrice record);

    HotelPrice selectByPrimaryKey(Long id);

    List<HotelPrice> selectAll();

    int updateByPrimaryKey(HotelPrice record);

    List<HotelPrice> selectByHotelId(Long id);

    List<HotelAvgPriceVO> selectStarAvg(Long cityId);
}