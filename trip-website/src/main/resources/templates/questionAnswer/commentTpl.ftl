
<#list answers as answer>
<div>
    <li>
        <div class="operate">
            <div class="zan" data-aid="${answer.id}"><i></i><span class="ding">${answer.thumbsupnum}</span></div>
        </div>
        <#--<div class="answer-side _js_answerAva">
            <!-- <a class="btn-ding _js_zan "><i></i><span data-real_num="3">3</span></a> &ndash;&gt;
        </div>-->
        <div class="answer-content _js_answer_content">
            <div class="answer-info clearfix">
                <div class="user-bar fl">
                    <a class="_j_filter_click avatar" href="javascript:;" target="_blank"><img
                            src="${answer.headImgUrl}"
                            width="48" height="48" class="photo"></a>
                    <a class="name" href="javascript:;" target="_blank">${answer.nickname}</a>
                    <a class="level" href="javascript:;" target="_blank" rel="nofollow">LV.${answer.level}</a>
                    <a class="identity i-guide" href="javascript:;" target="_blank">指路人</a>
                </div>
                <ul class="answer-medal fr">
                    <li class="gold">
                        <#if answer.isgold>
                        <div class="btn"><i></i><a href="javascript:;" target="_blank">金牌回答</a></div>
                        </#if>
                    </li>
                </ul>
            </div>
            <!-- 回答内容 -->
            <div class="_j_short_answer_item hide" style="display: block;">
                ${answer.content}
                    <div style="float: right">
                        <br>
                        <span class="date">发布于${answer.createTime?string("yyyy-MM-dd HH:mm:ss")}</span>
                    </div>
            </div>
        </div>
    </li>
</div>
</#list>
<script>
    //顶
    $(function () {
        var $this;
        //顶：点赞
        $(".zan").click(function () {

            $this = $(this);
            var aid = $(this).data("aid");
            $.get("/questionAnswer/answerThumbsup", {aid: aid}, function (data) {
                if (data.success) {
                    $this.children(".ding").html(data.data.thumbsupnum);
                    popup("顶成功啦"); //

                } else {
                    if (data.code == 102) {
                        popup(data.msg);
                    } else {
                        popup("今天你已经顶过了"); //
                    }

                }
            });
        })
    })
</script>