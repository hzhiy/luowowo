package cn.wolfcode.luowowo.airticket.query;

import cn.wolfcode.luowowo.airticket.vo.TicketCondition;
import cn.wolfcode.luowowo.common.query.QueryObject;
import cn.wolfcode.luowowo.common.util.DateUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by Administrator on 2019/8/24.
 */
@Setter
@Getter
public class FlightQuery extends QueryObject {

    private String orgCityName;

    private String dstCityName;

    private String orgCity;//出发城市

    private String dstCity;//目的城市

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date depTime;

    //具体时间
    private Integer timeType=-1;

    //出发的机场
    private Long  depPortId=-1L;
    //降落的机场
    private Long  arrPortId=-1L;
    //航空公司
    private Long airlineId = -1L;

    //排序 起飞时间排序，降落时间排序，价格排序
    private Integer orderType=-1;
   /* private int arrTimeSortType;
    private int priceSortTimeType;*/

   //飞机大中小
    private Integer planeType=-1;


    public TicketCondition getTicketCondition(){
        TicketCondition ticketCondition = null;
        if (timeType!=null && timeType!=-1){
            ticketCondition = TicketCondition.TIME_CONDITON.get(timeType);
            ticketCondition.setBeginDate(DateUtil.getDate(depTime,ticketCondition.getHour(),ticketCondition.getMinute(),ticketCondition.getSecond()));
            ticketCondition.setEndDate(DateUtil.getDate(depTime,ticketCondition.getEhour(),ticketCondition.getEminute(),ticketCondition.getEsecond()));
        }
        return ticketCondition;
    }

    public String getOrderBy(){
        String orderBy = null;
        switch (orderType){
            case 1:
                orderBy = "depTime asc";
                break;
            case 2:
                orderBy = "depTime desc";
                break;
            case 3:
                orderBy = "arrTime asc";
                break;
            case 4:
                orderBy = "arrTime desc";
                break;
            case 5:
                orderBy = "price asc";
                break;

            case 6:
                orderBy = "price deesc";
                break;
        }
        return orderBy;
    }





}
