package cn.wolfcode.luowowo.search.template;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * 酒店评论
 */
@Setter
@Getter
@Document(indexName="luowowo_hotel_comment",type="comment")
public class HotelCommentTemplate implements Serializable {

    @Id
    @Field(index=true,store=true,type = FieldType.Keyword)
    private String id;                                                  // id
    @Field(store=true, index = true,type = FieldType.Long)
    private Long hotelId;                                               // 酒店id
    @Field(store=true, index = true,type = FieldType.Integer)
    private Integer ensemble;                                           // 总体评价
    @Field(index=true,store=true,type = FieldType.Keyword)
    private String imgstr;                                              // 图片总和字符串
    @Field(store=true, index = false,type = FieldType.Long)
    private Long userId;                                                // 用户id
    @Field(index=true,store=true,type = FieldType.Keyword)
    private String username;                                            // 用户名
    @Field(store=true, index = true,type = FieldType.Integer)
    private int level;                                                  // 用户等级
    @Field(index=true,store=true,type = FieldType.Keyword)
    private String headUrl;                                             // 用户头像
    @Field(index=true,store=true,type = FieldType.Date)
    private Date createTime;                                            // 创建时间
    @Field(index=true,analyzer="ik_max_word",store=true,searchAnalyzer="ik_max_word",type = FieldType.Text, fielddata = true)
    private String content;                                             // 评论内容

    public List<String> getImgs() {
        if (StringUtils.hasLength(imgstr)){
            List<String> list = new ArrayList<>();
            for (String s : imgstr.split(",")) {
                list.add(s);
            }
            return list;
        }
        return Collections.EMPTY_LIST;
    }
}