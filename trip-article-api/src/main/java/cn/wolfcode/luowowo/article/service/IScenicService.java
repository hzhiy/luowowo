package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.Scenic;
import cn.wolfcode.luowowo.article.domain.ScenicTheme;
import cn.wolfcode.luowowo.article.query.ScenicQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 景点
 */
public interface IScenicService {

    /**
     * 获取单个
     * @param scenicId
     * @return 景点对象
     */
    Scenic getById(Long scenicId);

    /**
     * 获取五个最热门景点
     * @param qo 根据地区
     * @return 景点列表
     */
    List<Scenic> queryTop5ByDestId(ScenicQuery qo);

    /**
     *
     * @param destId
     * @return 景点对象
     */
    Scenic getByDestId(Long destId);

    /**
     * 查询最热门景点
     * @return
     */
    List<Scenic> queryHotScenic();

    /**
     * 查询景点主题
     * @param destId 地区id
     * @return
     */
    List<ScenicTheme> queryThemebyDestId(Long destId);

    PageInfo<Scenic> query(ScenicQuery qo);

    /**
     * 查询内部景点
     * @param scenicId 根据景点id获取父id查
     * @return 景点列表
     */
    List<Scenic> queryInScenicByScenicId(Long scenicId);
}
