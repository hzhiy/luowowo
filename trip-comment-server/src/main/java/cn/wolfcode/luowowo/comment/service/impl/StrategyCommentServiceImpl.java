package cn.wolfcode.luowowo.comment.service.impl;

import cn.wolfcode.luowowo.article.domain.StrategyCommend;
import cn.wolfcode.luowowo.article.query.StrategyDetailQuery;
import cn.wolfcode.luowowo.comment.domain.StrategyComment;
import cn.wolfcode.luowowo.comment.query.StrategyCommentQuery;
import cn.wolfcode.luowowo.comment.repository.IStrategyCommentRepository;
import cn.wolfcode.luowowo.comment.service.IStrategyCommentService;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by zhi on 2019/8/13.
 */
@Service
public class StrategyCommentServiceImpl implements IStrategyCommentService {

    @Autowired
    private IStrategyCommentRepository strategyCommentRepository;
    @Autowired
    private MongoTemplate template;

    public void saveOrUpdate(StrategyComment comment) {
        if (!StringUtils.hasLength(comment.getId())) {
            comment.setCreateTime(new Date());
            strategyCommentRepository.save(comment);
        }

    }

    @Override
    public Page list(StrategyDetailQuery qo) {
        // 查询条件
        Query query = new Query();
        query.addCriteria(Criteria.where("detailId").is(qo.getDetailId()));
        // 查总数
        long count = template.count(query, StrategyComment.class);
        if (count == 0) {
            return Page.empty();
        }
        // 分页:PageRequest.of(当前页0开始, 每页显示条数, 排序规则)
        Pageable request = PageRequest.of(qo.getCurrentPage() - 1, qo.getPageSize(), Sort.by(Sort.Direction.DESC, "createTime"));
        query.with(request);
        // 查某一页
        List<StrategyComment> list = template.find(query, StrategyComment.class);
        return new PageImpl(list, request, count);

    }

    @Override
    public void commentThumbUp(String mongoId, Long userId) {
        // 查询评论的数据
        Optional<StrategyComment> optional = strategyCommentRepository.findById(mongoId);
        StrategyComment comment = optional.get();
        // 得到评论的点赞用户列表
        if (comment != null) {
            List<Long> list = comment.getThumbuplist();
            // 判断当前用户是否在列表中
            if (list.contains(userId)) {
                // 存在就是取消点赞
                comment.setThumbupnum(comment.getThumbupnum() - 1);
                list.remove(userId);
            } else {
                // 不存在就是点赞
                comment.setThumbupnum(comment.getThumbupnum() + 1);
                list.add(userId);
            }
            strategyCommentRepository.save(comment);
        }
    }

    @Override
    public Page queryForList(StrategyCommentQuery qo) {
        Query query = new Query();
        if (qo.getState() != -1){
            query.addCriteria(Criteria.where("state").is(qo.getState()));
        }
        if (qo.getDetailId() != -1L){
            query.addCriteria(Criteria.where("detailId").is(qo.getDetailId()));
        }
        long count = template.count(query, StrategyComment.class);
        if (count == 0){
            return Page.empty();
        }
        PageRequest request = PageRequest.of(qo.getCurrentPage() - 1, qo.getPageSize(), Sort.by(Sort.Direction.DESC, qo.getOrder()));
        query.with(request);
        List<StrategyComment> list = template.find(query, StrategyComment.class);

        return new PageImpl(list, request, count);
    }

    @Override
    public void updateState(String id, int state) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        Update update = Update.update("state", state);
        template.updateFirst(query, update, StrategyComment.class);
    }

}
