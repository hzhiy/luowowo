package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TicketDetail extends BaseDomain{
    private Long ticketId;//门票id
    private String price;//价钱
    private String address;//地址
    private String businessHours;//营业时间
    private TicketContent ticketContent;//内容
}
