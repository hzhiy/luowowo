package cn.wolfcode.luowowo.cache.service;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.vo.ThemeDestVO;

import java.util.List;

/**
 * 地区缓存
 */
public interface IDestRedisService {

    /**
     * 获得对象
     * @param id
     * @return
     */
    ThemeDestVO getThemeDestVO(Long id);

    /**
     * 设置酒店地区缓存
     * @param vo
     */
    void setThemeDestVO(ThemeDestVO vo);

    //收藏
    boolean favor(Long did, Long id);

    /**
     * 获取用户收藏的地点
     * @param id
     * @return
     */
    List<Destination> queryByUserIdFavor(Long id);
}
