package cn.wolfcode.luowowo.airticket.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
public class Flight extends BaseDomain{
    private FlightNo flightNo;

    private AirPort depPort;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date depTime;

    private AirPort arrPort;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date arrTime;

    private Integer price;

    private Airline airline;

    private Plane plane;

    private City orgCity;

    private City dstCity;


}