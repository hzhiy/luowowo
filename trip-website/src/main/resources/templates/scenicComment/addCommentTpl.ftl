<div class="mfw-cmt rev-list">
    <ul>
        <li class="rev-item comment-item clearfix">

            <script>
                $(function () {
                    //回复评论
                    $("#_j_reply_list${(sct.id)!}").on("click", ".replyBtn", function () {
                        var username = $(this).data("username");
                        var toid = $(this).data("toid");
                        var formid = $(this).data("formid");
                        var headurl = $(this).data("headurl");

                        $("#toid").val(toid);
                        $("#formid").val(formid);
                        $("#headurl").val(headurl);
                        $("#username").val(username);

                        $("#reply${(sct.id)!}").attr("placeholder", "回复：" + username);

                    });


                    //回复用户
                    $("#_j_reply_item${(sct.id)!}").on("click", ".replyBtnTypeOne", function () {
                        var username = $(this).data("username");
                        var toid = $(this).data("toid");
                        var formid = $(this).data("formid");
                        var headurl = $(this).data("headurl");

                        $("#toid").val(toid);
                        $("#formid").val(formid);
                        $("#headurl").val(headurl);
                        $("#username").val(username);
                        $("#type").val(1);

                        $("#reply${(sct.id)!}").attr("placeholder", "回复：" + username);

                    });
                    //发表回复
                    $(".btn_submit_reply${(sct.id)!}").click(function () {
                        if (!$("#reply${(sct.id)!}").val()) {
                            alert("评论不能为空");
                            return;
                        }
                        $("#replyForm${(sct.id)!}").ajaxSubmit(function (data) {
                            $("#reply${(sct.id)!}").val("");
                            $("#reply${(sct.id)!}").attr("placeholder", "添加回复");
                            $("#comment_list${sct.id}").append(data);
                            $("#type").val("0");
                        })
                    });

                    //点击评论
                    $("#pinglun${sct.id!}").click(function () {
                        $("#addcomment${(sct.id)!}").toggle();
                    });

                    //点赞
                    $(".useful").click(function () {
                        var commentId = $(this).data("id"); // 文章id
                        var t = $(this);

                        $.get("/scenic/scenicThumbUp", {toid: commentId}, function (data) {
                            if (data.success) {
                                t.children('.useful-num').html(data.data.thumbupnum);
                                popup("点赞成功啦");
                            } else {
                                popup("取消点赞");
                                var number = (data.data.thumbupnum);
                                t.children('.useful-num').html(number)
                            }
                        });
                    });

                })
            </script>

            <!-- 点评 -->
            <div class="mfw-cmt _j_comment_item" id="_j_reply_list${(sct.id)!}">
                <div class="user"><a class="avatar" href="#" target="_blank"><img
                        src="${sct.headUrl!}"
                        width="48" height="48"></a><span
                        class="level">LV.${sct.level!}</span>
                </div>
                <a class="useful" data-id="${sct.id!}"
                   title="点赞">
                    <i></i><span id="uNum" class="useful-num">${sct.thumbupnum!}</span>
                </a>
                <a class="name" href="#" target="_blank">${sct.username!}</a>
                <span class="s-star s-star${sct.starNum!}"></span>
                <p class="rev-txt">${sct.content!}</p>


                <div class="rev-img">
                                                <#list sct.imgArr! as img>
                                                    <a href="#" target="_blank"><img src="${img}" width="200" height="120"></a>
                                                </#list>
                </div>

                <div class="info clearfix">
                    <a role="button" class="_j_reply re_reply replyBtn"
                       data-toid="${(sct.id)!}"
                       data-formid="${(sct.userId)!}"
                       data-headurl="${(sct.headUrl)!}"
                       data-username="${(sct.username)!}" title="添加回复">回复
                    </a>


                    <a class="btn-comment _j_comment commentReplyBtn" title="评论"
                       data-toid="${sct.id!}" id="pinglun${sct.id!}">评论
                    </a>
                    <span class="time">${(sct.createTime?string("yyyy-MM-dd"))!}</span>
                </div>
            </div>


            <!-- 回复模板 -->
            <div class="mfw-cmt _j_reply_item" id="_j_reply_item${(sct.id)!}">
                <div class="comment add-reply ">

                    <ul class="more_reply_box comment_list" id="comment_list${sct.id}">

                                                    <#if sct.usersReply??>

                                                        <#list sct.usersReply as ur>

                                                            <#if ur.type = 0>
                                                            <li>
                                                                <a href="/u/63107989.html" target="_blank">
                                                                    <img src="http:${ur.headUrl}"
                                                                         width="16" height="16">🍓${ur.refComment.username}🍓
                                                                </a>
                                                                ：${ur.refComment.content}
                                                                <a role="button" class="_j_reply re_reply replyBtnTypeOne" data-toid="${ur.id}" data-formid="${ur.userId}" data-headurl="${ur.headUrl}"
                                                                   data-username="${ur.username}" title="添加回复">回复</a>
                                                                <br><span
                                                                    class="time">${(ur.refComment.createTime?string("yyyy-MM-dd"))!}
                                                            </span>
                                                            </li>
                                                            <#else >
                                                            <li>
                                                                <a href="/u/52068941.html" target="_blank">
                                                                    <img src="${ur.refComment.headUrl}"
                                                                         width="16" height="16">${ur.refComment.username}
                                                                </a>
                                                                回复${ur.username}：${ur.refComment.content}
                                                                <a role="button" class="_j_reply re_reply replyBtnTypeOne"
                                                                   data-toid="${ur.id}"
                                                                   data-formid="${ur.userId}"
                                                                   data-headurl="${ur.headUrl}"
                                                                   data-username="${ur.username}" title="添加回复">回复</a>
                                                                <br><span
                                                                    class="time">${(ur.refComment.createTime?string("yyyy-MM-dd"))!}
                                                            </span>
                                                            </li>
                                                            </#if>

                                                        <div class="mcmt-quote">
                                                        </div>
                                                        <div class="mcmt-word">

                                                        </div>
                                                        </#list>
                                                    </#if>
                    </ul>


                    <div id="addcomment${(sct.id)!}" class="add-comment hide reply-form">
                        <form action="/scenic/usersReply" method="post" id="replyForm${(sct.id)!}">

                            <input type="hidden" name="toid"  id="toid">
                            <input type="hidden" name="fromId"  id="formid">
                            <input type="hidden" name="headUrl"  id="headurl">
                            <input type="hidden" name="fromName"  id="username">
                            <input type="hidden" name="type" value="0" id="type">

                            <textarea class="comment_reply" style="color: #000000;" id="reply${(sct.id)!}" name="content"></textarea>
                        </form>
                        <a class="btn btn_submit_reply${(sct.id)!}">发送</a>
                    </div>
                </div>
            </div>

        </li>
    </ul>
</div>



