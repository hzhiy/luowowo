package cn.wolfcode.luowowo.search.service.impl;

import cn.wolfcode.luowowo.search.query.SearchQueryObject;
import cn.wolfcode.luowowo.search.service.ISearchService;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.NestedQueryBuilder;
import org.elasticsearch.index.search.MultiMatchQuery;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhi on 2019/8/18.
 */
public class QiaoYiXia{

    @Autowired
    private ElasticsearchTemplate template;

    public <T> Page<T> searchWithHighlight(String index, String type, Class<T> clz, SearchQueryObject qo, String... fields) {
        // 先封装左边的


        // 替换高亮显示的字段
        //需要进行高亮显示的字段对象, 他是一个数组, 个数由搜索的字段个数决定: fields 个数决定
        HighlightBuilder.Field[] fs = new HighlightBuilder.Field[fields.length];
        for (int i = 0; i < fs.length; i++) {
            fs[i] = new HighlightBuilder.Field(fields[i]).preTags("<span style='color:red;'>")  //替换的高亮字前
                                                        .postTags("</span>");   // 替换的高亮字后
        }

        // 匹配关键字的指定字段
        MultiMatchQueryBuilder multiMatchQueryBuilder = new MultiMatchQueryBuilder(qo.getKeyword(), fields);
        // 查询对象
        NativeSearchQueryBuilder query = new NativeSearchQueryBuilder();
        // 封装查询对象
        query.withIndices(index).withTypes(type);
        query.withQuery(multiMatchQueryBuilder);
        query.withPageable(qo.getPageableNoSort());
        query.withHighlightFields(fs);

        // 查询

        // 参数1:条件查询对象   参数2: page返回list的泛型   参数3: 查询结果映射
        return template.queryForPage(query.build(), clz, new SearchResultMapper() {
            // 参数1: 是查询结果(就是右边的整个结果对象)  参数2: list 的泛型  参数3: 分页对象
            public <T> AggregatedPage<T> mapResults(SearchResponse response, Class<T> clz, Pageable pageable) {
                // 此list是要显示的内容对象
                List<T> list = new ArrayList<>();
                SearchHits searchHits = response.getHits();
                SearchHit[] hits = searchHits.getHits();
                for (SearchHit hit : hits) {
                    // 返回查询到的每个对象 转换成此项目中的template对象
                    T t = JSON.parseObject(hit.getSourceAsString(), clz);
                    //必须使用拥有高亮显示的效果字段替换原先的数据
                    Map<String, HighlightField> highlightFields = hit.getHighlightFields();
                    Map<String, String> map = new HashMap<>();
                    for (String field : fields) {
                        // 获得到高亮的字段
                        HighlightField highlightField = highlightFields.get(field);
                        // 要把高亮的字段设置回我们的对象

                        if (highlightField!=null) {
                            // 这里获得高亮字段的内容 是数组 我们自己要拼接
                            Text[] fragments = highlightField.fragments();
                            StringBuilder sb = new StringBuilder();
                            for (Text fragment : fragments) {
                                sb.append(fragment);
                            }
                            map.put(field, sb.toString());
                        }



                    }
                    // 替换高亮的字段内容
                    BeanUtils.copyProperties(t, map);
                    // 替换完之后添加到list
                    list.add(t);
                }

                AggregatedPageImpl<T> aggregatedPage = new AggregatedPageImpl<T>(list, pageable, searchHits.getTotalHits());


                return aggregatedPage;
            }
        });
    }
}
