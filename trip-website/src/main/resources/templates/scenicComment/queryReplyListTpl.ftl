<div class="mfw-cmt rev-list ajaxComment">
    <ul>
        <li class="rev-item comment-item clearfix">

            <#if comment.hasReply() == 0>
            <!-- 评论 -->
            <div class="mfw-cmt _j_comment_item">
                <div class="user"><a class="avatar" href="#" target="_blank"><img
                                src="${comment.headUrl!}"
                                width="48" height="48"></a><span
                            class="level">LV.${comment.userLevel!}</span></div>
                <a class="useful" data-id="191407415" title="点赞">
                    <i></i><span class="useful-num">${comment.thumbupnum!}</span>
                </a>
                <a class="name" href="#" target="_blank">${comment.username!}</a>
                <span class="s-star s-star${comment.statNum!}"></span>
                <p class="rev-txt">${comment.content!}</p>


                <div class="rev-img">
                    <#list comment.imgArr! as img>
                        <a href="#" target="_blank"><img src="/${img}" width="200"
                                                         height="120"></a>
                    </#list>
                </div>

                <div class="info clearfix">
                    <a class="btn-comment _j_comment commentReplyBtn" title="添加回复"
                       data-touser="${comment.username!}"
                       data-toid="${comment.id!}">回复点评</a>
                    <span class="time">${(comment.createTime?string("yyyy-MM-dd"))!}</span>
                </div>
            </div>

            <!-- 回复模板 -->
            <div class="mfw-cmt _j_reply_item">
                <div class="comment add-reply ">

                    <#else>

                        <ul class="more_reply_box comment_list">

                            <#if comment.sceneryReply.type == 1>
                                <li>
                                    <a href="#" target="_blank">
                                        <img src="${(comment.sceneryReply.refReply.headUrl)!}"
                                             width="16"
                                             height="16">${(comment.sceneryReply.refReply.username)!}
                                    </a>
                                    ：${(comment.sceneryReply.refReply.content)!}
                                    <a class="_j_reply re_reply"
                                       data-touser="${comment.sceneryReply.refReply.username!}"
                                       data-toid="${comment.sceneryReply.refReply.id!}"
                                       title="添加回复">回复</a>
                                    <br><span
                                            class="time">${(comment.sceneryReply.refReply.createTime?string("yyyy-MM-dd"))!}
                                                            </span>
                                </li>
                            <#else>
                                <div class="mcmt-quote">
                                </div>
                                <div class="mcmt-word">
                                    <p class="_j_reply_content">${(comment.sceneryReply.content)!}</p>
                                    <br><span
                                            class="time">${(comment.sceneryReply.createTime?string("yyyy-MM-dd"))!}
                                                            </span>
                                </div>
                            </#if>
                        </ul>

                    </#if>

                    <div class="add-comment hide reply-form">
                        <form action="/travel/commentAdd" method="post" id="replyForm">

                                                    <textarea class="comment_reply" data-comment_id="${comment.id}"
                                                              data-comment_username="${comment.username!}"
                                                              data-poi_id="${comment.sceneryId}"
                                                              data-poi_name="${comment.sceneryName}"
                                                              style="color: #000000;" id="replyContent">
                                                    </textarea>

                        </form>
                        <a class="btn btn_submit_reply">回复</a>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>