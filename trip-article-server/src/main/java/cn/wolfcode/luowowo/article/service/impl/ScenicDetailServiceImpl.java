package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.ScenicDetail;
import cn.wolfcode.luowowo.article.mapper.ScenicDetailMapper;
import cn.wolfcode.luowowo.article.service.IScenicDetailService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class ScenicDetailServiceImpl implements IScenicDetailService {

    @Autowired
    private ScenicDetailMapper scenicDetailMapper;

    @Override
    public ScenicDetail queryDetailById(Long scenicId) {
        return scenicDetailMapper.selectByScenicId(scenicId);
    }
}
