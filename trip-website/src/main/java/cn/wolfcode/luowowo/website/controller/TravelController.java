package cn.wolfcode.luowowo.website.controller;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.domain.Travel;
import cn.wolfcode.luowowo.article.domain.TravelContent;
import cn.wolfcode.luowowo.article.query.TravelDetailQuery;
import cn.wolfcode.luowowo.article.query.TravelQuery;
import cn.wolfcode.luowowo.article.service.IDestinationService;
import cn.wolfcode.luowowo.article.service.IStrategyDetailService;
import cn.wolfcode.luowowo.article.service.ITravelService;
import cn.wolfcode.luowowo.cache.emum.RedisKeys;
import cn.wolfcode.luowowo.cache.service.ITravelStatisRedisService;
import cn.wolfcode.luowowo.cache.service.IUserInfoRedisService;
import cn.wolfcode.luowowo.cache.vo.TravelStatisVO;
import cn.wolfcode.luowowo.comment.domain.StrategyComment;
import cn.wolfcode.luowowo.comment.domain.TravelComment;
import cn.wolfcode.luowowo.comment.service.IStrategyCommentService;
import cn.wolfcode.luowowo.comment.service.ITravelCommentService;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.website.annotation.RequireLogin;
import cn.wolfcode.luowowo.website.annotation.UserParam;
import cn.wolfcode.luowowo.website.util.CookieUtil;
import cn.wolfcode.luowowo.website.util.UMEditorUploader;
import cn.wolfcode.luowowo.website.util.UploadUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by zhi on 2019/8/11.
 */
@Controller
@RequestMapping("travel")
public class TravelController {
    @Reference
    private ITravelService travelService;
    @Reference
    private IDestinationService destinationService;
    @Reference
    private IStrategyDetailService strategyDetailService;
    @Reference
    private IStrategyCommentService strategyCommentService;
    @Reference
    private ITravelCommentService travelCommentService;
    @Reference
    private IUserInfoRedisService userInfoRedisService;
    @Reference
    private ITravelStatisRedisService travelStatisRedisService;


    @RequestMapping("")
    public String list(Model model, @ModelAttribute("qo")TravelQuery qo, @UserParam UserInfo userInfo){
        model.addAttribute("tcs", travelCommentService.listByCreateTimeTop10());
        model.addAttribute("pageInfo", travelService.query(qo));
        // 游记排行前10 (阅读量)
        model.addAttribute("ts", travelStatisRedisService.getTravelViewNumTop10());
        return "travel/list";
    }



    @RequestMapping("detail")
    public String detail(Model model, Long id, @UserParam UserInfo userInfo){
        Travel travel = travelService.get(id);
        TravelContent content = travelService.getContent(travel.getId());
        travel.setTravelContent(content);
        model.addAttribute("detail", travel);
        // 导航吐司
        model.addAttribute("toasts", destinationService.getToasts(travel.getDest().getId()));
        // 相关攻略
        model.addAttribute("sds", strategyDetailService.getDetailTop3(travel.getDest().getId()));
        // 相关游记
        model.addAttribute("ts", travelService.getTravelTop3(travel.getDest().getId()));
        // 评论
        TravelDetailQuery qo = new TravelDetailQuery();
        qo.setTravelId(id);
        model.addAttribute("list", travelCommentService.list(qo));
        travelStatisRedisService.viewNumIncrease(id , 1);
        model.addAttribute("vo", travelStatisRedisService.getTravelStatisVO(id));
        if (userInfo != null){
            boolean ret = travelStatisRedisService.isUserFavorTravel(userInfo.getId(), id);
            if (ret){
                model.addAttribute("isFavor", ret);
            }
        }
        travelStatisRedisService.addScore(RedisKeys.TRAVEL_STATIS_SORT.getPrefix(), RedisKeys.TRAVEL_STATIS_VO.join(id.toString()), 1);
        return "travel/detail";
    }

    @RequireLogin
    @RequestMapping("input")
    public String input(Model model, Long id){
        if (id != null){
            Travel travel = travelService.get(id);
            travel.setTravelContent(travelService.getContent(travel.getId()));
            model.addAttribute("tv", travel);
        }
        model.addAttribute("dests" ,destinationService.getDestByDeep(3));
        return "travel/input";
    }


    @RequireLogin
    @RequestMapping("saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(Travel travel, @UserParam UserInfo userInfo){
        travel.setAuthor(userInfo);
        Long travelId = travelService.saveOrUpdate(travel);
        return AjaxResult.SUCCESS.addObject(travelId);
    }


    @RequireLogin
    @RequestMapping("commentAdd")
    public String commentAdd(Model model, TravelComment comment,  int floor, @UserParam UserInfo userInfo){
        comment.setUserId(userInfo.getId());
        comment.setUsername(userInfo.getNickname());
        comment.setCity(userInfo.getCity());
        comment.setLevel(userInfo.getLevel());
        comment.setHeadUrl(userInfo.getHeadImgUrl());

        TravelComment travelComment = travelCommentService.saveOrUpdate(comment);
        model.addAttribute("c" , travelComment);
        model.addAttribute("floor", floor + 1);
        return "travel/commentTpl";
    }

    // 收藏
    @RequestMapping("favor")
    @ResponseBody
    public Object favor(Long tid, @UserParam UserInfo userInfo){
        AjaxResult result = new AjaxResult();
        if (userInfo == null){
            result.setSuccess(false);
            result.setCode(102);
            result.setMsg("请先登录");
            return result;
        }
        boolean ret = travelStatisRedisService.favor(tid, userInfo.getId());
        result.addObject(travelStatisRedisService.getTravelStatisVO(tid));
        result.setSuccess(ret);
        return result;
    }

    // 顶, 我顶!
    @RequestMapping("travelThumbup")
    @ResponseBody
    public Object strategyThumbup(Long tid, @UserParam UserInfo userInfo){
        AjaxResult result = new AjaxResult();
        if (userInfo == null){
            result.setSuccess(false);
            result.setCode(102);
            result.setMsg("请先登录");
            return result;
        }
        boolean ret = travelStatisRedisService.travelThumbup(tid, userInfo.getId());
        result.addObject(travelStatisRedisService.getTravelStatisVO(tid));
        result.setSuccess(ret);
        return result;
    }


    // 假分享
    @RequestMapping("sharenum")
    @ResponseBody
    public Object sharenum(Long tid, @UserParam UserInfo userInfo){
        AjaxResult result = new AjaxResult();
        if (userInfo == null){
            result.setSuccess(false);
            result.setCode(102);
            result.setMsg("请先登录");
            return result;
        }
        boolean ret = travelStatisRedisService.sharenum(tid, userInfo.getId());
        result.addObject(travelStatisRedisService.getTravelStatisVO(tid));
        result.setSuccess(ret);
        return result;
    }


    @Value("${file.path}")
    private String filePath;
    //上传图片
    @RequestMapping("/coverImageUpload")
    @ResponseBody
    public Object coverImageUpload(MultipartFile pic){
        String upload = UploadUtil.upload(pic, filePath);
        return upload;
    }

    //上传图片
    @RequestMapping("/contentImage")
    @ResponseBody
    public String uploadUEImage(MultipartFile upfile,HttpServletRequest request) throws Exception{
        UMEditorUploader up = new UMEditorUploader(request);
        String[] fileType = {".gif" , ".png" , ".jpg" , ".jpeg" , ".bmp"};
        up.setAllowFiles(fileType);
        up.setMaxSize(10000); //单位KB
        up.upload(upfile, filePath);

        String callback = request.getParameter("callback");
        String result = "{\"name\":\""+ up.getFileName() +"\", \"originalName\": \""+ up.getOriginalName() +"\", \"size\": "+ up.getSize()
                +", \"state\": \""+ up.getState() +"\", \"type\": \""+ up.getType() +"\", \"url\": \""+ up.getUrl() +"\"}";
        result = result.replaceAll( "\\\\", "\\\\" );
        if(callback == null ){
            return result ;
        }else{
            return "<script>"+ callback +"(" + result + ")</script>";
        }
    }



}
