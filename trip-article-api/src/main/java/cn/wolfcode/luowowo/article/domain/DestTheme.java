package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DestTheme extends BaseDomain {

    private Long parentId;

    private String name;

    private String imgUrl;

    private Integer sequence;

}