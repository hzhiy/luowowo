package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.Ticket;
import cn.wolfcode.luowowo.article.domain.TicketDetail;

import java.util.List;

/**
 * 门票详情
 */
public interface ITicketDetailService {


    /**
     * 单个
     * @param id
     * @return
     */
    TicketDetail getByTicketId(Long id);

    /**
     * 查所有
     * @return
     */
    List<TicketDetail> list();

}
