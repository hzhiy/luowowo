package cn.wolfcode.luowowo.article.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by zhi on 2019/8/10.
 */
@Setter
@Getter
public class DestinationQuery extends QueryObject {
    private Integer ishot = -1;
    private Long parentId = -1L;
    private Long destId = -1L;
    private Long catalogId = -1L;
}
