<div class="comm-item _j_comment_item" data-id="29968450">
    <div class="user">

        <a class="avatar" href="javascript:;"><img src="${hc.headUrl}" ></a>
        <a class="name" href="javascript:;">${hc.username}</a><br>
        <span class="lv">LV.${hc.level}</span>
        <div class="prop clearfix">

        </div>
    </div>
    <div class="like" data-useful="0">
        <span>${(hc.thumbupnum)!}</span>
        <a class="icon-bg icon-like _j_comment_useful " href="javascript:;" data-has-useful="0"></a>
    </div>
    <div class="txt">${hc.content}</div>
    <div class="img clearfix">
    <#if hc.imgs??>
      <#list hc.imgs as i>
          <a href="javascript:;" class="_j_album_comment_trigger" data-id="231684976"><img src="${i}"></a>
      </#list>
    </#if>
    </div>
    <div class="comm-meta">
        <span class="icon-bg comm-star comm-star${hc.ensemble}"></span>
        <span class="time">${hc.createTime?string('yyyy.MM.dd')}</span>
        <a class="r-report comm-report _j_comment_report">举报</a>
    </div>
    <div class="reply-box">
        <ul class="reply-list ul${hc.id}">
        <#if hc.refCommentlist??>
            <#list hc.refCommentlist as r>
            <#--回复酒店评论-->
                <li class="_j_comment_reply_item comment_reply_item" data-id="596580" data-uid="87991340" data-user-name="生生不息">
                    <a href="/u/87991340.html" target="_blank"><img src="${r.headUrl}" width="16" height="16">${r.username}</a>
                ${("回复"+r.refComment.username)!}: ${r.content}
                    <a class="_j_comment_reply_to_trigger re_replyreplayid${hc.id}"  href="javascript:;" data-username="${r.username}">回复</a>
                    <span class="r-report comm-reply-report _j_comment_report" >举报</span>
                    <br>
                    <span class="time">${r.createTime?string('yyyy.MM.dd')}</span>
                </li>
                <script>
                    $('.re_replyreplayid${hc.id}').click(function () {
                        var username = $(this).data('username');
                        $('.replayid${hc.id}').data('type', '1');
                        $('.id${hc.id}').data('username', username);
                        $('.replayid${hc.id}').data('reusername', username);
                        $('.id${hc.id}').click();
                    })
                </script>
            </#list>
        </#if>
        </ul>
        <div class="reply-add clearfix _j_comment_reply_add id${hc.id}" data-username="${(hc.username)!}">
            <textarea placeholder="添加回复..."></textarea>
            <a class="btn-reply com-sel replayid${hc.id}" data-type="0" data-type="0" data-reusername=""  data-hcid="${hc.id}" data-userid="${(userInfo.id)!}" data-username="${(userInfo.nickname)!}"
               data-headurl="${(userInfo.headImgUrl)!}"  href="javascript:;">回复</a>
        </div>
        <script>
            $('.re_replyreplayid${hc.id}').click(function () {
                var username = $(this).data('username');
                $('.replayid${hc.id}').data('type', '1');
                $('.id${hc.id}').data('username', username);
                $('.replayid${hc.id}').data('reusername', username);
                $('.id${hc.id}').click();
            })
            // 提交回复
            $('.replayid${hc.id} ').click(function () {
            <#if userInfo??>
                var type = $(this).data('type');
                var hcid = $(this).data('hcid');
                var userId = $(this).data('userid');
                var username = $(this).data('username');
                var headUrl = $(this).data('headurl');
                var content = $('.id${hc.id} textarea').val();
                var reusername = $(this).data('reusername');
                $.post("/hotel/reComment", {type:type, hcid:hcid, userId:userId, username:username, headUrl:headUrl, content:content, reusername:reusername}, function (data) {
                    $('.ul${hc.id}').append(data);
                })
                $(this).data('type', '0');
                $('.id${hc.id} textarea').val('');
                $('.id${hc.id} textarea').attr('placeholder', '添加回复...');
            <#else >
                popup('请先登录');
            </#if>
            })
        </script>
    </div>
</div>
<script>
    $('._j_comment_reply_add').mouseover(function () {
        $(this).addClass('focus');
        $('._j_comment_reply_add a').show();
    }).mouseout(function () {
        $(this).removeClass('focus');
        $('._j_comment_reply_add a').hide();
    })
    $('.id${hc.id}').click(function () {
        var name = $(this).data('username');
        $('.id${hc.id} textarea').attr('placeholder', "回复:"+name);
    })

    // 点赞与取消点赞
    $('._j_comment_useful').click(function () {
        var _this = $(this);
    <#if userInfo??>
        var hcid = $(_this).data('hcid');
        var uid = $(_this).data('uid');
        $.get("/hotel/commentThumbUp", {hcid: hcid, uid: uid}, function (data) {
            $('#searchForm').submit();
        })
    <#else >
        popup('请先登录')
    </#if>


    })
</script>