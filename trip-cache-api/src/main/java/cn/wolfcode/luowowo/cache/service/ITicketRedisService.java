package cn.wolfcode.luowowo.cache.service;

import cn.wolfcode.luowowo.article.domain.Ticket;

import java.util.List; /**
 * 门票缓存
 */
public interface ITicketRedisService {


    /**
     * 判断是有该地区的key和该key中是否有该门票
     * @param key
     * @param list
     */
    void hasKey(String key, List<Ticket> list);

    /**
     * 根据地区获取门票
     * @param ajaxDestId
     * @return
     */
    List<Ticket> getByDestId(Long ajaxDestId);
}
