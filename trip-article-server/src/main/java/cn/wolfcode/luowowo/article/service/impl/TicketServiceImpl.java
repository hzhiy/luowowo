package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.domain.StrategyDetail;
import cn.wolfcode.luowowo.article.domain.Ticket;
import cn.wolfcode.luowowo.article.mapper.TicketMapper;
import cn.wolfcode.luowowo.article.service.ITicketService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 *
 */
@Service
public class TicketServiceImpl implements ITicketService{

    @Autowired
    private TicketMapper ticketMapper;

    @Override
    public List<Ticket> list() {
        return ticketMapper.selectAll();
    }

    @Override
    public List<Ticket> getALLDest() {
        return ticketMapper.selectAll();
    }

    @Override
    public Ticket getById(Long id) {
        return ticketMapper.selectByPrimaryKey(id);
    }
}
