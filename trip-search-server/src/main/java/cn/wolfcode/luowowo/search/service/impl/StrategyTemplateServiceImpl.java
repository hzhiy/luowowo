package cn.wolfcode.luowowo.search.service.impl;

import cn.wolfcode.luowowo.search.query.SearchQueryObject;
import cn.wolfcode.luowowo.search.query.SearchQueryObject;
import cn.wolfcode.luowowo.search.repository.IStrategySearchRepository;
import cn.wolfcode.luowowo.search.service.IStrategyTemplateService;
import cn.wolfcode.luowowo.search.template.StrategyTemplate;
import cn.wolfcode.luowowo.search.vo.StatisVO;
import com.alibaba.dubbo.config.annotation.Service;
import org.elasticsearch.client.ElasticsearchClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.aggregations.bucket.composite.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;

import java.util.*;

/**
 * Created by zhi on 2019/8/17.
 */
@Service
public class StrategyTemplateServiceImpl implements IStrategyTemplateService {

    @Autowired
    private IStrategySearchRepository strategySearchRepository;
    @Autowired
    private ElasticsearchTemplate template;
    @Autowired
    private ElasticsearchClient client;

    public void saveOrUpdate(StrategyTemplate strategyTemplate) {
        strategySearchRepository.save(strategyTemplate);
    }

    @Override
    public List<Map<String, Object>> getThemeCommend() {
        List<Map<String, Object>> data = new ArrayList<>();

        // 先查左边
        List<StatisVO> themes = this.getStrategyByType("themeId", "themeName", "themeGroup");

        // 后查右边
        for (StatisVO statisVO : themes) {
            List<StatisVO> dests = this.getDestByThemeId(statisVO.getId());
            Map<String, Object> dataMap = new HashMap<>();
            dataMap.put("theme", statisVO);
            dataMap.put("dests", dests);
            data.add(dataMap);
        }
        return data;
    }


    public List<StatisVO> getDestByThemeId(Long id) {
        List<String> destName = new ArrayList<>();  // 去重用
        List<StatisVO> dests = new ArrayList<>();
        // 根据主题id查询内容
        Iterable<StrategyTemplate> themeid = strategySearchRepository.search(QueryBuilders.matchQuery("themeId", id));
        themeid.forEach(st -> {
            // 如果去重的集合中没有该地区名 则添加
            if (!destName.contains(st.getDestName())) {
                StatisVO vo = new StatisVO();
                vo.setId(st.getDestId());
                vo.setName(st.getDestName());
                dests.add(vo);
                destName.add(st.getDestName());
            }
        });

        return dests;
    }


    @Override
    public List<StatisVO> queryConditionGruop(Integer strategyType) {
        String fieldId = "";
        String fieldName = "";
        String groupName = "";
        if (strategyType == 0){
            fieldId = "countryId";
            fieldName = "countryName";
            groupName = "countryGroup";
        }else if (strategyType == 1){
            fieldId = "provinceId";
            fieldName = "provinceName";
            groupName = "provinceGroup";

        }else if (strategyType == 2){
            fieldId = "themeId";
            fieldName = "themeName";
            groupName = "themeGroup";
        }
        List<StatisVO> strategy = this.getStrategyByType(fieldId, fieldName, groupName);

        return strategy != null? strategy : Collections.EMPTY_LIST;
    }

    @Override
    public Page queryStrategyByTypeId(SearchQueryObject qo) {
        // 获得查询的类型 还有 查询的id
        TermQueryBuilder query = null;
         if (qo.getType() == SearchQueryObject.ABROADS_STRATEGY){
            query = QueryBuilders.termQuery("countryId",qo.getTypeValue());
        }else if (qo.getType() == SearchQueryObject.CHINAS_STRATEGY){
            query = QueryBuilders.termQuery("provinceId",qo.getTypeValue());
        }else if (qo.getType() == SearchQueryObject.THEMES_STRATEGY){
            query = QueryBuilders.termQuery("themeId",qo.getTypeValue());
        }
        Page<StrategyTemplate> search = strategySearchRepository.search(query, qo.getPage());
        return search;
    }

    @Override
    public List<StrategyTemplate> findByDestName(String destName) {
        return strategySearchRepository.findByDestName(destName);
    }


    private List<StatisVO> getStrategyByType(String fieldId, String fieldName, String groupName){
        // 定义条件查询对象
        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
        // 查询的索引库
        builder.withIndices(StrategyTemplate.INDEX_NAME);
        // 查询的类型
        builder.withTypes(StrategyTemplate.TYPE_NAME);
        List<CompositeValuesSourceBuilder<?>> sources = new ArrayList<>();
        // 查第一个分组主题id
        TermsValuesSourceBuilder idSourceBuilder = new TermsValuesSourceBuilder("id");
        idSourceBuilder.field(fieldId);
        // 查第二个分组主题name
        TermsValuesSourceBuilder nameSourceBuilder = new TermsValuesSourceBuilder("name");
        nameSourceBuilder.field(fieldName);
        // 将source添加到sources中作为分组查询的条件
        sources.add(idSourceBuilder);
        sources.add(nameSourceBuilder);
        // 创建聚合函数
        CompositeAggregationBuilder aggregationBuilder = new CompositeAggregationBuilder(groupName, sources);
        builder.addAggregation(aggregationBuilder);
        // 默认返回的是Page 但是我们要操作aggregations 这个对象没有 所以用AggregatedPageImpl接收
        AggregatedPageImpl<StrategyTemplate> search = (AggregatedPageImpl<StrategyTemplate>) strategySearchRepository.search(builder.build());
        // 通过自定义分组名 获得桶数据
        InternalComposite themeGroup = search.getAggregations().get(groupName);
        // 遍历bucket数据
        List<StatisVO> strategys = new ArrayList<>();
        for (CompositeAggregation.Bucket bucket : themeGroup.getBuckets()) {
            StatisVO vo = new StatisVO();
            Map<String, Object> key = bucket.getKey();
            // 国外攻略去除中国
            if ("countryId".equals(fieldId) && Long.parseLong(key.get("id").toString())==1L){
                continue;
            }
            vo.setId(Long.parseLong(key.get("id").toString()));
            vo.setName(key.get("name").toString());
            vo.setCount(bucket.getDocCount());
            strategys.add(vo);
        }
        // 排序
        Collections.sort(strategys, new Comparator<StatisVO>() {
            @Override
            public int compare(StatisVO o1, StatisVO o2) {
                return Integer.valueOf(o2.getCount() - o1.getCount() + "");
            }
        });
        return strategys;
    }
}
