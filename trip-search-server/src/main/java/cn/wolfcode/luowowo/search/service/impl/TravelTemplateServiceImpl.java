package cn.wolfcode.luowowo.search.service.impl;

import cn.wolfcode.luowowo.search.repository.ITravelSearchRepository;
import cn.wolfcode.luowowo.search.service.ITravelTemplateService;
import cn.wolfcode.luowowo.search.template.TravelTemplate;
import com.alibaba.dubbo.config.annotation.Service;
import org.elasticsearch.client.ElasticsearchClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

import java.util.List;

/**
 * Created by zhi on 2019/8/17.
 */
@Service
public class TravelTemplateServiceImpl implements ITravelTemplateService {

    @Autowired
    private ITravelSearchRepository travelSearchRepository;
    @Autowired
    private ElasticsearchTemplate template;
    @Autowired
    private ElasticsearchClient client;

    public void saveOrUpdate(TravelTemplate travelTemplate) {
        travelSearchRepository.save(travelTemplate);
    }

    @Override
    public List<TravelTemplate> findByDestName(String destName) {
        return travelSearchRepository.findByDestName(destName);
    }
}
