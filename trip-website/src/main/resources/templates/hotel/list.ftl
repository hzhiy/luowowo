<!DOCTYPE html>
<html>
<head>
    <link href="/styles/base.css" rel="stylesheet" type="text/css">
    <link href="/styles/hotel.css" rel="stylesheet" type="text/css">
    <link href="/styles/travelspot.css" rel="stylesheet" type="text/css">
    <link href="/styles/travelwiki.css" rel=" stylesheet " type="text/css ">
    <script type="text/javascript " src="/js/jquery/jquery.js "></script>
    <script type="text/javascript" src="/js/system/guide.js"></script>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>酒店列表</title>

    <link href="http://css.mafengwo.net/css/cv/css+base:css+jquery.suggest:css+plugins:css+plugins+jquery.jgrowl:css+other+popup:css+mfw-header.2015^YlVS^1559526017.css"
          rel="stylesheet" type="text/css"/>


    <link href="http://css.mafengwo.net/css/cv/css+hotel+hotel_index:css+jquery-ui-1.11.0.min:css+hotel+datepicker-range:css+hotel+number_guests_picker^YlVX^1552035728.css"
          rel="stylesheet" type="text/css"/>


    <script language="javascript"
            src="http://js.mafengwo.net/js/cv/js+jquery-1.8.1.min:js+global+json2:js+M+Module:js+M+M:js+M+Log:js+m.statistics:js+advert+inspector:js+corelib+underscore-1.6.0:js+corelib+backbone-1.1.2^YlBQ^1562232559.js"
            type="text/javascript" crossorigin="anonymous"></script>
    <style>
        #banner-con-gloable {
            display: block;
            position: fixed;
            bottom: 0;
            left: -100%;
            z-index: 110;
            width: 100%;
            height: 179px;
            overflow-x: hidden;
        }

        #banner-con-gloable .banner-btn-con {
            width: 100%;
            height: 162px;
            background: rgba(30, 15, 8, 0.95);
            position: absolute;
            bottom: 0;
        }

        #banner-con-gloable .banner-btn-con .close-btn {
            position: absolute;
            right: 35px;
            top: 24px;
            z-index: 120;
            height: 24px;
            width: 24px;
            cursor: pointer;
        }

        #banner-con-gloable .banner-image-con {
            position: absolute;
            right: 50%;
            bottom: 0;
            width: 1000px;
            margin-right: -500px;
        }

        #float-pannel-gloable {
            padding-left: 28px;
            padding-bottom: 20px;
            display: block;
            position: fixed;
            bottom: 0;
            z-index: 110;
            left: -230px;
        }

        #float-pannel-gloable .float-btn {
            width: 24 pxpx;
            height: 24px;
            position: absolute;
            right: 0;
            top: 0;
            z-index: 100;
        }

        #closed {
            height: 24px;
            width: 24px;
            vertical-algin: top;
            border: none;
            cursor: pointer;
        }

        .hotel-searchbar .hs-item-action {
            margin-left: 12px;
        }

        .hotel-searchbar .hs-item {
            float: left;
            width: 132px;
            margin-right: 3px;
            position: relative;
        }

        .hotel-searchbar .hs-btn {
            display: block;
            margin-top: 1px;
            width: 110px;
            height: 34px;
            background-color: #ffb200;
            border-radius: 3px;
            text-align: center;
            color: #fff;
            font-size: 14px;
            line-height: 34px;
            position: relative;
            overflow: hidden;
        }

        css + base:css + jq…w-header.20…:1
        a {
            background-color: transparent;
            text-decoration: none;
            color: #ff9d00;
            cursor: pointer;
        }
        .hotel-main {
            width: 1000px;
            margin: 0 auto;
            padding-top: 0px;
        }
        .hotel-main {
            position: relative;
            z-index: 5;
        }
        .btn-booking .icon-wxpay {
            margin-left: 4px;
            background-position: -250px -15px;
        }
    </style>
    <script type="text/javascript" src="/js/plugins/jquery-form/jquery.form.js"></script>
    <link href="/js/plugins/jqPaginator/jqPagination.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/plugins/jqPaginator/jq-paginator.min.js"></script>
    <script language="javascript" type="text/javascript">
        if (typeof M !== "undefined" && typeof M.loadResource === "function") {
            M.loadResource("http://js.mafengwo.net/js/cv/js+Dropdown:js+pageletcommon+pageHeadUserInfoWWWNormal:js+jquery.tmpl:js+M+module+InputListener:js+M+module+SuggestionXHR:js+M+module+DropList:js+M+module+Suggestion:js+M+module+MesSearchEvent:js+SiteSearch:js+AHeader:js+M+module+PageAdmin:js+M+module+Storage:js+M+module+Cookie:js+M+module+ResourceKeeper:js+jquery.jgrowl.min:js+AMessage:js+M+module+dialog+Layer:js+M+module+dialog+DialogBase:js+M+module+dialog+Dialog:js+M+module+dialog+alert:js+M+module+FrequencyVerifyControl:js+M+module+FrequencySystemVerify:js+ALogin:js+M+module+ScrollObserver:js+M+module+QRCode:js+AToolbar:js+ACnzzGaLog:js+ARecruit:js+ALazyLoad:js+plugins+dynamics:js+hotel+module+Log:js+hotel+module+Search_v2:js+xdate:js+hotel+module+FestivalDateConfig:js+jquery-ui-core:js+jquery-ui-datepicker:js+hotel+module+DateRangePicker:js+hotel+module+ModuleProvider:js+hotel+module+BookingDate:js+hotel+module+BookingGuests:js+hotel+module+NumberGuestsPicker:js+hotel+module+ImageLoader:js+hotel+index_v5:js+hotel+pc_app_guide^alxV^1562127144.js")
        }

        $(function () {


            $(".btn-sss").click(function () {
                $("#editForm").submit()
            })

            // 区域点击样式
            $('.restrictheight a').click(function () {
                var a = $(this);
                $('.restrictheight a').removeClass('on');
                $(a).addClass('on');
                var positionName = $(a).data('name');
                $('#positionName').val(positionName);
                $("#searchForm").submit();

            })
            // 酒店列表
                $("#searchForm").ajaxForm(function (data) {
                    $(".hotel-list").html(data);
                })
                $("#searchForm").submit();

            // 关键字搜索
            $('.htb-icon-search').click(function () {
                var keyword = $('.htb-searchbar input').val();
                $('input[name=keyword]').val(keyword);
                $("#searchForm").submit();
            })

            // 价格排序
            $('.price-sort').click(function () {
                var _this = $(this);
                if ($(_this).hasClass('on')){
                    if ($(_this).hasClass('price-down')){
                        $(_this).removeClass('price-down').addClass('price-up');
                        $('input[name=type]').val('2');

                    }else {
                        $(_this).removeClass('price-up').addClass('price-down');
                        $('input[name=type]').val('1');
                    }
                    $("#searchForm").submit();
                }else {
                    $('.sortbar-nav a').removeClass('on');
                    $(_this).addClass('on').addClass('price-down');
                    $('input[name=type]').val('1');
                    $("#searchForm").submit();
                }
            })
            // 综合排序
            $('.synthesize-sort').click(function () {
                $('.sortbar-nav a').removeClass('on').removeClass('price-down').removeClass('price-up');
                $('.synthesize-sort').addClass('on');
                $('input[name=type]').val('-1');
                $("#searchForm").submit();
            })


        })
    </script>
</head>
<body>
<#assign currentNav="hotel">
<#include "../common/navbar.ftl">


<script>
    $("li[name=${currentNav}]").addClass("header_nav_active");
</script>



<div class=" topziro hotel-main ">
    <div class="top-info clearfix">
        <div class="crumb">
            <span class="tit">您在这里：</span>
            <div class="item">
                <div class="drop">
                    <span class="hd"><a href="/hotel/" target="_blank">酒店<i></i></a></span>
                    <div class="bd" >
                        <i class="arrow"><b></b></i>
                        <div class="col">
                            <h3>周边国家和地区</h3>
                            <ul class="clearfix">
                                <#list countryList as c>
                                    <li><a href="#" target="_blank" >${c.name}<span> ${c.english}</span></a></li>
                                </#list>
                                <#--<li><a href="/hotel/10184/" target="_blank" title="韩国酒店">韩国<span>Korea</span></a></li>
                                <li><a href="/hotel/14293/" target="_blank" title="蒙古酒店">蒙古<span>Mongolia</span></a></li>
                                <li><a href="/hotel/10183/" target="_blank" title="日本酒店">日本<span>Japan</span></a></li>
                                <li><a href="/hotel/10300/" target="_blank" title="俄罗斯酒店">俄罗斯<span>Russia</span></a></li>
                                <li><a href="/hotel/10820/" target="_blank" title="老挝酒店">老挝<span>Laos</span></a></li>-->
                            </ul>
                        </div>
                        <div class="more"><a href="/hotel/" target="_blank">&gt;&gt;更多国家和地区</a></div>                            </div>
                </div>
                <em>&gt;</em>                    </div>
            <div class="item">
                <div class="drop">
                    <span class="hd"><a href="/hotel/21536/" target="_blank" >${country.name}酒店<i></i></a></span>
                    <div class="bd" >
                        <i class="arrow"><b></b></i>
                        <div class="col">
                            <h3>${country.name}其他城市</h3>
                            <ul class="clearfix">
                                <#list provinceList as pl>
                                <li><a href="/hotel/10065/" target="_blank" >${pl.name}<span> ${pl.english}</span></a></li>
                                </#list>
                                <#--<li><a href="/hotel/10099/" target="_blank" title="上海酒店推荐">上海<span>Shanghai</span></a></li>
                                <li><a href="/hotel/10195/" target="_blank" title="西安酒店推荐">西安<span>Xi'an</span></a></li>
                                <li><a href="/hotel/10198/" target="_blank" title="深圳酒店推荐">深圳<span>Shenzhen</span></a></li>
                                <li><a href="/hotel/10208/" target="_blank" title="重庆酒店推荐">重庆<span>ChongQing</span></a></li>
                                <li><a href="/hotel/10035/" target="_blank" title="成都酒店推荐">成都<span>Chengdu</span></a></li>-->
                            </ul>
                        </div>
                        <div class="more"><a href="/hotel/21536/" target="_blank">&gt;&gt;更多城市</a></div>                            </div>
                </div>
                <em>&gt;</em>                    </div>
            <div class="item cur"><a href="/hotel/10088/" target="_blank" >${dest.name}酒店预订</a></div>
        </div>


    </div>







    <div class="h-title">订酒店</div>
    <form class="form-hotel" action="/hotel/h" method="get" id="editForm">
        <div class="clearfix date-warp">
            <div class="add-travle">
                <input type="text" placeholder="出行目的地" id="_j_search_input" autocomplete="off" name="name"
                       value="${(qo.name)!}">
                <div class="not-cont" id="_j_search_shortcut_mdds" style="display:none;">
                    <ul>
                        <li class="clearfix">

                            <h2>国内</h2>
                            <p>
                            <#list dests as d >
                            <a href="/hotel/h?name=${d.name}&checkIn=${(qo.checkIn)!}&checkOut=${(qo.checkOut)!}" data-id="${d.id}" data-name="${d.name}">${d.name}</a>
                        </#list>
                            </p>
                        </li>
                        <li class="clearfix"><h2>海外</h2>
                            <p>
                            <#list overseas as o >
                                <a href="javascript:;" data-id="${o.id}" data-name="${o.name}">${o.name}</a>
                            </#list>
                            </p>
                        </li>
                    </ul>
                </div>
            <#--<div class="search-suggest-panel search-suggest-hotel" style="display:none;" id="_j_search_suggest"></div>-->
            </div>
            <div class="stay-time _j_booking_date_item" id="_j_check_in_date">
                <span>${(qo.checkIn)!}</span>
                <input type="text" placeholder="入住日期" readonly name="checkIn">
                <i></i>
            </div>
            <div class="leave-time _j_booking_date_item" id="_j_check_out_date">
                <span>${(qo.checkOut)}</span>
                <input type="text" placeholder="离店日期" readonly name="checkOut">
                <i></i>
            </div>
            <div class="hs-item hs-item-people" id="_j_booking_number_guests">
                <span>人数</span>
                <i class="icon-person"></i>
            </div>
            <div class="btn-search" id="_A_search_btn">
                <a role="button" href="javascript:" class="btn-sss"><i class=""></i></a>
            </div>
        </div>
        <div class="area-main clearfix">
            <div class="area-wrapper" id="_j_area_wrapper" style="">
                <dl class="item-area clearfix _j_area_list">


                    <dt>区域:</dt>
                    <dd>
                        <ul class="area-nav clearfix restrictheight" style="height: 112px;">
                            <li>
                                <a href="javascript:;" class="_j_area_name on" data-id="-1">全部</a>
                            </li>
                            <#list destChild as d>
                                <li>
                                    <a href="javascript:;" class="_j_area_name" data-id="${d.id}" data-name="${d.name}">${d.name}</a>
                                </li>
                            </#list>
                        </ul>
                    </dd>
                </dl>
                <dl class="item-info clearfix _j_area_desc_list" style="height: 105px;">
                    <dt style="">攻略:</dt>
                    <dd data-id="90" >
                        <div>

                            <p>${(dest.info)!}</p>
                        </div>
                    </dd>

                </dl>
                <dl class="item-price clearfix _j_area_price_list">
                    <dt data-id="" style="" class="anim-show">均价
                        <sup class="warn-mark">
                            <span class="warn-mark-icon"></span>
                        </sup>:
                    </dt>
                    <dd data-id="90" >
                        <ul class="clearfix">
                            <#list starPrice as sp>
                                <li><span class="hotel-rate rate${sp.star}"></span>￥${sp.avgPrice}</li>
                            </#list>

                            <#--<li><span class="hotel-rate rate3"></span>￥237</li>
                            <li><span class="hotel-rate rate4"></span>￥376</li>
                            <li><span class="hotel-rate rate5"></span>￥376</li>-->
                        </ul>
                    </dd>
                </dl>
            </div>

            <div class="area-maps" id="_j_map">
                <div class="area-maps" id="_j_map">
                    <img src="${(dest.coverUrl)!}" style="width: 330px;height: 330px;"/>
                </div>
            </div>
        </div>

        <#--关键字搜索-->
        <div class="hotel-sortbar clearfix">
            <div class="sortbar-info">
                <div class="total" id="_j_total_text"><span>5430</span> 家酒店</div>
            </span>
                <div class="htb-search" id="_j_keyword_filter">
                    <div class="htb-searchbar">
                        <input type="text" placeholder="搜索关键词">
                        <a class="htb-icon-search" href="javascript:;"></a>
                        <i class="icon-search-clear" style="display: none;">×</i>
                    </div>
                </div>
            </div>
            <div class="sortbar-nav" id="_j_sorter">
                <a class="synthesize-sort on" href="javascript:;" data-type="comment" class="on">综合排序</a>
                <a class="price-sort" href="javascript:;" data-type="price"><i></i>价格</a>
            </div>
        </div>

    </form>

    <div class="container">
        <div class="n-content">
            <div class="hotel-loading" id="_j_hotel_list_loading" style="display: none;">
                <i class="loading-m"></i>
            </div>
            <form action="/hotel/searchPage" method="post" id="searchForm">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <input type="hidden" name="positionName" id="positionName" >
                <input type="hidden" name="keyword" id="keyword" >
                <input type="hidden" name="type" id="type" value="-1">
                <input type="hidden" name="cityName" id="cityName" value="${(dest.name)!}">
                <div class="hotel-list" id="_j_hotel_list">
                </div>
            </form>
            <#--<div class="hotel-list" id="_j_hotel_list">
            </div>-->
        </div>
    </div>
</div>
<#include "../common/footer.ftl">
</body>
</html>