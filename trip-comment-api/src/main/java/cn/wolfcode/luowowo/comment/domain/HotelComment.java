package cn.wolfcode.luowowo.comment.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * 游记评论
 */
@Setter
@Getter
@Document("hotel_comment")
public class HotelComment implements Serializable {
    public static final int TRAVLE_COMMENT_TYPE_COMMENT = 0;            // 普通评论
    public static final int TRAVLE_COMMENT_TYPE = 1;                    // 评论的评论

    @Id
    private String id;                                                  // id
    private Long hotelId;                                               // 酒店id
    private String hotelName;                                           // 酒店名称
    private Long positionId;                                            // 区域id
    private String positionName;                                        // 区域名
    private Long cityId;                                                // 城市id
    private String cityName;                                            // 城市名

    private Integer ensemble;                                           // 总体评价
    private Integer location;                                           // 位置
    private Integer cleanliness;                                        // 清洁度
    private Integer facility;                                           // 设施
    private Integer service;                                            // 服务
    private Integer comfort;                                            // 舒适度
    private Integer food;                                               // 餐饮
    private String imgstr;                                              // 图片总和字符串


    private Long userId;                                                // 用户id
    private String username;                                            // 用户名
    private String city;                                                // 用户城市
    private int level;                                                  // 用户等级
    private String headUrl;                                             // 用户头像



    private int type = TRAVLE_COMMENT_TYPE_COMMENT;                     // 评论类别
    private Date createTime;                                            // 创建时间
    private String content;                                             // 评论内容
    private HotelComment refComment;               // 关联的评论
    private List<HotelComment> refCommentlist = new ArrayList<>();      // 关联评论的集合

    private int thumbupnum;                                             // 点赞数
    private List<String> thumbuplist = new ArrayList<>();               // 点赞的用户id

    public List<String> getImgs() {
        if (StringUtils.hasLength(imgstr)){
            List<String> list = new ArrayList<>();
            for (String s : imgstr.split(",")) {
                list.add(s);
            }
            return list;
        }
        return Collections.EMPTY_LIST;
    }
}