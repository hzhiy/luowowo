package cn.wolfcode.luowowo.airticket.mapper;

import cn.wolfcode.luowowo.airticket.domain.City;
import java.util.List;

public interface CityMapper {
    int deleteByPrimaryKey(Long id);

    int insert(City record);

    City selectByPrimaryKey(Long id);

    List<City> selectAll();

    int updateByPrimaryKey(City record);

    List<City> selectHotCity();

    List<City> selectNormalCity(int i);
}