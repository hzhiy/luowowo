package cn.wolfcode.luowowo.cache.service;

/**
 * Created by zhi on 2019/8/7.
 */

import cn.wolfcode.luowowo.member.domain.UserInfo;

import java.util.List;

/**
 * 用户缓存数据
 */
public interface IUserInfoRedisService {

    /**
     * 把验证码保存在缓存中
     * @param key
     * @param value
     */
    void saveVerifyCode(String key, String value);

    /**
     * 获取缓存中的验证码
     * @param key
     */
    String getVerifyCode(String key);

    /**
     * 把登录的用户保存在缓存中
     * @param username
     * @param userInfo
     * @return
     */
    String setUserInfo(String username, UserInfo userInfo);

    /**
     * 获取在redis中登录的用户
     * @param key
     * @return
     */
    UserInfo getUserInfo(String key);

    /**
     * 关注/取消关注及粉丝
     * @param id
     * @param uid
     */
    boolean attentionUser(Long id, Long uid);

    /**
     * 获取关注列表
     * @param id
     * @return
     */
    List<UserInfo> getAttentionUser(Long id);

    /**
     * 获取粉丝
     * @param uid
     * @return
     */
    List<UserInfo> getFans(Long uid);
}
