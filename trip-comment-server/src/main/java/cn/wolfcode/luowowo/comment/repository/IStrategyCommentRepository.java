package cn.wolfcode.luowowo.comment.repository;

import cn.wolfcode.luowowo.comment.domain.StrategyComment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * monogDB 攻略评论接口
 */
@Repository // 贴上@Repository注解，底层会创建出动态代理对象，交给Spring管理
public interface IStrategyCommentRepository extends MongoRepository<StrategyComment, String>{

}
