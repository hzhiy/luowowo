package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.UserAnswer;
import cn.wolfcode.luowowo.article.mapper.UserAnswerMapper;
import cn.wolfcode.luowowo.article.service.IUserAnswerService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Administrator on 2019/8/25.
 */
@Service
public class UserAnswerImpl implements IUserAnswerService {


    @Autowired
    private UserAnswerMapper userAnswerMapper;

    @Override
    public List<UserAnswer> list() {
        return userAnswerMapper.selectAll();
    }
}
