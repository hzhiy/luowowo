package cn.wolfcode.luowowo.cache.service.impl;

import cn.wolfcode.luowowo.article.domain.Travel;
import cn.wolfcode.luowowo.article.service.ITravelService;
import cn.wolfcode.luowowo.cache.emum.RedisKeys;
import cn.wolfcode.luowowo.cache.service.ITravelStatisRedisService;
import cn.wolfcode.luowowo.cache.vo.TravelStatisVO;
import cn.wolfcode.luowowo.common.util.DateUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by zhi on 2019/8/7.
 */
@Service
public class TravelStatisRedisServiceImpl implements ITravelStatisRedisService {
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Reference
    private ITravelService travelService;

    private static final int TRAVEL_VIEWNUM = 1;      //阅读数
    private static final int TRAVEL_REPLYNUM = 2;     //游记评论数
    private static final int TRAVEL_FAVORNUM = 3;     //收藏数
    private static final int TRAVEL_SHARENUM = 4;     //分享数
    private static final int TRAVEL_THUMBSUPNUM = 5;  //点赞个数

    // 统一加+1操作
    public void numIncrease(Long tid, int type, int i){

        // 获得vo对象
        TravelStatisVO vo = this.getTravelStatisVO(tid);
        switch (type){
            case TRAVEL_VIEWNUM:
                vo.setViewnum(vo.getViewnum() + i);
                break;
            case TRAVEL_REPLYNUM:
                vo.setReplynum(vo.getReplynum() + i);
                break;
            case TRAVEL_FAVORNUM:
                vo.setFavornum(vo.getFavornum() + i);
                break;
            case TRAVEL_SHARENUM:
                vo.setSharenum(vo.getSharenum() + i);
                break;
            case TRAVEL_THUMBSUPNUM:
                vo.setThumbsupnum(vo.getThumbsupnum() + i);
                break;
        }
        this.setTravelStatisVO(vo);
    }


    public TravelStatisVO getTravelStatisVO(Long id) {
        String key = RedisKeys.TRAVEL_STATIS_VO.join(id.toString());
        TravelStatisVO vo = null;
        // 判断游记对象存不存在
        if (redisTemplate.hasKey(key)) {
            // 存在就返回
            String voStr = redisTemplate.opsForValue().get(key);
            vo =  JSON.parseObject(voStr, TravelStatisVO.class);
        }else {
            // 不存在则初始化
            Travel travel = travelService.get(id);
            vo = new TravelStatisVO();
            try {
                BeanUtils.copyProperties(vo, travel);
                vo.setTravelId(travel.getId());
                vo.setDestId(travel.getDest().getId());
                vo.setDestName(travel.getDest().getName());
                this.setTravelStatisVO(vo);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return vo;
    }

    public void setTravelStatisVO(TravelStatisVO vo){
        redisTemplate.opsForValue().set(RedisKeys.TRAVEL_STATIS_VO.join(vo.getTravelId().toString()), JSON.toJSONString(vo));
    }

    @Override
    public boolean favor(Long tid, Long uid) {
        // 此收藏功能是站在用户角度
        boolean flag = true;
        // 设计好针对该用户的游记收藏集合并查询看是否存在
        String key = RedisKeys.USER_TRAVEL_STATIS_FAVOR.join(uid.toString());
        List<String> list = new ArrayList<>();
        if (redisTemplate.hasKey(key)) {
            // 存在  判断该游记的id是否存在该用户的游记收藏集合
            String listStr = redisTemplate.opsForValue().get(key);
            list = JSON.parseArray(listStr, String.class);
            if (list.contains(tid.toString())) {
                // 存在 做取消收藏操作  list剔除该游记id  收藏-1
                list.remove(tid.toString());
                this.numIncrease(tid, TRAVEL_FAVORNUM, -1);
                flag = false;
            }else {
                // 不存在  做收藏操作   list添加该游记id  收藏+1
                list.add(tid.toString());
                this.numIncrease(tid, TRAVEL_FAVORNUM, 1);
            }
        }else {
            // 不存在 新增
            list.add(tid.toString());
            this.numIncrease(tid, TRAVEL_FAVORNUM, 1);
        }
        redisTemplate.opsForValue().set(key, JSON.toJSONString(list));
        return flag;
    }

    @Override
    public boolean isUserFavorTravel(Long uid, Long tid) {
        String key = RedisKeys.USER_TRAVEL_STATIS_FAVOR.join(uid.toString());
        if (redisTemplate.hasKey(key)) {
            String listStr = redisTemplate.opsForValue().get(key);
            List<String> list = JSON.parseArray(listStr, String.class);
            if (list.contains(tid.toString())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void viewNumIncrease(Long tid, int i) {
        this.numIncrease(tid, TRAVEL_VIEWNUM, i);
    }

    @Override
    public void replyNumIncrease(Long tid, int i) {
        this.numIncrease(tid, TRAVEL_REPLYNUM, i);
    }

    @Override
    public boolean isVoExist(Long id) {
        return redisTemplate.hasKey(RedisKeys.TRAVEL_STATIS_VO.join(id.toString()));
    }

    @Override
    public boolean travelThumbup(Long tid, Long uid) {
        String key = RedisKeys.TRAVEL_STATIS_THUMBSUP.join(uid.toString(), tid.toString());
        // 存在表示该用户今天顶过该游记
        if (redisTemplate.hasKey(key)){
            return false;
        }
        // 不存在表示该用户今天没有顶过该游记
        Date date = new Date();
        this.numIncrease(tid, TRAVEL_THUMBSUPNUM, 1);
        redisTemplate.opsForValue().set(key, "1", DateUtil.getDateBetween(DateUtil.getEndDate(date), date), TimeUnit.SECONDS);
        return true;
    }

    @Override
    public Set<String> getKeys(String pattern) {
        Set<String> keys = redisTemplate.keys(pattern + "*");
        return keys;
    }

    @Override
    public String get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    @Override
    public boolean sharenum(Long tid, Long uid) {
        this.numIncrease(tid, TRAVEL_SHARENUM, 1);
        return true;
    }

    @Override
    public void addScore(String prefix, String value, int i) {
        redisTemplate.opsForZSet().incrementScore(prefix, value, i);
    }

    @Override
    public List<TravelStatisVO> getTravelViewNumTop10() {
        List<TravelStatisVO> list = new ArrayList<>();
        Set<String> voStrs = redisTemplate.opsForZSet().reverseRange(RedisKeys.TRAVEL_STATIS_SORT.getPrefix(), 0, 9);
        for (String s : voStrs) {
            String voStr = redisTemplate.opsForValue().get(s);
            list.add(JSON.parseObject(voStr, TravelStatisVO.class));
        }
        return list;
    }

    @Override
    public List<Travel> queryByUserIdToFavor(Long uid) {
        String FavorStr = redisTemplate.opsForValue().get(RedisKeys.USER_TRAVEL_STATIS_FAVOR.join(uid.toString()));
        if (StringUtils.hasLength(FavorStr)){
            List<Travel>list=new ArrayList<>();
            List<Long> travelId = JSON.parseArray(FavorStr, Long.class);
            for (Long id : travelId) {
                Travel travel = travelService.get(id);
                list.add(travel);
            }
            return list;
        }
        return null;
    }
}
