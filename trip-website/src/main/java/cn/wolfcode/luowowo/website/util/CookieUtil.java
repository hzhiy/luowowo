package cn.wolfcode.luowowo.website.util;

import cn.wolfcode.luowowo.cache.service.IUserInfoRedisService;
import cn.wolfcode.luowowo.common.util.Consts;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import com.alibaba.dubbo.config.annotation.Reference;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by zhi on 2019/8/8.
 */
public class CookieUtil {



    public static String getToken(HttpServletRequest request){
        Cookie[] cookies = request.getCookies();
        if (cookies != null){
            for (Cookie cookie : cookies) {
                if("token".equals(cookie.getName())){

                    return cookie.getValue();
                }
            }
        }
        return null;
    }


    public static void addCookie(String token, HttpServletResponse response){
        Cookie cookie = new Cookie("token", token);
        cookie.setPath("/");
        cookie.setMaxAge(Consts.USER_INFO_TOKEN_VAI_TIME * 60);
        response.addCookie(cookie);
    }
}

