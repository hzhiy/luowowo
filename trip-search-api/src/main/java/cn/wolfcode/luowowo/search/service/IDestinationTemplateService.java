package cn.wolfcode.luowowo.search.service;

import cn.wolfcode.luowowo.search.template.DestinationTemplate;

/**
 * Created by zhi on 2019/8/17.
 */
public interface IDestinationTemplateService {

    /**
     * 新增或者修改
     * @param destinationTemplate
     */
    void saveOrUpdate(DestinationTemplate destinationTemplate);

    /**
     * 通过地区名找地区对象
     * @param name
     * @return
     */
    DestinationTemplate findByName(String name);
}

