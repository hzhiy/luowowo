package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.TravelCommend;
import cn.wolfcode.luowowo.article.query.TravelCommendQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 游记推荐服务
 */
public interface ITravelCommendService {
    /**
     * 分页
     * @param qo
     * @return
     */
    PageInfo queryForList(TravelCommendQuery qo);

    /**
     * 增加或更新
     * @param travelCommend
     */
    void saveOrUpdate(TravelCommend travelCommend);

    /**
     * 删除
     * @param id
     */
    void delete(Long id);


    /**
     * 游记推荐前10
     * @return
     */
    List<TravelCommend> getTop10();
}
