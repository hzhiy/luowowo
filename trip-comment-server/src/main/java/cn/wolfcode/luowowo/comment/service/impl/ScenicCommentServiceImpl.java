package cn.wolfcode.luowowo.comment.service.impl;

import cn.wolfcode.luowowo.article.vo.UsersReplyVO;
import cn.wolfcode.luowowo.comment.domain.ScenicComment;
import cn.wolfcode.luowowo.comment.query.ScenicCommentQuery;
import cn.wolfcode.luowowo.comment.repository.IScenicCommentRepository;
import cn.wolfcode.luowowo.comment.service.IScenicCommentService;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * 景点点评
 */
@Service
public class ScenicCommentServiceImpl implements IScenicCommentService {
    @Autowired
    private IScenicCommentRepository scenicCommentRepository;
    @Autowired
    private MongoTemplate template;

    /**
     * 评论添加, 返回评论对象回显
     *
     * @param scenicComment 点评对象
     * @return 点评后对象
     */
    public ScenicComment addComment(ScenicComment scenicComment) {
        if (StringUtils.hasLength(scenicComment.getId())) {
            Optional<ScenicComment> optional = scenicCommentRepository.findById(scenicComment.getId());
            if (optional.isPresent()) {
                ScenicComment refComment = optional.get();
                scenicComment.setRefComment(refComment);
            }
        }
        return scenicCommentRepository.save(scenicComment);
    }

    @Override
    public Page<ScenicComment> queryCommentByScenicId(ScenicCommentQuery qo) {

        // 需要 count
        long count = scenicCommentRepository.countByScenicId(qo.getScenicId());
        if (count == 0) {
            return Page.empty(); // 没有就返回空页面
        }
        Query query = new Query();
        // 评论显示是时间降序
        PageRequest pageRequest = PageRequest.of(qo.getCurrentPage() - 1, qo.getPageSize(), Sort.by(Sort.Direction.DESC, "createTime"));

        query.with(pageRequest);
        query.addCriteria(Criteria.where("scenicId").is(qo.getScenicId()));

       // List<ScenicComment> list = scenicCommentRepository.findByScenicId(qo.getScenicId());
        List<ScenicComment> list = template.find(query, ScenicComment.class);
        // 需要 list request count
        return new PageImpl<>(list, pageRequest, count);
    }

    @Override
    public List<ScenicComment> queryByUserId(Long uid) {

        return scenicCommentRepository.findByUserId(uid);
    }

    @Override
    public void commentThumbUp(String toid, Long fromid) {
        if (toid.length() > 0 || fromid != null) {
            // 判断之前有无点赞
            Optional<ScenicComment> optional = scenicCommentRepository.findById(toid);
            ScenicComment comment = optional.get();
            List<Long> thumbuplist = comment.getThumbuplist();
            if (thumbuplist.contains(fromid)) {
                // 有点赞 点赞数 -1
                thumbuplist.remove(fromid);
                comment.setThumbupnum(comment.getThumbupnum() - 1);
            } else {
                thumbuplist.add(fromid);
                comment.setThumbupnum(comment.getThumbupnum() + 1);
            }
            scenicCommentRepository.save(comment);
        }
    }

    @Override
    public ScenicComment getscenicCommentById(String toid) {
        return scenicCommentRepository.findById(toid).get();
    }

    @Override
    public ScenicComment queryByCommentId(String toid) {
        return scenicCommentRepository.findById(toid).get();
    }

    @Override
    public List<ScenicComment> usersReply(UsersReplyVO vo, UserInfo userInfo) {
        if (vo.getToid() != null) {
            List<ScenicComment> list = new ArrayList<>();
            // 获取源评论
            ScenicComment commentSource = scenicCommentRepository.findById(vo.getToid()).get();

            ScenicComment hitReply = new ScenicComment(); // 被回复者 外层
            hitReply.setId(vo.getToid()); // 设置原评论id
            hitReply.setUserId(vo.getFromId());
            hitReply.setUsername(vo.getFromName());
            hitReply.setHeadUrl(vo.getHeadUrl());
            hitReply.setType(vo.getType()); // 设置评论类型

            ScenicComment forComment = new ScenicComment(); // 回复者 内层
            forComment.setUserId(userInfo.getId());
            forComment.setHeadUrl(userInfo.getHeadImgUrl());
            forComment.setUsername(userInfo.getNickname());
            forComment.setContent(vo.getContent());
            forComment.setCreateTime(new Date());

            hitReply.setRefComment(forComment);
            list.add(hitReply);
            commentSource.getUsersReply().add(hitReply);
            // Mongo给这个对象更新评论列表
            scenicCommentRepository.save(commentSource);
            return list;
        }
        return null;
    }

    @Override
    public List<ScenicComment> findAll() {
        return scenicCommentRepository.findAll();
    }

    @Override
    public Map<String, List> tagsCount() {

        HashMap<String, List> map = new HashMap<>();
        List tags = new ArrayList<>();
        long hasImg = scenicCommentRepository.countByHasImg(1);// todo 图片类型添加

        /*tags.add(scenicCommentRepository.countByStarNumBetween(0, 2));
        tags.add(scenicCommentRepository.countByStarNumBetween(3, 4));
        tags.add(scenicCommentRepository.countByStarNumBetween(5, 5));*/

        // 使用Template
        /*long countOne1 = getFourTagsCount(0, 2, ScenicComment.class, "countOne");
        long countTwo1 = getFourTagsCount(3, 4, ScenicComment.class, "countTwo");
        long countThree1 = getFourTagsCount(5, 5, ScenicComment.class, "countThree");*/

        long bad = scenicCommentRepository.countByStarNum(2);
        long middle0 = scenicCommentRepository.countByStarNum(3);
        long middle1 = scenicCommentRepository.countByStarNum(4);
        long middle = middle0 + middle1;
        long high = scenicCommentRepository.countByStarNum(5);


        tags.add(0, hasImg);
        tags.add(1, high);
        tags.add(2, middle);
        tags.add(3, bad);
        map.put("top4Tags", tags);
        return map;
    }

    public long getFourTagsCount(int low, int high, Class clz, String name) {

        Query query = new Query();
        query.addCriteria(Criteria.where("starNum").gte(low).lte(high));
        return template.count(query, clz, name);
    }


    @Override
    public long totalCommentNum() {
        return scenicCommentRepository.count();
    }


}
